package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 31/3/2017.
 */

public class TimeDialog extends DialogFragment {

    public static final String EXTRA_TIME = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_TIME";
    public static final String EXTRA_FROM = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_FROM";

    public static final int FROM_INDIVIDUAL = 0;
    public static final int FROM_GLOBAL = 1;

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private EditText editTime;
    private LinearLayout linearValues;
    private ImageView imgTimePlus;
    private ImageView imgTimeMinus;

    //Variable
    private TimeDialog.OnConfirmationTimeListener mCallback;
    private Activity exerciseActivity;
    private int time = 0;
    private int from = FROM_INDIVIDUAL;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        time = getArguments().getInt(EXTRA_TIME);
        from = getArguments().getInt(EXTRA_FROM);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadFrequencyValue();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static TimeDialog newInstance(Activity exerciseActivity, int time, int from) {
        TimeDialog timeDialog = new TimeDialog();
        Bundle args = new Bundle();
        args.putInt(EXTRA_TIME, time);
        args.putInt(EXTRA_FROM, from);
        timeDialog.exerciseActivity = exerciseActivity;
        timeDialog.setArguments(args);
        return timeDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_time, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnSend = view.findViewById(R.id.btnSend);
        editTime = view.findViewById(R.id.editTime);
        imgTimePlus = view.findViewById(R.id.imgTimePlus);
        imgTimeMinus = view.findViewById(R.id.imgTimeMinus);
        linearValues = view.findViewById(R.id.linearValues);

        setListeners();

        return view;
    }

    public void loadFrequencyValue(){
        editTime.setText(""+(time/60));
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, (Integer.parseInt(editTime.getText().toString())*60));
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true, (Integer.parseInt(editTime.getText().toString())*60));
            }
        });

        linearValues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTime.requestFocus();
            }
        });

        imgTimePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int time = Integer.parseInt(editTime.getText().toString());
                time = time +1;
                editTime.setText(""+time) ;
            }
        });

        imgTimeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int time = Integer.parseInt(editTime.getText().toString());
                time = time -1;

                if (time < 0)
                    time = 0;

                editTime.setText(""+time) ;
            }
        });
    }

    public interface OnConfirmationTimeListener {
        void onConfirmationTime(boolean send, int time, int from);
    }

    private void sendResult(boolean send, int time) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (time < 0)
                time = 0;

            mCallback.onConfirmationTime(send, time, from);
        }
    }
}