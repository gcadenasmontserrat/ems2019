package com.myofx.emsapp.ui.training;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.TrainingsEvent;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.server.Vest;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.training.adapters.ParticipantsGridAdapter;
import com.myofx.emsapp.ui.training.adapters.TrainingsAdapter;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.SpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Gerard on 28/12/2016.
 */

public class TrainingsActivity extends BaseActivity {

    public static final int RESULT_ADD_TRAINING = 1;
    public static final String RESULT_PARTICIPANTS = EmsApp.APP_PACKAGE + ".intent.extra.RESULT_PARTICIPANTS";
    public static final String EXTRA_TRAINING = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_TRAINING";
    public static final String EXTRA_STUDENTS = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_STUDENTS";
    public static final String EXTRA_SERVER_TYPE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_SERVER_TYPE";

    public static final int RESULT_NORMAL = 1;
    public static final int RESULT_UPDATE = 2;

    //UI
    private RecyclerView gridTraining;
    private RecyclerView gridParticipants;
    private RelativeLayout relativeBackground;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private Button btnAdd;

    //Variables
    private PreferencesManager pref;
    private ArrayList<Training> trainings;
    private ArrayList<User> participants;
    private int serverType = UDPServer.SERVER_NORMAL;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainings);
        pref = PreferencesManager.getInstance(this);
        user = pref.getUser();

        //GA
        GAUtils.trackScreen(TrainingsActivity.this, GAConfig.SCREEN_ENTRENAMIENTO + GAConfig.SCREEN_ENTRENAMIENTO_EJERCICIOS, GAConfig.SCREEN_PORTADA);

        if (getIntent().hasExtra(EXTRA_STUDENTS))
            participants = (ArrayList<User>) getIntent().getSerializableExtra(EXTRA_STUDENTS);

        if (getIntent().hasExtra(EXTRA_SERVER_TYPE))
            serverType = getIntent().getIntExtra(EXTRA_SERVER_TYPE, UDPServer.SERVER_NORMAL);

        bindUi();
        setListeners();
        loadTheme();
        loadToolbar();
        loadTrainings();
        loadParticipants();
    }

    public void bindUi(){
        gridTraining = findViewById(R.id.gridTraining);
        gridParticipants = findViewById(R.id.gridParticipants);
        relativeBackground = findViewById(R.id.relativeBackground);
        linearLoading = findViewById(R.id.linearLoading);
        txtLoading = findViewById(R.id.txtLoading);
        btnAdd = findViewById(R.id.btnAdd);

        gridTraining.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(TrainingsActivity.this, 4);
        gridTraining.setLayoutManager(layoutManager);
        gridTraining.setNestedScrollingEnabled(false);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.micro_margin);
        gridTraining.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        if (!EmsApp.isModeLocal())
            btnAdd.setVisibility(View.GONE);
    }

    public void setListeners() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainingsActivity.this, AddTrainingActivity.class);
                startActivityForResult(intent, RESULT_ADD_TRAINING);
            }
        });
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(TrainingsActivity.this));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(TrainingsActivity.this));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(TrainingsActivity.this));
    }

    public void loadToolbar(){
        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(TrainingsActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(TrainingsActivity.this));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(TrainingsActivity.RESULT_PARTICIPANTS, participants);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(TrainingsActivity.RESULT_PARTICIPANTS, participants);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    public void loadTrainings() {

        if (!EmsApp.isModeLocal()){
            EmsApi.getInstance().doGetTrainingsTask();
            txtLoading.setText(getString(R.string.loading_trainings));
            linearLoading.setVisibility(View.VISIBLE);
        } else {
            addDefaultTrainings();
            trainings.addAll(AppDatabase.getTrainings(TrainingsActivity.this, user.getCenter()));
            trainings.addAll(AppDatabase.getUserById(TrainingsActivity.this, user.getId()).getTrainings());
            gridTraining.setAdapter(new TrainingsAdapter(trainings, TrainingsActivity.this, serverType));
        }
    }

    @Subscribe
    public void onGetTrainingsEvent(TrainingsEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        addDefaultTrainings();

        for (Training training: event.getTrainings()){
            trainings.add(new Training(training.getIdTraining(), training.getTitle(), training.getMain(),training.getImage()));
        }

        gridTraining.setAdapter(new TrainingsAdapter(trainings, TrainingsActivity.this, serverType));
    }

    public void addDefaultTrainings(){
        trainings = new ArrayList<>();
        trainings.addAll(TrainingUtils.getTrainings(TrainingsActivity.this));
    }

    public void loadParticipants(){
        gridParticipants.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(TrainingsActivity.this, 12);
        gridParticipants.setLayoutManager(layoutManager);
        gridParticipants.setNestedScrollingEnabled(false);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.micro_margin);
        gridParticipants.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        gridParticipants.setAdapter(new ParticipantsGridAdapter(participants, TrainingsActivity.this, null, serverType));
    }

    public void goExerciceActivity(Training training){
        Intent intent = new Intent(this, ExerciseMultipleActivity.class);
        intent.putExtra(EXTRA_TRAINING, training);
        intent.putExtra(EXTRA_STUDENTS, participants);
        startActivityForResult(intent, RESULT_UPDATE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TrainingsActivity.RESULT_UPDATE) {
            if (resultCode == Activity.RESULT_OK){
                participants = (ArrayList<User>) data.getSerializableExtra(RESULT_PARTICIPANTS);
            }
        } else if (requestCode == RESULT_ADD_TRAINING) {
            if (resultCode == Activity.RESULT_OK){
                loadTrainings();
            }
        }
    }
}
