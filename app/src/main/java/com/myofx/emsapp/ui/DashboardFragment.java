package com.myofx.emsapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Section;
import com.myofx.emsapp.ui.adapters.DashboardAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.SpacesItemDecoration;

import java.util.ArrayList;

/**
 * Created by Gerard on 29/11/2016.
 */

public class DashboardFragment extends Fragment {

    //UI
    private View rootView;
    private RecyclerView gridDashboard;
    private RelativeLayout relativeBackground;

    //VARIABLE
    private ArrayList<Section> sections;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        bindUi();
        loadTheme();
        loadSections();
        return rootView;
    }

    public void bindUi(){
        gridDashboard = (RecyclerView) rootView.findViewById(R.id.gridDashboard);
        relativeBackground = (RelativeLayout) rootView.findViewById(R.id.relativeBackground);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
    }

    public void loadSections() {
        gridDashboard.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        gridDashboard.setLayoutManager(layoutManager);
        gridDashboard.setNestedScrollingEnabled(true);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.micro_margin);
        gridDashboard.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        sections = new ArrayList<>();
        sections.add(new Section());
        sections.add(new Section());
        sections.add(new Section());
        sections.add(new Section());
        sections.add(new Section());
        sections.add(new Section());

        gridDashboard.setAdapter(new DashboardAdapter(sections, getActivity()));
    }

}
