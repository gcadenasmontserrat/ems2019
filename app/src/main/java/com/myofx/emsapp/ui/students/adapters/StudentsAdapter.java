package com.myofx.emsapp.ui.students.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.profile.ProfileActivity;
import com.myofx.emsapp.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by Gerard on 27/12/2016.
 */

public class StudentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_USER = 0;
    private static final int TYPE_LOADING = 1;

    //VARIABLE
    private ArrayList<User> students;
    private Context context;

    public StudentsAdapter(ArrayList<User> students, Context context) {
        this.students = students;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewHolder) {
            if (students.get(position).getLastName() != null && !students.get(position).getLastName().equals(""))
                ((ViewHolder) holder).txtName.setText(students.get(position).getName()+" "+students.get(position).getLastName());
            else
                ((ViewHolder) holder).txtName.setText(students.get(position).getName());

            //Local student, not show Profile
            if (students.get(position).getMeasures() == null)
                ((ViewHolder) holder).txtProfile.setVisibility(View.INVISIBLE);

            ((ViewHolder) holder).txtProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra(ProfileActivity.EXTRA_PROFILE, students.get(position));
                    context.startActivity(intent);
                }
            });

            if (students.get(position).getImage() != null && !EmsApp.isModeLocal())
                ImageUtils.loadImageFromUrl(((ViewHolder) holder).imgUser, BuildConfig.IMAGE_PATH_URL+students.get(position).getImage());
        } else {
            ((ViewLoading) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_progress, viewGroup, false);
            return new ViewLoading(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_student, viewGroup, false);
            return new ViewHolder(view);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtProfile;
        private TextView txtName;
        private ImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            txtProfile = mView.findViewById(R.id.txtProfile);
            txtName = mView.findViewById(R.id.txtName);
            imgUser = mView.findViewById(R.id.imgUser);
        }
    }

    public static class ViewLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewLoading(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (students.get(position) instanceof User) {
            return TYPE_USER;
        } else {
            return TYPE_LOADING;
        }
    }

}