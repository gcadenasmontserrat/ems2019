package com.myofx.emsapp.ui.chromecast;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.cast.CastPresentation;
import com.google.android.gms.cast.CastRemoteDisplayLocalService;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.ui.training.adapters.VelocimetersMaxForceAdapter;

import java.util.ArrayList;

public class ExerciseMaxForcePresentationService extends CastRemoteDisplayLocalService {

    private static final String TAG = "PresentationService";

    //UI
    private ImageView imgTime1, imgTime2, imgTime3, imgTime4, imgTime5,
            imgTime6, imgTime7, imgTime8, imgTime9, imgTime10;
    private RecyclerView gridVelocimeters;
    private TextView txtMinTens;
    private TextView txtMinUnits;
    private TextView txtSecTens;
    private TextView txtSecUnits;

    // First screen
    public CastPresentation mPresentation;

    //Variables
    private VelocimetersMaxForceAdapter velocimetersMaxForceAdapter;
    private ArrayList<Training> participantsChromecast;
    private Context context;
    private String title;
    private int time;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onCreatePresentation(Display display) {
        createPresentation(display);
    }

    @Override
    public void onDismissPresentation() {
        dismissPresentation();
    }

    private void dismissPresentation() {
        if (mPresentation != null) {
            mPresentation.dismiss();
            mPresentation = null;
        }
    }

    private void createPresentation(Display display) {
        dismissPresentation();
        mPresentation = new ExerciseMaxForcePresentationService.FirstScreenPresentation(this, display);

        try {
            mPresentation.show();
        } catch (WindowManager.InvalidDisplayException ex) {
            dismissPresentation();
        }
    }

    private class FirstScreenPresentation extends CastPresentation {

        public FirstScreenPresentation(Context context, Display display) {
            super(context, display);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.cast_maxforce);

            bindUi();
            loadExercice();
        }

        public void bindUi(){
            gridVelocimeters = findViewById(R.id.gridVelocimeters);
            txtMinTens = findViewById(R.id.txtMinTens);
            txtMinUnits = findViewById(R.id.txtMinUnits);
            txtSecTens = findViewById(R.id.txtSecTens);
            txtSecUnits = findViewById(R.id.txtSecUnits);
            imgTime1 = findViewById(R.id.imgTime1);
            imgTime2 = findViewById(R.id.imgTime2);
            imgTime3 = findViewById(R.id.imgTime3);
            imgTime4 = findViewById(R.id.imgTime4);
            imgTime5 = findViewById(R.id.imgTime5);
            imgTime6 = findViewById(R.id.imgTime6);
            imgTime7 = findViewById(R.id.imgTime7);
            imgTime8 = findViewById(R.id.imgTime8);
            imgTime9 = findViewById(R.id.imgTime9);
            imgTime10 = findViewById(R.id.imgTime10);
            gridVelocimeters.setHasFixedSize(true);

            int columnNumber = 2;
            if(participantsChromecast.size() == 1)
                columnNumber = 1;

            if (EmsApp.isTablet(ExerciseMaxForcePresentationService.this)){
                if(participantsChromecast.size()<4)
                    columnNumber = participantsChromecast.size();
                else if(participantsChromecast.size() == 4)
                    columnNumber = 2;
                else
                    columnNumber = 4;
            }

            GridLayoutManager layoutManager = new GridLayoutManager(context, columnNumber);
            gridVelocimeters.setLayoutManager(layoutManager);
            gridVelocimeters.setNestedScrollingEnabled(false);

            for (Training training: participantsChromecast){
                if (training.getDevice().getDeviceBle() != null){
                    final android.os.Handler handler = new android.os.Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            for (Training training: participantsChromecast)
                                training.setForceTotal(training.getForceTotal()+training.getForceMaxForce());

                            velocimetersMaxForceAdapter.notifyDataSetChanged();
                            handler.postDelayed(this, 1000);
                        }
                    }, 1000);
                    break;
                }
            }
        }
    }

    public void loadExercice(){
        loadTime(time);

        velocimetersMaxForceAdapter = new VelocimetersMaxForceAdapter(participantsChromecast, true, gridVelocimeters, context);
        gridVelocimeters.setAdapter(velocimetersMaxForceAdapter);
    }

    //Load default Time
    public void loadTime(int timeUpdate){
        String minTens, minUnits, secTens, secUnits;
        if (timeUpdate >= 1){
            int mins = timeUpdate/60;
            int seconds = timeUpdate%60;

            if (mins >= 10){
                minTens = Integer.toString(mins).split("(?<=.)")[0];
                minUnits = Integer.toString(mins).split("(?<=.)")[1];
            } else {
                minTens = "0";
                minUnits = Integer.toString(mins).split("(?<=.)")[0];
            }

            if (seconds >= 10){
                secTens = Integer.toString(seconds).split("(?<=.)")[0];
                secUnits = Integer.toString(seconds).split("(?<=.)")[1];
            } else {
                secTens = "0";
                secUnits = Integer.toString(seconds).split("(?<=.)")[0];
            }
        } else {
            minTens = "0";
            minUnits = "0";
            secTens = "0";
            secUnits = "0";
        }

        txtMinTens.setText(minTens);
        txtMinUnits.setText(minUnits);
        txtSecTens.setText(secTens);
        txtSecUnits.setText(secUnits);

        updateTimer(minTens, minUnits, secTens, secUnits);
    }

    //Update Time
    public void updateTimer(String minTens, String minUnits, String secTens, String secUnits){
        txtMinTens.setText(minTens);
        txtMinUnits.setText(minUnits);
        txtSecTens.setText(secTens);
        txtSecUnits.setText(secUnits);
    }

    public void saveDefaultValues(ArrayList<Training> participants, int timeChromecast, Context context, String title){
        participantsChromecast = new ArrayList<>();
        participantsChromecast.addAll(participants);

        this.context = context;
        this.title = title;
        this.time = timeChromecast;
    }

    public void updateVelocimeters(){
        this.velocimetersMaxForceAdapter.notifyDataSetChanged();
    }

    public void updatePotency(int potency, String uidString){
        if (participantsChromecast != null){
            for (Training training: participantsChromecast)
                if (training.getUid() != null && training.getUid().equals(uidString))
                    training.setLevel(0,potency);

            velocimetersMaxForceAdapter.notifyDataSetChanged();
        }
    }

    public void updateMaxForce(int maxforce, String deviceAddress){
        for (Training training: participantsChromecast) {
            if (training.getDevice().getDeviceBle() != null && training.getDevice().getDeviceBle().getAddress().equals(deviceAddress)){
                training.setForceMaxForce(maxforce);

                if (maxforce > training.getForceMax())
                    training.setForceMax(maxforce);
            }
        }
    }

    public void updateEstimulationUi(int estimulationValue){
        if (estimulationValue == 100){
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 100){
            imgTime1.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 90){
            imgTime2.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime2.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 80){
            imgTime3.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime3.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 70){
            imgTime4.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime4.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 60){
            imgTime5.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime5.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 50){
            imgTime6.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime6.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 40){
            imgTime7.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime7.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 30){
            imgTime8.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime8.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 20){
            imgTime9.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime9.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 10){
            imgTime10.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime10.setImageResource(R.drawable.img_progress_grey_h);
        }
    }

    public void updatePauseUi(int estimulationValue){
        if (estimulationValue == 100){
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 100){
            imgTime1.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 90){
            imgTime2.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime2.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 80){
            imgTime3.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime3.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 70){
            imgTime4.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime4.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 60){
            imgTime5.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime5.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 50){
            imgTime6.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime6.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 40){
            imgTime7.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime7.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 30){
            imgTime8.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime8.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 20){
            imgTime9.setImageResource(R.drawable.img_progress_global_100);
        } else {
            imgTime9.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 10){
            imgTime10.setImageResource(R.drawable.img_progress_global_100);
        } else {
            imgTime10.setImageResource(R.drawable.img_progress_grey_h);
        }
    }
}
