package com.myofx.emsapp.ui.students;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.R;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.UUID;

public class AddStudentActivity extends BaseActivity {

    //Ui
    private RelativeLayout relativeBackground;
    private RelativeLayout relativeTitle;
    private TextView txtTitleContact;
    private EditText editName;
    private EditText editSurname;
    private EditText editEmail;
    private Button btnCreate;

    //Variables
    private PreferencesManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        pref = PreferencesManager.getInstance(this);

        bindUi();
        loadTheme();
        loadToolbar();
        setListeners();
    }

    public void bindUi(){
        relativeBackground = findViewById(R.id.relativeBackground);
        editName = findViewById(R.id.editName);
        editSurname = findViewById(R.id.editSurname);
        editEmail = findViewById(R.id.editEmail);
        btnCreate = findViewById(R.id.btnCreate);
        relativeTitle = findViewById(R.id.relativeTitle);
        txtTitleContact = findViewById(R.id.txtTitleContact);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(AddStudentActivity.this));
        relativeTitle.setBackgroundColor(ColorTheme.getLightBackgroundColor(AddStudentActivity.this));
        txtTitleContact.setTextColor(ColorTheme.getPrimaryColor(AddStudentActivity.this));

    }

    public void loadToolbar(){
        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(AddStudentActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(AddStudentActivity.this));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(MainActivity.RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(MainActivity.RESULT_CANCELED);
        finish();
        super.onBackPressed();
    }

    public void setListeners(){
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptCreateStudent();
            }
        });
    }

    public void attemptCreateStudent(){
        if (TextUtils.isEmpty(editName.getText().toString().trim()) || TextUtils.isEmpty(editSurname.getText().toString().trim()) || TextUtils.isEmpty(editEmail.getText().toString().trim()))
            Toast.makeText(AddStudentActivity.this, getString(R.string.student_required_value), Toast.LENGTH_LONG).show();
        else {
            User user = pref.getUser();

            //Required
            User newProfile = new User();
            newProfile.setId(UUID.randomUUID().toString());
//            newProfile.setUserid(user.getId());
            newProfile.setName(editName.getText().toString().trim());
            newProfile.setLastName(editSurname.getText().toString().trim());
            newProfile.setEmail(editEmail.getText().toString().trim());
            newProfile.setCenterId(user.getCenter());

            if (isNewEmail(editEmail.getText().toString().trim(), user)) {
                user.getProfiles().add(newProfile);
                AppDatabase.updateUser(this,user);
                AppDatabase.addLocalAction(this, new LocalAction(user.getId(), newProfile.getId(), getString(R.string.la_new_user)+" "+newProfile.getEmail() ,false, LocalAction.ACTION_TYPE_ADD_USER));
                pref.saveUser(user);
                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
            } else
                Toast.makeText(AddStudentActivity.this, getString(R.string.student_email_exist), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Check if exist a user with the new email
     */
    public boolean isNewEmail(String email, User user){
        for (User userCheck: user.getProfiles())
            if (userCheck.getEmail().equals(email))
                return false;

        return true;
    }
}
