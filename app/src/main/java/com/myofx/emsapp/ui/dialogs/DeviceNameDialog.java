package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.ui.MainActivity;

/**
 * Created by Gerard on 30/5/2017.
 */

public class DeviceNameDialog extends DialogFragment {

    public static final String EXTRA_DEVICE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_DEVICE";

    //Ui
    private AlertDialog alertdialog;
    private Button btnSend;
    private EditText editName;

    //Variable
    private DeviceNameDialog.OnConfirmationNameListener mCallback;
    private Activity mainActivity;
    private Device device;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        device = (Device) getArguments().getSerializable(EXTRA_DEVICE);
        mCallback = (MainActivity) mainActivity;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static DeviceNameDialog newInstance(Activity mainActivity, Device device) {
        DeviceNameDialog deviceNameDialog = new DeviceNameDialog();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DEVICE, device);
        deviceNameDialog.mainActivity = mainActivity;
        deviceNameDialog.setArguments(args);
        return deviceNameDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_device_name, null);
        editName = view.findViewById(R.id.editName);
        btnSend = view.findViewById(R.id.btnSend);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editName.getText().toString())){
                    device.setNameUser(editName.getText().toString());
                    sendResult(device);
                }
            }
        });
    }

    public interface OnConfirmationNameListener {
        void onConfirmationName(Device device);
    }

    private void sendResult(Device device) {
        if(mCallback != null) {
            alertdialog.dismiss();
            mCallback.onConfirmationName(device);
        }
    }
}
