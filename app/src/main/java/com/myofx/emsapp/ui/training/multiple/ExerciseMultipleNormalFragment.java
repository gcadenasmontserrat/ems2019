package com.myofx.emsapp.ui.training.multiple;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.CastRemoteDisplayLocalService;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.server.Vest;
import com.myofx.emsapp.ui.chromecast.ExerciseMaxForcePresentationService;
import com.myofx.emsapp.ui.dialogs.TimeDialog;
import com.myofx.emsapp.ui.training.adapters.ParticipantsGridAdapter;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.SpacesItemDecoration;
import com.myofx.emsapp.utils.autoButtons.AutoButtonsUtils;
import com.myofx.emsapp.utils.autoButtons.BtnCorporalMinus;
import com.myofx.emsapp.utils.autoButtons.BtnCorporalPlus;
import com.myofx.emsapp.utils.autoButtons.BtnMinusPotency;
import com.myofx.emsapp.utils.autoButtons.BtnPlusPotency;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/6/2017.
 */

public class ExerciseMultipleNormalFragment extends Fragment {

    private static final int MODE_LEVEL = 0;
    private static final int MODE_CRONAXIA = 1;

    //Ui
    private View rootView;
    private RecyclerView gridParticipants;
    private ImageButton btnPlusLeg;
    private ImageButton btnMinusLeg;
    private ImageButton btnPlusGlu;
    private ImageButton btnMinusGlu;
    private ImageButton btnPlusLum;
    private ImageButton btnMinusLum;
    private ImageButton btnPlusSho;
    private ImageButton btnMinusSho;
    private ImageButton btnPlusBic;
    private ImageButton btnMinusBic;
    private ImageButton btnPlusDor;
    private ImageButton btnMinusDor;
    private ImageButton btnPlusAbs;
    private ImageButton btnMinusAbs;
    private ImageButton btnPlusPec;
    private ImageButton btnMinusPec;
    private ImageButton btnPlusAux;
    private ImageButton btnMinusAux;
    private ImageButton btnPlusAux2;
    private ImageButton btnMinusAux2;
    private ImageButton btnPlusGlobal;
    private ImageButton btnMinusGlobal;
    private ImageButton btnPlayGlobal;
    private ImageButton btnStopGlobal;
    private ImageButton btnPauseGlobal;
    private TextView txtLeg;
    private TextView txtGlu;
    private TextView txtLum;
    private TextView txtSho;
    private TextView txtBic;
    private TextView txtDor;
    private TextView txtAbs;
    private TextView txtPec;
    private TextView txtAux;
    private TextView txtAux2;
    private TextView txtTime;
    private TextView txtChronaxy;
    private ImageView imgFrontPiernaSup;
    private ImageView imgFrontPectorales;
    private ImageView imgFrontBiceps;
    private ImageView imgFrontAbdominales;
    private ImageView imgBackTrapecio;
    private ImageView imgBackTriceps;
    private ImageView imgBackPiernaSup;
    private ImageView imgBackLumbares;
    private ImageView imgBackGluteos;
    private ImageView imgBackDorsales;
    private ImageView btnStopLeg;
    private ImageView btnStopGlu;
    private ImageView btnStopLum;
    private ImageView btnStopSho;
    private ImageView btnStopBic;
    private ImageView btnStopDor;
    private ImageView btnStopAbs;
    private ImageView btnStopPec;
    private ImageView btnStopAux;
    private ImageView btnStopAux2;
    private TextView txtPotency;
    private TextView txtLastPotency;
    private ImageView imgTimeRigth1, imgTimeRigth2, imgTimeRigth3, imgTimeRigth4, imgTimeRigth5,
            imgTimeRigth6, imgTimeRigth7, imgTimeRigth8, imgTimeRigth9, imgTimeRigth10;
    private ImageView imgPotLeft1, imgPotLeft2, imgPotLeft3, imgPotLeft4, imgPotLeft5,
            imgPotLeft6, imgPotLeft7, imgPotLeft8, imgPotLeft9, imgPotLeft10;
    private TextView txtRampType;
    private TextView txtFrequency;
    private TextView txtRelax;
    private TextView txtImpulseTime;
    private TextView txtImpulseType;
    private TextView txtStandard;
    private ImageView imgGlobal;
    private RelativeLayout relativeTime;
    private RelativeLayout linearMaxforce;
    private TextView txtForce;
    private RelativeLayout relativePotency;

    //Variable
    private ArrayList<User> participants;
    private View.OnClickListener btnPlusClickListener;
    private View.OnClickListener btnMinusClickListener;
    private View.OnTouchListener btnPlusOnTouchListener;
    private View.OnTouchListener btnMinussOnTouchListener;
    private View.OnLongClickListener btnPlusLongClickListener;
    private View.OnLongClickListener btnMinusLongClickListener;
    private View.OnClickListener btnStopClickListener;
    private ParticipantsGridAdapter participantsGridAdapter;
    private ArrayList<Training> trainings = new ArrayList<>();

    //Auto buttons
    private BtnPlusPotency btnPlusPotency;
    private BtnMinusPotency btnMinusPotency;
    private BtnCorporalPlus btnCorporalPlus;
    private BtnCorporalMinus btnCorporalMinus;
    private Handler btnPlusMinusHandler = new Handler();

    //Variables Fragment
    private int modeActive = MODE_LEVEL;

    //Variables Activity
    public static int trainingIndex = 0;
    public static int vestIndex = 0;
    private int potency;
    private int potencyPrePause = 0;
    private int connectionType = Device.DEVICE_BLE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_multiple_normal, container, false);

        //Load information
        trainings = ((ExerciseMultipleActivity)getActivity()).getTrainings();
        participants = ((ExerciseMultipleActivity)getActivity()).getParticipants();

        trainingIndex = 0;
        bindUi();
        setListeners();
        loadParticipants();
        updateTimeText(trainings.get(trainingIndex).getTime());
        loadButtonsPermissions();

        if (trainings.get(trainingIndex).getDevice().getDeviceBle() != null)
            linearMaxforce.setVisibility(View.VISIBLE);
        else
            linearMaxforce.setVisibility(View.GONE);

        connectionType = trainings.get(trainingIndex).getDevice().getDeviceType();
        if (connectionType == Device.DEVICE_WIFI)
            vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(trainings.get(0));

        //Init first selected
        ((ExerciseMultipleActivity)getActivity()).sendStandardSelected(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        ((ExerciseMultipleActivity)getActivity()).updateValuesDetails(trainings.get(trainingIndex));

        return rootView;
    }

    public static ExerciseMultipleNormalFragment newInstance() {
        ExerciseMultipleNormalFragment exerciseMultipleNormalFragment = new ExerciseMultipleNormalFragment();
        return exerciseMultipleNormalFragment;
    }

    public void bindUi(){
        gridParticipants = (RecyclerView) rootView.findViewById(R.id.gridParticipants);
        btnPlusLeg = (ImageButton) rootView.findViewById(R.id.btnPlusLeg);
        btnMinusLeg = (ImageButton) rootView.findViewById(R.id.btnMinusLeg);
        btnPlusGlu = (ImageButton) rootView.findViewById(R.id.btnPlusGlu);
        btnMinusGlu = (ImageButton) rootView.findViewById(R.id.btnMinusGlu);
        btnPlusLum = (ImageButton) rootView.findViewById(R.id.btnPlusLum);
        btnMinusLum = (ImageButton) rootView.findViewById(R.id.btnMinusLum);
        btnPlusSho = (ImageButton) rootView.findViewById(R.id.btnPlusSho);
        btnMinusSho = (ImageButton) rootView.findViewById(R.id.btnMinusSho);
        btnPlusBic = (ImageButton) rootView.findViewById(R.id.btnPlusBic);
        btnMinusBic = (ImageButton) rootView.findViewById(R.id.btnMinusBic);
        btnPlusDor = (ImageButton) rootView.findViewById(R.id.btnPlusDor);
        btnMinusDor = (ImageButton) rootView.findViewById(R.id.btnMinusDor);
        btnPlusAbs = (ImageButton) rootView.findViewById(R.id.btnPlusAbs);
        btnMinusAbs = (ImageButton) rootView.findViewById(R.id.btnMinusAbs);
        btnPlusPec = (ImageButton) rootView.findViewById(R.id.btnPlusPec);
        btnMinusPec = (ImageButton) rootView.findViewById(R.id.btnMinusPec);
        btnPlusAux = (ImageButton) rootView.findViewById(R.id.btnPlusAux);
        btnMinusAux = (ImageButton) rootView.findViewById(R.id.btnMinusAux);
        btnPlusAux2 = (ImageButton) rootView.findViewById(R.id.btnPlusAux2);
        btnMinusAux2 = (ImageButton) rootView.findViewById(R.id.btnMinusAux2);
        btnPlusGlobal = (ImageButton) rootView.findViewById(R.id.btnPlusGlobal);
        btnMinusGlobal = (ImageButton) rootView.findViewById(R.id.btnMinusGlobal);
        btnPlayGlobal = (ImageButton) rootView.findViewById(R.id.btnPlayGlobal);
        btnStopGlobal = (ImageButton) rootView.findViewById(R.id.btnStopGlobal);
        btnPauseGlobal = (ImageButton) rootView.findViewById(R.id.btnPauseGlobal);
        txtLeg = (TextView) rootView.findViewById(R.id.txtLeg);
        txtGlu = (TextView) rootView.findViewById(R.id.txtGlu);
        txtLum = (TextView) rootView.findViewById(R.id.txtLum);
        txtSho = (TextView) rootView.findViewById(R.id.txtSho);
        txtBic = (TextView) rootView.findViewById(R.id.txtBic);
        txtDor = (TextView) rootView.findViewById(R.id.txtDor);
        txtAbs = (TextView) rootView.findViewById(R.id.txtAbs);
        txtPec = (TextView) rootView.findViewById(R.id.txtPec);
        txtAux = (TextView) rootView.findViewById(R.id.txtAux);
        txtAux2 = (TextView) rootView.findViewById(R.id.txtAux2);
        txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        txtForce = (TextView) rootView.findViewById(R.id.txtForce);
        txtChronaxy = (TextView) rootView.findViewById(R.id.txtChronaxy);
        btnStopLeg = (ImageView) rootView.findViewById(R.id.btnStopLeg);
        btnStopGlu = (ImageView) rootView.findViewById(R.id.btnStopGlu);
        btnStopLum = (ImageView) rootView.findViewById(R.id.btnStopLum);
        btnStopSho = (ImageView) rootView.findViewById(R.id.btnStopSho);
        btnStopBic = (ImageView) rootView.findViewById(R.id.btnStopBic);
        btnStopDor = (ImageView) rootView.findViewById(R.id.btnStopDor);
        btnStopAbs = (ImageView) rootView.findViewById(R.id.btnStopAbs);
        btnStopPec = (ImageView) rootView.findViewById(R.id.btnStopPec);
        btnStopAux = (ImageView) rootView.findViewById(R.id.btnStopAux);
        btnStopAux2 = (ImageView) rootView.findViewById(R.id.btnStopAux2);
        txtRampType = (TextView) rootView.findViewById(R.id.txtRampType);
        txtFrequency = (TextView) rootView.findViewById(R.id.txtFrequency);
        txtRelax = (TextView) rootView.findViewById(R.id.txtRelax);
        txtImpulseTime = (TextView) rootView.findViewById(R.id.txtImpulseTime);
        txtImpulseType = (TextView) rootView.findViewById(R.id.txtImpulseType);
        imgGlobal = (ImageView) rootView.findViewById(R.id.imgGlobal);
        txtStandard = (TextView) rootView.findViewById(R.id.txtStandard);
        relativeTime = (RelativeLayout) rootView.findViewById(R.id.linearImpulseTime);
        linearMaxforce = (RelativeLayout) rootView.findViewById(R.id.linearMaxforce);
        relativePotency = (RelativeLayout) rootView.findViewById(R.id.relativePotency);

        //Potency
        txtLastPotency = (TextView) rootView.findViewById(R.id.txtLastPotency);
        txtPotency = (TextView) rootView.findViewById(R.id.txtPotency);
        imgTimeRigth1 = (ImageView) rootView.findViewById(R.id.imgTimeRigth1);
        imgTimeRigth2 = (ImageView) rootView.findViewById(R.id.imgTimeRigth2);
        imgTimeRigth3 = (ImageView) rootView.findViewById(R.id.imgTimeRigth3);
        imgTimeRigth4 = (ImageView) rootView.findViewById(R.id.imgTimeRigth4);
        imgTimeRigth5 = (ImageView) rootView.findViewById(R.id.imgTimeRigth5);
        imgTimeRigth6 = (ImageView) rootView.findViewById(R.id.imgTimeRigth6);
        imgTimeRigth7 = (ImageView) rootView.findViewById(R.id.imgTimeRigth7);
        imgTimeRigth8 = (ImageView) rootView.findViewById(R.id.imgTimeRigth8);
        imgTimeRigth9 = (ImageView) rootView.findViewById(R.id.imgTimeRigth9);
        imgTimeRigth10 = (ImageView) rootView.findViewById(R.id.imgTimeRigth10);
        imgPotLeft1 = (ImageView) rootView.findViewById(R.id.imgPotLeft1);
        imgPotLeft2 = (ImageView) rootView.findViewById(R.id.imgPotLeft2);
        imgPotLeft3 = (ImageView) rootView.findViewById(R.id.imgPotLeft3);
        imgPotLeft4 = (ImageView) rootView.findViewById(R.id.imgPotLeft4);
        imgPotLeft5 = (ImageView) rootView.findViewById(R.id.imgPotLeft5);
        imgPotLeft6 = (ImageView) rootView.findViewById(R.id.imgPotLeft6);
        imgPotLeft7 = (ImageView) rootView.findViewById(R.id.imgPotLeft7);
        imgPotLeft8 = (ImageView) rootView.findViewById(R.id.imgPotLeft8);
        imgPotLeft9 = (ImageView) rootView.findViewById(R.id.imgPotLeft9);
        imgPotLeft10 = (ImageView) rootView.findViewById(R.id.imgPotLeft10);

        //Corporal
        imgFrontPiernaSup = (ImageView) rootView.findViewById(R.id.imgFrontPiernaSup);
        imgFrontPectorales = (ImageView) rootView.findViewById(R.id.imgFrontPectorales);
        imgFrontBiceps = (ImageView) rootView.findViewById(R.id.imgFrontBiceps);
        imgFrontAbdominales = (ImageView) rootView.findViewById(R.id.imgFrontAbdominales);
        imgBackTrapecio = (ImageView) rootView.findViewById(R.id.imgBackTrapecio);
        imgBackTriceps = (ImageView) rootView.findViewById(R.id.imgBackTriceps);
        imgBackPiernaSup = (ImageView) rootView.findViewById(R.id.imgBackPiernaSup);
        imgBackLumbares = (ImageView) rootView.findViewById(R.id.imgBackLumbares);
        imgBackGluteos = (ImageView) rootView.findViewById(R.id.imgBackGluteos);
        imgBackDorsales = (ImageView) rootView.findViewById(R.id.imgBackDorsales);
    }

    public void loadParticipants(){
        gridParticipants.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 12);
        gridParticipants.setLayoutManager(layoutManager);
        gridParticipants.setNestedScrollingEnabled(false);

        //Select first participant
        participants.get(0).setSelected(true);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.micro_margin);
        participantsGridAdapter = new ParticipantsGridAdapter(participants, getActivity(), ExerciseMultipleNormalFragment.this, UDPServer.SERVER_NORMAL);
        gridParticipants.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        gridParticipants.setAdapter(participantsGridAdapter);

        potency = 0;
        updatePotencyUi(0);
    }

    public void setListeners(){
        btnPlusClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (modeActive){
                    case MODE_LEVEL:
                        switch (view.getId()){
                            case R.id.btnPlusLeg:
                                txtLeg.setText(doPlusValue(Integer.parseInt(txtLeg.getText().toString())+1, Vest.CHANNEL_LEG));
                                break;
                            case R.id.btnPlusGlu:
                                txtGlu.setText(doPlusValue(Integer.parseInt(txtGlu.getText().toString())+1, Vest.CHANNEL_GLU));
                                break;
                            case R.id.btnPlusLum:
                                txtLum.setText(doPlusValue(Integer.parseInt(txtLum.getText().toString())+1, Vest.CHANNEL_LUM));
                                break;
                            case R.id.btnPlusSho:
                                txtSho.setText(doPlusValue(Integer.parseInt(txtSho.getText().toString())+1, Vest.CHANNEL_SHO));
                                break;
                            case R.id.btnPlusBic:
                                txtBic.setText(doPlusValue(Integer.parseInt(txtBic.getText().toString())+1, Vest.CHANNEL_BIC));
                                break;
                            case R.id.btnPlusDor:
                                txtDor.setText(doPlusValue(Integer.parseInt(txtDor.getText().toString())+1, Vest.CHANNEL_DOR));
                                break;
                            case R.id.btnPlusAbs:
                                txtAbs.setText(doPlusValue(Integer.parseInt(txtAbs.getText().toString())+1, Vest.CHANNEL_ABS));;
                                break;
                            case R.id.btnPlusPec:
                                txtPec.setText(doPlusValue(Integer.parseInt(txtPec.getText().toString())+1, Vest.CHANNEL_PEC));
                                break;
                            case R.id.btnPlusAux:
                                txtAux.setText(doPlusValue(Integer.parseInt(txtAux.getText().toString())+1, Vest.CHANNEL_AUX));
                                break;
                            case R.id.btnPlusAux2:
                                txtAux2.setText(doPlusValue(Integer.parseInt(txtAux2.getText().toString())+1, Vest.CHANNEL_AUX_2));
                                break;
                        }
                        break;
                    case MODE_CRONAXIA:
                        switch (view.getId()){
                            case R.id.btnPlusLeg:
                                txtLeg.setText(doPlusValue(Integer.parseInt(txtLeg.getText().toString())+5, Vest.CHANNEL_LEG));
                                break;
                            case R.id.btnPlusGlu:
                                txtGlu.setText(doPlusValue(Integer.parseInt(txtGlu.getText().toString())+5, Vest.CHANNEL_GLU));
                                break;
                            case R.id.btnPlusLum:
                                txtLum.setText(doPlusValue(Integer.parseInt(txtLum.getText().toString())+5, Vest.CHANNEL_LUM));
                                break;
                            case R.id.btnPlusSho:
                                txtSho.setText(doPlusValue(Integer.parseInt(txtSho.getText().toString())+5, Vest.CHANNEL_SHO));
                                break;
                            case R.id.btnPlusBic:
                                txtBic.setText(doPlusValue(Integer.parseInt(txtBic.getText().toString())+5, Vest.CHANNEL_BIC));
                                break;
                            case R.id.btnPlusDor:
                                txtDor.setText(doPlusValue(Integer.parseInt(txtDor.getText().toString())+5, Vest.CHANNEL_DOR));
                                break;
                            case R.id.btnPlusAbs:
                                txtAbs.setText(doPlusValue(Integer.parseInt(txtAbs.getText().toString())+5, Vest.CHANNEL_ABS));;
                                break;
                            case R.id.btnPlusPec:
                                txtPec.setText(doPlusValue(Integer.parseInt(txtPec.getText().toString())+5, Vest.CHANNEL_PEC));
                                break;
                            case R.id.btnPlusAux:
                                txtAux.setText(doPlusValue(Integer.parseInt(txtAux.getText().toString())+5, Vest.CHANNEL_AUX));
                                break;
                            case R.id.btnPlusAux2:
                                txtAux2.setText(doPlusValue(Integer.parseInt(txtAux2.getText().toString())+5, Vest.CHANNEL_AUX_2));
                                break;
                        }
                        break;
                }
            }
        };

        btnPlusOnTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                    if (btnCorporalPlus != null)
                        btnCorporalPlus.setAutoIncrement(false);
                }
                return false;
            }
        };

        btnPlusLongClickListener = new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View view) {
                int channelPlus = Vest.CHANNEL_LEG;
                int plusValueChannel = 0;
                switch (view.getId()){
                    case R.id.btnPlusLeg:
                        channelPlus = Vest.CHANNEL_LEG;
                        plusValueChannel = Integer.parseInt(txtLeg.getText().toString());
                        break;
                    case R.id.btnPlusGlu:
                        channelPlus =  Vest.CHANNEL_GLU;
                        plusValueChannel = Integer.parseInt(txtGlu.getText().toString());
                        break;
                    case R.id.btnPlusLum:
                        channelPlus =  Vest.CHANNEL_LUM;
                        plusValueChannel = Integer.parseInt(txtLum.getText().toString());
                        break;
                    case R.id.btnPlusSho:
                        channelPlus =  Vest.CHANNEL_SHO;
                        plusValueChannel = Integer.parseInt(txtSho.getText().toString());
                        break;
                    case R.id.btnPlusBic:
                        channelPlus =  Vest.CHANNEL_BIC;
                        plusValueChannel = Integer.parseInt(txtBic.getText().toString());
                        break;
                    case R.id.btnPlusDor:
                        channelPlus =  Vest.CHANNEL_DOR;
                        plusValueChannel = Integer.parseInt(txtDor.getText().toString());
                        break;
                    case R.id.btnPlusAbs:
                        channelPlus =  Vest.CHANNEL_ABS;
                        plusValueChannel = Integer.parseInt(txtAbs.getText().toString());
                        break;
                    case R.id.btnPlusPec:
                        channelPlus =  Vest.CHANNEL_PEC;
                        plusValueChannel = Integer.parseInt(txtPec.getText().toString());
                        break;
                    case R.id.btnPlusAux:
                        channelPlus =  Vest.CHANNEL_AUX;
                        plusValueChannel = Integer.parseInt(txtAux.getText().toString());
                        break;
                    case R.id.btnPlusAux2:
                        channelPlus =  Vest.CHANNEL_AUX_2;
                        plusValueChannel = Integer.parseInt(txtAux2.getText().toString());
                        break;
                }

                if (plusValueChannel >= 60 && modeActive == MODE_LEVEL) {
                    if (btnCorporalPlus != null)
                        btnCorporalPlus.setAutoIncrement(false);
                } else {
                    btnPlusMinusHandler.removeCallbacksAndMessages(null);
                    btnCorporalPlus = new BtnCorporalPlus(ExerciseMultipleNormalFragment.this, BtnCorporalPlus.FROM_MULTIPLE_NORMAL,btnPlusMinusHandler,getContext(),
                            channelPlus, plusValueChannel, modeActive, UDPServer.SERVER_NORMAL);
                    btnCorporalPlus.setAutoIncrement(true);
                    btnPlusMinusHandler.post(btnCorporalPlus);
                }
                return false;
            }
        };

        btnMinusClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (modeActive){
                    case MODE_LEVEL:
                        switch (view.getId()){
                            case R.id.btnMinusLeg:
                                txtLeg.setText(doMinusValue(Integer.parseInt(txtLeg.getText().toString())-1, Vest.CHANNEL_LEG));
                                break;
                            case R.id.btnMinusGlu:
                                txtGlu.setText(doMinusValue(Integer.parseInt(txtGlu.getText().toString())-1, Vest.CHANNEL_GLU));
                                break;
                            case R.id.btnMinusLum:
                                txtLum.setText(doMinusValue(Integer.parseInt(txtLum.getText().toString())-1, Vest.CHANNEL_LUM));
                                break;
                            case R.id.btnMinusSho:
                                txtSho.setText(doMinusValue(Integer.parseInt(txtSho.getText().toString())-1, Vest.CHANNEL_SHO));
                                break;
                            case R.id.btnMinusBic:
                                txtBic.setText(doMinusValue(Integer.parseInt(txtBic.getText().toString())-1, Vest.CHANNEL_BIC));
                                break;
                            case R.id.btnMinusDor:
                                txtDor.setText(doMinusValue(Integer.parseInt(txtDor.getText().toString())-1, Vest.CHANNEL_DOR));
                                break;
                            case R.id.btnMinusAbs:
                                txtAbs.setText(doMinusValue(Integer.parseInt(txtAbs.getText().toString())-1, Vest.CHANNEL_ABS));
                                break;
                            case R.id.btnMinusPec:
                                txtPec.setText(doMinusValue(Integer.parseInt(txtPec.getText().toString())-1, Vest.CHANNEL_PEC));
                                break;
                            case R.id.btnMinusAux:
                                txtAux.setText(doMinusValue(Integer.parseInt(txtAux.getText().toString())-1, Vest.CHANNEL_AUX));
                                break;
                            case R.id.btnMinusAux2:
                                txtAux2.setText(doMinusValue(Integer.parseInt(txtAux2.getText().toString())-1, Vest.CHANNEL_AUX_2));
                                break;
                        }
                        break;
                    case MODE_CRONAXIA:
                        switch (view.getId()){
                            case R.id.btnMinusLeg:
                                txtLeg.setText(doMinusValue(Integer.parseInt(txtLeg.getText().toString())-5, Vest.CHANNEL_LEG));
                                break;
                            case R.id.btnMinusGlu:
                                txtGlu.setText(doMinusValue(Integer.parseInt(txtGlu.getText().toString())-5, Vest.CHANNEL_GLU));
                                break;
                            case R.id.btnMinusLum:
                                txtLum.setText(doMinusValue(Integer.parseInt(txtLum.getText().toString())-5, Vest.CHANNEL_LUM));
                                break;
                            case R.id.btnMinusSho:
                                txtSho.setText(doMinusValue(Integer.parseInt(txtSho.getText().toString())-5, Vest.CHANNEL_SHO));
                                break;
                            case R.id.btnMinusBic:
                                txtBic.setText(doMinusValue(Integer.parseInt(txtBic.getText().toString())-5, Vest.CHANNEL_BIC));
                                break;
                            case R.id.btnMinusDor:
                                txtDor.setText(doMinusValue(Integer.parseInt(txtDor.getText().toString())-5, Vest.CHANNEL_DOR));
                                break;
                            case R.id.btnMinusAbs:
                                txtAbs.setText(doMinusValue(Integer.parseInt(txtAbs.getText().toString())-5, Vest.CHANNEL_ABS));
                                break;
                            case R.id.btnMinusPec:
                                txtPec.setText(doMinusValue(Integer.parseInt(txtPec.getText().toString())-5, Vest.CHANNEL_PEC));
                                break;
                            case R.id.btnMinusAux:
                                txtAux.setText(doMinusValue(Integer.parseInt(txtAux.getText().toString())-5, Vest.CHANNEL_AUX));
                                break;
                            case R.id.btnMinusAux2:
                                txtAux2.setText(doMinusValue(Integer.parseInt(txtAux2.getText().toString())-5, Vest.CHANNEL_AUX_2));
                                break;
                        }
                        break;
                }
            }
        };

        btnMinussOnTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                    if (btnCorporalMinus != null)
                        btnCorporalMinus.setAutoDecrement(false);
                }
                return false;
            }
        };

        btnMinusLongClickListener = new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View view) {
                int channelMinus = Vest.CHANNEL_LEG;
                int minusValueChannel = 0;
                switch (view.getId()){
                    case R.id.btnMinusLeg:
                        channelMinus = Vest.CHANNEL_LEG;
                        minusValueChannel = Integer.parseInt(txtLeg.getText().toString());
                        break;
                    case R.id.btnMinusGlu:
                        channelMinus = Vest.CHANNEL_GLU;
                        minusValueChannel = Integer.parseInt(txtGlu.getText().toString());
                        break;
                    case R.id.btnMinusLum:
                        channelMinus =  Vest.CHANNEL_LUM;
                        minusValueChannel = Integer.parseInt(txtLum.getText().toString());
                        break;
                    case R.id.btnMinusSho:
                        channelMinus =  Vest.CHANNEL_SHO;
                        minusValueChannel = Integer.parseInt(txtSho.getText().toString());
                        break;
                    case R.id.btnMinusBic:
                        channelMinus =  Vest.CHANNEL_BIC;
                        minusValueChannel = Integer.parseInt(txtBic.getText().toString());
                        break;
                    case R.id.btnMinusDor:
                        channelMinus =  Vest.CHANNEL_DOR;
                        minusValueChannel = Integer.parseInt(txtDor.getText().toString());
                        break;
                    case R.id.btnMinusAbs:
                        channelMinus =  Vest.CHANNEL_ABS;
                        minusValueChannel = Integer.parseInt(txtAbs.getText().toString());
                        break;
                    case R.id.btnMinusPec:
                        channelMinus =  Vest.CHANNEL_PEC;
                        minusValueChannel = Integer.parseInt(txtPec.getText().toString());
                        break;
                    case R.id.btnMinusAux:
                        channelMinus =  Vest.CHANNEL_AUX;
                        minusValueChannel = Integer.parseInt(txtAux.getText().toString());
                        break;
                    case R.id.btnMinusAux2:
                        channelMinus =  Vest.CHANNEL_AUX_2;
                        minusValueChannel = Integer.parseInt(txtAux2.getText().toString());
                        break;
                }

                btnPlusMinusHandler.removeCallbacksAndMessages(null);
                btnCorporalMinus = new BtnCorporalMinus(ExerciseMultipleNormalFragment.this, BtnCorporalPlus.FROM_MULTIPLE_NORMAL,btnPlusMinusHandler,getContext(),
                        channelMinus, minusValueChannel, modeActive, UDPServer.SERVER_NORMAL);
                btnCorporalMinus.setAutoDecrement(true);
                btnPlusMinusHandler.post(btnCorporalMinus);
                return false;
            }
        };

        btnStopClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btnStopLeg:
                        txtLeg.setText(doStopValue(Vest.CHANNEL_LEG));
                        break;
                    case R.id.btnStopGlu:
                        txtGlu.setText(doStopValue(Vest.CHANNEL_GLU));
                        break;
                    case R.id.btnStopLum:
                        txtLum.setText(doStopValue(Vest.CHANNEL_LUM));
                        break;
                    case R.id.btnStopSho:
                        txtSho.setText(doStopValue(Vest.CHANNEL_SHO));
                        break;
                    case R.id.btnStopBic:
                        txtBic.setText(doStopValue(Vest.CHANNEL_BIC));
                        break;
                    case R.id.btnStopDor:
                        txtDor.setText(doStopValue(Vest.CHANNEL_DOR));
                        break;
                    case R.id.btnStopAbs:
                        txtAbs.setText(doStopValue(Vest.CHANNEL_ABS));
                        break;
                    case R.id.btnStopPec:
                        txtPec.setText(doStopValue(Vest.CHANNEL_PEC));
                        break;
                    case R.id.btnStopAux:
                        txtAux.setText(doStopValue(Vest.CHANNEL_AUX));
                        break;
                    case R.id.btnStopAux2:
                        txtAux2.setText(doStopValue(Vest.CHANNEL_AUX_2));
                        break;
                }
            }
        };

        btnPlusGlobal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (trainings.get(trainingIndex).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
                    if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                        if (btnPlusPotency != null)
                            btnPlusPotency.setAutoIncrement(false);
                    }
                } else
                    Toast.makeText(getContext(), getString(R.string.training_not_started), Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        btnPlusGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trainings.get(trainingIndex).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
                    incrementPotency();
                } else
                    Toast.makeText(getContext(), getString(R.string.training_not_started), Toast.LENGTH_SHORT).show();
            }
        });

        btnPlusGlobal.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (trainings.get(trainingIndex).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
                    if (potency >= 60) {
                        if ((btnPlusPotency != null))
                            btnPlusPotency.setAutoIncrement(false);
                    } else {
                        btnPlusMinusHandler.removeCallbacksAndMessages(null);
                        btnPlusPotency = new BtnPlusPotency(ExerciseMultipleNormalFragment.this, BtnPlusPotency.FROM_MULTIPLE_NORMAL,btnPlusMinusHandler,getContext()
                                , null, null, 0, UDPServer.SERVER_NORMAL);
                        btnPlusPotency.setAutoIncrement(true);
                        btnPlusMinusHandler.post(btnPlusPotency);
                    }
                } else
                    Toast.makeText(getContext(), getString(R.string.training_not_started), Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        btnMinusGlobal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                    if (btnMinusPotency != null)
                        btnMinusPotency.setAutoDecrement(false);
                }
                return false;
            }
        });

        btnMinusGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decrementPotency();
            }
        });

        btnMinusGlobal.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                btnPlusMinusHandler.removeCallbacksAndMessages(null);
                btnMinusPotency = new BtnMinusPotency(ExerciseMultipleNormalFragment.this, BtnPlusPotency.FROM_MULTIPLE_NORMAL,btnPlusMinusHandler,getContext()
                        , null, null, 0, UDPServer.SERVER_NORMAL);
                btnMinusPotency.setAutoDecrement(true);
                btnPlusMinusHandler.post(btnMinusPotency);
                return false;
            }
        });

        //START
        btnPlayGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).createConnectionIndividual();
                //Show controls
                switch (trainings.get(trainingIndex).getTrainingStatus()) {
                    case Training.TRAINING_STATUS_PAUSE:
                        ((ExerciseMultipleActivity)getActivity()).startConfigTimer();

                        switch (connectionType){
                            case Device.DEVICE_WIFI:
                                ((ExerciseMultipleActivity)getActivity()).restartProgram(vestIndex, trainings.get(trainingIndex).getUid(), trainings.get(trainingIndex));
                                ((ExerciseMultipleActivity)getActivity()).sendChannelsLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                                break;
                            case Device.DEVICE_BLE:
                                ((ExerciseMultipleActivity)getActivity()).restartProgram(vestIndex, trainings.get(trainingIndex).getUid(), trainings.get(trainingIndex));
                                break;
                        }

                        ((ExerciseMultipleActivity)getActivity()).setStartNormal(true);
                        break;
                    case Training.TRAINING_STATUS_STOP:
                        ((ExerciseMultipleActivity)getActivity()).startConfigTimer();

                        switch (connectionType){
                            case Device.DEVICE_WIFI:
                                ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                                ((ExerciseMultipleActivity)getActivity()).sendStartProgram(vestIndex, trainings.get(trainingIndex).getUid());
                                break;
                            case Device.DEVICE_BLE:
                                ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                                break;
                        }

                        ((ExerciseMultipleActivity)getActivity()).setStartNormal(true);
                        break;
                }

                trainings.get(trainingIndex).setTrainingStatus(Training.TRAINING_STATUS_PLAY);
                updateButtonsUi();
            }
        });

        //PAUSE
        btnPauseGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, trainings.get(trainingIndex).getUid());
                trainings.get(trainingIndex).setTrainingStatus(Training.TRAINING_STATUS_PAUSE);
                trainings.get(trainingIndex).setLastPotency(potency);
                txtLastPotency.setText(getString(R.string.potency_previous)+" "+trainings.get(trainingIndex).getLastPotency()+" %");
                potencyPrePause = potency;
                potency = 0;
                trainings.get(trainingIndex).setLevel(0, potency);
                ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                updatePotencyUi(potency);
                updatePauseUi(0);
                updateEstimulationUi(100);
                updateButtonsUi();
            }
        });

        //STOP
        btnStopGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, trainings.get(trainingIndex).getUid());
                trainings.get(trainingIndex).setLastPotency(potency);
                txtLastPotency.setText(getString(R.string.potency_previous)+" "+trainings.get(trainingIndex).getLastPotency()+" %");
                trainings.get(trainingIndex).setTrainingStatus(Training.TRAINING_STATUS_STOP);
                potency = 0;
                trainings.get(trainingIndex).setLevel(0, potency);
                ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                updatePotencyUi(potency);
                ((ExerciseMultipleActivity)getActivity()).setStartNormal(false);
                updateTimeText(trainings.get(trainingIndex).getTime());
                updatePauseUi(0);
                updateEstimulationUi(100);
                updateButtonsUi();
            }
        });

        txtChronaxy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (modeActive){
                    case MODE_LEVEL:
                        txtChronaxy.setBackgroundResource(R.drawable.btn_default_push);
                        modeActive = MODE_CRONAXIA;
                        break;
                    case MODE_CRONAXIA:
                        modeActive = MODE_LEVEL;
                        txtChronaxy.setBackgroundResource(R.drawable.btn_default);
                        break;
                }
                changeModeValues();
            }
        });

        txtRampType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).showRampDialog(trainings.get(trainingIndex));
            }
        });

        txtFrequency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).showFrequencyDialog(trainings.get(trainingIndex), false);
            }
        });

        txtRelax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).showRelaxDialog(trainings.get(trainingIndex));
            }
        });

        txtImpulseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).showImpulseTimeDialog(trainings.get(trainingIndex));
            }
        });

        txtStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).showStandardDialog(trainings.get(trainingIndex));
            }
        });

        relativeTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesManager pref = PreferencesManager.getInstance(getContext());
                User userCoach = pref.getUser();

                if (userCoach.getUserPmissions().isTime())
                    ((ExerciseMultipleActivity)getActivity()).showTimeDialog(trainings.get(trainingIndex), TimeDialog.FROM_INDIVIDUAL);
            }
        });

        relativePotency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((ExerciseMultipleActivity)getActivity()).showPotencyDialog(potency, TimeDialog.FROM_INDIVIDUAL);
            }
        });

        imgGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)getActivity()).scrollPager();
            }
        });

        btnPlusLeg.setOnClickListener(btnPlusClickListener);btnMinusLeg.setOnClickListener(btnMinusClickListener);btnStopLeg.setOnClickListener(btnStopClickListener);
        btnPlusLeg.setOnLongClickListener(btnPlusLongClickListener);btnPlusLeg.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusLeg.setOnLongClickListener(btnMinusLongClickListener);btnMinusLeg.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusGlu.setOnClickListener(btnPlusClickListener);btnMinusGlu.setOnClickListener(btnMinusClickListener);btnStopGlu.setOnClickListener(btnStopClickListener);
        btnPlusGlu.setOnLongClickListener(btnPlusLongClickListener);btnPlusGlu.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusGlu.setOnLongClickListener(btnMinusLongClickListener);btnMinusGlu.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusLum.setOnClickListener(btnPlusClickListener);btnMinusLum.setOnClickListener(btnMinusClickListener);btnStopLum.setOnClickListener(btnStopClickListener);
        btnPlusLum.setOnLongClickListener(btnPlusLongClickListener);btnPlusLum.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusLum.setOnLongClickListener(btnMinusLongClickListener);btnMinusLum.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusSho.setOnClickListener(btnPlusClickListener);btnMinusSho.setOnClickListener(btnMinusClickListener);btnStopSho.setOnClickListener(btnStopClickListener);
        btnPlusSho.setOnLongClickListener(btnPlusLongClickListener);btnPlusSho.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusSho.setOnLongClickListener(btnMinusLongClickListener);btnMinusSho.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusBic.setOnClickListener(btnPlusClickListener);btnMinusBic.setOnClickListener(btnMinusClickListener);btnStopBic.setOnClickListener(btnStopClickListener);
        btnPlusBic.setOnLongClickListener(btnPlusLongClickListener);btnPlusBic.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusBic.setOnLongClickListener(btnMinusLongClickListener);btnMinusBic.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusDor.setOnClickListener(btnPlusClickListener);btnMinusDor.setOnClickListener(btnMinusClickListener);btnStopDor.setOnClickListener(btnStopClickListener);
        btnPlusDor.setOnLongClickListener(btnPlusLongClickListener);btnPlusDor.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusDor.setOnLongClickListener(btnMinusLongClickListener);btnMinusDor.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusAbs.setOnClickListener(btnPlusClickListener);btnMinusAbs.setOnClickListener(btnMinusClickListener);btnStopAbs.setOnClickListener(btnStopClickListener);
        btnPlusAbs.setOnLongClickListener(btnPlusLongClickListener);btnPlusAbs.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusAbs.setOnLongClickListener(btnMinusLongClickListener);btnMinusAbs.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusPec.setOnClickListener(btnPlusClickListener);btnMinusPec.setOnClickListener(btnMinusClickListener);btnStopPec.setOnClickListener(btnStopClickListener);
        btnPlusPec.setOnLongClickListener(btnPlusLongClickListener);btnPlusPec.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusPec.setOnLongClickListener(btnMinusLongClickListener);btnMinusPec.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusAux.setOnClickListener(btnPlusClickListener);btnMinusAux.setOnClickListener(btnMinusClickListener);btnStopAux.setOnClickListener(btnStopClickListener);
        btnPlusAux.setOnLongClickListener(btnPlusLongClickListener);btnPlusAux.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusAux.setOnLongClickListener(btnMinusLongClickListener);btnMinusAux.setOnTouchListener(btnMinussOnTouchListener);
        btnPlusAux2.setOnClickListener(btnPlusClickListener);btnMinusAux2.setOnClickListener(btnMinusClickListener);btnStopAux2.setOnClickListener(btnStopClickListener);
        btnPlusAux2.setOnLongClickListener(btnPlusLongClickListener);btnPlusAux2.setOnTouchListener(btnPlusOnTouchListener);
        btnMinusAux2.setOnLongClickListener(btnMinusLongClickListener);btnMinusAux2.setOnTouchListener(btnMinussOnTouchListener);
    }

    public String doMinusValue(int value, int channel){
        switch (modeActive){
            case MODE_LEVEL:
                if(value < 0)
                    value = 0;

                decrementCorporal(value, channel);
                updateCorporalUi(channel, value);
                break;
            case MODE_CRONAXIA:
                if(value < 25)
                    value = 25;

                decrementCorporal(value, channel);
                break;
        }
        return ""+value;
    }

    public String doPlusValue (int value, int channel){
        switch (modeActive){
            case MODE_LEVEL:
                if(value > 100)
                    value = 100;

                incrementCorporal(value,channel);
                updateCorporalUi(channel, value);
                break;
            case MODE_CRONAXIA:
                if(value > 400)
                    value = 400;

                incrementCorporal(value,channel);
                break;
        }
        return ""+value;
    }

    public String doStopValue( int channel){

        switch (modeActive) {
            case MODE_LEVEL:
                trainings.get(trainingIndex).setLevel(channel,0);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChannelsLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                updateCorporalUi(channel, 0);
                return ""+0;
            case MODE_CRONAXIA:
                trainings.get(trainingIndex).setChronaxy(channel,25);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChronaxyLevels(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                return ""+25;
        }
        return ""+0;
    }

    public void doStopChangeProgram(){
        potency = potencyPrePause;

        ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, trainings.get(trainingIndex).getUid());
        trainings.get(trainingIndex).setTrainingStatus(Training.TRAINING_STATUS_STOP);
        trainings.get(trainingIndex).setLevel(0, potency);
        trainings.get(trainingIndex).setLoadPotency(potency);
        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        updatePotencyUi(potency);
        ((ExerciseMultipleActivity)getActivity()).setStartNormal(false);
        updateTimeText(trainings.get(trainingIndex).getTime());
        updatePauseUi(0);
        updateEstimulationUi(100);
        updateButtonsUi();
    }

    public void updateStatus(final int time, boolean isEstimulating, int estimulationMs, int index){

        //UpdateTime
        if ((index == vestIndex)){
            updateTimeText(time);

            float progressEstimulation = 0;
            int esimulationTime = trainings.get(trainingIndex).getEstimulationTime();
            int pauseTime = trainings.get(trainingIndex).getPauseTime();

            //Update Progress bar
            if (isEstimulating){
                progressEstimulation = ((float) estimulationMs / esimulationTime)*100;
                updateEstimulationUi((int)progressEstimulation);
            } else {
                progressEstimulation = ((float) estimulationMs / pauseTime)*100;
                updatePauseUi((int)progressEstimulation);
            }
        }
    }

    public void updateTimeText(int time){
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null){
            exerciseMaxForcePresentationService.loadTime(time);
        }

        if (time > 1){
            String aux1 = "";
            String aux2 = "";
            if((time/60)<10)
                aux1="0";
            if((time%60)<10)
                aux2="0";
            txtTime.setText(aux1 +time/60+" : "+ aux2 +time%60);
        } else {
            btnStopGlobal.callOnClick();
            ((ExerciseMultipleActivity)getActivity()).sendResultsTraining();
        }
    }

    public void updateCorporalUi(int channel, int value){
        switch (channel){
            case Vest.CHANNEL_LEG:
                if (value > 0){
                    imgBackPiernaSup.setVisibility(View.VISIBLE);
                    imgFrontPiernaSup.setVisibility(View.VISIBLE);
                } else {
                    imgBackPiernaSup.setVisibility(View.GONE);
                    imgFrontPiernaSup.setVisibility(View.GONE);
                }
                break;
            case Vest.CHANNEL_GLU:
                if (value > 0)
                    imgBackGluteos.setVisibility(View.VISIBLE);
                else
                    imgBackGluteos.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_LUM:
                if (value > 0)
                    imgBackLumbares.setVisibility(View.VISIBLE);
                else
                    imgBackLumbares.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_SHO:
                if (value > 0)
                    imgBackTrapecio.setVisibility(View.VISIBLE);
                else
                    imgBackTrapecio.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_BIC:
                if (value > 0) {
                    imgBackTriceps.setVisibility(View.VISIBLE);
                    imgFrontBiceps.setVisibility(View.VISIBLE);
                } else{
                    imgBackTriceps.setVisibility(View.GONE);
                    imgFrontBiceps.setVisibility(View.GONE);
                }
                break;
            case Vest.CHANNEL_DOR:
                if (value > 0)
                    imgBackDorsales.setVisibility(View.VISIBLE);
                else
                    imgBackDorsales.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_ABS:
                if (value > 0)
                    imgFrontAbdominales.setVisibility(View.VISIBLE);
                else
                    imgFrontAbdominales.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_PEC:
                if (value > 0)
                    imgFrontPectorales.setVisibility(View.VISIBLE);
                else
                    imgFrontPectorales.setVisibility(View.GONE);
                break;
            case Vest.CHANNEL_AUX:
                break;
            case Vest.CHANNEL_AUX_2:
                break;
        }
    }

    public void updatePotencyPagerUi() {
        potency = trainings.get(trainingIndex).getLevel(0);
        updatePotencyUi(trainings.get(trainingIndex).getLevel(0));
    }

    public void updateButtonsUi(){
        if (((ExerciseMultipleActivity)getActivity()).isStartGlobal()){
            btnPlayGlobal.setVisibility(View.GONE);
            btnStopGlobal.setVisibility(View.GONE);
        } else {
            btnPlayGlobal.setVisibility(View.VISIBLE);
            btnStopGlobal.setVisibility(View.VISIBLE);

            //Show controls
            switch (trainings.get(trainingIndex).getTrainingStatus()) {
                case Training.TRAINING_STATUS_PLAY:
                case Training.TRAINING_STATUS_RESTART:
                    btnPauseGlobal.setVisibility(View.VISIBLE);
                    btnPlayGlobal.setVisibility(View.GONE);
                    break;
                case Training.TRAINING_STATUS_PAUSE:
                    btnPauseGlobal.setVisibility(View.GONE);
                    btnPlayGlobal.setVisibility(View.VISIBLE);
                    break;
                case Training.TRAINING_STATUS_STOP:
                    btnPauseGlobal.setVisibility(View.GONE);
                    btnPlayGlobal.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    public void updatePotencyUi(int potencyValue) {
        txtPotency.setText(potencyValue+" %");

        if (potency == 0){
            imgPotLeft1.setImageResource(R.drawable.img_progress_grey_1);
        }if (potency > 0){
            imgPotLeft1.setImageResource(R.drawable.img_progress_1);
        } else {
            imgPotLeft1.setImageResource(R.drawable.img_progress_grey_1);
        } if (potency > 10){
            imgPotLeft2.setImageResource(R.drawable.img_progress_2);
        } else {
            imgPotLeft2.setImageResource(R.drawable.img_progress_grey_2);
        } if (potency > 20){
            imgPotLeft3.setImageResource(R.drawable.img_progress_3);
        } else {
            imgPotLeft3.setImageResource(R.drawable.img_progress_grey_3);
        }if (potency > 30){
            imgPotLeft4.setImageResource(R.drawable.img_progress_4);
        } else {
            imgPotLeft4.setImageResource(R.drawable.img_progress_grey_4);
        }if (potency > 40){
            imgPotLeft5.setImageResource(R.drawable.img_progress_5);
        } else {
            imgPotLeft5.setImageResource(R.drawable.img_progress_grey_5);
        }if (potency > 50){
            imgPotLeft6.setImageResource(R.drawable.img_progress_6);
        } else {
            imgPotLeft6.setImageResource(R.drawable.img_progress_grey_6);
        }if (potency > 60){
            imgPotLeft7.setImageResource(R.drawable.img_progress_7);
        } else {
            imgPotLeft7.setImageResource(R.drawable.img_progress_grey_7);
        }if (potency > 70){
            imgPotLeft8.setImageResource(R.drawable.img_progress_8);
        } else {
            imgPotLeft8.setImageResource(R.drawable.img_progress_grey_8);
        }if (potency > 80){
            imgPotLeft9.setImageResource(R.drawable.img_progress_9);
        } else {
            imgPotLeft9.setImageResource(R.drawable.img_progress_grey_9);
        }if (potency > 90){
            imgPotLeft10.setImageResource(R.drawable.img_progress_10);
        } else {
            imgPotLeft10.setImageResource(R.drawable.img_progress_grey_10);
        }
    }

    public void updateEstimulationUi(int estimulationValue) {

        //Chromecast
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updateEstimulationUi(estimulationValue);

        if (estimulationValue == 100){
            imgTimeRigth1.setImageResource(R.drawable.img_progress_grey_1);
        }if (estimulationValue < 100){
            imgTimeRigth1.setImageResource(R.drawable.img_estim_1);
        } else {
            imgTimeRigth1.setImageResource(R.drawable.img_progress_grey_1);
        } if (estimulationValue < 90){
            imgTimeRigth2.setImageResource(R.drawable.img_estim_2);
        } else {
            imgTimeRigth2.setImageResource(R.drawable.img_progress_grey_2);
        } if (estimulationValue < 80){
            imgTimeRigth3.setImageResource(R.drawable.img_estim_3);
        } else {
            imgTimeRigth3.setImageResource(R.drawable.img_progress_grey_3);
        }if (estimulationValue < 70){
            imgTimeRigth4.setImageResource(R.drawable.img_estim_4);
        } else {
            imgTimeRigth4.setImageResource(R.drawable.img_progress_grey_4);
        }if (estimulationValue < 60){
            imgTimeRigth5.setImageResource(R.drawable.img_estim_5);
        } else {
            imgTimeRigth5.setImageResource(R.drawable.img_progress_grey_5);
        }if (estimulationValue < 50){
            imgTimeRigth6.setImageResource(R.drawable.img_estim_6);
        } else {
            imgTimeRigth6.setImageResource(R.drawable.img_progress_grey_6);
        }if (estimulationValue < 40){
            imgTimeRigth7.setImageResource(R.drawable.img_estim_7);
        } else {
            imgTimeRigth7.setImageResource(R.drawable.img_progress_grey_7);
        }if (estimulationValue < 30){
            imgTimeRigth8.setImageResource(R.drawable.img_estim_8);
        } else {
            imgTimeRigth8.setImageResource(R.drawable.img_progress_grey_8);
        }if (estimulationValue < 20){
            imgTimeRigth9.setImageResource(R.drawable.img_estim_9);
        } else {
            imgTimeRigth9.setImageResource(R.drawable.img_progress_grey_9);
        }if (estimulationValue < 10){
            imgTimeRigth10.setImageResource(R.drawable.img_estim_10);
        } else {
            imgTimeRigth10.setImageResource(R.drawable.img_progress_grey_10);
        }
    }

    public void updatePauseUi(int estimulationValue) {

        //Chromecast
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updatePauseUi(estimulationValue);

        if (estimulationValue == 100){
            imgTimeRigth1.setImageResource(R.drawable.img_progress_grey_1);
        }if (estimulationValue < 100){
            imgTimeRigth1.setImageResource(R.drawable.img_relax_1);
        } else {
            imgTimeRigth1.setImageResource(R.drawable.img_progress_grey_1);
        } if (estimulationValue < 90){
            imgTimeRigth2.setImageResource(R.drawable.img_relax_2);
        } else {
            imgTimeRigth2.setImageResource(R.drawable.img_progress_grey_2);
        } if (estimulationValue < 80){
            imgTimeRigth3.setImageResource(R.drawable.img_relax_3);
        } else {
            imgTimeRigth3.setImageResource(R.drawable.img_progress_grey_3);
        }if (estimulationValue < 70){
            imgTimeRigth4.setImageResource(R.drawable.img_relax_4);
        } else {
            imgTimeRigth4.setImageResource(R.drawable.img_progress_grey_4);
        }if (estimulationValue < 60){
            imgTimeRigth5.setImageResource(R.drawable.img_relax_5);
        } else {
            imgTimeRigth5.setImageResource(R.drawable.img_progress_grey_5);
        }if (estimulationValue < 50){
            imgTimeRigth6.setImageResource(R.drawable.img_relax_6);
        } else {
            imgTimeRigth6.setImageResource(R.drawable.img_progress_grey_6);
        }if (estimulationValue < 40){
            imgTimeRigth7.setImageResource(R.drawable.img_relax_7);
        } else {
            imgTimeRigth7.setImageResource(R.drawable.img_progress_grey_7);
        }if (estimulationValue < 30){
            imgTimeRigth8.setImageResource(R.drawable.img_relax_8);
        } else {
            imgTimeRigth8.setImageResource(R.drawable.img_progress_grey_8);
        }if (estimulationValue < 20){
            imgTimeRigth9.setImageResource(R.drawable.img_relax_9);
        } else {
            imgTimeRigth9.setImageResource(R.drawable.img_progress_grey_9);
        }if (estimulationValue < 10){
            imgTimeRigth10.setImageResource(R.drawable.img_relax_10);
        } else {
            imgTimeRigth10.setImageResource(R.drawable.img_progress_grey_10);
        }
    }

    public void loadLevelsValue(){
        switch (modeActive){
            case MODE_LEVEL:
                txtLeg.setText(""+trainings.get(trainingIndex).getLevelLeg());
                txtGlu.setText(""+trainings.get(trainingIndex).getLevelGlu());
                txtLum.setText(""+trainings.get(trainingIndex).getLevelLum());
                txtDor.setText(""+trainings.get(trainingIndex).getLevelDor());
                txtSho.setText(""+trainings.get(trainingIndex).getLevelSho());
                txtAbs.setText(""+trainings.get(trainingIndex).getLevelAbs());
                txtPec.setText(""+trainings.get(trainingIndex).getLevelPec());
                txtBic.setText(""+trainings.get(trainingIndex).getLevelBic());
                txtAux.setText(""+trainings.get(trainingIndex).getLevelAux());
                txtAux2.setText(""+trainings.get(trainingIndex).getLevelAux2());
                txtLastPotency.setText(getString(R.string.potency_previous)+" "+trainings.get(trainingIndex).getLastPotency()+" %");
                break;
            case MODE_CRONAXIA:
                txtLeg.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_LEG));
                txtGlu.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_GLU));
                txtLum.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_LUM));
                txtDor.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_DOR));
                txtSho.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_SHO));
                txtAbs.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_ABS));
                txtPec.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_PEC));
                txtBic.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_BIC));
                txtAux.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_AUX));
                txtAux2.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_AUX_2));
                break;
        }

        updateCorporalUi(Vest.CHANNEL_LEG, trainings.get(trainingIndex).getLevelLeg());
        updateCorporalUi(Vest.CHANNEL_GLU, trainings.get(trainingIndex).getLevelGlu());
        updateCorporalUi(Vest.CHANNEL_LUM, trainings.get(trainingIndex).getLevelLum());
        updateCorporalUi(Vest.CHANNEL_DOR, trainings.get(trainingIndex).getLevelDor());
        updateCorporalUi(Vest.CHANNEL_SHO, trainings.get(trainingIndex).getLevelSho());
        updateCorporalUi(Vest.CHANNEL_ABS, trainings.get(trainingIndex).getLevelAbs());
        updateCorporalUi(Vest.CHANNEL_PEC, trainings.get(trainingIndex).getLevelPec());
        updateCorporalUi(Vest.CHANNEL_BIC, trainings.get(trainingIndex).getLevelBic());
        updateCorporalUi(Vest.CHANNEL_AUX, trainings.get(trainingIndex).getLevelAux());
        updateCorporalUi(Vest.CHANNEL_AUX_2, trainings.get(trainingIndex).getLevelAux2());
    }

    public void changeModeValues(){
        switch (modeActive){
            case MODE_LEVEL:
                txtLeg.setText(""+trainings.get(trainingIndex).getLevelLeg());
                txtGlu.setText(""+trainings.get(trainingIndex).getLevelGlu());
                txtLum.setText(""+trainings.get(trainingIndex).getLevelLum());
                txtDor.setText(""+trainings.get(trainingIndex).getLevelDor());
                txtSho.setText(""+trainings.get(trainingIndex).getLevelSho());
                txtAbs.setText(""+trainings.get(trainingIndex).getLevelAbs());
                txtPec.setText(""+trainings.get(trainingIndex).getLevelPec());
                txtBic.setText(""+trainings.get(trainingIndex).getLevelBic());
                txtAux.setText(""+trainings.get(trainingIndex).getLevelAux());
                txtAux2.setText(""+trainings.get(trainingIndex).getLevelAux2());
                break;
            case MODE_CRONAXIA:
                txtLeg.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_LEG));
                txtGlu.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_GLU));
                txtLum.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_LUM));
                txtDor.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_DOR));
                txtSho.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_SHO));
                txtAbs.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_ABS));
                txtPec.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_PEC));
                txtBic.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_BIC));
                txtAux.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_AUX));
                txtAux2.setText(""+trainings.get(trainingIndex).getChronaxy(Vest.CHANNEL_AUX_2));
                break;
        }
    }

    public void updateVestIndex(int positionSelected){
        trainingIndex = positionSelected;
        vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(trainings.get(positionSelected));
        potency = trainings.get(trainingIndex).getLevel(0);
        ((ExerciseMultipleActivity)getActivity()).setPositionSelected(positionSelected);
        ((ExerciseMultipleActivity)getActivity()).sendStandardSelected(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        ((ExerciseMultipleActivity)getActivity()).updateValuesDetails(trainings.get(trainingIndex));
        updatePotencyUi(potency);
        updatePauseUi(100);
        updateTimeText(trainings.get(trainingIndex).getTime());
        updateButtonsUi();
        participantsGridAdapter.notifyDataSetChanged();

        updateForceMaxForce(0);
        if (trainings.get(trainingIndex).getDevice().getDeviceBle() != null)
            linearMaxforce.setVisibility(View.VISIBLE);
        else
            linearMaxforce.setVisibility(View.GONE);

    }

    public void callOnclickButtonStopChangeTime(){
        ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, trainings.get(trainingIndex).getUid());
        txtLastPotency.setText(getString(R.string.potency_previous)+" "+trainings.get(trainingIndex).getLastPotency()+" %");
        trainings.get(trainingIndex).setTrainingStatus(Training.TRAINING_STATUS_STOP);
        potency = 0;
        trainings.get(trainingIndex).setLevel(0, potency);
        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        updatePotencyUi(potency);
        ((ExerciseMultipleActivity)getActivity()).setStartNormal(false);
        updateTimeText(trainings.get(trainingIndex).getTime());
        updatePauseUi(0);
        updateEstimulationUi(100);
        updateButtonsUi();
    }

    public int getVestIndex() {
        return vestIndex;
    }

    public Training getTraining() {
        return trainings.get(trainingIndex);
    }

    public void incrementPotency(){
        trainings.get(trainingIndex).setChangeLevel(true);
        if (AutoButtonsUtils.checkPlusPotencyValue(potency) && btnPlusPotency != null)
            btnPlusPotency.setAutoIncrement(false);

        potency = potency +1;
        if(potency > 100)
            potency = 100;

        trainings.get(trainingIndex).setLevel(0, potency);
        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex),trainings.get(trainingIndex).getUid());
        updatePotencyUi(potency);
    }

    public void decrementPotency(){
        trainings.get(trainingIndex).setChangeLevel(true);
        if (AutoButtonsUtils.checkMinusPotencyValue(potency) && btnMinusPotency != null)
            btnMinusPotency.setAutoDecrement(false);

        potency = potency -1;
        if(potency < 0)
            potency = 0;

        trainings.get(trainingIndex).setLevel(0, potency);
        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        updatePotencyUi(potency);
    }

    public void updatePotencyValue(int potencyUpdated){
        potency = potencyUpdated;
        trainings.get(trainingIndex).setLevel(0, potency);
        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
        updatePotencyUi(potency);
    }

    public void incrementCorporal(int plusValue, int channel){
        switch (modeActive){
            case MODE_LEVEL:
                if (AutoButtonsUtils.checkPlusPotencyValue(plusValue) && btnCorporalPlus != null)
                    btnCorporalPlus.setAutoIncrement(false);

                if(plusValue > 100)
                    plusValue = 100;

                trainings.get(trainingIndex).setLevel(channel,plusValue);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChannelsLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                break;
            case MODE_CRONAXIA:
                if(plusValue > 400)
                    plusValue = 400;

                trainings.get(trainingIndex).setChronaxy(channel,plusValue);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChronaxyLevels(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                break;
        }

        //Update channel value
        switch (channel){
            case Vest.CHANNEL_LEG:
                txtLeg.setText(""+plusValue);
                break;
            case Vest.CHANNEL_GLU:
                txtGlu.setText(""+plusValue);
                break;
            case Vest.CHANNEL_LUM:
                txtLum.setText(""+plusValue);
                break;
            case Vest.CHANNEL_SHO:
                txtSho.setText(""+plusValue);
                break;
            case Vest.CHANNEL_BIC:
                txtBic.setText(""+plusValue);
                break;
            case Vest.CHANNEL_DOR:
                txtDor.setText(""+plusValue);
                break;
            case Vest.CHANNEL_ABS:
                txtAbs.setText(""+plusValue);
                break;
            case Vest.CHANNEL_PEC:
                txtPec.setText(""+plusValue);
                break;
            case Vest.CHANNEL_AUX:
                txtAux.setText(""+plusValue);
                break;
            case Vest.CHANNEL_AUX_2:
                txtAux2.setText(""+plusValue);
                break;
        }
    }

    public void decrementCorporal(int minusValue, int channel){
        switch (modeActive){
            case MODE_LEVEL:
                if (AutoButtonsUtils.checkMinusPotencyValue(minusValue) && btnCorporalMinus != null)
                    btnCorporalMinus.setAutoDecrement(false);

                if(minusValue < 0)
                    minusValue = 0;

                trainings.get(trainingIndex).setLevel(channel,minusValue);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChannelsLevel(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                break;
            case MODE_CRONAXIA:
                if(minusValue < 25)
                    minusValue = 25;

                trainings.get(trainingIndex).setChronaxy(channel,minusValue);
                trainings.get(trainingIndex).setChangeLevel(true);
                ((ExerciseMultipleActivity)getActivity()).sendChronaxyLevels(vestIndex, trainings.get(trainingIndex), trainings.get(trainingIndex).getUid());
                break;
        }

        //Update channel value
        switch (channel){
            case Vest.CHANNEL_LEG:
                txtLeg.setText(""+minusValue);
                break;
            case Vest.CHANNEL_GLU:
                txtGlu.setText(""+minusValue);
                break;
            case Vest.CHANNEL_LUM:
                txtLum.setText(""+minusValue);
                break;
            case Vest.CHANNEL_SHO:
                txtSho.setText(""+minusValue);
                break;
            case Vest.CHANNEL_BIC:
                txtBic.setText(""+minusValue);
                break;
            case Vest.CHANNEL_DOR:
                txtDor.setText(""+minusValue);
                break;
            case Vest.CHANNEL_ABS:
                txtAbs.setText(""+minusValue);
                break;
            case Vest.CHANNEL_PEC:
                txtPec.setText(""+minusValue);
                break;
            case Vest.CHANNEL_AUX:
                txtAux.setText(""+minusValue);
                break;
            case Vest.CHANNEL_AUX_2:
                txtAux2.setText(""+minusValue);
                break;
        }
    }

    public void updatePotencyClickDevice(Training training, int potencyDevice){
        trainings.get(trainings.indexOf(training)).setLevel(0, potencyDevice);
        if (trainings.indexOf(training) == trainingIndex){
            updatePotencyUi(potency);
            potency = trainings.get(trainings.indexOf(training)).getLevel(0);
        }
        trainings.get(trainingIndex).setChangeLevel(true);
    }

    public void updateForceMaxForce(int force){
        txtForce.setText(force+" Kg");
    }

    public void loadButtonsPermissions(){
        PreferencesManager pref = PreferencesManager.getInstance(getContext());
        User userCoach = pref.getUser();

        if (!userCoach.getUserPmissions().isChronaxy()) txtChronaxy.setVisibility(View.GONE);
        if (!userCoach.getUserPmissions().isFreq()) txtFrequency.setVisibility(View.GONE);
        if (!userCoach.getUserPmissions().isRelax()) txtRelax.setVisibility(View.GONE);
        if (!userCoach.getUserPmissions().isImpulse()) txtImpulseTime.setVisibility(View.GONE);
        if (!userCoach.getUserPmissions().isRamp()) txtRampType.setVisibility(View.GONE);
        if (!userCoach.getUserPmissions().isSectionGlobal()) imgGlobal.setVisibility(View.GONE);
    }

    public void updateBleStatus(boolean status, String adress) {
        for (Training training: trainings)
            if (training.getDevice().getDeviceBleAdress().equals(adress)){
                for (User user: participants){
                    if (user.getId().equals(training.getDevice().getUserAsigned().getId())) {
                        user.setBleAvailable(status);
                        break;
                    }
                }
            }

        participantsGridAdapter.notifyDataSetChanged();
    }

    public static int getTrainingIndex() {
        return trainingIndex;
    }
}

