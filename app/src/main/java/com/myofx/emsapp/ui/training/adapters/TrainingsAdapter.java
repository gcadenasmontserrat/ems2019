package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.ui.dialogs.DialogTrainingInformation;
import com.myofx.emsapp.ui.training.TrainingsActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by Gerard on 28/12/2016.
 */

public class TrainingsAdapter extends RecyclerView.Adapter<TrainingsAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Training> trainings;
    private Context context;

    public TrainingsAdapter(ArrayList<Training> trainings, Context context, int serverType) {
        this.trainings = trainings;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    @Override
    public void onBindViewHolder(TrainingsAdapter.ViewHolder holder, final int position) {

        TrainingsAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TrainingsActivity)context).goExerciceActivity(trainings.get(position));
            }
        });

        TrainingsAdapter.ViewHolder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                DialogTrainingInformation dialogTrainingInformation = DialogTrainingInformation.newInstance(trainings.get(position));
                dialogTrainingInformation.show(((TrainingsActivity)context).getSupportFragmentManager(), DialogTrainingInformation.class.getSimpleName());
                return false;
            }
        });

        //Load colorTheme
        holder.txtName.setTextColor(ColorTheme.getPrimaryColor(context));
        holder.linearItem.setBackgroundColor(ColorTheme.getLightBackgroundColor(context));
        holder.txtName.setText(trainings.get(position).getTitle());

        if (trainings.get(position).getImage() != null && !TextUtils.isEmpty(trainings.get(position).getImage())){
            if (trainings.get(position).getImage().contains("http")){
                ImageUtils.loadImageFromUrl(holder.imgSection, trainings.get(position).getImage());
            } else {
                ImageUtils.loadImageFromDrawable(holder.imgSection, ImageUtils.getImageIdByName(trainings.get(position).getImage(),context));
            }
        }

    }

    @Override
    public TrainingsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_training, viewGroup, false);
        return new TrainingsAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private RelativeLayout linearItem;
        private TextView txtName;
        private ImageView imgSection;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            linearItem = mView.findViewById(R.id.linearItem);
            txtName = mView.findViewById(R.id.txtName);
            imgSection = mView.findViewById(R.id.imgSection);
        }
    }

}