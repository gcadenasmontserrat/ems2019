package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.ui.local.LocalFragment;

public class PasswordDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnSend;
    private EditText editPassword;

    //Variable
    private PasswordDialog.OnConfirmationNameListener mCallback;
    private LocalFragment localFragment;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = localFragment;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static PasswordDialog newInstance(LocalFragment localFragment) {
        PasswordDialog passwordDialog = new PasswordDialog();
        Bundle args = new Bundle();
        passwordDialog.localFragment = localFragment;
        passwordDialog.setArguments(args);
        return passwordDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_password, null);
        editPassword = view.findViewById(R.id.editPassword);
        btnSend = view.findViewById(R.id.btnSend);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editPassword.getText().toString())){
                    sendResult(editPassword.getText().toString());
                }
            }
        });
    }

    public interface OnConfirmationNameListener {
        void onPutPassword(String password);
    }

    private void sendResult(String password) {
        if(mCallback != null) {
            alertdialog.dismiss();
            mCallback.onPutPassword(password);
        }
    }
}