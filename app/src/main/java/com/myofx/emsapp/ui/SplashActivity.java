package com.myofx.emsapp.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.AppInfoEvent;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.ui.login.LoginActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Gerard on 28/11/2016.
 */

public class SplashActivity extends BaseActivity {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;

    //Ui
    private RelativeLayout relativeBackground;

    //Variables
    private PreferencesManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        pref = PreferencesManager.getInstance(this);

        //GA
        GAUtils.trackScreen(SplashActivity.this, GAConfig.SCREEN_LOGIN + GAConfig.SCREEN_LOGIN_SPLASH, GAConfig.SCREEN_LOGIN);

        bindUi();
        loadTheme();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            requestBlePermission();
        else
            startApp();
    }

    public void goLoginActivity (){
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void goMainActivity (){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public  void bindUi(){
        relativeBackground = findViewById(R.id.relativeBackground);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryColor(SplashActivity.this));
    }

    @Subscribe
    public void onGetLinksEvent(AppInfoEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        pref.saveAppInfo(event.getAppInfo());

        if (pref.getAccessToken() != null)
            EmsApi.getInstance().doUserTask();
        else
            goLoginActivity();
    }

    @Subscribe
    public void onGetMainEvent(UserEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        pref.saveUser(event.getUser());
        pref.saveProfile(event.getUser().getId());

        if (event.getUser().getConfiguration() != null)
            pref.saveColorTheme(ColorTheme.getThemeFromColor(event.getUser().getConfiguration().getColor()));

        goMainActivity();
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        Toast.makeText(SplashActivity.this, event.getMsgError(), Toast.LENGTH_SHORT).show();

        pref.saveColorTheme(R.style.AppThemeBlue);
        PreferencesManager.clear();
        goLoginActivity();
    }

    @Subscribe
    public void onErrorEvent(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        pref.saveColorTheme(R.style.AppThemeBlue);
        PreferencesManager.clear();
        goLoginActivity();
    }

    public void requestBlePermission(){
        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            } else
                startApp();
        } else
            startApp();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults != null && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    startApp();
                else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        showLocationPermissionRationale();
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.relativeBackground), getString(R.string.permission_bar_title), Snackbar.LENGTH_LONG);
                        snackbar.setAction(getString(R.string.permission_bar_button), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        snackbar.show();
                    }
                }
            }
        }
    }

    private void showLocationPermissionRationale() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.permission_denied_title));
        builder.setMessage(getString(R.string.permission_denied_info));
        builder.setPositiveButton(getString(R.string.permission_denied_sure),  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.permission_denied_retry),  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestBlePermission();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void startApp(){
        if (!EmsApp.isModeLocal())
            EmsApi.getInstance().getAppInfo();
        else if (pref.getUser() != null){
            EmsApp.setModeLocal(true);
            goMainActivity();
        } else {
            pref.saveColorTheme(R.style.AppThemeBlue);
            PreferencesManager.clear();
            goLoginActivity();
        }
    }

}
