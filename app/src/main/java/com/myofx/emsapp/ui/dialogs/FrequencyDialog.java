package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.ui.training.TrainingUtils;
import com.myofx.emsapp.ui.training.adapters.ProgramsAdapter;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

import java.util.ArrayList;

/**
 * Created by Gerard on 28/3/2017.
 */

public class FrequencyDialog extends DialogFragment implements ProgramsAdapter.Callback {

    public static final int ACTION_CANCEL = 0;
    public static final int ACTION_LOAD = 1;
    public static final int ACTION_FREQUENCY = 2;

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private Button btnProgram;
    private EditText editFreq;
    private ImageView imgNormalPlus;
    private ImageView imgNormalMinus;
    private RecyclerView gridPrograms;

    //Variable
    private FrequencyDialog.OnConfirmationFrequencyListener mCallback;
    private Activity exerciseActivity;
    private int frequency = 0;
    private ArrayList<Training> trainings = new ArrayList<>();
    private Training oldProgram;
    private Training newProgram;
    private boolean fromGlobal;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        if (exerciseActivity instanceof ExerciseMultipleActivity)
            mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadFrequencyValue();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static FrequencyDialog newInstance(Activity exerciseActivity, int frequency, Training program, boolean fromGlobal) {
        FrequencyDialog frequencyDialog = new FrequencyDialog();
        frequencyDialog.frequency = frequency;
        frequencyDialog.oldProgram = program;
        frequencyDialog.exerciseActivity = exerciseActivity;
        frequencyDialog.fromGlobal = fromGlobal;

        return frequencyDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_frequency, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnSend = view.findViewById(R.id.btnSend);
        editFreq = view.findViewById(R.id.editFreq);
        imgNormalPlus = view.findViewById(R.id.imgNormalPlus);
        imgNormalMinus = view.findViewById(R.id.imgNormalMinus);
        gridPrograms = view.findViewById(R.id.gridPrograms);
        btnProgram = view.findViewById(R.id.btnProgram);

        gridPrograms.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        gridPrograms.setLayoutManager(layoutManager);
        gridPrograms.setNestedScrollingEnabled(false);

        loadPrograms();
        setListeners();

        return view;
    }

    public void loadFrequencyValue(){
        editFreq.setText(""+frequency);
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, Integer.parseInt(editFreq.getText().toString()), ACTION_CANCEL);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editFreq.getText().toString()))
                    editFreq.setText(""+1);

                sendResult(true, Integer.parseInt(editFreq.getText().toString()), ACTION_FREQUENCY);
            }
        });

        btnProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oldProgram.getTrainingStatus() != Training.TRAINING_STATUS_PLAY)
                    sendResult(true, Integer.parseInt(editFreq.getText().toString()), ACTION_LOAD);
                else
                    Toast.makeText(getContext(), getString(R.string.load_program_training_play), Toast.LENGTH_SHORT).show();
            }
        });

        imgNormalPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freq = Integer.parseInt(editFreq.getText().toString());
                freq = freq +1;

                if (freq > 150)
                    freq = 150;
                else if (freq < 1)
                    freq = 1;

                editFreq.setText(""+freq);
            }
        });

        imgNormalMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freq = Integer.parseInt(editFreq.getText().toString());
                freq = freq -1;

                if (freq > 150)
                    freq = 150;
                else if (freq < 1)
                    freq = 1;

                editFreq.setText(""+freq);
            }
        });
    }

    @Override
    public void onProgramClick(Training program) {
        newProgram = program;
        gridPrograms.setAdapter(new ProgramsAdapter(trainings, newProgram, getContext(), FrequencyDialog.this));
    }

    public interface OnConfirmationFrequencyListener {
        void onConfirmationFrequencyProgram(boolean send, int frequency, int action, Training trainingNew ,Training trainingOld, boolean fromGlobal);
    }

    private void sendResult(boolean send, int frequency, int action) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (frequency > 150)
                frequency = 150;

            if (newProgram == null)
                mCallback.onConfirmationFrequencyProgram(send, frequency, action, oldProgram, oldProgram, fromGlobal);
            else {
                mCallback.onConfirmationFrequencyProgram(send, frequency, action, newProgram, oldProgram, fromGlobal);
            }
        }
    }

    private void loadPrograms(){
        trainings.addAll(TrainingUtils.getTrainings(getContext()));
        gridPrograms.setAdapter(new ProgramsAdapter(trainings, oldProgram, getContext(), FrequencyDialog.this));
    }
}
