package com.myofx.emsapp.ui.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.MainActivity;

import java.util.ArrayList;

public class DeviceEmsBleAdapter extends RecyclerView.Adapter<DeviceEmsBleAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<BluetoothDevice> devices;
    private Context context;

    public DeviceEmsBleAdapter(ArrayList<BluetoothDevice> devices, Context context) {
        this.devices = devices;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void onBindViewHolder(DeviceEmsBleAdapter.ViewHolder itemsViewHolder, final int position) {

        DeviceEmsBleAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).setBleEmsDeviceSelected(devices.get(position));
            }
        });

        itemsViewHolder.txtName.setText(devices.get(position).getName());
    }

    @Override
    public DeviceEmsBleAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, viewGroup, false);
        return new DeviceEmsBleAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            txtName = (TextView) mView.findViewById(R.id.txtName);
        }
    }

}