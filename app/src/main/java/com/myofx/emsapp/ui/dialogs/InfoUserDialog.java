package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.training.TrainingParticipantsFragment;

/**
 * Created by Gerard on 25/4/2017.
 */

public class InfoUserDialog extends DialogFragment {

    public static final String EXTRA_SERVER_TYPE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_SERVER_TYPE";
    public static final String EXTRA_USER = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_USER";

    //Ui
    private AlertDialog alertdialog;
    private Button btnClose;
    private Button btnAccept;
    private TextView txtDialog;

    //Variable
    private User user;
    private OnConfirmationUserListener mCallback;
    private Fragment fragment;
    private int serverType = UDPServer.SERVER_NORMAL;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        user = (User) getArguments().getSerializable(EXTRA_USER);
        serverType = (Integer) getArguments().getSerializable(EXTRA_SERVER_TYPE);
        mCallback = (TrainingParticipantsFragment) fragment;

        loadMessage();

        alertdialog = builder.create();
        return alertdialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_info_user, null);
        btnClose = (Button) view.findViewById(R.id.btnClose);
        btnAccept = (Button) view.findViewById(R.id.btnAccept);
        txtDialog = (TextView) view.findViewById(R.id.txtDialog);

        setListeners();

        return view;
    }

    public void setListeners(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialog.dismiss();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendResult(user);
            }
        });
    }

    public static InfoUserDialog newInstance(User user, Fragment fragment, int serverType) {
        InfoUserDialog infoUserDialog = new InfoUserDialog();
        infoUserDialog.fragment = fragment;
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SERVER_TYPE, serverType);
        args.putSerializable(EXTRA_USER, user);
        infoUserDialog.setArguments(args);
        return infoUserDialog;
    }

    public void loadMessage(){
        if (user.isNotify() || user.isBlock()){
            txtDialog.setText(getString(R.string.dialog_user_notify));
            btnClose.setText(getString(R.string.dialog_cancel));
            btnAccept.setText(getString(R.string.dialog_ignore_notify));
        }
    }

    public interface OnConfirmationUserListener {
        void onConfirmationUser (User user);
    }

    private void sendResult(User user) {
        if(mCallback != null) {
            mCallback.onConfirmationUser(user);
            alertdialog.dismiss();
        }
    }

}