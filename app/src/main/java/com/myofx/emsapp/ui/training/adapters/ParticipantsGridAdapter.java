package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleNormalFragment;
import com.myofx.emsapp.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by Gerard on 11/1/2017.
 */

public class ParticipantsGridAdapter extends RecyclerView.Adapter<ParticipantsGridAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<User> participant;
    private Context context;
    private Fragment fragment;
    private int serverType;

    public ParticipantsGridAdapter(ArrayList<User> participant, Context context, Fragment fragment, int serverType) {
        this.participant = participant;
        this.context = context;
        this.fragment = fragment;
        this.serverType = serverType;
    }

    @Override
    public int getItemCount() {
        return participant.size();
    }

    @Override
    public void onBindViewHolder(ParticipantsGridAdapter.ViewHolder holder, final int position) {

        if (participant.get(position).getLastName() != null)
            holder.txtName.setText(participant.get(position).getName()+" "+participant.get(position).getLastName().charAt(0)+".");
        else
            holder.txtName.setText(participant.get(position).getName());

        if (participant.get(position).getImage() != null)
            ImageUtils.loadImageFromUrl(holder.imgUser, BuildConfig.IMAGE_PATH_URL+participant.get(position).getImage());

        if (participant.get(position).isSelected())
            holder.imgSelected.setVisibility(View.VISIBLE);
        else
            holder.imgSelected.setVisibility(View.GONE);

        if (participant.get(position).isBleAvailable())
            holder.imgLed.setImageResource(R.drawable.ic_led_available);
        else
            holder.imgLed.setImageResource(R.drawable.ic_led_assigned);

        ParticipantsGridAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment != null){
                    for (User user: participant){
                        user.setSelected(false);
                    }

                    participant.get(position).setSelected(true);

                    ((ExerciseMultipleNormalFragment)fragment).updateVestIndex(position);
                }
            }
        });
    }

    @Override
    public ParticipantsGridAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_participant_grid, viewGroup, false);
        return new ParticipantsGridAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;
        private ImageView imgUser;
        private ImageView imgSelected;
        private ImageView imgLed;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            txtName = mView.findViewById(R.id.txtName);
            imgUser = mView.findViewById(R.id.imgUser);
            imgSelected = mView.findViewById(R.id.imgSelected);
            imgLed = mView.findViewById(R.id.imgLed);
        }
    }

}