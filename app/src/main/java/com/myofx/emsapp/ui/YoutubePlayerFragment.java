package com.myofx.emsapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.config.Constants;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

/**
 * Created by Gerard on 30/11/2016.
 */

public class YoutubePlayerFragment extends YouTubePlayerSupportFragment {

    public static final String EXTRA_URL_VIDEO = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_URL_VIDEO";

    //Variables
    private YouTubePlayer activePlayer;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        initVideo();

        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public static YoutubePlayerFragment newInstance(String urlVideo) {
        YoutubePlayerFragment playerYouTubeFrag = new YoutubePlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_URL_VIDEO, urlVideo);
        playerYouTubeFrag.setArguments(bundle);
        return playerYouTubeFrag;
    }

    public void initVideo() {

        initialize(Constants.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                Toast.makeText(getActivity(), getString(R.string.error_youtube_video), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                activePlayer = player;
                activePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                if (!wasRestored) {
                    activePlayer.loadVideo(getArguments().getString(EXTRA_URL_VIDEO), 0);
                }
            }
        });
    }
}
