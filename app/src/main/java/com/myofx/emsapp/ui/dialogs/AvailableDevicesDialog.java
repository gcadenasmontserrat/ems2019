package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.bluetooth.BluetoothEmsCommands;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.ui.adapters.AvailableDevicesAdapter;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.PreferencesManagerLocal;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/4/2017.
 */

public class AvailableDevicesDialog extends DialogFragment {

    public static final String EXTRA_SERVER_TYPE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_SERVER_TYPE";
    public static final String EXTRA_LIST_USER = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_LIST_USER";

    //Ui
    private AlertDialog alertdialog;
    private RecyclerView listDevices;
    private Button btnClose;
    private TextView txtDialog;
    private LinearLayout linearLoading;
    private LinearLayout linearStatus;

    //Variable
    private PreferencesManager pref;
    private PreferencesManagerLocal prefLocal;
    private User user;
    private int connectionType = Device.DEVICE_BLE;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);
        pref = PreferencesManager.getInstance(getActivity());

        user = (User) getArguments().getSerializable(EXTRA_LIST_USER);

        //Get connectionType
        connectionType = pref.getUser().getConfiguration().getConnectionType();

        checkAvailable();

        alertdialog = builder.create();
        return alertdialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_available_devices, null);
        listDevices = view.findViewById(R.id.listDevices);
        btnClose = view.findViewById(R.id.btnClose);
        txtDialog = view.findViewById(R.id.txtDialog);
        linearLoading = view.findViewById(R.id.linearLoading);
        linearStatus = view.findViewById(R.id.linearStatus);

        setListeners();

        return view;
    }

    public void setListeners(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialog.dismiss();
            }
        });
    }

    public static AvailableDevicesDialog newInstance(User user, int serverType) {
        AvailableDevicesDialog availableDevicesDialog = new AvailableDevicesDialog();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_LIST_USER, user);
        args.putSerializable(EXTRA_SERVER_TYPE, serverType);
        availableDevicesDialog.setArguments(args);
        return availableDevicesDialog;
    }

    public void loadDevices(){
        txtDialog.setText(getString(R.string.dialog_asign));
        btnClose.setVisibility(View.VISIBLE);
        linearStatus.setVisibility(View.VISIBLE);
        linearLoading.setVisibility(View.GONE);

        pref = PreferencesManager.getInstance(getActivity());
        listDevices.setVisibility(View.VISIBLE);
        listDevices.setLayoutManager(new LinearLayoutManager(getActivity()));
        listDevices.setNestedScrollingEnabled(false);
        listDevices.setAdapter(new AvailableDevicesAdapter(pref.getAllDevices(), getActivity(), user, this));

    }

    public void checkAvailable(){
        txtDialog.setText(getString(R.string.device_check_available));
        ((MainActivity)getContext()).setStartCheckDevice(true);

        btnClose.setVisibility(View.GONE);
        linearLoading.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                checkDevicesShow();
            }
        }, 4000);
    }

    public void updateAssignments(){
        this.dismiss();
        ((MainActivity)getContext()).updateAssignaments();
    }

    public void checkDevicesShow() {
        ArrayList<Device> devicesCheck;
        ArrayList<String> devicesConnected = new ArrayList<>();

        ((MainActivity)getContext()).setStartCheckDevice(false);

        switch (connectionType){
            case Device.DEVICE_WIFI:
                devicesConnected = ((MainActivity) getContext()).getCheckDevicesId();
                break;
            case Device.DEVICE_BLE:
                devicesConnected = BluetoothEmsCommands.getCheckDevicesId();
                break;
        }

        pref = PreferencesManager.getInstance(getActivity());
        devicesCheck = pref.getAllDevices();

        //Recorrer devices a validar i comparar con los registrados. Si tienen ID igual es que esta disponible.
        for (Device deviceCheck : devicesCheck) {
            if(!devicesConnected.isEmpty()){
                for (String deviceUid : devicesConnected){
                    String deviceId = "";
                    switch (connectionType){
                        case Device.DEVICE_WIFI:
                            deviceId = deviceCheck.getUidString();
                            break;
                        case Device.DEVICE_BLE:
                            deviceId = deviceCheck.getDeviceBleAdress();
                            break;
                    }

                    if (deviceId.equals(deviceUid)){
                        deviceCheck.setAvailable(true);
                        break;
                    } else
                        deviceCheck.setAvailable(false);
                }
            } else
                deviceCheck.setAvailable(false);
        }
        pref.saveDevices(devicesCheck);
        loadDevices();
    }
}
