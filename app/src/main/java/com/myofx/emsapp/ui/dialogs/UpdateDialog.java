package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;

public class UpdateDialog extends DialogFragment {

    public static final String EXTRA_PATH = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_PATH";

    //Ui
    private AlertDialog alertdialog;
    private TextView txtLoading;
    private TextView txtInfo;

    //Variables
    private String pathFirmware;
    private PreferencesManager pref;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);
        pref = PreferencesManager.getInstance(getContext());

        pathFirmware = getArguments().getString(EXTRA_PATH);

        alertdialog = builder.create();
        return alertdialog;
    }

    public static UpdateDialog newInstance() {
        UpdateDialog updateDialog = new UpdateDialog();
        Bundle args = new Bundle();
        updateDialog.setArguments(args);
        return updateDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_update, null);
        txtLoading = view.findViewById(R.id.txtLoading);
        txtInfo = view.findViewById(R.id.txtInfo);

        loadTheme();
        return view;
    }

    public void loadStatus(String status){
        txtLoading.setText(status);
    }

    public void loadTheme(){
        txtLoading.setTextColor(ColorTheme.getPrimaryColor(getContext()));
        txtInfo.setTextColor(ColorTheme.getPrimaryColor(getContext()));
    }
}
