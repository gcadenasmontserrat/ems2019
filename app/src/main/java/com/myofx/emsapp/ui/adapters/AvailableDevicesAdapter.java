package com.myofx.emsapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.dialogs.AvailableDevicesDialog;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/4/2017.
 */

public class AvailableDevicesAdapter extends RecyclerView.Adapter<AvailableDevicesAdapter.ViewHolder> {

    //VARIABLE
    private PreferencesManager pref;
    private ArrayList<Device> devices;
    private Context context;
    private User user;
    private AvailableDevicesDialog dialog;

    public AvailableDevicesAdapter(ArrayList<Device> devices, Context context, User user, AvailableDevicesDialog dialog) {
        pref = PreferencesManager.getInstance(context);
        this.devices = devices;
        this.context = context;
        this.user = user;
        this.dialog = dialog;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void onBindViewHolder(AvailableDevicesAdapter.ViewHolder itemsViewHolder, final int position) {
        AvailableDevicesAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                switch (devices.get(position).getDeviceType()){
                    case Device.DEVICE_WIFI:
                        if (devices.get(position).getUserAsigned() != null){
                            Toast.makeText(context, context.getString(R.string.device_asigned), Toast.LENGTH_SHORT).show();
                        } else if (!devices.get(position).isAvailable()){
                            Toast.makeText(context, context.getString(R.string.device_no_available), Toast.LENGTH_SHORT).show();
                        } else {
                            devices.get(position).setUserAsigned(user);
                            pref.saveDevices(devices);
                            dialog.updateAssignments();
                        }
                    break;
                        case Device.DEVICE_BLE:
                            devices.get(position).setUserAsigned(user);
                            pref.saveDevices(devices);
                            dialog.updateAssignments();
                            break;
                }

            }
        });

        itemsViewHolder.txtName.setText(devices.get(position).getNameUser());

        itemsViewHolder.imgStatus.setVisibility(View.VISIBLE);
        if (devices.get(position).getUserAsigned() != null){
            itemsViewHolder.imgStatus.setImageResource(R.drawable.ic_led_assigned);
        } else {
            if (devices.get(position).isAvailable())
                itemsViewHolder.imgStatus.setImageResource(R.drawable.ic_led_available);
            else
                itemsViewHolder.imgStatus.setImageResource(R.drawable.ic_led_not_available);
        }
    }

    @Override
    public AvailableDevicesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, viewGroup, false);
        return new AvailableDevicesAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;
        private ImageView imgStatus;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            txtName = (TextView) mView.findViewById(R.id.txtName);
            imgStatus = (ImageView) mView.findViewById(R.id.imgStatus);
        }
    }

}