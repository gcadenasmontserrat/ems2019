package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.VelocimeterUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VelocimetersMaxForceAdapter extends RecyclerView.Adapter<VelocimetersMaxForceAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Training> trainingUser;
    private boolean chromecast;
    private RecyclerView gridVelocimeters;
    private Context context;

    public VelocimetersMaxForceAdapter(ArrayList<Training> trainingUser, boolean chromecast, RecyclerView gridVelocimeters, Context context) {
        this.trainingUser = trainingUser;
        this.chromecast = chromecast;
        this.gridVelocimeters = gridVelocimeters;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return trainingUser.size();
    }

    @Override
    public void onBindViewHolder(VelocimetersMaxForceAdapter.ViewHolder holder, final int position) {

        if (EmsApp.isTablet(context)){
            ViewGroup.LayoutParams params =  holder.relativeItem.getLayoutParams();
            if (trainingUser.size() == 1){
                params.width = gridVelocimeters.getWidth();
                params.height = gridVelocimeters.getHeight();
            } else if (trainingUser.size() < 4){
                params.width = gridVelocimeters.getWidth()/trainingUser.size();
                params.height = gridVelocimeters.getHeight();
                setFewUsersMargins(holder, trainingUser.size());
            } else if (trainingUser.size() == 4){
                params.width = gridVelocimeters.getWidth()/2;
                params.height = gridVelocimeters.getHeight()/2;
            } else {
                params.width = gridVelocimeters.getWidth()/4;
                params.height = gridVelocimeters.getHeight()/2;
                setFewUsersMargins(holder, trainingUser.size());
            }
            holder.relativeItem.setLayoutParams(params);
        }

        //User image
        if (trainingUser.get(position).getUser().getImage() != null)
            ImageUtils.loadImageFromUrl(holder.imgUser, BuildConfig.IMAGE_PATH_URL+trainingUser.get(position).getUser().getImage());

        //Load values
        holder.txtName.setText(trainingUser.get(position).getUser().getName());
        holder.txtLastName.setText(trainingUser.get(position).getUser().getLastName());

        if (trainingUser.get(position).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
            holder.txtForce.setText("" + trainingUser.get(position).getForceMaxForce());
            updateForceTotal(trainingUser.get(position).getForceTotal(), holder);

            if (trainingUser.get(position).getForceMaxForce() <= 60)
                setVelocimeterImage((VelocimeterUtils.getForcePercentDefault(trainingUser.get(position).getUser(), trainingUser.get(position).getForceMaxForce()) / 5), holder);
            else
                setVelocimeterImage(19,  holder);
        } else {
            holder.txtForce.setText("0");
            setVelocimeterImage(19,  holder);

            trainingUser.get(position).setForceTotal(0);
            if (trainingUser.get(position).getTrainingStatus() == Training.TRAINING_STATUS_STOP)
                updateForceTotal(0, holder);
        }
    }

    public void setVelocimeterImage(int state, ViewHolder holder){
        switch (state){
            case 0:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_0);
                break;
            case 1:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_1);
                break;
            case 2:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_2);
                break;
            case 3:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_3);
                break;
            case 4:
                holder. imgVelocimeter.setImageResource(R.drawable.img_velocimeter_4);
                break;
            case 5:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_5);
                break;
            case 6:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_6);
                break;
            case 7:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_7);
                break;
            case 8:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_8);
                break;
            case 9:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_9);
                break;
            case 10:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_10);
                break;
            case 11:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_11);
                break;
            case 12:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_12);
                break;
            case 13:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_13);
                break;
            case 14:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_14);
                break;
            case 15:
                holder. imgVelocimeter.setImageResource(R.drawable.img_velocimeter_15);
                break;
            case 16:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_16);
                break;
            case 17:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_17);
                break;
            case 18:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_18);
                break;
            case 19:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_19);
                break;

        }
    }
    public void updateForceTotal(int totalForce, ViewHolder holder){

        if (totalForce == 0){
            holder.txtTotal0.setText(""+0);
            holder.txtTotal1.setText(""+0);
            holder.txtTotal2.setText(""+0);
            holder.txtTotal3.setText(""+0);
            holder.txtTotal4.setText(""+0);
        } else {
            List<String> digits = Arrays.asList(Integer.toString(totalForce).split("(?<=.)"));
            Collections.reverse(digits);

            for(int i=0; i<digits.size();i++){
                switch (i){
                    case 0:
                        holder.txtTotal0.setText(digits.get(i));
                        break;
                    case 1:
                        holder.txtTotal1.setText(digits.get(i));
                        break;
                    case 2:
                        holder.txtTotal2.setText(digits.get(i));
                        break;
                    case 3:
                        holder.txtTotal3.setText(digits.get(i));
                        break;
                    case 4:
                        holder.txtTotal4.setText(digits.get(i));
                        break;
                }
            }
        }
    }

    @Override
    public VelocimetersMaxForceAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_velocimeter_maxforce_group_chromecast, viewGroup, false);

        if (trainingUser.size() < 4)
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_velocimeter_maxforce_group_few_users_chromecast, viewGroup, false);

        return new VelocimetersMaxForceAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private ImageView imgUser;
        private TextView txtForce;
        private TextView txtTotal0;
        private TextView txtTotal1;
        private TextView txtTotal2;
        private TextView txtTotal3;
        private TextView txtTotal4;
        private TextView txtName;
        private TextView txtLastName;
        private ImageView imgVelocimeter;
        private LinearLayout relativeItem;
        private LinearLayout linearTotal;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            imgUser = mView.findViewById(R.id.imgUser);
            txtForce = mView.findViewById(R.id.txtForce);
            txtTotal0 = mView.findViewById(R.id.txtTotal0);
            txtTotal1 = mView.findViewById(R.id.txtTotal1);
            txtTotal2 = mView.findViewById(R.id.txtTotal2);
            txtTotal3 = mView.findViewById(R.id.txtTotal3);
            txtTotal4 = mView.findViewById(R.id.txtTotal4);
            txtName = mView.findViewById(R.id.txtName);
            txtLastName = mView.findViewById(R.id.txtLastName);
            imgVelocimeter = mView.findViewById(R.id.imgVelocimeter);
            relativeItem = mView.findViewById(R.id.relativeItem);
            linearTotal = mView.findViewById(R.id.linearTotal);
        }
    }

    // DpToPx
    private int DpToPx(float dp) {
        return (int)(dp * (context.getResources().getDisplayMetrics().densityDpi / 160f));
    }

    public void setFewUsersMargins(ViewHolder holder, int users){
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        switch (users){
            case 2:
                if (!chromecast){
                    textParams.setMargins(0, 0,  DpToPx(80), DpToPx(30));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);

                    linearParams.setMargins(0, 0,  DpToPx(30), 0);
                    holder.linearTotal.setLayoutParams(linearParams);
                } else {
                    textParams.setMargins(0, 0,  DpToPx(60), DpToPx(40));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);

                    linearParams.setMargins(0, 0,  DpToPx(20), 0);
                    holder.linearTotal.setLayoutParams(linearParams);
                }
                break;
            case 3:
                if (!chromecast){
                    textParams.setMargins(0, 0,  DpToPx(65), DpToPx(45));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);

                    linearParams.setMargins(0, 0,  DpToPx(30), 0);
                    holder.linearTotal.setLayoutParams(linearParams);
                } else {
                    textParams.setMargins(0, 0,  DpToPx(45), DpToPx(65));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);

                    linearParams.setMargins(0, 0,  DpToPx(20), 0);
                    holder.linearTotal.setLayoutParams(linearParams);
                }
                break;
            default:
                if (!chromecast){
                    textParams.setMargins(0, 0,  DpToPx(50), DpToPx(20));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);
                    holder.txtForce.setTextSize(45);
                } else {
                    textParams.setMargins(0, 0,  DpToPx(30), DpToPx(10));
                    textParams.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.imgVelocimeter);
                    holder.txtForce.setLayoutParams(textParams);
                    holder.txtForce.setTextSize(35);
                }
                break;
        }
    }
}
