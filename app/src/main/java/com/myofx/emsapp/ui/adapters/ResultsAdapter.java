package com.myofx.emsapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Result;
import com.myofx.emsapp.utils.ColorTheme;

import java.util.ArrayList;

/**
 * Created by Gerard on 30/11/2016.
 */

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Result> results;
    private Context context;

    public ResultsAdapter(ArrayList<Result> results, Context context) {
        this.results = results;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    @Override
    public void onBindViewHolder(ResultsAdapter.ViewHolder holder, final int position) {

        //Load colorTheme
        holder.txtTitle.setTextColor(ColorTheme.getAccentColor(context));
        holder.linearTitle.setBackgroundColor(ColorTheme.getLightBackgroundColor(context));
    }

    @Override
    public ResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_results, viewGroup, false);
        return new ResultsAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private LinearLayout linearTitle;
        private TextView txtTitle;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            linearTitle = (LinearLayout) mView.findViewById(R.id.linearTitle);
            txtTitle = (TextView) mView.findViewById(R.id.txtTitle);
        }
    }

}