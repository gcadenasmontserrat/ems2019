package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 1/6/2017.
 */

public class EndExerciseDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;

    //Variable
    private EndExerciseDialog.OnConfirmationEndListener mCallback;
    private Activity exerciseActivity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static EndExerciseDialog newInstance(Activity exerciseActivityt) {
        EndExerciseDialog endExerciseDialog = new EndExerciseDialog();
        endExerciseDialog.exerciseActivity = exerciseActivityt;
        return endExerciseDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_end_exercise, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnAccept = (Button) view.findViewById(R.id.btnAccept);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true);
            }
        });
    }

    public interface OnConfirmationEndListener {
        void onConfirmationEnd(boolean delete);
    }

    private void sendResult(boolean delete) {
        if(mCallback != null) {
            mCallback.onConfirmationEnd(delete);
            alertdialog.dismiss();
        }
    }
}