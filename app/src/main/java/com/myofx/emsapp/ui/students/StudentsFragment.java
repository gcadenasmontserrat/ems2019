package com.myofx.emsapp.ui.students;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.SearchEvent;
import com.myofx.emsapp.api.events.StudentsEvent;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.students.adapters.StudentsAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Gerard on 27/12/2016.
 */

public class StudentsFragment extends Fragment {

    public static final int RESULT_ADD_USER = 1;

    //UI
    private View rootView;
    private RelativeLayout relativeBackground;
    private LinearLayout linearHeaders;
    private RecyclerView listStudents;
    private ImageView imgSearch;
    private EditText editSearch;
    private RelativeLayout relativeEdit;
    private TextView txtEmpty;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private TextView txtName;
    private ImageView imgUser;
    private RecyclerView listSearch;
    private Button btnAdd;

    //VARIABLE
    private User user;
    private PreferencesManager pref;
    private ArrayList<User> students;
    private ArrayList<User> studentsSearch;
    private StudentsAdapter searchAdapter;
    private StudentsAdapter studentsAdapter;
    private boolean isSearch = false;

    //LoadMore
    private NestedScrollView scroll;
    private boolean isloading = false;
    private int page = 1;
    private boolean loadMore = false;
    private boolean allItemsLoaded = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_students, container, false);
        pref = PreferencesManager.getInstance(getContext());
        user = pref.getUser();

        bindUi();
        setListeners();
        loadTheme();
        loadMode();
        loadStudents();
        loadData();

        return rootView;
    }

    public void bindUi(){
        listStudents = rootView.findViewById(R.id.listStudents);
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        linearHeaders = rootView.findViewById(R.id.linearHeaders);
        imgSearch = rootView.findViewById(R.id.imgSearch);
        editSearch = rootView.findViewById(R.id.editSearch);
        relativeEdit = rootView.findViewById(R.id.relativeEdit);
        txtEmpty = rootView.findViewById(R.id.txtEmpty);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        txtLoading = rootView.findViewById(R.id.txtLoading);
        txtName = rootView.findViewById(R.id.txtName);
        imgUser = rootView.findViewById(R.id.imgUser);
        scroll = rootView.findViewById(R.id.scroll);
        listSearch = rootView.findViewById(R.id.listSearch);
        btnAdd = rootView.findViewById(R.id.btnAdd);

        students = new ArrayList<>();
        listStudents.setLayoutManager(new LinearLayoutManager(getActivity()));
        listStudents.setNestedScrollingEnabled(false);
        setNestedScrollEndless();
        studentsAdapter = new StudentsAdapter(students, getActivity());
        listStudents.setAdapter(studentsAdapter);

        studentsSearch = new ArrayList<>();
        listSearch.setLayoutManager(new LinearLayoutManager(getActivity()));
        listSearch.setNestedScrollingEnabled(false);
        setNestedScrollEndless();
        searchAdapter = new StudentsAdapter(students, getActivity());
        listSearch.setAdapter(searchAdapter);
    }

    public void setListeners(){
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateListSearch(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){

                    if (!TextUtils.isEmpty(editSearch.getText().toString()))
                        searchStudents(editSearch.getText().toString());

                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearch) {
                    isSearch = false;
                    editSearch.setText("");
                    imgSearch.setImageResource(R.drawable.ic_search);
                    listSearch.setVisibility(View.GONE);
                    listStudents.setVisibility(View.VISIBLE);
                }
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddStudentActivity.class);
                startActivityForResult(intent, RESULT_ADD_USER);
            }
        });
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        linearHeaders.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        imgSearch.setColorFilter(ColorTheme.getPrimaryColor(getActivity()));
        relativeEdit.setBackgroundColor(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setHintTextColor(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setTextColor(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        txtEmpty.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(getActivity()));
    }

    public void loadMode (){
        if (EmsApp.isModeLocal())
            btnAdd.setVisibility(View.VISIBLE);
    }

    public void loadStudents(){

        if (EmsApp.isModeLocal()){
            if (user.getProfiles() != null && user.getProfiles().size() <= 0)
                txtEmpty.setVisibility(View.VISIBLE);
            else {
                txtEmpty.setVisibility(View.GONE);
                students.addAll(user.getProfiles());
                studentsAdapter.notifyDataSetChanged();
                allItemsLoaded = true;
            }
        } else {
            txtLoading.setText(getString(R.string.loading_get_students));
            linearLoading.setVisibility(View.VISIBLE);

            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            EmsApi.getInstance().getStudentsTask(page);
        }
    }

    public void searchStudents(String search){
        txtLoading.setText(getString(R.string.loading_get_students));
        linearLoading.setVisibility(View.VISIBLE);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        EmsApi.getInstance().searchStudentsTask(search);
    }

    public void loadData(){
        User user = pref.getUser();
        txtName.setText(user.getName());
        ImageUtils.loadImageFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+user.getImage());
    }

    @Subscribe
    public void onGetSearchEvent(SearchEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);
        listStudents.setVisibility(View.GONE);
        listSearch.setVisibility(View.VISIBLE);
        imgSearch.setImageResource(R.drawable.ic_close);
        isSearch = true;
        isloading = false;

        studentsSearch = new ArrayList<>();
        studentsSearch.addAll(event.getUsers());

        searchAdapter = new StudentsAdapter(studentsSearch, getActivity());
        listSearch.setAdapter(searchAdapter);
    }

    @Subscribe
    public void onGetStudentsEvent(StudentsEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);

        page = page +1;
        if (loadMore)
            loadMore = false;
        else
            students.clear();

        ArrayList<User> usersAdd = event.getUsers();
        for (User user: usersAdd)
            students.add(user);

        studentsAdapter.notifyDataSetChanged();

        if (students.size() >= event.getTotalItems())
            allItemsLoaded = true;

        isloading = false;

        if (students.size() <= 0)
            txtEmpty.setVisibility(View.VISIBLE);
        else
            txtEmpty.setVisibility(View.GONE);
    }

    public void updateListSearch(String search){
        if (students.size() > 0){
            if (!search.equals("")){
                studentsSearch = new ArrayList<>();
                for (User student: students){
                    if ( student.getName()!=null && student.getName().toLowerCase().contains(search.toLowerCase()))
                        studentsSearch.add(student);
                }

                studentsAdapter = new StudentsAdapter(studentsSearch, getActivity());
                listStudents.setAdapter(studentsAdapter);
            } else {
                studentsAdapter = new StudentsAdapter(students, getActivity());
                listStudents.setAdapter(studentsAdapter);
            }
        }
    }

    public void setNestedScrollEndless(){
        scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (!allItemsLoaded) {
                    if (!isloading && scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) && !isSearch) {
                        loadMore = true;
                        isloading = true;
                        loadStudents();
                    }
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_ADD_USER) {
            if (resultCode == Activity.RESULT_OK){
                txtEmpty.setVisibility(View.GONE);
                user = pref.getUser();
                students.clear();
                students.addAll(user.getProfiles());
                studentsAdapter.notifyDataSetChanged();
                allItemsLoaded = true;
            }
        }
    }
}
