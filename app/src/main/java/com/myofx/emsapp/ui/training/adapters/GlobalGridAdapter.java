package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleGlobalFragment;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.VelocimeterUtils;
import com.myofx.emsapp.utils.autoButtons.AutoButtonsUtils;
import com.myofx.emsapp.utils.autoButtons.BtnMinusPotency;
import com.myofx.emsapp.utils.autoButtons.BtnPlusPotency;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Gerard on 20/6/2017.
 */

public class GlobalGridAdapter extends RecyclerView.Adapter<GlobalGridAdapter.ViewHolder> {

    //Variable
    private ArrayList<Training> trainings;
    private Context context;
    private int vestIndex = 0;
    private int potency = 0;
    private Fragment globalFragment;
    private int serverType;

    //Auto buttons
    private BtnPlusPotency btnPlusPotency;
    private BtnMinusPotency btnMinusPotency;
    private Handler btnPlusMinusHandler = new Handler();

    public GlobalGridAdapter(ArrayList<Training> trainings, Context context, Fragment globalFragment, int serverType) {
        this.trainings = trainings;
        this.context = context;
        this.globalFragment = globalFragment;
        this.serverType = serverType;
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    @Override
    public void onBindViewHolder(final GlobalGridAdapter.ViewHolder holder, final int position) {

        //Load name
        if (trainings.get(position).getUser().getLastName() != null)
            holder.txtName.setText(trainings.get(position).getUser().getName() + " " + trainings.get(position).getUser().getLastName().charAt(0) + ".");
        else
            holder.txtName.setText(trainings.get(position).getUser().getName());

        //Load Image
        String imageProfile = "";
        if (trainings.get(position).getUser().getImage() != null)
            imageProfile = trainings.get(position).getUser().getImage();

        ImageUtils.loadImageProfileFromUrl(holder.imgUser, BuildConfig.IMAGE_PATH_URL + imageProfile);

        vestIndex = ((ExerciseMultipleActivity)context).getVestIndexUser(trainings.get(position));

        //Load potency
        holder.txtPotency.setText("" + trainings.get(position).getLevel(0));

        //Show controls
        switch (trainings.get(position).getTrainingStatus()) {
            case Training.TRAINING_STATUS_PLAY:
            case Training.TRAINING_STATUS_RESTART:
                holder.btnPauseUser.setImageResource(R.drawable.btn_pause_global_style);
                holder.btnPauseUser.setVisibility(View.VISIBLE);
                holder.btnPlayUser.setVisibility(View.GONE);
                break;
            case Training.TRAINING_STATUS_PAUSE:
                holder.txtPotency.setText("" + 0);
                holder.btnPauseUser.setVisibility(View.GONE);
                holder.btnPlayUser.setVisibility(View.VISIBLE);
                break;
            case Training.TRAINING_STATUS_STOP:
                holder.btnPlayUser.setVisibility(View.VISIBLE);
                holder.btnPauseUser.setImageResource(R.drawable.btn_pause_global_disabled);
                holder.btnPlayUser.setVisibility(View.GONE);
                break;
        }

        //Update force and velocimeter image
        if (trainings.get(position).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
            //Update force MaxForce
            if (trainings.get(position).getDevice().getDeviceBle() != null)
                holder.txtForce.setText("" + trainings.get(position).getForceMaxForce());

            if (trainings.get(position).getForceMaxForce() <= 60)
                setVelocimeterImage((VelocimeterUtils.getForcePercentDefault(trainings.get(position).getUser(), trainings.get(position).getForceMaxForce()) / 5), holder);
            else
                setVelocimeterImage(19,  holder);
        } else {
            setVelocimeterImage(19,  holder);
            holder.txtForce.setText("0");
        }

        //Load status BLE commands
        if (trainings.get(position).getUser().isBleAvailable())
            holder.imgUser.setBorderColor(context.getResources().getColor(R.color.bluePrimary));
        else
            holder.imgUser.setBorderColor(context.getResources().getColor(R.color.orangeIcons));

        holder.btnPauseUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trainings.get(position).getTrainingStatus() != Training.TRAINING_STATUS_STOP) {

                    //Pause training
                    vestIndex = ((ExerciseMultipleActivity)context).getVestIndexUser(trainings.get(position));
                    ((ExerciseMultipleActivity)context).setPotencyLevelAndStop(vestIndex, trainings.get(position), trainings.get(position).getUid());

                    //Update potency
                    holder.txtPotency.setText(""+0);
                    trainings.get(position).setLevel(0, 0);

                    //Update buttons
                    trainings.get(position).setTrainingStatus(Training.TRAINING_STATUS_PAUSE);
                    holder.btnPauseUser.setVisibility(View.GONE);
                    holder.btnPlayUser.setVisibility(View.VISIBLE);

                    //Update UI control global
                    ((ExerciseMultipleGlobalFragment)globalFragment).updateControlsUi();
                }
            }
        });

        holder.btnPlayUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trainings.get(position).setTime(((ExerciseMultipleActivity)context).getTimeGlobalRemaining());

                boolean allPaused = true;
                for (Training training: trainings) {
                    if (training.getTrainingStatus() == Training.TRAINING_STATUS_PLAY)
                        allPaused = false;
                }

                if (allPaused){
                    trainings.get(position).setTrainingStatus(Training.TRAINING_STATUS_PLAY);

                    switch (trainings.get(position).getDevice().getDeviceType()){
                        case Device.DEVICE_WIFI:
                            vestIndex = ((ExerciseMultipleActivity)context).getVestIndexUser(trainings.get(position));
                            ((ExerciseMultipleActivity)context).sendProgram(vestIndex, trainings.get(position), trainings.get(position).getUid());
                            ((ExerciseMultipleActivity)context).sendStartProgram(vestIndex, trainings.get(position).getUid());
                            break;
                        case Device.DEVICE_BLE:
                            ((ExerciseMultipleActivity)context).sendProgram(vestIndex, trainings.get(position), trainings.get(position).getUid());
                            break;
                    }
                } else {
                    trainings.get(position).setTrainingStatus(Training.TRAINING_STATUS_RESTART);
                }

                //Update buttons
                holder.btnPauseUser.setVisibility(View.VISIBLE);
                holder.btnPauseUser.setImageResource(R.drawable.btn_pause_global_style);
                holder.btnPlayUser.setVisibility(View.GONE);

                //Update UI control global
                ((ExerciseMultipleGlobalFragment)globalFragment).updateControlsUi();
            }
        });

        holder.btnPlusUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incrementPotency(holder,position);
            }
        });

        holder.btnPlusUser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                    if (btnPlusPotency != null)
                        btnPlusPotency.setAutoIncrement(false);
                }
                return false;
            }
        });

        holder.btnPlusUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (potency >= 60) {
                    if (btnPlusPotency != null){
                        btnPlusPotency.setAutoIncrement(false);
                    }
                } else {
                    btnPlusMinusHandler.removeCallbacksAndMessages(null);
                    btnPlusPotency = new BtnPlusPotency(null, BtnPlusPotency.FROM_MULTIPLE_GLOBAL,btnPlusMinusHandler,context,
                            GlobalGridAdapter.this, holder, position, serverType);
                    btnPlusPotency.setAutoIncrement(true);
                    btnPlusMinusHandler.post(btnPlusPotency);
                }
                return false;
            }
        });

        holder.btnMinusUser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if( (event.getAction()== MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL)){
                    if (btnMinusPotency != null)
                        btnMinusPotency.setAutoDecrement(false);
                }
                return false;
            }
        });

        holder.btnMinusUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decrementPotency(holder,position);
            }
        });

        holder.btnMinusUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                btnPlusMinusHandler.removeCallbacksAndMessages(null);
                btnMinusPotency = new BtnMinusPotency(null, BtnPlusPotency.FROM_MULTIPLE_GLOBAL,btnPlusMinusHandler,context,
                        GlobalGridAdapter.this, holder,position,serverType);
                btnMinusPotency.setAutoDecrement(true);
                btnPlusMinusHandler.post(btnMinusPotency);
                return false;
            }
        });

        holder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExerciseMultipleActivity)context).goPageIndividual(position);
            }
        });
    }

    @Override
    public GlobalGridAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_global, viewGroup, false);
        return new GlobalGridAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;
        private TextView txtPotency;
        private CircleImageView imgUser;
        private ImageButton btnPauseUser;
        private ImageButton btnPlayUser;
        private ImageButton btnPlusUser;
        private ImageButton btnMinusUser;
        private TextView txtForce;
        private ImageView imgVelocimeter;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            txtName = mView.findViewById(R.id.txtName);
            txtPotency = mView.findViewById(R.id.txtPotency);
            imgUser = mView.findViewById(R.id.imgUser);
            btnPauseUser = mView.findViewById(R.id.btnPauseUser);
            btnPlayUser = mView.findViewById(R.id.btnPlayUser);
            btnPlusUser = mView.findViewById(R.id.btnPlusUser);
            btnMinusUser = mView.findViewById(R.id.btnMinusUser);
            txtForce = mView.findViewById(R.id.txtForce);
            imgVelocimeter = mView.findViewById(R.id.imgVelocimeter);
        }
    }

    public void incrementPotency(ViewHolder holder, int position){
        if (trainings.get(position).getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
            vestIndex = getVestIndexUser(trainings.get(position));
            potency = trainings.get(position).getLevel(0);

            if (AutoButtonsUtils.checkPlusPotencyValue(potency) && btnPlusPotency != null)
                btnPlusPotency.setAutoIncrement(false);

            potency = potency +1;
            if(potency > 100)
                potency = 100;

            holder.txtPotency.setText(""+potency);
            trainings.get(position).setLevel(0, potency);

            setPotencyLevel(vestIndex, trainings.get(position));
        } else
            Toast.makeText(context, context.getString(R.string.training_not_started), Toast.LENGTH_SHORT).show();

    }

    public void decrementPotency(ViewHolder holder, int position){
        vestIndex = getVestIndexUser(trainings.get(position));
        potency = trainings.get(position).getLevel(0);

        if (AutoButtonsUtils.checkMinusPotencyValue(potency) && btnMinusPotency != null)
            btnMinusPotency.setAutoDecrement(false);

        potency = potency -1;
        if(potency < 0)
            potency = 0;

        holder.txtPotency.setText(""+potency);
        trainings.get(position).setLevel(0, potency);

        setPotencyLevel(vestIndex, trainings.get(position));
    }

    public void setPotencyLevel(int vestIndex, Training training){
        ((ExerciseMultipleActivity)context).setPotencyLevel(vestIndex, training, training.getUid());
    }

    public int getVestIndexUser(Training training){
        vestIndex = ((ExerciseMultipleActivity)context).getVestIndexUser(training);
        return vestIndex;
    }

    public void setVelocimeterImage(int state, ViewHolder holder){
        switch (state){
            case 0:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_0);
                break;
            case 1:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_1);
                break;
            case 2:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_2);
                break;
            case 3:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_3);
                break;
            case 4:
                holder. imgVelocimeter.setImageResource(R.drawable.img_velocimeter_4);
                break;
            case 5:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_5);
                break;
            case 6:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_6);
                break;
            case 7:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_7);
                break;
            case 8:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_8);
                break;
            case 9:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_9);
                break;
            case 10:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_10);
                break;
            case 11:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_11);
                break;
            case 12:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_12);
                break;
            case 13:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_13);
                break;
            case 14:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_14);
                break;
            case 15:
                holder. imgVelocimeter.setImageResource(R.drawable.img_velocimeter_15);
                break;
            case 16:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_16);
                break;
            case 17:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_17);
                break;
            case 18:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_18);
                break;
            case 19:
                holder.imgVelocimeter.setImageResource(R.drawable.img_velocimeter_19);
                break;

        }
    }

}