package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;

/**
 * Created by Gerard on 2/10/2017.
 */

public class InformationDialog extends DialogFragment {

    public static final String EXTRA_MESSAGE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_MESSAGE";

    //Ui
    private AlertDialog alertdialog;
    private Button btnAccept;
    private TextView txtDialog;

    //Variables
    private String message;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        message = getArguments().getString(EXTRA_MESSAGE);
        loadInformation();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static InformationDialog newInstance(String message) {
        InformationDialog informationDialog = new InformationDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, message);
        informationDialog.setArguments(args);
        return informationDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_information, null);
        btnAccept = (Button) view.findViewById(R.id.btnAccept);
        txtDialog = (TextView) view.findViewById(R.id.txtDialog);

        setListeners();
        return view;
    }

    public void loadInformation(){
        txtDialog.setText(message);
    }

    public void setListeners() {
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog.dismiss();
            }
        });
    }
}