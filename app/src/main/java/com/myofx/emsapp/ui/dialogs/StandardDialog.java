package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 29/5/2017.
 */

public class StandardDialog extends DialogFragment {

    public static final int ACTION_CANCEL = 0;
    public static final int ACTION_LOAD = 1;
    public static final int ACTION_SAVE = 2;

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnLoad;
    private Button btnSave;

    //Variable
    private StandardDialog.OnConfirmationStandardeListener mCallback;
    private Activity exerciseActivity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static StandardDialog newInstance(Activity exerciseActivity, Training training) {
        StandardDialog standardDialog = new StandardDialog();
        standardDialog.exerciseActivity = exerciseActivity;
        return standardDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_standard, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnLoad = view.findViewById(R.id.btnLoad);
        btnSave = view.findViewById(R.id.btnSave);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(ACTION_CANCEL);
                dismiss();
            }
        });

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(ACTION_LOAD);
                dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(ACTION_SAVE);
                dismiss();
            }
        });
    }

    public interface OnConfirmationStandardeListener {
        void onConfirmationStandard(int action);
    }

    private void sendResult(int action) {
        if(mCallback != null) {
            mCallback.onConfirmationStandard(action);
        }
    }
}
