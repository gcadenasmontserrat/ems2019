package com.myofx.emsapp.ui.login;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Gerard on 1/12/2016.
 */

public class RecoveryPassActivity extends BaseActivity {

    //UI
    private TextView txtErrorUsername;
    private EditText editUsername;
    private Button btnSend;
    private LinearLayout linearLoading;
    private TextView txtLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_pass);

        bindUi();
        setListeners();
        loadToolbar();
    }

    public void bindUi(){
        editUsername = (EditText) findViewById(R.id.editUsername);
        btnSend = (Button) findViewById(R.id.btnSend);
        txtErrorUsername = (TextView) findViewById(R.id.txtErrorUsername);
        linearLoading = (LinearLayout) findViewById(R.id.linearLoading);
        txtLoading = (TextView) findViewById(R.id.txtLoading);
    }

    public void setListeners() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRecovery();
            }
        });

        editUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorUsername.setVisibility(View.INVISIBLE);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        editUsername.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO)
                    attemptRecovery();
                return false;
            }
        });
    }

    public void loadToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void attemptRecovery(){
        String user = editUsername.getText().toString().trim();

        if (TextUtils.isEmpty(user)) {
            txtErrorUsername.setText(getString(R.string.error_required));
            txtErrorUsername.setVisibility(View.VISIBLE);
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(user).matches()) {
            txtErrorUsername.setText(getString(R.string.error_bad_username));
            txtErrorUsername.setVisibility(View.VISIBLE);
        } else {
            EmsApi.recoveryPasswordTask(user);
            txtLoading.setText(getString(R.string.settings_link_device));
            linearLoading.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onGetRecoveryPassEvent(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        Toast.makeText(RecoveryPassActivity.this, getString(R.string.settings_link_device),
                Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void onErrorEvent(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);
        Toast.makeText(RecoveryPassActivity.this, event.getError().getMessageError(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);
        Toast.makeText(RecoveryPassActivity.this, event.getMsgError(), Toast.LENGTH_SHORT).show();
    }
}
