package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 30/11/2017.
 */

public class PotencyDialog extends DialogFragment {

    public static final String EXTRA_POTENCY = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_POTENCY";
    public static final String EXTRA_FROM = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_FROM";

    public static final int FROM_INDIVIDUAL = 0;
    public static final int FROM_GLOBAL = 1;

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private EditText editPotency;
    private LinearLayout linearValues;
    private ImageView imgPotencyPlus;
    private ImageView imgPotencyMinus;

    //Variable
    private OnConfirmationPotencyListener mCallback;
    private Activity exerciseActivity;
    private int potency = 0;
    private int from = FROM_INDIVIDUAL;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        potency = getArguments().getInt(EXTRA_POTENCY);
        from = getArguments().getInt(EXTRA_FROM);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadFrequencyValue();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static PotencyDialog newInstance(Activity exerciseActivity, int potency, int from) {
        PotencyDialog potencyDialog = new PotencyDialog();
        Bundle args = new Bundle();
        args.putInt(EXTRA_POTENCY, potency);
        args.putInt(EXTRA_FROM, from);
        potencyDialog.exerciseActivity = exerciseActivity;
        potencyDialog.setArguments(args);
        return potencyDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_potency, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        editPotency = (EditText) view.findViewById(R.id.editPotency);
        imgPotencyPlus = (ImageView) view.findViewById(R.id.imgPotencyPlus);
        imgPotencyMinus = (ImageView) view.findViewById(R.id.imgPotencyMinus);
        linearValues = (LinearLayout) view.findViewById(R.id.linearValues);

        setListeners();

        return view;
    }

    public void loadFrequencyValue(){
        editPotency.setText(""+potency);
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, Integer.parseInt(editPotency.getText().toString()));
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true, Integer.parseInt(editPotency.getText().toString()));
            }
        });

        linearValues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPotency.requestFocus();
            }
        });

        imgPotencyPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int potency = Integer.parseInt(editPotency.getText().toString());
                potency = potency +1;
                editPotency.setText(""+potency) ;
            }
        });

        imgPotencyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int potency = Integer.parseInt(editPotency.getText().toString());
                potency = potency -1;

                if (potency < 0)
                    potency = 0;
                if (potency > 100)
                    potency = 100;

                editPotency.setText(""+potency) ;
            }
        });
    }

    public interface OnConfirmationPotencyListener {
        void onConfirmationPotency(boolean send, int time, int from);
    }

    private void sendResult(boolean send, int potency) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (potency < 0)
                potency = 0;
            if (potency > 100)
                potency = 100;

            mCallback.onConfirmationPotency(send, potency, from);
        }
    }
}
