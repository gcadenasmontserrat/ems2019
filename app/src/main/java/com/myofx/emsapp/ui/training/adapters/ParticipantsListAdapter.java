package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.training.TrainingParticipantsFragment;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;

/**
 * Created by Gerard on 28/12/2016.
 */

public class ParticipantsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_USER = 0;
    private static final int TYPE_LOADING = 1;

    //VARIABLE
    private PreferencesManager pref;
    private ArrayList<Device> devices;
    private ArrayList<User> students;
    private Context context;
    private TrainingParticipantsFragment fragment;

    public ParticipantsListAdapter(ArrayList<User> students, Context context, TrainingParticipantsFragment fragment) {
        pref = PreferencesManager.getInstance(context);
        devices = pref.getAllDevices();
        this.students = students;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).txtStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ViewHolder) holder).txtStatus.getText().toString().equalsIgnoreCase(context.getString(R.string.training_status_selected))){
                        for (Device device: devices){
                            if (device.getUserAsigned() != null && students.get(position).getId().equals(device.getUserAsigned().getId())){
                                device.setUserAsigned(null);
                                device.setDeviceBle(null);
                                pref.saveDevices(devices);
                                fragment.removeSelected(students.get(position));
                            }
                        }
                    } else {
                        fragment.showDevices(students.get(position));
                    }
                }
            });

            for (Device device: devices){
                if (device.getUserAsigned() != null && students.get(position).getId().equals(device.getUserAsigned().getId())){
                    ((ViewHolder) holder).txtEms.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).txtEms.setText(device.getNameUser());
                    ((ViewHolder) holder).txtStatus.setText(context.getString(R.string.training_status_selected));

                    //MaxForce Device
                    if (device.getDeviceBle() != null)
                        ((ViewHolder) holder).txtMaxForce.setText(device.getDeviceBle().getAddress());
                    else
                        ((ViewHolder) holder).txtMaxForce.setText(context.getString(R.string.training_maxforce));
                }
            }

            if (students.get(position).getLastName() != null && !students.get(position).getLastName().equals(""))
                ((ViewHolder) holder).txtName.setText(students.get(position).getName()+" "+students.get(position).getLastName());
            else
                ((ViewHolder) holder).txtName.setText(students.get(position).getName());

            if (students.get(position).getImage() != null)
                ImageUtils.loadImageFromUrl(((ViewHolder) holder).imgUser, BuildConfig.IMAGE_PATH_URL+students.get(position).getImage());
        } else {
            ((ViewLoading) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_progress, viewGroup, false);
            return new ViewLoading(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_participant_list, viewGroup, false);
            return new ViewHolder(view);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtStatus;
        private TextView txtEms;
        private TextView txtName;
        private TextView txtMaxForce;
        private ImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            txtStatus = (TextView) mView.findViewById(R.id.txtStatus);
            txtEms = (TextView) mView.findViewById(R.id.txtEms);
            txtName = (TextView) mView.findViewById(R.id.txtName);
            imgUser = (ImageView) mView.findViewById(R.id.imgUser);
            txtMaxForce = (TextView) mView.findViewById(R.id.txtMaxForce);
        }
    }

    public static class ViewLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewLoading(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (students.get(position) instanceof User) {
            return TYPE_USER;
        } else {
            return TYPE_LOADING;
        }
    }
}