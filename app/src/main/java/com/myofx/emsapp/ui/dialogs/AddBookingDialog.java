package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.R;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CalendarDay;
import com.myofx.emsapp.models.CenterService;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.diary.DiaryUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.UUID;

public class AddBookingDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;
    private TextView txtDay;
    private RelativeLayout relativeService;
    private RelativeLayout relativeUser;
    private TextView txtService;
    private TextView txtUser;

    //Variable
    private OnConfirmationBookCallback mCallback;
    private PreferencesManager pref;
    private CalendarDay booking;
    private String day;
    private ArrayList<String> servicesName = new ArrayList<>();
    private int serviceSelected = 0;
    private ArrayList<CenterService> centerServices = new ArrayList<>();

    private ArrayList<String> usersName = new ArrayList<>();
    private int userSelected = 0;
    private ArrayList<User> users = new ArrayList<>();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);
        pref = PreferencesManager.getInstance(getContext());

        loadBooking();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static AddBookingDialog newInstance(CalendarDay booking, String day, OnConfirmationBookCallback mCallback) {
        AddBookingDialog addBookingDialog = new AddBookingDialog();
        addBookingDialog.booking = booking;
        addBookingDialog.day = day;
        addBookingDialog.mCallback = mCallback;
        return addBookingDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_booking, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnAccept = view.findViewById(R.id.btnAccept);
        txtDay = view.findViewById(R.id.txtDay);
        relativeService = view.findViewById(R.id.relativeService);
        relativeUser = view.findViewById(R.id.relativeUser);
        txtService = view.findViewById(R.id.txtService);
        txtUser = view.findViewById(R.id.txtUser);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog.dismiss();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCallback != null) {
                    if (txtService.getText().toString().equals("-"))
                        Toast.makeText(getContext(), getContext().getString(R.string.error_booking_no_service), Toast.LENGTH_SHORT).show();
                    else if (txtUser.getText().toString().equals("-"))
                        Toast.makeText(getContext(), getContext().getString(R.string.error_booking_no_user), Toast.LENGTH_SHORT).show();
                    else {
                        alertdialog.dismiss();
                        attemptCreateBooking();
                        mCallback.onConfirmationBook();
                    }
                }
            }
        });

        relativeService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showServicesSelector();
            }
        });

        relativeUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUserSelector();
            }
        });
    }

    private void loadBooking(){
        txtDay.setText(DiaryUtils.getDayAndHourDate(day));

        centerServices.addAll(pref.getUser().getCenterServices());
        for (CenterService centerService : centerServices) {
            servicesName.add(centerService.getName());
        }

        users.addAll(pref.getUser().getProfiles());
        for (User user : users) {
            usersName.add(user.getName());
        }
    }

    private void showServicesSelector(){
        txtService.setText(servicesName.get(serviceSelected));

        final CharSequence[] names = servicesName.toArray(new CharSequence[servicesName.size()]);
        AlertDialog.Builder dialogSelector = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        dialogSelector.setSingleChoiceItems(names, serviceSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int position) {
                serviceSelected = position;
                txtService.setText(names[position]);
                d.dismiss();
            }
        });
        dialogSelector.setTitle(getString(R.string.diary_service));
        dialogSelector.show();
    }

    private void showUserSelector(){
        txtUser.setText(usersName.get(userSelected));

        final CharSequence[] names = usersName.toArray(new CharSequence[usersName.size()]);
        AlertDialog.Builder dialogSelector = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        dialogSelector.setSingleChoiceItems(names, userSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int position) {
                userSelected = position;
                txtUser.setText(names[position]);
                d.dismiss();
            }
        });
        dialogSelector.setTitle(getString(R.string.diary_service));
        dialogSelector.show();
    }

    private void attemptCreateBooking(){
        User user = pref.getUser();

        Booking bookingAdd = new Booking(UUID.randomUUID().toString(), day,centerServices.get(serviceSelected).getId() ,user.getId(), users.get(userSelected).getId());
        AppDatabase.addBooking(getContext(), bookingAdd);
        AppDatabase.addLocalAction(getContext(), new LocalAction(user.getId(), bookingAdd.getId(), getString(R.string.la_save_booking)+" "+DiaryUtils.getDayAndHourDate(day) ,false, LocalAction.ACTION_TYPE_BOOKING));
    }

    public interface OnConfirmationBookCallback {
        void onConfirmationBook();
    }
}
