package com.myofx.emsapp.ui.diary;

import android.util.Log;

import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CalendarDay;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DiaryUtils {

    public static ArrayList<CalendarDay> getDefaultDay(int addDays, List<Booking> bookings){

        //Current Date
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 30);
        Date currentDate = addDaysToDateName(cal.getTime(), addDays);

        //Add header day
        ArrayList<CalendarDay> bookingDay = new ArrayList<>();
        bookingDay.add(new CalendarDay(changeDateFormatDiary(currentDate), true));

        //Add 25 hours from 8:30 to 20:30
        for (int hour=0; hour<25; hour++)
            bookingDay.add(new CalendarDay(addHoursToDate(currentDate, hour)));

        //Add bookings to Date
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        for (Booking book : bookings){
            try {
                Date dateBooking = format.parse(book.getDate());
                //Booking have same day than the day
                if (sameDay(dateBooking, currentDate)){
                    for (CalendarDay calendarDay : bookingDay){
                        if (!calendarDay.isTypeDay() && sameHour(dateBooking, calendarDay.getDate())){
                            calendarDay.setBooking(book);
                            break;
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return bookingDay;
    }

    public static Date addDaysToDateName (Date currentDate, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, days);
        currentDate = c.getTime();

        return currentDate;
    }

    public static String changeDateFormatDiary(Date currentDate){
        SimpleDateFormat df = new SimpleDateFormat("EEEE,dd MMM");
        String formattedDate = df.format(currentDate);

        return formattedDate;
    }

    public static String changeDateFormat(Date currentDate){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDate = df.format(currentDate);

        return formattedDate;
    }

    public static String getHourFromDate(String date){
        try {
            return new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "-";
    }

    public static String getDayAndHourDate(String date){
        try {
            return new SimpleDateFormat("dd MMMM, hh:mm").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "-";
    }

    public static String addHoursToDate (Date currentDate, int hour) {
        final long ONE_MINUTE_IN_MILLIS = 60000;

        long curTimeInMs = currentDate.getTime();
        Date afterAddingMins = new Date(curTimeInMs + ((30*hour) * ONE_MINUTE_IN_MILLIS));
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(afterAddingMins);
    }

    public static boolean sameDay(Date dateToday, Date dateBooking){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(dateToday);
        cal2.setTime(dateBooking);
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }

    public static boolean sameHour(Date dateToday, String dateCalendarDay){
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dateCalendarDay);
            String calendarDate = new SimpleDateFormat("HH:mm").format(date);
            String todayDate = new SimpleDateFormat("HH:mm").format(dateToday);

            if (calendarDate.equals(todayDate))
                return true;
            else
                return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
