package com.myofx.emsapp.ui.training;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.R;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.UUID;

public class AddTrainingActivity extends BaseActivity {

    //Ui
    private RelativeLayout relativeBackground;
    private RelativeLayout relativeTitle;
    private TextView txtTitleTraining;
    private EditText editName;
    private EditText editDuration;
    private EditText editEsimulationTime;
    private EditText editPauseTime;
    private EditText editRampUp;
    private EditText editRampDown;
    private EditText editFreq;
    private EditText editFreqRelax;
    private EditText editPotencyRelax;
    private EditText editPulseRelax;
    private EditText editChronaxy;

    private Button btnCreate;

    //Variables
    private PreferencesManager pref;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_training);
        pref = PreferencesManager.getInstance(this);
        user = pref.getUser();

        bindUi();
        loadTheme();
        loadToolbar();
        setListeners();
    }

    public void bindUi(){
        relativeBackground = findViewById(R.id.relativeBackground);
        editName = findViewById(R.id.editName);
        editDuration = findViewById(R.id.editDuration);
        editEsimulationTime = findViewById(R.id.editEsimulationTime);
        editPauseTime = findViewById(R.id.editPauseTime);
        editRampUp = findViewById(R.id.editRampUp);
        editRampDown = findViewById(R.id.editRampDown);
        editFreq = findViewById(R.id.editFreq);
        editFreqRelax = findViewById(R.id.editFreqRelax);
        editPotencyRelax = findViewById(R.id.editPotencyRelax);
        editPulseRelax = findViewById(R.id.editPulseRelax);
        btnCreate = findViewById(R.id.btnCreate);
        relativeTitle = findViewById(R.id.relativeTitle);
        txtTitleTraining = findViewById(R.id.txtTitleTraining);
        editChronaxy = findViewById(R.id.editChronaxy);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(AddTrainingActivity.this));
        relativeTitle.setBackgroundColor(ColorTheme.getLightBackgroundColor(AddTrainingActivity.this));
        txtTitleTraining.setTextColor(ColorTheme.getPrimaryColor(AddTrainingActivity.this));
    }

    public void loadToolbar(){
        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(AddTrainingActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(AddTrainingActivity.this));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(MainActivity.RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(MainActivity.RESULT_CANCELED);
        finish();
        super.onBackPressed();
    }

    public void setListeners(){
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptCreateTraining();
            }
        });
    }

    public void attemptCreateTraining(){
        if (TextUtils.isEmpty(editName.getText().toString().trim()) || TextUtils.isEmpty(editDuration.getText().toString().trim()) || TextUtils.isEmpty(editEsimulationTime.getText().toString().trim())
                || TextUtils.isEmpty(editPauseTime.getText().toString().trim()) || TextUtils.isEmpty(editRampUp.getText().toString().trim()) || TextUtils.isEmpty(editRampDown.getText().toString().trim())
                || TextUtils.isEmpty(editFreq.getText().toString().trim()) || TextUtils.isEmpty(editFreqRelax.getText().toString().trim()) || TextUtils.isEmpty(editPotencyRelax.getText().toString().trim())
                || TextUtils.isEmpty(editPulseRelax.getText().toString().trim())  || TextUtils.isEmpty(editChronaxy.getText().toString().trim())) {
            Toast.makeText(AddTrainingActivity.this, getString(R.string.training_required_value), Toast.LENGTH_LONG).show();
        //Estimulation time
        } else if (Integer.parseInt(editEsimulationTime.getText().toString()) > 60 || Integer.parseInt(editPauseTime.getText().toString()) > 60) {
            Toast.makeText(AddTrainingActivity.this, getString(R.string.training_estim_error), Toast.LENGTH_SHORT).show();
        //Ramp time
        } else if (Integer.parseInt(editRampUp.getText().toString()) > 10 || Integer.parseInt(editRampDown.getText().toString()) > 10) {
            Toast.makeText(AddTrainingActivity.this, getString(R.string.training_ramp_error), Toast.LENGTH_SHORT).show();
        //Frequency
        } else if (Integer.parseInt(editFreq.getText().toString()) < 1 || Integer.parseInt(editFreq.getText().toString()) > 150 ||
                Integer.parseInt(editFreqRelax.getText().toString()) < 1 || Integer.parseInt(editFreqRelax.getText().toString()) > 150) {
                Toast.makeText(AddTrainingActivity.this, getString(R.string.training_freq_error), Toast.LENGTH_SHORT).show();
        //Chronaxia and Chronaxia relax
        } else if (Integer.parseInt(editPulseRelax.getText().toString()) < 25 || Integer.parseInt(editPulseRelax.getText().toString()) > 400 ||
                Integer.parseInt(editChronaxy.getText().toString()) < 25 || Integer.parseInt(editChronaxy.getText().toString()) > 400) {
            Toast.makeText(AddTrainingActivity.this, getString(R.string.training_cronaxy_error), Toast.LENGTH_SHORT).show();
        } else {
            Training training = new Training(UUID.randomUUID().toString(), editName.getText().toString(),
                    new int[]{1,
                            Integer.parseInt(editFreq.getText().toString()),
                            Integer.parseInt(editEsimulationTime.getText().toString())*1000,
                            Integer.parseInt(editPauseTime.getText().toString())*1000,
                            Integer.parseInt(editChronaxy.getText().toString()),
                            Integer.parseInt(editRampUp.getText().toString())*1000,
                            Integer.parseInt(editRampDown.getText().toString())*1000,
                            Integer.parseInt(editDuration.getText().toString()),
                            Integer.parseInt(editPotencyRelax.getText().toString()),
                            Integer.parseInt(editFreqRelax.getText().toString()),
                            Integer.parseInt(editPulseRelax.getText().toString())},
                    "");
            training.setCenter(user.getCenter());

            //Save training to DB
            if (isNewIdName(training)) {
                AppDatabase.addTraining(this, training);
                AppDatabase.addLocalAction(this, new LocalAction(user.getCenter(), training.getIdTraining(), getString(R.string.la_new_training)+" "+training.getTitle() ,false, LocalAction.ACTION_TYPE_ADD_TRAINING));

                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
            } else
                Toast.makeText(AddTrainingActivity.this, getString(R.string.training_name_exist), Toast.LENGTH_LONG).show();

            Toast.makeText(AddTrainingActivity.this, R.string.training_created, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Check if exist a training with same IdName
     */
    public boolean isNewIdName(Training training){
        ArrayList<Training> trainings =  new ArrayList<>();
        trainings.addAll(AppDatabase.getTrainings(AddTrainingActivity.this, user.getCenter()));

        for (Training trainingCheck: trainings)
            if (trainingCheck.getIdTraining().equals(training.getIdTraining()))
                return false;

        return true;
    }
}
