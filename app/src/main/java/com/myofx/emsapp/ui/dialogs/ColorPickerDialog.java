package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.models.Color;
import com.myofx.emsapp.ui.settings.SettingsFragment;
import com.myofx.emsapp.ui.adapters.ColorsAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.PreferencesManagerLocal;
import com.myofx.emsapp.utils.SpacesItemDecoration;

import java.util.ArrayList;

/**
 * Created by Gerard on 21/12/2016.
 */

public class ColorPickerDialog extends DialogFragment {

    public static final String EXTRA_SERVER_TYPE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_SERVER_TYPE";

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;
    private RecyclerView gridColors;

    //Variable
    private OnConfirmationColorListener mCallback;
    private Fragment settingsFragment;
    private ArrayList<Color> colors;
    private PreferencesManager pref;
    private PreferencesManagerLocal prefLocal;
    private int serverType;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        pref = PreferencesManager.getInstance(getActivity());
        prefLocal = PreferencesManagerLocal.getInstance(getActivity());

        serverType = (Integer) getArguments().getSerializable(EXTRA_SERVER_TYPE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = (SettingsFragment) settingsFragment;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static ColorPickerDialog newInstance(Fragment settingsFragment, int serverType) {
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
        colorPickerDialog.settingsFragment = settingsFragment;
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SERVER_TYPE, serverType);
        colorPickerDialog.setArguments(args);
        return colorPickerDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_color_picker, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnAccept = view.findViewById(R.id.btnAccept);
        gridColors = view.findViewById(R.id.gridColors);

        setListeners();

        loadColors();

        return view;
    }

    public void loadColors(){
        gridColors.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 6);
        gridColors.setLayoutManager(layoutManager);
        gridColors.setNestedScrollingEnabled(false);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.half_margin);
        gridColors.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        colors = new ArrayList<>();
        colors = ColorTheme.getColors();

        for (Color color: colors){
            if (color.getColorTheme() == pref.getColorTheme())
                color.setSelected(true);
        }
        gridColors.setAdapter(new ColorsAdapter(colors, getActivity()));
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Color color: colors)
                    if (color.isSelected()){
                        pref.saveColorTheme(color.getColorTheme());
                    }
                sendResult(true);
            }
        });
    }

    public interface OnConfirmationColorListener {
        void onConfirmationColor(boolean delete);
    }

    private void sendResult(boolean updateColor) {
        if(mCallback != null) {
            alertdialog.dismiss();
            mCallback.onConfirmationColor(updateColor);
        }
    }
}
