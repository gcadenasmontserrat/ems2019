package com.myofx.emsapp.ui.settings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.ui.MainActivity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Gerard on 12/1/2017.
 */

public class DevicesConectedAdapter extends RecyclerView.Adapter<DevicesConectedAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Device> devices;
    private Context context;

    public DevicesConectedAdapter(ArrayList<Device> devices, Context context) {
        this.devices = devices;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void onBindViewHolder(final DevicesConectedAdapter.ViewHolder holder, final int position) {

        switch (devices.get(position).getDeviceType()){
            case Device.DEVICE_WIFI:
                try {
                    if (devices.get(position).isNewDevice())
                        holder.txtName.setText(devices.get(position).getNameUser() +" - "+new String(devices.get(position).getUIDNewByte(), "UTF-8"));
                    else {
                        String UUID =  Arrays.toString(devices.get(position).getUIDOldByte());
                        UUID = UUID.replace("[","");
                        UUID =  UUID.replace("]","");
                        UUID =  UUID.replace(", ","");
                        holder.txtName.setText(devices.get(position).getNameUser() +" - "+UUID);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            case Device.DEVICE_BLE:
                holder.txtUpdate.setVisibility(View.VISIBLE);
                holder.txtName.setText(devices.get(position).getNameUser()+" - "+devices.get(position).getName());
                break;
        }

        //Status
        holder.imgStatus.setVisibility(View.VISIBLE);
        if (devices.get(position).isAvailable())
            holder.imgStatus.setImageResource(R.drawable.ic_led_available);
        else
            holder.imgStatus.setImageResource(R.drawable.ic_led_not_available);


        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)context).deleteDevice(devices.get(position));
            }
        });

        holder.txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)context).updateBLEDevice(devices.get(position));
            }
        });
    }

    @Override
    public DevicesConectedAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_conected, viewGroup, false);
        return new DevicesConectedAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;
        private TextView txtDelete;
        private TextView txtUpdate;
        private ImageView imgStatus;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            txtName = mView.findViewById(R.id.txtName);
            txtDelete = mView.findViewById(R.id.txtDelete);
            imgStatus = mView.findViewById(R.id.imgStatus);
            txtUpdate = mView.findViewById(R.id.txtUpdate);
        }
    }
}