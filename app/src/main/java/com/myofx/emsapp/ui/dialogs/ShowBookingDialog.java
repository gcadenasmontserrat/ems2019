package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.CalendarDay;

public class ShowBookingDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;
    private TextView txtDay;
    private TextView txtService;

    //Variable
    private CalendarDay booking;
    private String day;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        loadBooking();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static ShowBookingDialog newInstance(CalendarDay booking, String day) {
        ShowBookingDialog showBookingDialog = new ShowBookingDialog();
        showBookingDialog.booking = booking;
        showBookingDialog.day = day;
        return showBookingDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_show_booking, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnAccept = view.findViewById(R.id.btnAccept);
        txtDay = view.findViewById(R.id.txtDay);
        txtService = view.findViewById(R.id.txtService);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog.dismiss();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog.dismiss();
            }
        });
    }

    private void loadBooking(){
        txtDay.setText(day);
    }
}