package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.training.TrainingParticipantsFragment;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;

/**
 * Created by Gerard on 3/7/2017.
 */

public class SelectedListAdapter extends RecyclerView.Adapter<SelectedListAdapter.ViewHolder> {

    //VARIABLE
    private PreferencesManager pref;
    private ArrayList<Device> devices;
    private ArrayList<User> students;
    private Context context;
    private TrainingParticipantsFragment fragment;

    public SelectedListAdapter(ArrayList<User> students, Context context, TrainingParticipantsFragment fragment) {
        pref = PreferencesManager.getInstance(context);
        devices = pref.getAllDevices();
        this.students = students;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    @Override
    public void onBindViewHolder(final SelectedListAdapter.ViewHolder holder, final int position) {
        holder.txtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.txtStatus.getText().toString().equalsIgnoreCase(context.getString(R.string.training_status_selected))){
                    for (Device device: devices){
                        if (device.getUserAsigned() != null && students.get(position).getId().equals(device.getUserAsigned().getId())){
                            device.setUserAsigned(null);
                            device.setDeviceBle(null);
                            pref.saveDevices(devices);
                            removeSelected(students.get(position));
                            break;
                        }
                    }
                    fragment.updateListRemove(students);
                } else {
                    fragment.showDevices(students.get(position));
                }
            }
        });

        holder.txtMaxForce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Device device: devices){
                    if (device.getUserAsigned() != null && students.get(position).getId().equals(device.getUserAsigned().getId())){
                        fragment.scanDevice(device);
                        break;
                    }
                }
            }
        });

        for (Device device: devices){
            if (device.getUserAsigned() != null && students.get(position).getId().equals(device.getUserAsigned().getId())){
                holder.txtEms.setVisibility(View.VISIBLE);
                holder.txtEms.setText(device.getNameUser());
                holder.txtStatus.setText(context.getString(R.string.training_status_selected));

                //MaxForce Device
                if (device.getDeviceBle() != null){
                    holder.txtMaxForce.setText(device.getDeviceBle().getAddress());
                } else
                    holder.txtMaxForce.setText(context.getString(R.string.training_add_maxforce));
            }
        }

        if (students.get(position).getLastName() != null && !students.get(position).getLastName().equals(""))
            holder.txtName.setText(students.get(position).getName()+" "+students.get(position).getLastName());
        else
            holder.txtName.setText(students.get(position).getName());

        if (students.get(position).getImage() != null)
            ImageUtils.loadImageFromUrl(holder.imgUser, BuildConfig.IMAGE_PATH_URL+students.get(position).getImage());

        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            holder.txtMaxForce.setVisibility(View.GONE);
        }
    }

    @Override
    public SelectedListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_participant_list, viewGroup, false);
        return new SelectedListAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtStatus;
        private TextView txtEms;
        private TextView txtName;
        private TextView txtMaxForce;
        private ImageView imgUser;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            txtStatus = (TextView) mView.findViewById(R.id.txtStatus);
            txtEms = (TextView) mView.findViewById(R.id.txtEms);
            txtName = (TextView) mView.findViewById(R.id.txtName);
            imgUser = (ImageView) mView.findViewById(R.id.imgUser);
            txtMaxForce = (TextView) mView.findViewById(R.id.txtMaxForce);
        }
    }

    public void removeSelected(User user) {
        if (students != null) {
            devices = pref.getAllDevices();
            int index = students.indexOf(user);
            students.remove(user);
            notifyItemRemoved(index);
        }
    }
}