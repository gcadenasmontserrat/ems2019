package com.myofx.emsapp.ui;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.utils.ColorTheme;

/**
 * Created by Gerard on 29/11/2016.
 */

public class GeneralWebViewActivity extends BaseActivity {

    public static final String EXTRA_TITLE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_TITLE";
    public static final String EXTRA_URL = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_URL";

    //UI
    private WebView wvConditions;
    private TextView txtTitle;
    private RelativeLayout relativeBackground;

    //Variables
    private String title;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conditions);

        if (getIntent().hasExtra(EXTRA_TITLE)) {
            title = getIntent().getStringExtra(EXTRA_TITLE);
        }

        if (getIntent().hasExtra(EXTRA_URL)) {
            url = getIntent().getStringExtra(EXTRA_URL);
        }

        bindUi();
        loadTheme();
        loadToolbar();
        loadInformation();
    }

    public void bindUi(){
        relativeBackground = (RelativeLayout) findViewById(R.id.relativeBackground);
        wvConditions = (WebView) findViewById(R.id.wvConditions);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(GeneralWebViewActivity.this));
    }

    public void loadToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(GeneralWebViewActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(GeneralWebViewActivity.this));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadInformation(){
        wvConditions.setWebViewClient(new WebViewClient());
        wvConditions.getSettings().setBuiltInZoomControls(true);
        wvConditions.getSettings().setDisplayZoomControls(false);
        wvConditions.getSettings().setUseWideViewPort(true);
        wvConditions.getSettings().setLoadWithOverviewMode(true);
        wvConditions.loadUrl(url);

        txtTitle.setText(title);
    }
}
