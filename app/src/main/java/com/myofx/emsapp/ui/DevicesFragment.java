package com.myofx.emsapp.ui;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.DeviceInfoEvent;
import com.myofx.emsapp.bluetooth.BluetoothEmsCommands;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.DeviceInfo;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.settings.adapters.DevicesConectedAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Gerard on 19/4/2017.
 */

public class DevicesFragment extends Fragment {

    //UI
    private View rootView;
    private RelativeLayout relativeBackground;
    private LinearLayout linearHeaders;
    private RecyclerView listDevices;
    private TextView txtEmpty;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private TextView txtName;
    private ImageView imgUser;
    private Button btnAdd;
    private Button btnUpdateStatus;

    //VARIABLE
    private ArrayList<Device> devices;
    private DevicesConectedAdapter devicesAdapter;
    private PreferencesManager pref;
    private int connectionType = Device.DEVICE_WIFI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_devices, container, false);
        pref = PreferencesManager.getInstance(getContext());

        //Get connectionType
        connectionType = pref.getUser().getConfiguration().getConnectionType();

        bindUi();
        loadTheme();
        setListeners();
        checkAvailableDevices();
        loadData();

        return rootView;
    }

    public void bindUi(){
        listDevices = rootView.findViewById(R.id.listDevices);
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        linearHeaders = rootView.findViewById(R.id.linearHeaders);
        btnAdd = rootView.findViewById(R.id.btnAdd);
        txtEmpty = rootView.findViewById(R.id.txtEmpty);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        txtLoading = rootView.findViewById(R.id.txtLoading);
        txtName = rootView.findViewById(R.id.txtName);
        imgUser = rootView.findViewById(R.id.imgUser);
        btnUpdateStatus = rootView.findViewById(R.id.btnUpdateStatus);

        listDevices.setLayoutManager(new LinearLayoutManager(getActivity()));
        listDevices.setNestedScrollingEnabled(false);
    }

    public void setListeners(){
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (connectionType){
                    case Device.DEVICE_WIFI:
                        ((MainActivity)getActivity()).setBroadcastList();
                        break;
                    case Device.DEVICE_BLE:
                        ((MainActivity)getContext()).scanEmsDevice();
                        break;
                }
            }
        });

        btnUpdateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAvailableDevices();
            }
        });
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        linearHeaders.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        txtEmpty.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(getActivity()));
    }

    public void checkAvailableDevices() {
        txtLoading.setText(getString(R.string.loading_info_devices));
        linearLoading.setVisibility(View.VISIBLE);

        ((MainActivity)getContext()).setStartCheckDevice(true);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                checkDevicesShow();
            }
        }, 6000);
    }

    public void checkDevicesShow() {

        ArrayList<Device> devicesCheck;
        ArrayList<String> devicesConnected = new ArrayList<>();

        ((MainActivity)getContext()).setStartCheckDevice(false);

        switch (connectionType){
            case Device.DEVICE_WIFI:
                devicesConnected = ((MainActivity) getContext()).getCheckDevicesId();
                break;
            case Device.DEVICE_BLE:
                devicesConnected = BluetoothEmsCommands.getCheckDevicesId();
                break;
        }

        pref = PreferencesManager.getInstance(getActivity());
        devicesCheck = pref.getAllDevices();

        //Recorrer devices a validar i comparar con los registrados. Si tienen ID igual es que esta disponible.
        for (Device deviceCheck : devicesCheck) {
            if(!devicesConnected.isEmpty()){
                for (String deviceUid : devicesConnected){
                    String deviceId = "";
                    switch (connectionType){
                        case Device.DEVICE_WIFI:
                            deviceId = deviceCheck.getUidString();
                            break;
                        case Device.DEVICE_BLE:
                            deviceId = deviceCheck.getDeviceBleAdress();
                            break;
                    }

                    if (deviceId.equals(deviceUid)){
                        deviceCheck.setAvailable(true);
                        break;
                    } else
                        deviceCheck.setAvailable(false);
                }
            } else
                deviceCheck.setAvailable(false);
        }
        pref.saveDevices(devicesCheck);
        loadDevices();
    }

    public void loadDevices(){
        devices = pref.getAllDevices();
        devicesAdapter = new DevicesConectedAdapter(devices, getActivity());
        listDevices.setAdapter(devicesAdapter);

        //Load information devices
        if (!devices.isEmpty()) {
            if ( devices.get(0).getDeviceType() == Device.DEVICE_WIFI) {
                txtEmpty.setVisibility(View.GONE);
                ArrayList<String> devicesUUID = new ArrayList<>();
                for (Device device: devices){
                    try {
                        if (device.isNewDevice()){
                            devicesUUID.add((new String(device.getUIDNewByte(), "UTF-8")).trim());
                        } else {
                            String UUID =  Arrays.toString(device.getUIDOldByte());
                            UUID = UUID.replace("[","");
                            UUID =  UUID.replace("]","");
                            UUID =  UUID.replace(", ","");
                            devicesUUID.add(UUID);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                if (!EventBus.getDefault().isRegistered(this))
                    EventBus.getDefault().register(this);

                if (!EmsApp.isModeLocal())
                    EmsApi.getInstance().getDevicesInformation(devicesUUID);
                else
                    linearLoading.setVisibility(View.GONE);
            } else {
                linearLoading.setVisibility(View.GONE);

                pref.saveDevices(devices);
                if (devices.size() <= 0)
                    txtEmpty.setVisibility(View.VISIBLE);
                else
                    txtEmpty.setVisibility(View.GONE);
            }
        } else {
            linearLoading.setVisibility(View.GONE);
            txtEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void ondDevicesInformation(DeviceInfoEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);

        for (Device device: devices){
            for (DeviceInfo deviceInfo: event.getDevicesInfo()){
                String uid = "";

                try {
                    if (device.isNewDevice())
                        uid = new String(device.getUIDNewByte(), "UTF-8").trim();
                    else {
                        String UUID =  Arrays.toString(device.getUIDOldByte());
                        UUID = UUID.replace("[","");
                        UUID =  UUID.replace("]","");
                        UUID =  UUID.replace(", ","");
                        uid = UUID;
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                if (uid.equals(deviceInfo.getSerial())){
                    device.setPotency(deviceInfo.getMaxPower());
                }

            }
        }

        pref.saveDevices(devices);
        if (devices.size() <= 0)
            txtEmpty.setVisibility(View.VISIBLE);
        else
            txtEmpty.setVisibility(View.GONE);
    }

    @Subscribe
    public void onGetDevicesInformation(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);
    }

    public void loadData(){
        User user = pref.getUser();
        txtName.setText(user.getName());
        ImageUtils.loadImageFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+user.getImage());
    }

}