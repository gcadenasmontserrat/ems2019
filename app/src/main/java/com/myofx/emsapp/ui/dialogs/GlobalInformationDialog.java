package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.R;

/**
 * Created by Gerard on 21/6/2017.
 */

public class GlobalInformationDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        alertdialog = builder.create();
        return alertdialog;
    }

    public static GlobalInformationDialog newInstance() {
        GlobalInformationDialog globalInformationDialog = new GlobalInformationDialog();
        return globalInformationDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_global_info, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}