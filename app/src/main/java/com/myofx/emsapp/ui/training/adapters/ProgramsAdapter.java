package com.myofx.emsapp.ui.training.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.utils.ColorTheme;

import java.util.ArrayList;

public class ProgramsAdapter extends RecyclerView.Adapter<ProgramsAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Training> trainings;
    private Context context;
    private Training actualProgram;
    private ProgramsAdapter.Callback callback;

    public ProgramsAdapter(ArrayList<Training> trainings, Training actualProgram, Context context, ProgramsAdapter.Callback callback) {
        this.trainings = trainings;
        this.context = context;
        this.actualProgram = actualProgram;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    @Override
    public void onBindViewHolder(ProgramsAdapter.ViewHolder holder, final int position) {

        holder.txtTitle.setText(trainings.get(position).getTitle());

        if (trainings.get(position).getIdTraining().equals(actualProgram.getIdTraining()))
            holder.linearItem.setBackgroundColor(ColorTheme.getPrimaryDarkColor(context));
        else
            holder.linearItem.setBackgroundColor(ColorTheme.getLightBackgroundColor(context));

        ProgramsAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null)
                    callback.onProgramClick(trainings.get(position));
            }
        });
    }

    @Override
    public ProgramsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_program, viewGroup, false);
        return new ProgramsAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private LinearLayout linearItem;
        private TextView txtTitle;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            linearItem = mView.findViewById(R.id.linearItem);
            txtTitle = mView.findViewById(R.id.txtTitle);
        }
    }

    public interface Callback {
        void onProgramClick(Training training);
    }

}
