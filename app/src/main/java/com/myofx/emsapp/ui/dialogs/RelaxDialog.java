package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 31/10/2017.
 */

public class RelaxDialog extends DialogFragment {

    public static final String EXTRA_FREQUENCY_RELAX = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_FREQUENCY_RELAX";
    public static final String EXTRA_PULSE_RELAX = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_PULSE_RELAX";
    public static final String EXTRA_FREQ_POTENCY = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_FREQ_POTENCY";

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private EditText editFreqRelax;
    private EditText editPulse;
    private EditText editFreqPotency;
    private ImageView imgRelaxPlus;
    private ImageView imgRelaxMinus;
    private ImageView imgPulsePlus;
    private ImageView imgPulseMinus;
    private ImageView imgRelaxPotencyPlus;
    private ImageView imgRelaxPotencyMinus;

    //Variable
    private RelaxDialog.OnConfirmationRelaxListener mCallback;
    private Activity exerciseActivity;
    private int frequencyRelax = 0;
    private int pulseRelax = 0;
    private int freqPotency = 0;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        frequencyRelax = getArguments().getInt(EXTRA_FREQUENCY_RELAX);
        pulseRelax = getArguments().getInt(EXTRA_PULSE_RELAX);
        freqPotency = getArguments().getInt(EXTRA_FREQ_POTENCY);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadFrequencyValue();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static RelaxDialog newInstance(Activity exerciseActivity, int frequencyRelax, int pulseRelax, int freqPotency) {
        RelaxDialog relaxDialog = new RelaxDialog();
        Bundle args = new Bundle();
        args.putInt(EXTRA_FREQUENCY_RELAX, frequencyRelax);
        args.putInt(EXTRA_PULSE_RELAX, pulseRelax);
        args.putInt(EXTRA_FREQ_POTENCY, freqPotency);
        relaxDialog.exerciseActivity = exerciseActivity;
        relaxDialog.setArguments(args);
        return relaxDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_relax, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        editFreqRelax = (EditText) view.findViewById(R.id.editFreqRelax);
        editPulse = (EditText) view.findViewById(R.id.editPulse);
        imgRelaxPlus = (ImageView) view.findViewById(R.id.imgRelaxPlus);
        imgRelaxMinus = (ImageView) view.findViewById(R.id.imgRelaxMinus);
        imgPulsePlus = (ImageView) view.findViewById(R.id.imgPulsePlus);
        imgPulseMinus = (ImageView) view.findViewById(R.id.imgPulseMinus);
        editFreqPotency = (EditText) view.findViewById(R.id.editFreqPotency);
        imgRelaxPotencyPlus = (ImageView) view.findViewById(R.id.imgRelaxPotencyPlus);
        imgRelaxPotencyMinus = (ImageView) view.findViewById(R.id.imgRelaxPotencyMinus);

        setListeners();

        return view;
    }

    public void loadFrequencyValue(){
        editFreqRelax.setText(""+frequencyRelax);
        editPulse.setText(""+pulseRelax);
        editFreqPotency.setText(""+freqPotency);
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, Integer.parseInt(editFreqRelax.getText().toString()),
                        Integer.parseInt(editPulse.getText().toString()),
                        Integer.parseInt(editFreqPotency.getText().toString()));
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editFreqRelax.getText().toString()))
                    editFreqRelax.setText(""+1);

                if (TextUtils.isEmpty(editPulse.getText().toString()))
                    editPulse.setText(""+25);

                if (TextUtils.isEmpty(editPulse.getText().toString()))
                    editFreqPotency.setText(""+0);

                sendResult(true, Integer.parseInt(editFreqRelax.getText().toString()),
                        Integer.parseInt(editPulse.getText().toString()),
                        Integer.parseInt(editFreqPotency.getText().toString()));
            }
        });

        imgRelaxPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freqRelax = Integer.parseInt(editFreqRelax.getText().toString());
                freqRelax = freqRelax +1;

                if (freqRelax > 150)
                    freqRelax = 150;
                else if (freqRelax < 1)
                    freqRelax = 1;

                editFreqRelax.setText(""+freqRelax);
            }
        });

        imgRelaxMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freqRelax = Integer.parseInt(editFreqRelax.getText().toString());
                freqRelax = freqRelax -1;

                if (freqRelax > 150)
                    freqRelax = 150;

                editFreqRelax.setText(""+freqRelax);
            }
        });

        imgPulsePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pulse = Integer.parseInt(editPulse.getText().toString());
                pulse = pulse +1;

                if (pulse > 400)
                    pulse = 400;
                else if (pulse < 25)
                    pulse = 25;

                editPulse.setText(""+pulse);
            }
        });

        imgPulseMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pulse = Integer.parseInt(editPulse.getText().toString());
                pulse = pulse -1;

                if (pulse > 400)
                    pulse = 400;
                else if (pulse < 25)
                    pulse = 25;

                editPulse.setText(""+pulse);
            }
        });

        imgRelaxPotencyPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freqRelax = Integer.parseInt(editFreqPotency.getText().toString());
                freqRelax = freqRelax +1;

                if (freqRelax > 100)
                    freqRelax = 100;
                else if (freqRelax < 1)
                    freqRelax = 1;

                editFreqPotency.setText(""+freqRelax);
            }
        });

        imgRelaxPotencyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freqRelax = Integer.parseInt(editFreqPotency.getText().toString());
                freqRelax = freqRelax -1;

                if (freqRelax > 100)
                    freqRelax = 100;

                editFreqPotency.setText(""+freqRelax);
            }
        });
    }

    public interface OnConfirmationRelaxListener {
        void onConfirmationRelax(boolean send, int frequencyRelax, int pulseRelax, int freqPotency);
    }

    private void sendResult(boolean send, int frequencyRelax, int pulseRelax, int freqPotency) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (frequencyRelax > 150)
                frequencyRelax = 150;

            if (pulseRelax > 400)
                pulseRelax = 400;
            else if (pulseRelax < 25)
                pulseRelax = 25;

            if (freqPotency > 100)
                freqPotency = 100;

            mCallback.onConfirmationRelax(send,frequencyRelax, pulseRelax,freqPotency);
        }
    }
}