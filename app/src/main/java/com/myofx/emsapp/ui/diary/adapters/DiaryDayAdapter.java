package com.myofx.emsapp.ui.diary.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.CalendarDay;
import com.myofx.emsapp.ui.diary.DiaryUtils;
import com.myofx.emsapp.utils.ColorTheme;

import java.util.ArrayList;

public class DiaryDayAdapter extends RecyclerView.Adapter<DiaryDayAdapter.ViewHolder> {

    //VARIABLE
    private DiaryDayAdapter.Callback callback;
    private ArrayList<CalendarDay> bookings;
    private Context context;

    public DiaryDayAdapter(ArrayList<CalendarDay> bookings, Context context, DiaryDayAdapter.Callback callback) {
        this.bookings = bookings;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    @Override
    public void onBindViewHolder(DiaryDayAdapter.ViewHolder holder, final int position) {

        if (bookings.get(position).isTypeDay()){
            holder.linearBooking.setBackgroundColor(ColorTheme.getLightBackgroundColor(context));
            holder.txtTitle.setTextColor(ColorTheme.getPrimaryColor(context));
            holder.txtTitle.setTextSize(context.getResources().getDimension(R.dimen.txt_title_diary));
            holder.txtTitle.setTypeface(null, Typeface.BOLD);
            holder.txtTitle.setText(bookings.get(position).getDate());
            holder.divider.setVisibility(View.GONE);
        } else {

            if (bookings.get(position).getBooking() != null)
                holder.txtTitle.setBackgroundColor(ContextCompat.getColor(context, R.color.greyLightBackground));
            else
                holder.linearBooking.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            holder.divider.setVisibility(View.VISIBLE);
            holder.txtTitle.setTextColor(context.getResources().getColor(R.color.textColorPrimary));
            holder.txtTitle.setTextSize(context.getResources().getDimension(R.dimen.txt_info_diary));
            holder.txtTitle.setTypeface(null, Typeface.NORMAL);
            holder.txtTitle.setText(DiaryUtils.getHourFromDate(bookings.get(position).getDate()));
        }

        DiaryDayAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bookings.get(position).isTypeDay())
                    if (callback != null)
                        callback.onBookingClick(bookings.get(position), bookings.get(position).getDate());
            }
        });
    }

    @Override
    public DiaryDayAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_booking, viewGroup, false);
        return new DiaryDayAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private LinearLayout linearBooking;
        private TextView txtTitle;
        private View divider;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            linearBooking = mView.findViewById(R.id.linearBooking);
            txtTitle = mView.findViewById(R.id.txtTitle);
            divider = mView.findViewById(R.id.divider);
        }
    }

    public interface Callback {
        void onBookingClick(CalendarDay booking, String day);
    }

}