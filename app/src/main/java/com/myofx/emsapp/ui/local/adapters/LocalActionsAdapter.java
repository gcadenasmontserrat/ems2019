package com.myofx.emsapp.ui.local.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.utils.ColorTheme;

import java.util.ArrayList;

public class LocalActionsAdapter extends RecyclerView.Adapter<LocalActionsAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<LocalAction> localActions;
    private Context context;

    public LocalActionsAdapter(ArrayList<LocalAction> localActions, Context context) {
        this.localActions = localActions;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return localActions.size();
    }

    @Override
    public void onBindViewHolder(LocalActionsAdapter.ViewHolder itemsViewHolder, final int position) {
        itemsViewHolder.txtData.setText(localActions.get(position).getData());
        itemsViewHolder.imgAction.setColorFilter(ColorTheme.getPrimaryColor(context));

        switch (localActions.get(position).getType()){
            case LocalAction.ACTION_TYPE_ADD_USER:
                itemsViewHolder.imgAction.setImageResource(R.drawable.ic_action_user);
                break;
            case LocalAction.ACTION_TYPE_ADD_TRAINING:
                itemsViewHolder.imgAction.setImageResource(R.drawable.ic_action_training);
                break;
            case LocalAction.ACTION_TYPE_BOOKING:
                itemsViewHolder.imgAction.setImageResource(R.drawable.ic_action_booking);
                break;
        }
    }

    @Override
    public LocalActionsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_local_action, viewGroup, false);
        return new LocalActionsAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtData;
        private ImageView imgAction;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            imgAction = mView.findViewById(R.id.imgAction);
            txtData = mView.findViewById(R.id.txtData);
        }
    }

}
