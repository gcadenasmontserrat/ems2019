package com.myofx.emsapp.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.Configuration;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.GeneralWebViewActivity;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.ui.SplashActivity;
import com.myofx.emsapp.ui.dialogs.ColorPickerDialog;
import com.myofx.emsapp.ui.dialogs.LogoutDialog;
import com.myofx.emsapp.ui.training.VideoActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Method;

/**
 * Created by Gerard on 28/12/2016.
 */

public class SettingsFragment extends Fragment implements LogoutDialog.OnConfirmationLogoutListener, ColorPickerDialog.OnConfirmationColorListener {

    //UI
    private View rootView;
    private RadioButton rbMeters;
    private RadioButton rbMiles;
    private RadioButton rbWifi;
    private RadioButton rbBle;
    private RelativeLayout relativeConditions;
    private RelativeLayout relativeVideo;
    private RelativeLayout relativeInformation;
    private RelativeLayout relativeSupport;
    private RelativeLayout relativeLogout;
    private RelativeLayout relativeBackground;
    private RelativeLayout relativeTitle;
    private RelativeLayout relativeColor;
    private RelativeLayout relativeRecommendations;
    private RelativeLayout relativeContraindications;
    private RelativeLayout relativeVersion;
    private TextView txtTitle;
    private ImageView imgSettings;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private TextView txtVersion;

    //Variables
    private boolean updateColor = false;
    private PreferencesManager pref;
    private User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        pref = PreferencesManager.getInstance(getActivity());
        user = pref.getUser();

        bindUi();
        loadTheme();
        loadMode();
        loadUserConfiguration();
        setListeners();

        return rootView;
    }

    public void bindUi(){
        rbMeters = rootView.findViewById(R.id.rbMeters);
        rbMiles = rootView.findViewById(R.id.rbMiles);
        rbWifi = rootView.findViewById(R.id.rbWifi);
        rbBle = rootView.findViewById(R.id.rbBle);
        relativeConditions = rootView.findViewById(R.id.relativeConditions);
        relativeVideo = rootView.findViewById(R.id.relativeVideo);
        relativeInformation = rootView.findViewById(R.id.relativeInformation);
        relativeSupport = rootView.findViewById(R.id.relativeSupport);
        relativeLogout = rootView.findViewById(R.id.relativeLogout);
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        relativeTitle = rootView.findViewById(R.id.relativeTitle);
        relativeColor = rootView.findViewById(R.id.relativeColor);
        txtTitle = rootView.findViewById(R.id.txtTitle);
        imgSettings = rootView.findViewById(R.id.imgSettings);
        relativeRecommendations = rootView.findViewById(R.id.relativeRecommendations);
        relativeContraindications = rootView.findViewById(R.id.relativeContraindications);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        txtLoading = rootView.findViewById(R.id.txtLoading);
        txtVersion = rootView.findViewById(R.id.txtVersion);
        relativeVersion = rootView.findViewById(R.id.relativeVersion);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        relativeTitle.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtTitle.setTextColor(ColorTheme.getPrimaryColor(getActivity()));
        imgSettings.setColorFilter(ColorTheme.getIconsColor(getActivity()));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(getActivity()));
    }

    public void loadMode(){
        if (EmsApp.isModeLocal()){
            relativeConditions.setVisibility(View.GONE);
            relativeVideo.setVisibility(View.GONE);
            relativeInformation.setVisibility(View.GONE);
            relativeSupport.setVisibility(View.GONE);
            relativeRecommendations.setVisibility(View.GONE);
            relativeContraindications.setVisibility(View.GONE);
        }
    }

    public void setListeners(){
        CompoundButton.OnCheckedChangeListener listenerRb = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setTypeface(isChecked ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                saveConfiguration();
            }
        };

        CompoundButton.OnCheckedChangeListener listenerConnectionType = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setTypeface(isChecked ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                saveConfiguration();
                ((MainActivity)getActivity()).loadConnectionType();
            }
        };

        rbMeters.setOnCheckedChangeListener(listenerRb);
        rbMiles.setOnCheckedChangeListener(listenerRb);
        rbWifi.setOnCheckedChangeListener(listenerConnectionType);
        rbBle.setOnCheckedChangeListener(listenerConnectionType);

        View.OnClickListener listenerOptions = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.relativeConditions:
                        goGeneralWebViewActivity(getString(R.string.settings_conditions), "http://myofx.eu/privacy.html");
                        break;
                    case R.id.relativeVideo:
                        Intent intent = new Intent(getActivity(), VideoActivity.class);
                        intent.putExtra(VideoActivity.EXTRA_ID_VIDEO, pref.getAppInfo().getLinks().getVideo());
                        startActivity(intent);
                        break;
                    case R.id.relativeInformation:
                        goGeneralWebViewActivity(getString(R.string.settings_information), pref.getAppInfo().getLinks().getProductInfo());
                        break;
                    case R.id.relativeSupport:
                        goGeneralWebViewActivity(getString(R.string.settings_support), pref.getAppInfo().getLinks().getTechnicalSupport());
                        break;
                    case R.id.relativeLogout:
                        showLogoutDialog();
                        break;
                    case R.id.relativeColor:
                        showColorsDialog();
                        break;
                    case R.id.relativeRecommendations:
                        goGeneralWebViewActivity(getString(R.string.settings_recommendations), pref.getAppInfo().getLinks().getMedicalRecommendations());
                        break;
                    case R.id.relativeContraindications:
                        goGeneralWebViewActivity(getString(R.string.settings_contraindications), pref.getAppInfo().getLinks().getContraindications());
                        break;
                    case R.id.relativeVersion:
                        goGooglePlay();
                        break;
                }
            }
        };

        relativeConditions.setOnClickListener(listenerOptions);
        relativeVideo.setOnClickListener(listenerOptions);
        relativeInformation.setOnClickListener(listenerOptions);
        relativeSupport.setOnClickListener(listenerOptions);
        relativeLogout.setOnClickListener(listenerOptions);
        relativeColor.setOnClickListener(listenerOptions);
        relativeRecommendations.setOnClickListener(listenerOptions);
        relativeContraindications.setOnClickListener(listenerOptions);
        relativeVersion.setOnClickListener(listenerOptions);
    }

    public void goGooglePlay(){
        final String appPackageName = getActivity().getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void goSplashActivity(){
        Intent intent = new Intent(getActivity(), SplashActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void showLogoutDialog(){
        LogoutDialog logoutDialog = LogoutDialog.newInstance(this);
        logoutDialog.show(getFragmentManager(), LogoutDialog.class.getSimpleName());
    }

    public void showColorsDialog(){
        ColorPickerDialog colorPickerDialog = ColorPickerDialog.newInstance(this, UDPServer.SERVER_NORMAL);
        colorPickerDialog.show(getFragmentManager(), ColorPickerDialog.class.getSimpleName());
    }

    public void goGeneralWebViewActivity(String title, String url){
        Intent intent = new Intent(getActivity(), GeneralWebViewActivity.class);
        intent.putExtra(GeneralWebViewActivity.EXTRA_TITLE, title);
        intent.putExtra(GeneralWebViewActivity.EXTRA_URL, url);
        startActivity(intent);
    }

    @Override
    public void onConfirmationLogout(boolean logout) {
        if (logout){
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            if (EmsApp.isModeLocal()) {
                pref.saveColorTheme(R.style.AppThemeGreen);
                PreferencesManager.clear();
                goSplashActivity();
            } else {
                EmsApi.getInstance().doLogout();
                txtLoading.setText(getString(R.string.loading_logout));
                linearLoading.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onLogoutEvent(DefaultEvent defaultEvent) {
        EventBus.getDefault().removeStickyEvent(this);

        linearLoading.setVisibility(View.GONE);
        pref.saveColorTheme(R.style.AppThemeGreen);
        PreferencesManager.clear();
        goSplashActivity();
    }

    @Subscribe
    public void onErrorEvent(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);
        Toast.makeText(getActivity(), event.getError().getMessageError(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);
        Toast.makeText(getActivity(), event.getMsgError(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConfirmationColor(boolean updateColor) {
        this.updateColor = updateColor;
        saveConfiguration();
    }

    public void loadUserConfiguration(){
        User user = pref.getUser();

        if (user.getConfiguration() != null){
            if (user.getConfiguration().getUnits().equals(Configuration.DISTANCE_METERS))
                rbMeters.setChecked(true);
            else
                rbMiles.setChecked(true);

            if (user.getConfiguration().getConnectionType() == Device.DEVICE_WIFI)
                rbWifi.setChecked(true);
            else
                rbBle.setChecked(true);
        } else {
            rbMeters.setChecked(true);
            rbWifi.setChecked(true);
        }

        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        try{
            Method method = wifiManager.getClass().getDeclaredMethod("getWifiApState");
            method.setAccessible(true);
            int actualState = (Integer) method.invoke(wifiManager, (Object[]) null);
        } catch (Exception e) {
            Log.e(this.getClass().toString(), "", e);
        }

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            txtVersion.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void saveConfiguration(){
        //Units
        String units = Configuration.DISTANCE_METERS;
        if (rbMiles.isChecked())
            units = Configuration.DISTANCE_MILLES;

        //Connection type
        int connectionType = Device.DEVICE_WIFI;
        if (rbBle.isChecked())
            connectionType = Device.DEVICE_BLE;

        //Update User
        user.getConfiguration().setUnits(units);
        user.getConfiguration().setColor(ColorTheme.getColorFromTheme(pref.getColorTheme()));
        user.getConfiguration().setConnectionType(connectionType);

        if (EmsApp.isModeLocal()) {
            pref.saveUser(user);
            AppDatabase.updateUser(getActivity(), user);

            if (updateColor)
                ((MainActivity)getActivity()).closeDrawerAndRecreate();
        } else {
            //Save config
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            EmsApi.getInstance().saveConfiguration(units, connectionType, ColorTheme.getColorFromTheme(pref.getColorTheme()));
        }
    }

    @Subscribe
    public void onSaveConfigEvent(UserEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        pref.saveUser(event.getUser());
        ((MainActivity)getActivity()).loadConnectionType();
        AppDatabase.updateUser(getActivity(), event.getUser());

        if (updateColor)
            ((MainActivity)getActivity()).closeDrawerAndRecreate();
    }
}