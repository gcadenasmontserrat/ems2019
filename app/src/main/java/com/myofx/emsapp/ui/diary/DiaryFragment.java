package com.myofx.emsapp.ui.diary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CalendarDay;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.dialogs.AddBookingDialog;
import com.myofx.emsapp.ui.dialogs.StandardDialog;
import com.myofx.emsapp.ui.diary.adapters.DiaryDayAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.DateUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Gerard on 28/12/2016.
 */

public class DiaryFragment extends Fragment implements DiaryDayAdapter.Callback, AddBookingDialog.OnConfirmationBookCallback {

    //UI
    private View rootView;
    private WebView wvDiary;
    private RelativeLayout relativeBackground;
    private LinearLayout linearLoading;
    private LinearLayout linearCalendar;
    private RecyclerView listDay1, listDay2, listDay3, listDay4, listDay5;
    private TextView txtPreviousDays;
    private TextView txtNextDays;
    private RelativeLayout relativeWeek;
    private NestedScrollView scrollDiaryLocal;
    private CardView cardDiary;

    //Variables
    private List<Booking> bookings = new ArrayList<>();
    private DiaryDayAdapter diaryDayAdapter1, diaryDayAdapter2, diaryDayAdapter3, diaryDayAdapter4,
            diaryDayAdapter5;
    private ArrayList<CalendarDay> days1, days2, days3, days4, days5;
    private int startDay = 0;

    //Variables
    private PreferencesManager pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_diary, container, false);
        pref = PreferencesManager.getInstance(getContext());

        bindUi();
        setListeners();
        loadTheme();

        if (!EmsApp.isModeLocal())
            loadDiary();
        else
            loadCalendar();

        return rootView;
    }

    public void bindUi(){
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        wvDiary = rootView.findViewById(R.id.wvDiary);
        linearCalendar = rootView.findViewById(R.id.linearCalendar);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        listDay1 = rootView.findViewById(R.id.listDay1);
        listDay2 = rootView.findViewById(R.id.listDay2);
        listDay3 = rootView.findViewById(R.id.listDay3);
        listDay4 = rootView.findViewById(R.id.listDay4);
        listDay5 = rootView.findViewById(R.id.listDay5);
        txtPreviousDays = rootView.findViewById(R.id.txtPreviousDays);
        txtNextDays = rootView.findViewById(R.id.txtNextDays);
        linearLoading.setVisibility(View.VISIBLE);
        relativeWeek = rootView.findViewById(R.id.relativeWeek);
        scrollDiaryLocal = rootView.findViewById(R.id.scrollDiaryLocal);
        cardDiary = rootView.findViewById(R.id.cardDiary);

        listDay1.setLayoutManager(new LinearLayoutManager(getActivity())); listDay1.setNestedScrollingEnabled(false);
        listDay2.setLayoutManager(new LinearLayoutManager(getActivity())); listDay2.setNestedScrollingEnabled(false);
        listDay3.setLayoutManager(new LinearLayoutManager(getActivity())); listDay3.setNestedScrollingEnabled(false);
        listDay4.setLayoutManager(new LinearLayoutManager(getActivity())); listDay4.setNestedScrollingEnabled(false);
        listDay5.setLayoutManager(new LinearLayoutManager(getActivity())); listDay5.setNestedScrollingEnabled(false);
    }

    public void setListeners(){
        txtPreviousDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDay = startDay -5;
                loadCalendar();
            }
        });

        txtNextDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDay = startDay +5;
                loadCalendar();
            }
        });
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
    }

    public void loadDiary() {
        cardDiary.setVisibility(View.VISIBLE);
        
        Map<String, String> headers = new HashMap<>();
        headers.put(WSConfig.HEADER_LANGUAGE, Locale.getDefault().getLanguage());

        wvDiary.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                linearLoading.setVisibility(View.GONE);
            }
        });

        wvDiary.getSettings().setBuiltInZoomControls(true);
        wvDiary.getSettings().setDisplayZoomControls(false);
        wvDiary.getSettings().setUseWideViewPort(true);
        wvDiary.getSettings().setLoadWithOverviewMode(true);
        wvDiary.getSettings().setJavaScriptEnabled(true);

        wvDiary.loadUrl(BuildConfig.DIARY_URL+pref.getAccessToken(), headers);
    }

    public void loadCalendar() {
        User user = pref.getUser();

        bookings.addAll(user.getBookings());
        bookings.addAll(AppDatabase.getBookingsUser(getContext(), user.getId()));

        scrollDiaryLocal.setVisibility(View.VISIBLE);
        relativeWeek.setVisibility(View.VISIBLE);
        linearCalendar.setVisibility(View.VISIBLE);

        if (startDay == 0)
            txtPreviousDays.setVisibility(View.GONE);
        else
            txtPreviousDays.setVisibility(View.VISIBLE);

        //Load days
        days1 = DiaryUtils.getDefaultDay(startDay+0, bookings);
        days2 = DiaryUtils.getDefaultDay(startDay+1, bookings);
        days3 = DiaryUtils.getDefaultDay(startDay+2, bookings);
        days4 = DiaryUtils.getDefaultDay(startDay+3, bookings);
        days5 = DiaryUtils.getDefaultDay(startDay+4, bookings);

        diaryDayAdapter1 = new DiaryDayAdapter(days1, getActivity(), this); listDay1.setAdapter(diaryDayAdapter1);
        diaryDayAdapter2 = new DiaryDayAdapter(days2, getActivity(), this); listDay2.setAdapter(diaryDayAdapter2);
        diaryDayAdapter3 = new DiaryDayAdapter(days3, getActivity(), this); listDay3.setAdapter(diaryDayAdapter3);
        diaryDayAdapter4 = new DiaryDayAdapter(days4, getActivity(), this); listDay4.setAdapter(diaryDayAdapter4);
        diaryDayAdapter5 = new DiaryDayAdapter(days5, getActivity(), this); listDay5.setAdapter(diaryDayAdapter5);
    }

    @Override
    public void onBookingClick(CalendarDay booking, String day) {
        if (booking.getBooking() == null){
            AddBookingDialog addBookingDialog = AddBookingDialog.newInstance(booking, day, this);
            addBookingDialog.show(getFragmentManager(), StandardDialog.class.getSimpleName());
        }
    }

    @Override
    public void onConfirmationBook() {
        loadCalendar();
    }
}