package com.myofx.emsapp.ui.training.multiple;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.CastRemoteDisplayLocalService;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.chromecast.ExerciseMaxForcePresentationService;
import com.myofx.emsapp.ui.dialogs.TimeDialog;
import com.myofx.emsapp.ui.training.adapters.GlobalGridAdapter;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/6/2017.
 */

public class ExerciseMultipleGlobalFragment  extends Fragment {

    private static final int UPDATE_TIME = 1000;

    //Ui
    private View rootView;
    private RecyclerView gridGlobal;
    private ImageButton btnPlayGlobal;
    private ImageButton btnStopGlobal;
    private ImageButton btnPlusGlobal;
    private ImageButton btnMinusGlobal;
    private ImageButton btnPauseGlobal;
    private ImageView imgTime1, imgTime2, imgTime3, imgTime4, imgTime5,
            imgTime6, imgTime7, imgTime8, imgTime9, imgTime10;
    private TextView txtMinTens;
    private TextView txtMinUnits;
    private TextView txtSecTens;
    private TextView txtSecUnits;
    private LinearLayout linearTime;
    private long mLastClickPlus;
    private long mLastClickMinus;

    //Variables
    private GlobalGridAdapter globalGridAdapter;
    private ArrayList<Training> trainings = new ArrayList<>();
    private Training trainingGlobal;
    private int vestIndex = 0;
    private int globalPotency = 0;
    private int connectionType = Device.DEVICE_WIFI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_multiple_global, container, false);

        //Load information
        trainingGlobal = ((ExerciseMultipleActivity)getActivity()).getTrainingGlobal();
        trainings = ((ExerciseMultipleActivity)getActivity()).getTrainings();


        connectionType = trainings.get(0).getDevice().getDeviceType();

        bindUi();
        setListeners();
        loadGlobalParticipants();
        updateTimeText(trainingGlobal.getTime());

        return rootView;
    }

    public static ExerciseMultipleGlobalFragment newInstance() {
        ExerciseMultipleGlobalFragment exerciseMultipleGlobalFragment = new ExerciseMultipleGlobalFragment();
        return exerciseMultipleGlobalFragment;
    }

    public void bindUi(){
        gridGlobal = rootView.findViewById(R.id.gridGlobal);
        btnPlayGlobal = rootView.findViewById(R.id.btnPlay);
        btnStopGlobal = rootView.findViewById(R.id.btnStop);
        btnPlusGlobal = rootView.findViewById(R.id.btnPlus);
        btnMinusGlobal = rootView.findViewById(R.id.btnMinus);
        btnPauseGlobal = rootView.findViewById(R.id.btnPause);
        imgTime1 = rootView.findViewById(R.id.imgTime1);
        imgTime2 = rootView.findViewById(R.id.imgTime2);
        imgTime3 = rootView.findViewById(R.id.imgTime3);
        imgTime4 = rootView.findViewById(R.id.imgTime4);
        imgTime5 = rootView.findViewById(R.id.imgTime5);
        imgTime6 = rootView.findViewById(R.id.imgTime6);
        imgTime7 = rootView.findViewById(R.id.imgTime7);
        imgTime8 = rootView.findViewById(R.id.imgTime8);
        imgTime9 = rootView.findViewById(R.id.imgTime9);
        imgTime10 = rootView.findViewById(R.id.imgTime10);
        txtMinTens = rootView.findViewById(R.id.txtMinTens);
        txtMinUnits = rootView.findViewById(R.id.txtMinUnits);
        txtSecTens = rootView.findViewById(R.id.txtSecTens);
        txtSecUnits = rootView.findViewById(R.id.txtSecUnits);
        linearTime = rootView.findViewById(R.id.linearTime);
    }

    public void setListeners(){
        btnPlayGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((ExerciseMultipleActivity)getActivity()).isStartNormal()){
                    Toast.makeText(getActivity(), R.string.exercise_normal_started, Toast.LENGTH_LONG).show();
                } else {
                    ((ExerciseMultipleActivity)getActivity()).setMaxForceIndividual(false);
                    ((ExerciseMultipleActivity)getActivity()).createConnections();
                }
            }
        });

        btnStopGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((ExerciseMultipleActivity)getActivity()).isStartNormal()){
                    Toast.makeText(getActivity(), R.string.exercise_normal_started, Toast.LENGTH_LONG).show();
                } else {
                    globalPotency = 0;
                    for (Training training: trainings){
                        training.setTrainingStatus(Training.TRAINING_STATUS_STOP);
                        vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);
                        ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, training.getUid());

                        //Reset values
                        training.setLevel(0, globalPotency);
                        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, training, training.getUid());
                    }
                    ((ExerciseMultipleActivity)getActivity()).setStartGlobal(false);
                    globalGridAdapter.notifyDataSetChanged();

                    updateTimeText(trainingGlobal.getTime());
                    updatePotencyUi();
                    updatePauseUi(0);
                    updateEstimulationUi(100);

                    btnPlayGlobal.setVisibility(View.VISIBLE);
                    btnPauseGlobal.setVisibility(View.GONE);
                }
            }
        });

        btnPauseGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Training training: trainings){
                    training.setTrainingStatus(Training.TRAINING_STATUS_PAUSE);
                    vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);

                    training.setLevel(0, 0);
                    ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, training, training.getUid());
                    ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, training.getUid());
                }

                //Update view UI
                btnPlayGlobal.setVisibility(View.VISIBLE);
                btnPauseGlobal.setVisibility(View.GONE);
                globalGridAdapter.notifyDataSetChanged();
                updatePotencyUi();

                //Chromecast
                ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
                if (exerciseMaxForcePresentationService != null)
                    exerciseMaxForcePresentationService.updateVelocimeters();
            }
        });

        btnPlusGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean allStarted = true;
                for (Training training: trainings) {
                    if (training.getTrainingStatus() != Training.TRAINING_STATUS_PLAY)
                        allStarted = false;
                }

                if (allStarted) {
                    if (SystemClock.elapsedRealtime() - mLastClickPlus < 200)
                        return;

                    mLastClickPlus = SystemClock.elapsedRealtime();

                    for (Training training: trainings){
                        int userPotency = training.getLevel(0);

                        userPotency = userPotency +1;

                        if(userPotency > 100)
                            userPotency = 100;

                        training.setLevel(0, userPotency);
                        vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);
                        ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, training, training.getUid());
                    }
                    updatePotencyUi();
                } else
                    Toast.makeText(getContext(), getString(R.string.training_not_started), Toast.LENGTH_SHORT).show();

            }
        });

        btnMinusGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickMinus < 200)
                    return;

                mLastClickMinus = SystemClock.elapsedRealtime();

                for (Training training: trainings){
                    int userPotency = training.getLevel(0);

                    userPotency = userPotency - 1;

                    if(userPotency < 0)
                        userPotency = 0;

                    training.setLevel(0, userPotency);
                    vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);
                    ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, training, training.getUid());
                }
                updatePotencyUi();
            }
        });

        linearTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesManager pref = PreferencesManager.getInstance(getContext());
                User userCoach = pref.getUser();

                if (userCoach.getUserPmissions().isTimeGlobal())
                    ((ExerciseMultipleActivity)getActivity()).showTimeDialog(trainingGlobal, TimeDialog.FROM_GLOBAL);
            }
        });
    }

    public void doPlay(){
        //PAUSE STATE
        if (((ExerciseMultipleActivity)getActivity()).isStartGlobal()){
            for (Training training: trainings){
                if (training.getTrainingStatus() != Training.TRAINING_STATUS_CHANGE_PROGRAM)
                    training.setTime(((ExerciseMultipleActivity)getActivity()).getTimeGlobalRemaining());

                training.setTrainingStatus(Training.TRAINING_STATUS_RESTART);
                vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);

                switch (connectionType){
                    case Device.DEVICE_WIFI:
                        ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                        ((ExerciseMultipleActivity)getActivity()).sendStartProgram(vestIndex, training.getUid());
                        break;
                    case Device.DEVICE_BLE:
                        ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                        break;
                }
                ((ExerciseMultipleActivity)getActivity()).setStartGlobal(true);
            }
        }
        //STOP STATE
        else {
            for (Training training: trainings){
                training.setTrainingStatus(Training.TRAINING_STATUS_PLAY);
                training.setTime(trainingGlobal.getTime());
                vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);

                switch (connectionType){
                    case Device.DEVICE_WIFI:
                        ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                        ((ExerciseMultipleActivity)getActivity()).sendStartProgram(vestIndex, training.getUid());
                        break;
                    case Device.DEVICE_BLE:
                        ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                        break;
                }
                ((ExerciseMultipleActivity)getActivity()).setStartGlobal(true);
            }
        }
        updateVestIndexGlobal();
        //Update view UI
        btnPlayGlobal.setVisibility(View.GONE);
        btnPauseGlobal.setVisibility(View.VISIBLE);
        globalGridAdapter.notifyDataSetChanged();
    }

    public void doStopChangeProgram(){
        trainingGlobal = ((ExerciseMultipleActivity)getActivity()).getTrainingGlobal();
        trainings = ((ExerciseMultipleActivity)getActivity()).getTrainings();

        int potency = 0;
        for (Training training: trainings){
            vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);
            potency = training.getLoadPotency();

            if (potency < 0)
                potency = 0;

            ((ExerciseMultipleActivity)getActivity()).sendStopButton(vestIndex, training.getUid());
            training.setTrainingStatus(Training.TRAINING_STATUS_CHANGE_PROGRAM);
            training.setLevel(0, potency);
            training.setLoadPotency(potency);
            ((ExerciseMultipleActivity)getActivity()).setPotencyLevel(vestIndex, training, training.getUid());
            updatePotencyUi();
            ((ExerciseMultipleActivity)getActivity()).setStartNormal(false);
            updateTimeText(training.getTime());
            updatePauseUi(0);
            updateEstimulationUi(100);
        }

    }

    public void loadGlobalParticipants(){
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);

        gridGlobal.setHasFixedSize(true);
        gridGlobal.setLayoutManager(layoutManager);
        gridGlobal.setNestedScrollingEnabled(false);

        globalGridAdapter = new GlobalGridAdapter(trainings, getActivity(), ExerciseMultipleGlobalFragment.this, UDPServer.SERVER_NORMAL);
        gridGlobal.setAdapter(globalGridAdapter);

        for (Training training: trainings){
            if (training.getDevice().getDeviceBle() != null){
                final android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        globalGridAdapter.notifyDataSetChanged();
                        handler.postDelayed(this, UPDATE_TIME);
                    }
                }, UPDATE_TIME);
                break;
            }
        }
    }

    public void stopAllUsers(){
        btnStopGlobal.callOnClick();
    }

    public void updateControlsUi(){
        boolean allPaused = true;
        for (Training training: trainings) {
            if (training.getTrainingStatus() == Training.TRAINING_STATUS_PLAY)
                allPaused = false;
        }

        if (allPaused){
            btnPlayGlobal.setVisibility(View.VISIBLE);
            btnPauseGlobal.setVisibility(View.GONE);
        } else {
            btnPlayGlobal.setVisibility(View.GONE);
            btnPauseGlobal.setVisibility(View.VISIBLE);
            updateVestIndexGlobal();
        }
    }

    public void updatePotencyUi() {
        globalGridAdapter.notifyDataSetChanged();
    }

    public void updateEstimulationUi(int estimulationValue) {

        //Chromecast
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updateEstimulationUi(estimulationValue);

        if (estimulationValue == 100){
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 100){
            imgTime1.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 90){
            imgTime2.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime2.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 80){
            imgTime3.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime3.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 70){
            imgTime4.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime4.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 60){
            imgTime5.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime5.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 50){
            imgTime6.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime6.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 40){
            imgTime7.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime7.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 30){
            imgTime8.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime8.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 20){
            imgTime9.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime9.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 10){
            imgTime10.setImageResource(R.drawable.img_progress_global_red);
        } else {
            imgTime10.setImageResource(R.drawable.img_progress_grey_h);
        }
    }

    public void updatePauseUi(int estimulationValue) {

        //Chromecast
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updatePauseUi(estimulationValue);

        if (estimulationValue == 100){
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 100){
            imgTime1.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime1.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 90){
            imgTime2.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime2.setImageResource(R.drawable.img_progress_grey_h);
        } if (estimulationValue < 80){
            imgTime3.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime3.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 70){
            imgTime4.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime4.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 60){
            imgTime5.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime5.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 50){
            imgTime6.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime6.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 40){
            imgTime7.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime7.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 30){
            imgTime8.setImageResource(R.drawable.img_progress_global);
        } else {
            imgTime8.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 20){
            imgTime9.setImageResource(R.drawable.img_progress_global_100);
        } else {
            imgTime9.setImageResource(R.drawable.img_progress_grey_h);
        }if (estimulationValue < 10){
            imgTime10.setImageResource(R.drawable.img_progress_global_100);
        } else {
            imgTime10.setImageResource(R.drawable.img_progress_grey_h);
        }
    }

    public void updateStatus(final int time, boolean isEstimulating, int estimulationMs){

        //UpdateTime
        if (((ExerciseMultipleActivity)getActivity()).isStartGlobal()){
            updateTimeText(time);

            float progressEstimulation = 0;
            int esimulationTime = trainingGlobal.getEstimulationTime();
            int pauseTime = trainingGlobal.getPauseTime();

            //Update Progress bar
            if (isEstimulating){
                progressEstimulation = ((float) estimulationMs / esimulationTime)*100;
                updateEstimulationUi((int)progressEstimulation);

                if (estimulationMs < 200){
                    for (Training training: trainings){
                        if (training.getTrainingStatus() == Training.TRAINING_STATUS_RESTART){
                            training.setTrainingStatus(Training.TRAINING_STATUS_PLAY);
                            vestIndex = ((ExerciseMultipleActivity)getActivity()).getVestIndexUser(training);

                            switch (connectionType){
                                case Device.DEVICE_WIFI:
                                    ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                                    ((ExerciseMultipleActivity)getActivity()).sendStartProgram(vestIndex, training.getUid());
                                    break;
                                case Device.DEVICE_BLE:
                                    ((ExerciseMultipleActivity)getActivity()).sendProgram(vestIndex, training, training.getUid());
                                    break;
                            }
                        }
                    }
                }
            } else {
                progressEstimulation = ((float) estimulationMs / pauseTime)*100;
                updatePauseUi((int)progressEstimulation);
            }
        }
    }

    public void updateTimeText(int time){

        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.loadTime(time);

        String minTens, minUnits, secTens, secUnits;
        if (time >= 1){
            int mins = time/60;
            int seconds = time%60;

            if (mins >= 10){
                minTens = Integer.toString(mins).split("(?<=.)")[0];
                minUnits = Integer.toString(mins).split("(?<=.)")[1];
            } else {
                minTens = "0";
                minUnits = Integer.toString(mins).split("(?<=.)")[0];
            }

            if (seconds >= 10){
                secTens = Integer.toString(seconds).split("(?<=.)")[0];
                secUnits = Integer.toString(seconds).split("(?<=.)")[1];
            } else {
                secTens = "0";
                secUnits = Integer.toString(seconds).split("(?<=.)")[0];
            }
        } else {
            minTens = "0";
            minUnits = "0";
            secTens = "0";
            secUnits = "0";

            btnStopGlobal.callOnClick();
            ((ExerciseMultipleActivity)getActivity()).sendResultsTraining();
        }

        txtMinTens.setText(minTens);
        txtMinUnits.setText(minUnits);
        txtSecTens.setText(secTens);
        txtSecUnits.setText(secUnits);
    }

    public void updateVestIndexGlobal(){
        for (Training training: trainings){
            if (training.getTrainingStatus() == Training.TRAINING_STATUS_PLAY){
                ((ExerciseMultipleActivity)getActivity()).setVestIndexGlobal(training);
                break;
            }
        }
    }

    public void updatePotencyClickDevice(Training training, int potencyDevice){
        trainings.get(trainings.indexOf(training)).setLevel(0, potencyDevice);
        updatePotencyUi();
    }

    public void updateStopDevice(Training trainingStop){
        for (Training training: trainings){
            if (training.getUid().equals(trainingStop.getUid())){
                training.setTrainingStatus(Training.TRAINING_STATUS_PAUSE);
                training.setLevel(0, 0);
                globalGridAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateRestartDevice(Training trainingRestart, Context context){
        updateVestIndexGlobal();
        trainingRestart.setTime(((ExerciseMultipleActivity)context).getTimeGlobalRemaining());
        boolean allPaused = true;

        for (Training training: trainings) {
            if (training.getTrainingStatus() == Training.TRAINING_STATUS_PLAY)
                allPaused = false;
        }

        if (allPaused){
            for (Training training: trainings) {
                if (training.getUid().equals(trainingRestart.getUid()))
                    training.setTrainingStatus(Training.TRAINING_STATUS_PLAY);
            }

            vestIndex = ((ExerciseMultipleActivity)context).getVestIndexUser(trainingRestart);
            ((ExerciseMultipleActivity)context).sendProgram(vestIndex, trainingRestart, trainingRestart.getUid());
            ((ExerciseMultipleActivity)context).sendStartProgram(vestIndex, trainingRestart.getUid());
        } else {
            for (Training training: trainings) {
                if (training.getUid().equals(trainingRestart.getUid()))
                    training.setTrainingStatus(Training.TRAINING_STATUS_RESTART);
            }
        }
        updateControlsUi();
        globalGridAdapter.notifyDataSetChanged();
    }

    public void updateForceMaxForce(String deviceAddress, int force){
        for (Training training: trainings)
            if (training.getDevice().getDeviceBle()!=null && training.getDevice().getDeviceBle().getAddress().equals(deviceAddress))
                training.setForceMaxForce(force);
    }

    public void updateBleStatus(boolean status, String adress) {
        for (Training training: trainings)
            if (training.getDevice().getDeviceBleAdress().equals(adress))
                training.getUser().setBleAvailable(status);

        globalGridAdapter.notifyDataSetChanged();
    }
}