package com.myofx.emsapp.ui.chromecast.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.utils.ImageUtils;

import java.util.ArrayList;

/**
 * Created by Gerard on 5/5/2017.
 */

public class ParticipantsChromecastAdapter extends RecyclerView.Adapter<ParticipantsChromecastAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Training> participants;

    public ParticipantsChromecastAdapter(ArrayList<Training> participants, Context context) {
        this.participants = participants;
    }

    @Override
    public int getItemCount() {
        return participants.size();
    }

    @Override
    public void onBindViewHolder(ParticipantsChromecastAdapter.ViewHolder holder, final int position) {
        holder.relativeUser.setVisibility(View.VISIBLE);
        holder.txtPotency.setText("" + participants.get(position).getLevel(0));

        if (participants.get(position).getImage() != null)
            ImageUtils.loadImageFromUrl(holder.imgUser, BuildConfig.IMAGE_PATH_URL + participants.get(position).getUser().getImage());

        if (participants.get(position).getDevice().getDeviceBle() != null) {
            holder.linearMax.setVisibility(View.VISIBLE);
            holder.relativeTotal.setVisibility(View.VISIBLE);
            holder.linearMaxForce.setVisibility(View.VISIBLE);
        } else {
            holder.linearMax.setVisibility(View.INVISIBLE);
            holder.relativeTotal.setVisibility(View.INVISIBLE);
            holder.linearMaxForce.setVisibility(View.INVISIBLE);
        }

        holder.txtForce.setText(""+participants.get(position).getForceMaxForce());
        holder.txtTotal.setText(""+participants.get(position).getForceTotal()+"Kg");
        holder.txtForceMax.setText(""+participants.get(position).getForceMax()+"Kg");
        holder.txtName.setText(""+participants.get(position).getUser().getName());

        if (participants.get(position).getUser().getLastName() != null)
            holder.txtName.setText(participants.get(position).getUser().getName()+" "+participants.get(position).getUser().getLastName().charAt(0)+".");
        else
            holder.txtName.setText(participants.get(position).getUser().getName());
    }

    @Override
    public ParticipantsChromecastAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_participant_chromecast_grid, viewGroup, false);
        return new ParticipantsChromecastAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private ImageView imgUser;
        private TextView txtPotency;
        private RelativeLayout relativeUser;
        private TextView txtForce;
        private TextView txtTotal;
        private TextView txtForceMax;
        private LinearLayout linearMax;
        private RelativeLayout relativeTotal;
        private LinearLayout linearMaxForce;
        private TextView txtName;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            imgUser = (ImageView) mView.findViewById(R.id.imgUser);
            txtPotency = (TextView) mView.findViewById(R.id.txtPotency);
            relativeUser = (RelativeLayout) mView.findViewById(R.id.relativeUser);
            txtForce = (TextView) mView.findViewById(R.id.txtForce);
            txtTotal = (TextView) mView.findViewById(R.id.txtTotal);
            txtForceMax = (TextView) mView.findViewById(R.id.txtForceMax);
            linearMaxForce = (LinearLayout) mView.findViewById(R.id.linearMaxForce);
            linearMax = (LinearLayout) mView.findViewById(R.id.linearMax);
            relativeTotal = (RelativeLayout) mView.findViewById(R.id.relativeTotal);
            txtName = (TextView) mView.findViewById(R.id.txtName);
        }
    }

}
