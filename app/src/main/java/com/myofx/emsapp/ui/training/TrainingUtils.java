package com.myofx.emsapp.ui.training;

import android.content.Context;

import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.server.Vest;

import java.util.ArrayList;

public class TrainingUtils {

    public static ArrayList<Training> getTrainings(Context ctx){
        ArrayList<Training> trainings = new ArrayList<>();

        //ADAPT
        Training training = new Training("id_adapt", "ADAPT", new int[]{1, 85, 1000, 0, 350, 0, 0, 300, 0, 1, 150},"");
        trainings.add(training);

        //BASIC
        training = new Training("id_basic","BASIC", new int[]{1, 85, 4000, 4000, 350, 1000, 0, 720, 0, 1, 150},"");
        trainings.add(training);

        //RELAX
        training = new Training("id_relax","RELAX", new int[]{1, 100, 1000, 1000, 150, 0, 0, 300, 0, 1, 150},"");
        trainings.add(training);

        //ADVANCED
        training = new Training("id_advanced","ADVANCED", new int[]{1, 85, 4000, 4000, 350, 0, 0, 720, 0, 1, 150},"");
        trainings.add(training);

        //METABOLIC
        training = new Training("id_metabolic","METABOLIC", new int[]{1, 7, 1000, 0, 350, 0, 0, 300, 0, 1, 150},"");
        trainings.add(training);

        //AEROBIC
        training = new Training("id_aerobic","AEROBIC", new int[]{1, 20, 1000, 0, 350, 0, 0, 300, 0, 1, 150},"");
        trainings.add(training);

        //ADVANCED CHRONAXY ADJUSTED
        training = new Training("id_advanced_adjusted","ADVANCED CHRONAXY ADJUSTED", new int[]{1, 85, 4000, 4000, 350, 0, 0, 720, 0, 1, 150},"");
        addChronaxyTraining(training);
        trainings.add(training);

        return trainings;
    }

    public static Training addChronaxyTraining(Training training){
        training.setChronaxy(Vest.CHANNEL_LEG,350);
        training.setChronaxy(Vest.CHANNEL_GLU,300);
        training.setChronaxy(Vest.CHANNEL_LUM,180);
        training.setChronaxy(Vest.CHANNEL_SHO,250);
        training.setChronaxy(Vest.CHANNEL_BIC,150);
        training.setChronaxy(Vest.CHANNEL_DOR,250);
        training.setChronaxy(Vest.CHANNEL_ABS,300);
        training.setChronaxy(Vest.CHANNEL_PEC,250);
        training.setChronaxy(Vest.CHANNEL_AUX,150);
        training.setChronaxy(Vest.CHANNEL_AUX_2,150);
        return training;
    }
}
