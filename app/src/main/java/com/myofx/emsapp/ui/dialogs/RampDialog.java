package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 28/3/2017.
 */

public class RampDialog extends DialogFragment {

    public static final String EXTRA_RAMP_UP = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_IMPULSE_ESTIM";
    public static final String EXTRA_RAMP_DWON = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_IMPULSE_PAUSE";

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private ImageView imgUpPlus;
    private ImageView imgUpMinus;
    private ImageView imgDownPlus;
    private ImageView imgDownMinus;
    private EditText editRampUp;
    private EditText editRampDwon;

    //Variable
    private RampDialog.OnConfirmationRampListener mCallback;
    private Activity exerciseActivity;
    private int rampUpValue = 0;
    private int rampDownValue = 0;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        rampUpValue = getArguments().getInt(EXTRA_RAMP_UP);
        rampDownValue = getArguments().getInt(EXTRA_RAMP_DWON);

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadRampValues();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static RampDialog newInstance(Activity exerciseActivity, int rampUp, int rampDown) {
        RampDialog rampDialog = new RampDialog();
        Bundle args = new Bundle();
        args.putInt(EXTRA_RAMP_UP, rampUp);
        args.putInt(EXTRA_RAMP_DWON, rampDown);
        rampDialog.exerciseActivity = exerciseActivity;
        rampDialog.setArguments(args);
        return rampDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ramp, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        imgUpPlus = (ImageView) view.findViewById(R.id.imgEstimPlus);
        imgUpMinus = (ImageView) view.findViewById(R.id.imgEstimMinus);
        imgDownPlus = (ImageView) view.findViewById(R.id.imgPausePlus);
        imgDownMinus = (ImageView) view.findViewById(R.id.imgPauseMinus);
        editRampUp = (EditText) view.findViewById(R.id.editRampUp);
        editRampDwon = (EditText) view.findViewById(R.id.editRampDwon);

        setListeners();

        return view;
    }

    public void loadRampValues(){
        editRampUp.setText(""+((double)rampUpValue/1000));
        editRampDwon.setText(""+((double)rampDownValue/1000));
    }

    public void setListeners() {
        imgUpPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rampUpValue = (int)(Double.parseDouble(editRampUp.getText().toString())*1000);
                rampUpValue = rampUpValue +100;

                if (rampUpValue > 10000)
                    rampUpValue = 10000;

                editRampUp.setText(""+((double)rampUpValue/1000));
            }
        });

        imgUpMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rampUpValue = (int)(Double.parseDouble(editRampUp.getText().toString())*1000);
                rampUpValue = rampUpValue -100;

                if (rampUpValue < 0)
                    rampUpValue = 0;

                editRampUp.setText(""+((double)rampUpValue/1000));
            }
        });

        imgDownPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rampDownValue = (int)(Double.parseDouble(editRampDwon.getText().toString())*1000);
                rampDownValue = rampDownValue +100;

                if (rampDownValue > 10000)
                    rampDownValue = 10000;

                editRampDwon.setText(""+((double)rampDownValue/1000));
            }
        });

        imgDownMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rampDownValue = (int)(Double.parseDouble(editRampDwon.getText().toString())*1000);
                rampDownValue = rampDownValue -100;

                if (rampDownValue < 0)
                    rampDownValue = 0;

                editRampDwon.setText(""+((double)rampDownValue/1000));
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, (int)(Double.parseDouble(editRampUp.getText().toString())*1000), (int)(Double.parseDouble(editRampDwon.getText().toString())*1000));
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editRampUp.getText().toString()))
                    editRampUp.setText(""+0);

                if (TextUtils.isEmpty(editRampDwon.getText().toString()))
                    editRampDwon.setText(""+0);

                sendResult(true, (int)(Double.parseDouble(editRampUp.getText().toString())*1000), (int)(Double.parseDouble(editRampDwon.getText().toString())*1000));
            }
        });
    }

    public interface OnConfirmationRampListener {
        void onConfirmationRamp(boolean send, int rampUpValue, int rampDownValue);
    }

    private void sendResult(boolean send, int rampUpValue, int rampDownValue) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (rampUpValue > 10000)
                rampUpValue = 10000;
            else if (rampUpValue < 0)
                rampUpValue = 0;

            if (rampDownValue > 10000)
                rampDownValue = 10000;
            else if (rampDownValue < 0)
                rampDownValue = 0;

            mCallback.onConfirmationRamp(send, rampUpValue, rampDownValue);
        }
    }
}