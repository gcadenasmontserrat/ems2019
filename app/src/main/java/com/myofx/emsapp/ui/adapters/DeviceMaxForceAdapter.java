package com.myofx.emsapp.ui.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.MainActivity;

import java.util.ArrayList;

/**
 * Created by Gerard on 2/10/2017.
 */

public class DeviceMaxForceAdapter extends RecyclerView.Adapter<DeviceMaxForceAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<BluetoothDevice> devices;
    private Context context;

    public DeviceMaxForceAdapter(ArrayList<BluetoothDevice> devices, Context context) {
        this.devices = devices;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void onBindViewHolder(DeviceMaxForceAdapter.ViewHolder itemsViewHolder, final int position) {
        DeviceMaxForceAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).assignDevice(devices.get(position));
            }
        });

        if (devices.get(position).getName() != null && devices.get(position).getName().length() > 0)
            itemsViewHolder.txtName.setText(devices.get(position).getName());
        else
            itemsViewHolder.txtName.setText(context.getString(R.string.device_no_name));

        itemsViewHolder.txtAdress.setText(devices.get(position).getAddress());
    }

    @Override
    public DeviceMaxForceAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_maxforce, viewGroup, false);
        return new DeviceMaxForceAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static View mView;
        private TextView txtName;
        private TextView txtAdress;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            txtName = (TextView) mView.findViewById(R.id.txtName);
            txtAdress = (TextView) mView.findViewById(R.id.txtAdress);
        }
    }

}