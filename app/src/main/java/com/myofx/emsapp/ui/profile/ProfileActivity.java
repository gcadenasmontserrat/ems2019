package com.myofx.emsapp.ui.profile;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.config.Constants;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.DateUtils;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.ImageUtils;

/**
 * Created by Gerard on 28/11/2016.
 */

public class ProfileActivity extends BaseActivity {

    public static final String EXTRA_PROFILE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_PROFILE";

    //UI
    private EditText editNickname;
    private EditText editEmail;
    private EditText editName;
    private EditText editDate;
    private EditText editAddress;
    private EditText editCity;
    private EditText editPostalCode;
    private EditText editCountry;
    private EditText editWeb;
    private EditText editHeight;
    private EditText editWeight;
    private EditText editFats;
    private EditText editNonFat;
    private EditText editWaist;
    private EditText editLeftLeg;
    private EditText editRightLeg;
    private EditText editLeftArm;
    private EditText editRightArm;
    private EditText editMetabolic;
    private EditText editStrength;
    private EditText editSytolic;
    private EditText editDiastolic;
    private RelativeLayout relativeTitlePersonal;
    private RelativeLayout relativeTitleCorporal;
    private RelativeLayout relativeBackground;
    private TextView txtTitlePersonal;
    private TextView txtTitleCorporal;
    private ImageView imgPersonalInfo;
    private ImageView imgCorporalInfo;
    private ImageView imgUser;
    private ImageView imgBackground;
    private TextView txtName;
    private LinearLayout linearLoading;
    private TextView txtLoading;

    //Variables
    private User profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //GA
        GAUtils.trackScreen(ProfileActivity.this, GAConfig.SCREEN_PORTADA + GAConfig.SCREEN_ALUMNOS_VER, GAConfig.SCREEN_PORTADA);

        if (getIntent().hasExtra(EXTRA_PROFILE)) {
            profile = (User )getIntent().getSerializableExtra(EXTRA_PROFILE);
        }

        bindUi();
        loadTheme();
        loadToolbar();
        setData();
    }

    public void bindUi(){
        editName = findViewById(R.id.editName);
        editDate = findViewById(R.id.editDate);
        editAddress = findViewById(R.id.editAddress);
        editCity = findViewById(R.id.editCity);
        editPostalCode = findViewById(R.id.editPostalCode);
        editCountry = findViewById(R.id.editCountry);
        editWeb = findViewById(R.id.editWeb);
        editEmail = findViewById(R.id.editEmail);
        editNickname = findViewById(R.id.editNickname);
        editHeight = findViewById(R.id.editHeight);
        editWeight = findViewById(R.id.editWeight);
        editFats = findViewById(R.id.editFats);
        editNonFat = findViewById(R.id.editNonFat);
        editWaist = findViewById(R.id.editWaist);
        editLeftLeg = findViewById(R.id.editLeftLeg);
        editRightLeg = findViewById(R.id.editRightLeg);
        editMetabolic = findViewById(R.id.editMetabolic);
        editStrength = findViewById(R.id.editStrength);
        editSytolic = findViewById(R.id.editSytocil);
        editDiastolic = findViewById(R.id.editDiastolic);
        relativeTitlePersonal = findViewById(R.id.relativeTitlePersonal);
        relativeTitleCorporal = findViewById(R.id.relativeTitleCorporal);
        relativeBackground = findViewById(R.id.relativeBackground);
        txtTitlePersonal = findViewById(R.id.txtTitlePersonal);
        txtTitleCorporal = findViewById(R.id.txtTitleCorporal);
        imgPersonalInfo = findViewById(R.id.imgPersonalInfo);
        imgCorporalInfo = findViewById(R.id.imgCorporalInfo);
        imgUser = findViewById(R.id.imgUser);
        imgBackground = findViewById(R.id.imgBackground);
        txtName = findViewById(R.id.txtName);
        linearLoading = findViewById(R.id.linearLoading);
        txtLoading = findViewById(R.id.txtLoading);
        editLeftArm = findViewById(R.id.editLeftArm);
        editRightArm = findViewById(R.id.editRightArm);

        setEnableForm(false);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(ProfileActivity.this));
        relativeTitlePersonal.setBackgroundColor(ColorTheme.getLightBackgroundColor(ProfileActivity.this));
        relativeTitleCorporal.setBackgroundColor(ColorTheme.getLightBackgroundColor(ProfileActivity.this));
        txtTitlePersonal.setTextColor(ColorTheme.getAccentColor(ProfileActivity.this));
        txtTitleCorporal.setTextColor(ColorTheme.getAccentColor(ProfileActivity.this));
        imgPersonalInfo.setColorFilter(ColorTheme.getIconsColor(ProfileActivity.this));
        imgCorporalInfo.setColorFilter(ColorTheme.getIconsColor(ProfileActivity.this));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(ProfileActivity.this));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(ProfileActivity.this));
    }

    public void loadToolbar(){
        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(ProfileActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(ProfileActivity.this));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(MainActivity.RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(MainActivity.RESULT_CANCELED);
        finish();
        super.onBackPressed();
    }

    public void setEnableForm(boolean enabled){
        editName.setEnabled(enabled);
        editDate.setEnabled(enabled);
        editAddress.setEnabled(enabled);
        editCity.setEnabled(enabled);
        editPostalCode.setEnabled(enabled);
        editCountry.setEnabled(enabled);
        editWeb.setEnabled(enabled);
        editEmail.setEnabled(enabled);
        editNickname.setEnabled(enabled);
        editHeight.setEnabled(enabled);
        editWeight.setEnabled(enabled);
        editFats.setEnabled(enabled);
    }

    public void setData(){

        ImageUtils.loadImageFromDrawable(imgBackground, ImageUtils.getImageIdByName("delete_background_blur",this));

        if (profile.getImage() != null) {
            ImageUtils.loadImageFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+profile.getImage());
        }

        //Personal
        txtName.setText(profile.getName());
        editName.setText(profile.getName());

        if (profile.getBirthdate() != null)
            editDate.setText(DateUtils.changeDateFormat(profile.getBirthdate()));

        if (profile.getAddress() != null)
            editAddress.setText(profile.getAddress());

        if (profile.getCity() != null)
            editCity.setText(profile.getCity());

        if (profile.getPostalCode() != null)
            editPostalCode.setText(profile.getPostalCode());

        if (profile.getCountry() != null)
            editCountry.setText(profile.getCountry());

        if (profile.getWeb() != null)
            editWeb.setText(profile.getWeb());

        if (profile.getEmail() != null)
            editEmail.setText(profile.getEmail());

        if (profile.getNickname() != null)
            editNickname.setText(profile.getNickname());

        if (profile.getGender().equals(Constants.GENDER_MAN)){
            imgPersonalInfo.setColorFilter(ContextCompat.getColor(this,R.color.gender_m));
            imgCorporalInfo.setColorFilter(ContextCompat.getColor(this,R.color.gender_m));
        } else {
            imgPersonalInfo.setColorFilter(ContextCompat.getColor(this,R.color.gender_f));
            imgCorporalInfo.setColorFilter(ContextCompat.getColor(this,R.color.gender_f));
        }

        //Corporal
        editHeight.setText(""+profile.getMeasures().getHeight());
        editWeight.setText(""+profile.getMeasures().getWeight());
        editFats.setText(""+profile.getMeasures().getBodyFat());
        editNonFat.setText(""+profile.getMeasures().getNonFatMass());
        editWaist.setText(""+profile.getMeasures().getWaistMeasurement());
        editRightArm.setText(""+profile.getMeasures().getRightArmMeasurement());
        editLeftArm.setText(""+profile.getMeasures().getLeftArmMeasurement());
        editLeftLeg.setText(""+profile.getMeasures().getLeftLegMeasurement());
        editRightLeg.setText(""+profile.getMeasures().getRightLegMeasurement());
        editStrength.setText(""+profile.getMeasures().getStrength());
        editSytolic.setText(""+profile.getMeasures().getSystolicBloodPressure());
        editDiastolic.setText(""+profile.getMeasures().getDiastolicBloodPressure());
    }
}
