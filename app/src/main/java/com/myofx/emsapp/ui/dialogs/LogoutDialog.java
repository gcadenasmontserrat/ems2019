package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.settings.SettingsFragment;

/**
 * Created by Gerard on 30/11/2016.
 */

public class LogoutDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;

    //Variable
    private OnConfirmationLogoutListener mCallback;
    private Fragment settingsFragment;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = (SettingsFragment) settingsFragment;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static LogoutDialog newInstance(Fragment settingsFragment) {
        LogoutDialog logoutDialog = new LogoutDialog();
        logoutDialog.settingsFragment = settingsFragment;
        return logoutDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_logout, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnAccept = (Button) view.findViewById(R.id.btnAccept);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true);
            }
        });
    }

    public interface OnConfirmationLogoutListener {
        void onConfirmationLogout(boolean delete);
    }

    private void sendResult(boolean delete) {
        if(mCallback != null) {
            mCallback.onConfirmationLogout(delete);
            alertdialog.dismiss();
        }
    }
}
