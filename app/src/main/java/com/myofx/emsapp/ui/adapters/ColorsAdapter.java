package com.myofx.emsapp.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Color;

import java.util.ArrayList;


/**
 * Created by Gerard on 21/12/2016.
 */

public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ViewHolder> {

    //VARIABLE
    private ArrayList<Color> colors;
    private Context context;

    public ColorsAdapter(ArrayList<Color> colors, Context context) {
        this.colors = colors;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    @Override
    public void onBindViewHolder(ColorsAdapter.ViewHolder holder, final int position) {

        holder.linearColor.setBackgroundColor(ContextCompat.getColor(context, colors.get(position).getColorPrimary()));

        if (colors.get(position).isSelected())
            holder.imgSelected.setVisibility(View.VISIBLE);
        else
            holder.imgSelected.setVisibility(View.GONE);

        ColorsAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                colors.get(position).setSelected(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public ColorsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_color, viewGroup, false);
        return new ColorsAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View linearColor;
        private ImageView imgSelected;
        private static View mView;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            linearColor =  mView.findViewById(R.id.linearColor);
            imgSelected = (ImageView) mView.findViewById(R.id.imgSelected);
        }
    }

    public void clearSelected(){
        for (Color color: colors)
            color.setSelected(false);
    }
}