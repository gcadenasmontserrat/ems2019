package com.myofx.emsapp.ui.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.PushTokenEvent;
import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.GeneralWebViewActivity;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Gerard on 28/11/2016.
 */

public class LoginActivity extends BaseActivity {

    //UI
    private TextView txtErrorUsername;
    private TextView txtErrorPassword;
    private EditText editUsername;
    private EditText editPassword;
    private Button btnEnter;
    private TextView txtRememberPass;
    private TextView txtInformation;
    private TextView txtSupport;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private VideoView videoViewBg;
    private CheckBox cbLocal;

    //Variables
    private PreferencesManager pref;
    private String user;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = PreferencesManager.getInstance(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //GA
        GAUtils.trackScreen(LoginActivity.this, GAConfig.SCREEN_LOGIN + GAConfig.SCREEN_LOGIN_LOGIN, GAConfig.SCREEN_LOGIN );

        bindUi();
        loadTheme();
        setListeners();
    }

    public void bindUi(){
        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        btnEnter = findViewById(R.id.btnEnter);
        txtRememberPass = findViewById(R.id.txtRememberPass);
        txtInformation = findViewById(R.id.txtInformation);
        txtSupport = findViewById(R.id.txtSupport);
        txtErrorUsername = findViewById(R.id.txtErrorUsername);
        txtErrorPassword = findViewById(R.id.txtErrorPassword);
        linearLoading = findViewById(R.id.linearLoading);
        txtLoading = findViewById(R.id.txtLoading);
        videoViewBg = findViewById(R.id.videoViewBg);
        cbLocal = findViewById(R.id.cbLocal);
    }

    public void loadTheme(){
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(LoginActivity.this));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(LoginActivity.this));
    }

    public void setListeners() {
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        txtRememberPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RecoveryPassActivity.class);
                startActivity(intent);
            }
        });

        txtInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goGeneralWebViewActivity(getString(R.string.settings_information), pref.getAppInfo().getLinks().getProductInfo());
            }
        });

        txtSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goGeneralWebViewActivity(getString(R.string.settings_support), pref.getAppInfo().getLinks().getTechnicalSupport());
            }
        });

        editUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorUsername.setVisibility(View.INVISIBLE);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorPassword.setVisibility(View.INVISIBLE);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO){
                    attemptLogin();
                }
                return false;
            }
        });
    }

    public void goGeneralWebViewActivity(String title, String url){
        Intent intent = new Intent(LoginActivity.this, GeneralWebViewActivity.class);
        intent.putExtra(GeneralWebViewActivity.EXTRA_TITLE, title);
        intent.putExtra(GeneralWebViewActivity.EXTRA_URL, url);
        startActivity(intent);
    }

    public void attemptLogin() {
        user = editUsername.getText().toString().trim();
        password = editPassword.getText().toString().trim();

        if (!TextUtils.isEmpty(user)){
            if (TextUtils.isEmpty(user)) {
                txtErrorUsername.setText(getString(R.string.error_required));
                txtErrorUsername.setVisibility(View.VISIBLE);
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(user).matches()) {
                txtErrorUsername.setText(getString(R.string.error_bad_username));
                txtErrorUsername.setVisibility(View.VISIBLE);
            } else if (TextUtils.isEmpty(password)) {
                txtErrorPassword.setText(getString(R.string.error_required));
                txtErrorPassword.setVisibility(View.VISIBLE);
            }  else if (!cbLocal.isChecked()){
                doLogin(user, password);
            } else {
                doLoginLocal(user, password);
            }
        }
    }

    public void goMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void doLogin(String user, String password){
        EmsApi.getInstance().doLoginTask(user, password);
        txtLoading.setText(getString(R.string.loading_login));
        linearLoading.setVisibility(View.VISIBLE);
    }

    public void doLoginLocal(String email, String password) {
        User user = AppDatabase.checkUserCredential(this, password, email);

        if (user != null) {
            EmsApp.setModeLocal(true);
            openMainActivity(user);
        } else
            showInfoLocalMode();
    }

    @Subscribe
    public void onGetMainEvent(UserEvent event) throws Exception {
        EventBus.getDefault().removeStickyEvent(event);

        EmsApp.setModeLocal(false);
        openMainActivity(event.getUser());
        AppDatabase.addLoginUser(this, password, event.getUser());
    }

    public void openMainActivity(User user){
        pref.saveUser(user);
        pref.saveProfile(user.getId());
        pref.saveAccessToken(user.getAccesToken());
        pref.saveRefreshToken(user.getRefreshToken());
        pref.saveColorTheme(ColorTheme.getThemeFromColor(user.getConfiguration().getColor()));

//        //Register push token
//        EmsApi.getInstance().doRegisterDevice(FirebaseInstanceId.getInstance().getInstanceId().getResult().getToken());

        goMainActivity();
    }

    @Subscribe
    public void onGetPushTokenEvent(PushTokenEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        linearLoading.setVisibility(View.GONE);
        goMainActivity();
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        Toast.makeText(LoginActivity.this, event.getMsgError(), Toast.LENGTH_SHORT).show();
        linearLoading.setVisibility(View.GONE);

        doLoginLocal(user, password);
    }

    @Subscribe
    public void onErrorEvent(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(event.getError().getMessageError()))
            Toast.makeText(LoginActivity.this, event.getError().getMessageError(), Toast.LENGTH_SHORT).show();
        else  if (!TextUtils.isEmpty(event.getError().getErrorDescription()))
            Toast.makeText(LoginActivity.this, event.getError().getErrorDescription(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume(){
        super.onResume();
        initVideo();
    }

    public void initVideo(){
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.intro);
        videoViewBg.setVideoURI(uri);
        videoViewBg.start();

        videoViewBg.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    private void showInfoLocalMode() {
        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle(getString(R.string.dialog_offline));
        alertDialog.setMessage(getString(R.string.dialog_offline_new));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.dialog_accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
