package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Training;

/**
 * Created by Gerard on 8/8/2017.
 */

public class DialogTrainingInformation extends DialogFragment {

    public static final String EXTRA_TRAINING = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_TRAINING";

    //Ui
    private AlertDialog alertdialog;
    private Button btnClose;
    private TextView txtDialog;
    private TextView txtTime;
    private TextView txtFrequency;
    private TextView txtFrequencyRelax;
    private TextView txtPotencyRelax;
    private TextView txtPulse;
    private TextView txtChronaxy;
    private TextView txtEstimTime;
    private TextView txtPauseTime;
    private TextView txtRampUp;
    private TextView txtRampDwon;

    //Variable
    private Training training;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        training = (Training) getArguments().getSerializable(EXTRA_TRAINING);

        loadTraining();

        alertdialog = builder.create();
        return alertdialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_info_training, null);
        btnClose = (Button) view.findViewById(R.id.btnClose);
        txtDialog = (TextView) view.findViewById(R.id.txtDialog);
        txtTime = (TextView) view.findViewById(R.id.txtTime);
        txtFrequency = (TextView) view.findViewById(R.id.txtFrequency);
        txtFrequencyRelax = (TextView) view.findViewById(R.id.txtFrequencyRelax);
        txtPotencyRelax = (TextView) view.findViewById(R.id.txtPotencyRelax);
        txtPulse = (TextView) view.findViewById(R.id.txtPulse);
        txtChronaxy = (TextView) view.findViewById(R.id.txtChronaxy);
        txtEstimTime = (TextView) view.findViewById(R.id.txtEstimTime);
        txtPauseTime = (TextView) view.findViewById(R.id.txtPauseTime);
        txtRampUp = (TextView) view.findViewById(R.id.txtRampUp);
        txtRampDwon = (TextView) view.findViewById(R.id.txtRampDwon);

        setListeners();

        return view;
    }

    public void setListeners(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialog.dismiss();
            }
        });
    }


    public static DialogTrainingInformation newInstance(Training training) {
        DialogTrainingInformation dialogTrainingInformation = new DialogTrainingInformation();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_TRAINING, training);
        dialogTrainingInformation.setArguments(args);
        return dialogTrainingInformation;
    }

    public void loadTraining(){

        int time = training.getTime();
        if (time > 1){
            String aux1 = "";
            String aux2 = "";
            if((time/60)<10)
                aux1="0";
            if((time%60)<10)
                aux2="0";
            txtTime.setText(": "+aux1+time/60+":"+ aux2 +time%60);
        }

        txtDialog.setText(training.getTitle());
        txtFrequency.setText(": "+training.getFrequency());
        txtFrequencyRelax.setText(": "+training.getFrequencyRelax());
        txtPotencyRelax.setText(": "+training.getPotencyRelax());
        txtPulse.setText(": "+training.getPulseRelax());
        txtChronaxy.setText(": "+training.getChronaxyDefault());
        txtEstimTime.setText(": "+((double)training.getEstimulationTime()/1000));
        txtPauseTime.setText(": "+((double)training.getPauseTime()/1000));
        txtRampUp.setText(": "+((double)training.getRampUp()/1000));
        txtRampDwon.setText(": "+((double)training.getRampDown()/1000));
    }

}
