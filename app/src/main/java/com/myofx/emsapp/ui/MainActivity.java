package com.myofx.emsapp.ui;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.events.DeviceStatusEvent;
import com.myofx.emsapp.api.events.PotencyEvent;
import com.myofx.emsapp.api.events.ProgressExerciceEvent;
import com.myofx.emsapp.api.events.StartEvent;
import com.myofx.emsapp.api.events.StopEvent;
import com.myofx.emsapp.bluetooth.BluetoothEmsCommands;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.dfu.DfuService;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.server.DataParser;
import com.myofx.emsapp.server.ServerAppModel;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.dialogs.ConfirmationUpdateDialog;
import com.myofx.emsapp.ui.dialogs.DeviceNameDialog;
import com.myofx.emsapp.ui.dialogs.InformationDialog;
import com.myofx.emsapp.ui.dialogs.ScanDevicesDialog;
import com.myofx.emsapp.ui.dialogs.ScanEmsBleDialog;
import com.myofx.emsapp.ui.dialogs.ScanMaxForceDialog;
import com.myofx.emsapp.ui.dialogs.UpdateDialog;
import com.myofx.emsapp.ui.diary.DiaryFragment;
import com.myofx.emsapp.ui.local.LocalFragment;
import com.myofx.emsapp.ui.settings.SettingsFragment;
import com.myofx.emsapp.ui.students.StudentsFragment;
import com.myofx.emsapp.ui.training.TrainingParticipantsFragment;
import com.myofx.emsapp.ui.training.VideoActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.fonts.CustomFontNavigationView;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

/**
 * Created by Gerard on 29/11/2016.
 */

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, DeviceNameDialog.OnConfirmationNameListener,
        ConfirmationUpdateDialog.OnConfirmationUpdateListener {

    //UI
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private CustomFontNavigationView navigationView;
    private View headerMenu;
    private TextView txtTitle;
    private ImageView imgUser;
    private TextView txtName;
    private LinearLayout linearHeaderMenu;

    //Variables
    private ActionBarDrawerToggle toggle;
    private int idSectionSelected = R.id.itemDashboard;
    private boolean recreate = false;
    private static ArrayList<String> checkDevicesId = new ArrayList<>();
    private static boolean startCheckDevice = false;

    //Server connection
    private static PreferencesManager pref;
    private ScanDevicesDialog scanDevicesDialog;
    public UDPServer server;
    public static ArrayList<byte[]> UIDsBroad = new ArrayList<>();
    public static ArrayList<String> nameBroad = new ArrayList<>();
    public static ArrayList<InetAddress> ipBroad = new ArrayList<>();
    public static ArrayList<int[]> listChannelsVest = new ArrayList<>();
    public ArrayList<int[]> listProgramsVest = new ArrayList<>();
    public ArrayList<byte[]> ipsOrderList = new ArrayList<>();
    public static ServerAppModel data = new ServerAppModel();
    public static InetAddress ip;
    public static byte[] message;
    private IntentFilter myServerFilter;
    private ArrayList<Device> devices;
    private int connectionType = Device.DEVICE_BLE;

    //Bluetooth
    private Device deviceSelected;
    private ArrayList<BluetoothDevice> devicesMaxForceBle;
    private ArrayList<BluetoothDevice> devicesEmsBle;
    private static final long SCAN_PERIOD = 5000;
    private Handler mHandlerScan;
    private BluetoothAdapter mBluetoothAdapter;
    private ScanEmsBleDialog scanEmsBleDialog;
    private ScanMaxForceDialog scanMaxForceDialog;

    //DFU
    private UpdateDialog updateDialog;
    private ArrayList<BluetoothDevice> dfuDevices;
    private Device dfuDeviceUpdate;
    private boolean isDfuMode = false;
    private boolean updateBootloader = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = PreferencesManager.getInstance(this);

        //GA
        GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_PORTADA + GAConfig.SCREEN_PORTADA_INICIO, GAConfig.SCREEN_PORTADA);

        bindUi();
        loadTheme();
        loadToolbar();
        setListeners();
        loadData();
        loadHotSpot();
        initBlueTooth();
        loadConnectionType();

        //Load first section (Dashboard)
        DashboardFragment dashboardFragment = new DashboardFragment();
        loadFragmentSection(dashboardFragment);

        if (!EmsApp.isModeLocal()){
            Menu menu = navigationView.getMenu();
            menu.findItem(R.id.itemLocal).setVisible(false);
        } else
            showOfflineDialog();

        //Listener DFU
        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
    }

    public void bindUi(){
        navigationView = findViewById(R.id.navigationView);
        headerMenu = navigationView.getHeaderView(0);
        drawer = findViewById(R.id.drawer_layout);
        txtTitle = findViewById(R.id.txtTitle);
        imgUser = headerMenu.findViewById(R.id.imgUser);
        txtName = headerMenu.findViewById(R.id.txtName);
        linearHeaderMenu = headerMenu.findViewById(R.id.linearHeaderMenu);

        drawer.setScrimColor(Color.TRANSPARENT);
    }

    public void loadTheme(){
        linearHeaderMenu.setBackgroundColor(ColorTheme.getPrimaryColor(MainActivity.this));
    }

    public void setListeners(){

        navigationView.setNavigationItemSelectedListener(this);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name){
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

                if (recreate){
                    recreate = false;
                    recreate();
                }
            }

            public void onDrawerOpened(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                Resources r = getResources();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 235, r.getDisplayMetrics());
                float moveFactor = (px * slideOffset);
                findViewById(R.id.main).setPadding((int)moveFactor,0,0,0);
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    public void loadToolbar(){
        toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(MainActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(MainActivity.this));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        item.setChecked(true);
        if(item.getGroupId() == R.id.groupPrincipal){
            switch (id) {
                case R.id.itemDashboard:
                    txtTitle.setText(getString(R.string.menu_section_dashboard));
                    idSectionSelected = R.id.itemDashboard;
                    break;
                case R.id.itemStudents:
                    txtTitle.setText(getString(R.string.menu_section_students));
                    idSectionSelected = R.id.itemStudents;
                    break;
                case R.id.itemTraining:
                    txtTitle.setText(getString(R.string.menu_section_training));
                    idSectionSelected = R.id.itemTraining;
                    break;
                case R.id.itemDiary:
                    txtTitle.setText(getString(R.string.menu_section_diary));
                    idSectionSelected = R.id.itemDiary;
                    break;
                case R.id.itemConfiguration:
                    txtTitle.setText(getString(R.string.menu_section_configuration));
                    idSectionSelected = R.id.itemConfiguration;
                    break;
                case R.id.itemDevices:
                    txtTitle.setText(getString(R.string.menu_section_devices));
                    idSectionSelected = R.id.itemDevices;
                    break;
                case R.id.itemLocal:
                    txtTitle.setText(getString(R.string.menu_section_local));
                    idSectionSelected = R.id.itemLocal;
                    break;
            }
            loadSection(idSectionSelected);
        }

        return true;
    }

    public void loadSection (int idSection) {
        switch (idSection) {
            case R.id.itemDashboard:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DashboardFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_PORTADA + GAConfig.SCREEN_PORTADA_INICIO, GAConfig.SCREEN_PORTADA);

                    DashboardFragment dashboardFragment = new DashboardFragment();
                    loadFragmentSection(dashboardFragment);
                }
                break;
            case R.id.itemStudents:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StudentsFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_ALUMNOS + GAConfig.SCREEN_ALUMNOS_ALUMNOS, GAConfig.SCREEN_PORTADA);

                    StudentsFragment studentsFragment = new StudentsFragment();
                    loadFragmentSection(studentsFragment);
                }
                break;
            case R.id.itemTraining:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof TrainingParticipantsFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_ENTRENAMIENTO + GAConfig.SCREEN_ENTRENAMIENTO_PARTICIPANTES, GAConfig.SCREEN_PORTADA);

                    //Clear MaxFocre Devices
                    devices = pref.getAllDevices();
                    for (Device device: devices){
                        device.setUserAsigned(null);
                        device.setDeviceBle(null);
                    }
                    pref.saveDevices(devices);

                    TrainingParticipantsFragment trainingParticipantsFragment = new TrainingParticipantsFragment();
                    loadFragmentSection(trainingParticipantsFragment);
                }
                break;
            case R.id.itemDiary:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DiaryFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_AGENDA + GAConfig.SCREEN_AGENDA_AGENDA, GAConfig.SCREEN_PORTADA);

                    DiaryFragment diaryFragment = new DiaryFragment();
                    loadFragmentSection(diaryFragment);
                }
                break;
            case R.id.itemConfiguration:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SettingsFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_CONFIGURACION + GAConfig.SCREEN_CONFIGURACION_CONFIGURACION, GAConfig.SCREEN_PORTADA);

                    SettingsFragment settingsFragment = new SettingsFragment();
                    loadFragmentSection(settingsFragment);
                }
                break;
            case R.id.itemDevices:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DevicesFragment)){
                    //GA
                    GAUtils.trackScreen(MainActivity.this, GAConfig.SCREEN_DISPOSITIVOS + GAConfig.SCREEN_DISPOSITIVOS_DISPOSITIVOS, GAConfig.SCREEN_PORTADA);

                    DevicesFragment devicesFragment = new DevicesFragment();
                    loadFragmentSection(devicesFragment);
                }
                break;
            case R.id.itemLocal:
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof LocalFragment)){
                    LocalFragment localFragment = new LocalFragment();
                    loadFragmentSection(localFragment);
                }
                break;
        }
    }

    //Load section selected on menu
    public void loadFragmentSection (Fragment fragmentSection) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_content, fragmentSection);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void loadSectionFromDashboard(int idSection){
        switch (idSection) {
            case R.id.itemDashboard:
                navigationView.getMenu().performIdentifierAction(R.id.itemDashboard, 0);
                loadSection(R.id.itemDashboard);
                break;
            case R.id.itemStudents:
                navigationView.getMenu().performIdentifierAction(R.id.itemStudents, 0);
                loadSection(R.id.itemStudents);
                break;
            case R.id.itemTraining:
                navigationView.getMenu().performIdentifierAction(R.id.itemTraining, 0);
                loadSection(R.id.itemTraining);
                break;
            case R.id.itemDiary:
                navigationView.getMenu().performIdentifierAction(R.id.itemDiary, 0);
                loadSection(R.id.itemDiary);
                break;
            case R.id.itemConfiguration:
                navigationView.getMenu().performIdentifierAction(R.id.itemConfiguration, 0);
                loadSection(R.id.itemConfiguration);
                break;
            case R.id.itemDevices:
                navigationView.getMenu().performIdentifierAction(R.id.itemDevices, 0);
                loadSection(R.id.itemDevices);
                break;
            case R.id.itemLocal:
                navigationView.getMenu().performIdentifierAction(R.id.itemLocal, 0);
                loadSection(R.id.itemLocal);
                break;
        }
    }

    public void loadNotices(){
        Intent intent = new Intent(MainActivity.this, VideoActivity.class);
        intent.putExtra(VideoActivity.EXTRA_ID_VIDEO, pref.getAppInfo().getLinks().getVideo());
        startActivity(intent);
    }

    public void closeDrawerAndRecreate(){
        if (drawer.isDrawerOpen(GravityCompat.START)){
            recreate = true;
            drawer.closeDrawers();
            navigationView.getMenu().getItem(0).setChecked(true);
        } else{
            navigationView.getMenu().getItem(0).setChecked(true);
            recreate();
        }
    }

    public void loadData(){
        User user = pref.getUser();
        txtName.setText(user.getName());
        ImageUtils.loadImageFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+user.getImage());
    }

    public void loadHotSpot(){
        boolean hotspot = pref.getHotSpotStatus();
        if (!hotspot){
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if(wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(false);
            }

            WifiConfiguration netConfig = new WifiConfiguration();
            netConfig = initWifiAPConfig(netConfig);

            try{
                Method setWifiApMethod = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                setWifiApMethod.invoke(wifiManager, netConfig,true);

                Method isWifiApEnabledmethod = wifiManager.getClass().getMethod("isWifiApEnabled");
                while(!(Boolean)isWifiApEnabledmethod.invoke(wifiManager)){}

                Method getWifiApStateMethod = wifiManager.getClass().getMethod("getWifiApState");
                getWifiApStateMethod.invoke(wifiManager);

                Method getWifiApConfigurationMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
                netConfig = (WifiConfiguration)getWifiApConfigurationMethod.invoke(wifiManager);

                Log.e("CLIENT", "\nSSID:"+netConfig.SSID+"\nPassword:"+netConfig.preSharedKey+"\n");

                bindServer();
            } catch (Exception e) {
                Log.e(this.getClass().toString(), "", e);
            }
        }
    }

    private WifiConfiguration initWifiAPConfig(WifiConfiguration wifiConfiguration){
        wifiConfiguration.SSID = WSConfig.HOTSPOT_SSID;
        wifiConfiguration.preSharedKey = WSConfig.HOTSPOT_PASS;
        wifiConfiguration.hiddenSSID = false;
        wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wifiConfiguration.allowedKeyManagement.set((WifiConfiguration.KeyMgmt.WPA_PSK));
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        return  wifiConfiguration;
    }

    public void updateAssignaments(){
        if ((getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof TrainingParticipantsFragment)){
            TrainingParticipantsFragment trainingParticipantsFragment = (TrainingParticipantsFragment) getSupportFragmentManager().findFragmentById(R.id.main_content);
            trainingParticipantsFragment.updateList();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      SERVER CONNECTION
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void bindServer() {
        server = new UDPServer(UDPServer.SERVER_NORMAL);
        server.runUdpServer(this);
        myServerFilter = new IntentFilter();
        myServerFilter.addAction(UDPServer.MESSAGE_RECEIVED);
        registerReceiver(myServerReciver, myServerFilter);

        sincDevices();
    }

    public static void checkBroadcastList() throws UnsupportedEncodingException {
        boolean isNewDevice = DataParser.checkNewDevice(message);

        for (int i = 0; i < data.vests.size(); i++) {
            if(Arrays.equals(data.vests.get(i).UID, DataParser.getUID(message, isNewDevice))){
                if (!data.vests.get(i).getIP().toString().equals(ip.toString())){

                    //Update vata vest ip
                    data.vests.get(i).setIP(ip);

                    //Update device ip
                    ArrayList<Device> oldDevices = pref.getAllDevices();
                    for (Device oldDevice: oldDevices){
                        if (oldDevice.getUidString().equals(data.vests.get(i).getUidString())){
                            oldDevice.setIp(data.vests.get(i).getIP());
                        }
                    }
                    pref.saveDevices(oldDevices);
                }
                return;
            }
        }

        for (int i = 0; i < UIDsBroad.size(); i++) {
            if(Arrays.equals(UIDsBroad.get(i),DataParser.getUID(message, isNewDevice))){
                return;
            }
        }

        if (isNewDevice)
            nameBroad.add("myoFX-EMS "+new String(DataParser.getUID(message, isNewDevice), "UTF-8"));
        else {
            String UUID =  Arrays.toString(DataParser.getUID(message, isNewDevice));
            UUID = UUID.replace("[","");
            UUID =  UUID.replace("]","");
            UUID =  UUID.replace(", ","");
            nameBroad.add("myoFX-EMS "+ UUID);
        }

        UIDsBroad.add(DataParser.getUID(message, isNewDevice));
        ipBroad.add(ip);
    }

    //SERVER RECEIVER VALUES
    public static void setBrodcastValue(){
        boolean isNewDevice = DataParser.checkNewDevice(message);
        if(data.getVestsCount()>0){
            for (int i = 0; i < data.vests.size(); i++) {
                if(Arrays.equals(data.vests.get(i).UID,DataParser.getUID(message,isNewDevice))){
                        String response = new String(message, 0, message.length);
                        if (response.contains("myoFX")){
                            if(DataParser.getCommandType(message,isNewDevice)==2){
                                switch(DataParser.getCommand(message,isNewDevice)) {
                                    case 0://keep alive
                                        break;
                                    case 5://stageº
                                        break;
                                    case 6://levels
                                        break;
                                    case 11://Exercice status
                                        if (startCheckDevice)
                                            addDeviceToCheck(data.vests.get(i).getUidString());

                                        EventBus.getDefault().postSticky(new DeviceStatusEvent(data.vests.get(i).getUidString(),
                                                ""+DataParser.getStatus(message,isNewDevice)[0],
                                                DataParser.getStatus(message,isNewDevice)[4] +"."+DataParser.getStatus(message,isNewDevice)[3] +"."+DataParser.getStatus(message,isNewDevice)[2]));
                                        break;
                                    case 12://cycle
                                        if(DataParser.getCycle(message,isNewDevice)[1] == 1)
                                            EventBus.getDefault().postSticky(new ProgressExerciceEvent(true,DataParser.getCycle(message,isNewDevice)[3],DataParser.getCycle(message,isNewDevice)[0], i));
                                        else
                                            EventBus.getDefault().postSticky(new ProgressExerciceEvent(false,DataParser.getCycle(message,isNewDevice)[3],DataParser.getCycle(message,isNewDevice)[0],i));
                                        break;
                                }
                            } else if(DataParser.getCommandType(message,isNewDevice) == 3){
                                switch(DataParser.getCommand(message,isNewDevice)) {
                                    case 1:
                                        EventBus.getDefault().postSticky(new StopEvent(data.vests.get(i).getUidString()));
                                        break;
                                    case 2:
                                        EventBus.getDefault().postSticky(new StartEvent(data.vests.get(i).getUidString()));
                                        break;
                                    case 4://Potency level
                                        EventBus.getDefault().postSticky(new PotencyEvent(DataParser.getMains(message,isNewDevice)[0], data.vests.get(i).getUidString()));
                                        break;
                                }
                            }
                        }
                    return;
                }
            }
        }
    }

    //BLE RECEIVER VALUES
    public void setBroadcastList(){
        boolean addDevice = true;
        ArrayList<Device> devicesAdded = pref.getAllDevices();
        final ArrayList<String> listDevices = new ArrayList<>();
        for (int i = 0; i < UIDsBroad.size(); i++) {
            for (Device deviceAdd: devicesAdded){
                addDevice = true;
                if (deviceAdd.getName().equals(nameBroad.get(i))){
                    addDevice = false;
                    break;
                }
            }

            if (addDevice)
                listDevices.add(nameBroad.get(i));
        }

        scanDevicesDialog = ScanDevicesDialog.newInstance(listDevices, UDPServer.SERVER_NORMAL);
        scanDevicesDialog.setCancelable(false);
        scanDevicesDialog.show(getSupportFragmentManager(), ScanDevicesDialog.class.getSimpleName());
    }

    private BroadcastReceiver myServerReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String myParam = intent.getExtras().getString("MESSAGE_STRING");
            if (myParam != null) {

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, myParam, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    };

    public void setBleEmsDeviceSelected(BluetoothDevice bleDevice) {
        EmsApp.getInstance().getmBluetoothLeService().connect(bleDevice.getAddress());
        Device device = new Device(bleDevice.getName(), bleDevice.getAddress());

        openDeviceNameDialog(device);
    }

    public void setDeviceSelected(String nameSelected){
        int position = 0;
        for (String name: nameBroad){
            if (name.equals(nameSelected))
                position = nameBroad.indexOf(nameSelected);
        }

        Device device = new Device(nameBroad.get(position), UIDsBroad.get(position), ipBroad.get(position));
        openDeviceNameDialog(device);
    }

    public void openDeviceNameDialog(Device device){
        DeviceNameDialog deviceNameDialog = DeviceNameDialog.newInstance(this, device);
        deviceNameDialog.setCancelable(false);
        deviceNameDialog.show(getSupportFragmentManager(), DeviceNameDialog.class.getSimpleName());
    }

    @Override
    public void onConfirmationName(Device device) {
        switch (device.getDeviceType()){
            case Device.DEVICE_WIFI:
                data.addVest(device.getName(),device.getUid(),device.getIp(), device.isNewDevice());
                pref.saveDevice(device);

                ipsOrderList.add(device.getUid());
                server.uids.add(device.getUid());
                server.ips.add(device.getIp());

                listChannelsVest.add( new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                listProgramsVest.add( new int[] { 85, 4000, 4000, 350, 0, 0, 1200, 0, 7, 200  });

                EmsApp.setData(data);
                EmsApp.setServer(server);

                scanDevicesDialog.dismiss();

                if ((getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DevicesFragment)){
                    DevicesFragment devicesFragment = (DevicesFragment) getSupportFragmentManager().findFragmentById(R.id.main_content);
                    devicesFragment.checkAvailableDevices();
                }
                break;
            case Device.DEVICE_BLE:
                pref.saveDevice(device);
                scanEmsBleDialog.dismiss();

                if ((getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DevicesFragment)){
                    DevicesFragment devicesFragment = (DevicesFragment) getSupportFragmentManager().findFragmentById(R.id.main_content);
                    devicesFragment.checkAvailableDevices();
                }
                break;
        }
    }

    public void sincDevices(){
        devices = new ArrayList<>();
        devices = pref.getAllDevices();

        //Clear old values
        data.vests.clear();
        ipsOrderList.clear();
        server.uids.clear();
        server.ips.clear();
        listChannelsVest.clear();
        listProgramsVest.clear();

        for (Device device: devices){
            data.addVest(device.getName(),device.getUid(),device.getIp(),device.isNewDevice());

            ipsOrderList.add(device.getUid());
            server.uids.add(device.getUid());
            server.ips.add(device.getIp());

            listChannelsVest.add( new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            listProgramsVest.add( new int[] { 85, 4000, 4000, 350, 0, 0, 1200, 0, 7, 200  });

            EmsApp.setData(data);
            EmsApp.setServer(server);
        }
    }

    public void deleteDevice (Device deviceDelete){
        devices = pref.getAllDevices();
        ArrayList<Device> newDevices = new ArrayList<>();

        for (Device device: devices){
            if (!device.getName().equals(deviceDelete.getName()))
                newDevices.add(device);
        }

        pref.saveDevices(newDevices);

        switch (deviceDelete.getDeviceType()){
            case Device.DEVICE_WIFI:
                sincDevices();
            break;
            case Device.DEVICE_BLE:
                if (EmsApp.getInstance().getmBluetoothLeService() != null)
                    EmsApp.getInstance().getmBluetoothLeService().disconnect(deviceDelete.getDeviceBleAdress());
                break;
        }

        if ((getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DevicesFragment)){
            DevicesFragment devicesFragment = (DevicesFragment) getSupportFragmentManager().findFragmentById(R.id.main_content);
            devicesFragment.loadDevices();
        }
    }

    //Update BLE device
    public void updateBLEDevice (Device deviceUpdate){
        this.dfuDeviceUpdate = deviceUpdate;
        showConfirmDialog();
    }

    //----------------------------------------------------------------------------------------------
    //-----------------------------------------BLUETOOTH--------------------------------------------
    //----------------------------------------------------------------------------------------------
    public void initBlueTooth(){
        mHandlerScan = new Handler();

        //Check if have BLE
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            InformationDialog informationDialog = InformationDialog.newInstance(getString(R.string.bluetooth_ble_not_supported));
            informationDialog.show(getSupportFragmentManager(), InformationDialog.class.getSimpleName());
            return;
        }

        // Initializes a Bluetooth adapter.
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            InformationDialog informationDialog = InformationDialog.newInstance(getString(R.string.bluetooth_not_supported));
            informationDialog.show(getSupportFragmentManager(), InformationDialog.class.getSimpleName());
            return;
        }
    }

    public void scanMaxForceDevice(Device device){
        deviceSelected = device;
        devicesMaxForceBle = new ArrayList<>();
        showScanMaxForceDialog();

        // Stops scanning after a pre-defined scan period.
        mHandlerScan.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);

                if (scanMaxForceDialog != null && scanMaxForceDialog.isAdded())
                    scanMaxForceDialog.loadDevices(devicesMaxForceBle);
            }
        }, SCAN_PERIOD);

        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    public void scanEmsDevice(){
        devicesEmsBle = new ArrayList<>();
        showScanEmsForceDialog();

        // Stops scanning after a pre-defined scan period.
        mHandlerScan.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);

                if (scanEmsBleDialog != null && scanEmsBleDialog.isAdded())
                    scanEmsBleDialog.loadDevices(devicesEmsBle);
            }
        }, SCAN_PERIOD);

        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice deviceBle, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //MAXFORCE
                            if (devicesMaxForceBle != null && !devicesMaxForceBle.contains(deviceBle) && deviceBle.getName() != null && deviceBle.getName().contains("MaxForce")){
                                devices = pref.getAllDevices();
                                boolean added = false;
                                for (Device deviceCheck: devices){
                                    if (deviceCheck.getDeviceBle() != null && deviceCheck.getDeviceBle().getAddress().equals(deviceBle.getAddress())){
                                        added = true;
                                        break;
                                    }
                                }
                                if (!added)
                                    devicesMaxForceBle.add(deviceBle);
                                //EMS
                            } else  if (devicesEmsBle != null && !devicesEmsBle.contains(deviceBle) && deviceBle.getName() != null && deviceBle.getName().contains("EMS BLE")){
                                devices = pref.getAllDevices();
                                boolean added = false;
                                for (Device deviceCheck: devices){
                                    if (deviceCheck.getDeviceBle() != null && deviceCheck.getDeviceBle().getAddress().equals(deviceBle.getAddress())){
                                        added = true;
                                        break;
                                    }
                                }
                                if (!added)
                                    devicesEmsBle.add(deviceBle);
                            }
                        }
                    });
                }
            };

    public void showScanMaxForceDialog(){
        scanMaxForceDialog = ScanMaxForceDialog.newInstance();
        scanMaxForceDialog.setCancelable(false);
        scanMaxForceDialog.show(getSupportFragmentManager(), ScanDevicesDialog.class.getSimpleName());
    }

    public void showScanEmsForceDialog(){
        scanEmsBleDialog = ScanEmsBleDialog.newInstance();
        scanEmsBleDialog.setCancelable(false);
        scanEmsBleDialog.show(getSupportFragmentManager(), ScanDevicesDialog.class.getSimpleName());
    }

    //Create connection with selected device
    public void assignDevice(BluetoothDevice device){
        if (scanEmsBleDialog != null)
            scanEmsBleDialog.dismiss();

        if (scanMaxForceDialog != null)
            scanMaxForceDialog.dismiss();

        if ((getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof TrainingParticipantsFragment)){
            TrainingParticipantsFragment trainingParticipantsFragment = (TrainingParticipantsFragment) getSupportFragmentManager().findFragmentById(R.id.main_content);
            trainingParticipantsFragment.updateMaxForceAssigment(deviceSelected, device);
        }
    }

    public static void setStartCheckDevice(boolean start) {
        if (start){
            checkDevicesId.clear();
            startCheckDevice = true;
        } else
            startCheckDevice = false;

        BluetoothEmsCommands.setStartCheckDevice(startCheckDevice);
    }

    public static void addDeviceToCheck(String uidAdd){
        if (!checkDevicesId.contains(uidAdd))
            checkDevicesId.add(uidAdd);
    }

    public static ArrayList<String> getCheckDevicesId() {
        return checkDevicesId;
    }

    public void connectPreviousEmsDevices(){
        EmsApp.getInstance().getmBluetoothLeService().startKeepAlive(this);

        if (pref.getAllDevices() != null) {
            for (Device deviceReconnect: pref.getAllDevices())
                EmsApp.getInstance().getmBluetoothLeService().connect(deviceReconnect.getDeviceBleAdress());
        }
    }

    public void loadConnectionType(){
        connectionType = pref.getUser().getConfiguration().getConnectionType();
        switch (connectionType){
            case Device.DEVICE_WIFI:
                bindServer();
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().initBleEms(this);
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////
    //////////////////////////// DFU UPDATE///////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    private static final int DFU_ERROR_DISCONECT = 202;
    private int reconnectCountDfu = 0;

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter() {
        @Override
        public void onDeviceConnecting(final String deviceAddress) {
            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_connecting));
            else
                Toast.makeText(getApplicationContext(),getString(R.string.dfu_connecting) , Toast.LENGTH_SHORT).show();

            if(reconnectCountDfu > 0){
                reconnectCountDfu = 0;
//                searchDFUDevice();
            }else{
                reconnectCountDfu++;
            }
        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress) {
            isDfuMode = true;
            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_starting));
            else
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_starting), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onEnablingDfuMode(final String deviceAddress) {
            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_enabling_bootloader));
            else
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_enabling_bootloader), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFirmwareValidating(final String deviceAddress) {
            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_validating));
            else
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_validating), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress) {
            if(updateDialog!=null){
                updateDialog.loadStatus(getString(R.string.dfu_disconnecting));
            }else{
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_disconnecting), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onDfuCompleted(final String deviceAddress) {
            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_update_completed));
            else
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_update_completed), Toast.LENGTH_SHORT).show();


            reconnectCountDfu = 0;
            if (dfuDeviceUpdate != null) {
                isDfuMode = false;
                EmsApp.getInstance().getmBluetoothLeService().connect(dfuDeviceUpdate.getDeviceBleAdress());
                if(updateDialog!=null)
                    updateDialog.dismiss();
            } else {
                isDfuMode = false;
                if(updateDialog!=null)
                    updateDialog.dismiss();
            }
        }

        @Override
        public void onDfuAborted(final String deviceAddress) {
            if (updateDialog != null)
                updateDialog.loadStatus(getString(R.string.dfu_process_aborted));
            else
                Toast.makeText(getApplicationContext(), getString(R.string.dfu_process_aborted), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent, final float speed, final float avgSpeed, final int currentPart, final int partsTotal) {

            if(updateDialog!=null)
                updateDialog.loadStatus(getString(R.string.dfu_progress)+" "+percent);
            else
                if((percent%10==0))
                    Toast.makeText(getApplicationContext(), getString(R.string.dfu_progress)+" "+percent, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType, final String message) {
            isDfuMode = false;
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            updateDialog.dismiss();
        }
    };

    public void showConfirmDialog(){
        ConfirmationUpdateDialog confirmationUpdateDialog = ConfirmationUpdateDialog.newInstance(this);
        confirmationUpdateDialog.show(getSupportFragmentManager(), ConfirmationUpdateDialog.class.getSimpleName());
    }

    @Override
    public void onConfirmationUpdate(boolean update) {
        if (update)
            startaDFU();
    }

    public void showUpdateDialog(){
        updateDialog = UpdateDialog.newInstance();
        updateDialog.setCancelable(false);
        updateDialog.show(getSupportFragmentManager(), ScanDevicesDialog.class.getSimpleName());
    }

    //Start DFU update proces
    public void startaDFU() {
        final DfuServiceInitiator starter = new DfuServiceInitiator(dfuDeviceUpdate.getDeviceBleAdress())
                .setDeviceName(dfuDeviceUpdate.getDeviceBleAdress())
                .setKeepBond(false)
                .setForceDfu(false)
                .setPacketsReceiptNotificationsEnabled(true)
                .setPacketsReceiptNotificationsValue(12)
                .setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);


        Log.v("DFU", "device: "+dfuDeviceUpdate.getDeviceBleAdress()+" device name:"+ dfuDeviceUpdate.getName());
        Log.e("DFU","update Firm: ");
        showUpdateDialog();
        starter.setZip(R.raw.emsble_update_101);
        starter.start(this, DfuService.class);
    }

    private void showOfflineDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.dialog_offline));
        alertDialog.setMessage(getString(R.string.dialog_offline_desc));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.dialog_accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
