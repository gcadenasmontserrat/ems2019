package com.myofx.emsapp.ui.training.multiple;

import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.MediaRouteButton;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.CastRemoteDisplayLocalService;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.BleStatusEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.DeviceStatusEvent;
import com.myofx.emsapp.api.events.PotencyEvent;
import com.myofx.emsapp.api.events.ProgressExerciceEvent;
import com.myofx.emsapp.api.events.StartEvent;
import com.myofx.emsapp.api.events.StopEvent;
import com.myofx.emsapp.bluetooth.CharacteristicChangeListener;
import com.myofx.emsapp.bluetooth.GattManager;
import com.myofx.emsapp.bluetooth.operations.GattSetNotificationOperation;
import com.myofx.emsapp.config.GAConfig;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.CorporalZone;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.SessionResults;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.models.UserMachine;
import com.myofx.emsapp.server.ServerAppModel;
import com.myofx.emsapp.server.ServerEmsCommands;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.server.Vest;
import com.myofx.emsapp.ui.chromecast.ExerciseMaxForcePresentationService;
import com.myofx.emsapp.ui.dialogs.EndExerciseDialog;
import com.myofx.emsapp.ui.dialogs.FrequencyDialog;
import com.myofx.emsapp.ui.dialogs.GlobalInformationDialog;
import com.myofx.emsapp.ui.dialogs.ImpulseTimeDialog;
import com.myofx.emsapp.ui.dialogs.PotencyDialog;
import com.myofx.emsapp.ui.dialogs.RampDialog;
import com.myofx.emsapp.ui.dialogs.RelaxDialog;
import com.myofx.emsapp.ui.dialogs.StandardDialog;
import com.myofx.emsapp.ui.dialogs.TimeDialog;
import com.myofx.emsapp.ui.training.TrainingsActivity;
import com.myofx.emsapp.ui.training.adapters.ExerciseMultiplePagerAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.GAUtils;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.ViewPagerCustomScroll;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Gerard on 3/2/2017.
 */

public class ExerciseMultipleActivity extends BaseActivity implements RampDialog.OnConfirmationRampListener, FrequencyDialog.OnConfirmationFrequencyListener,
        ImpulseTimeDialog.OnConfirmationImpulseTimeListener, RelaxDialog.OnConfirmationRelaxListener,
        TimeDialog.OnConfirmationTimeListener, StandardDialog.OnConfirmationStandardeListener, EndExerciseDialog.OnConfirmationEndListener,
        PotencyDialog.OnConfirmationPotencyListener {

    //Ui
    private ExerciseMultipleNormalFragment fragmentNormal = new ExerciseMultipleNormalFragment();
    private ExerciseMultipleGlobalFragment fragmentGlobal = new ExerciseMultipleGlobalFragment();
    private ViewPagerCustomScroll pagerExercice;
    private LinearLayout linearLoading;
    private RelativeLayout linearLoadingMaxforce;
    private TextView txtLoading;
    private TextView txtTitle;

    private TextView txtFrequencyNormal;
    private TextView txtFrequencyRelax;
    private TextView txtPotencyRelax;
    private TextView txtPulse;
    private TextView txtEstimTime;
    private TextView txtPauseTime;
    private TextView txtRampUp;
    private TextView txtRampDown;
    private TextView txtBatery;
    private TextView txtFirmware;

    //Variable
    private ExerciseMultiplePagerAdapter exerciceMultiplePagerAdapter;
    private PreferencesManager pref;
    private User user;
    private ArrayList<User> participants;
    private Training trainingGlobal;
    private ArrayList<Training> trainings;
    private CountDownTimer countDownTimer;
    private boolean startGlobal = false;
    private boolean startNormal = false;
    private int timeGlobalRemaining;
    public static int vestIndexGlobal = 0;
    private int positionSelected = 0;
    private boolean maxForceIndividual = false;

    //Variables connection
    private static ServerAppModel data;
    private UDPServer server;
    private IntentFilter myServerFilter;

    //Chromecast
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter mMediaRouter;
    private int mRouteCount;
    private MediaRouteButton mediaRouteButton;
    private CastDevice castDevice;
    private MediaRouter.Callback mMediaRouterCallback =
            new MediaRouter.Callback() {
                @Override
                public void onRouteAdded(MediaRouter router, MediaRouter.RouteInfo route) {
                    if (++mRouteCount == 1) {
                    }
                }

                @Override
                public void onRouteRemoved(MediaRouter router, MediaRouter.RouteInfo route) {
                    if (--mRouteCount == 0) {
                    }
                }

                @Override
                public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
                    Log.d("TAG", "onRouteSelected");
                    castDevice = CastDevice.getFromBundle(info.getExtras());
                    if (castDevice != null) {
                        startCastService();
                    }
                }

                @Override
                public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
                    Log.d("TAG", "onRouteSelected");

                }
            };

    //BlueTooth
    private GattManager mGattManager;
    private GattManager mGattManagerSecond;
    private GattManager mGattManagerThird;
    private GattManager mGattManagerFourth;
    private GattManager mGattManagerFive;
    private GattManager mGattManagerSix;
    private GattManager mGattManagerSeven;
    private GattManager mGattManagerEigth;
    private boolean haveMaxForce = false;
    private boolean maxForceConected = false;

    private int connectionType = Device.DEVICE_WIFI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //GA
        GAUtils.trackScreen(ExerciseMultipleActivity.this, GAConfig.SCREEN_ENTRENAMIENTO + GAConfig.SCREEN_ENTRENAMIENTO_EJERCICIO, GAConfig.SCREEN_PORTADA);

        pref = PreferencesManager.getInstance(this);
        user = pref.getUser();

        //Get connectionType
        connectionType = pref.getUser().getConfiguration().getConnectionType();

        //Get Training
        if (getIntent().hasExtra(TrainingsActivity.EXTRA_TRAINING))
            trainingGlobal = (Training) getIntent().getSerializableExtra(TrainingsActivity.EXTRA_TRAINING);

        //Get Participants
        if (getIntent().hasExtra(TrainingsActivity.EXTRA_STUDENTS))
            participants = (ArrayList<User>) getIntent().getSerializableExtra(TrainingsActivity.EXTRA_STUDENTS);

        //Server connection
        bindServer();

        bindUi();
        setListeners();
        loadToolbar();
        loadParticipants();
        loadPagerExercice();
        setMediaRoute();
        startConfigTimer();
        globalSectionPermission();

        if (android.os.Build.VERSION.SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

//        sendStandardSelected(0, trainings.get(0), trainings.get(0).getDevice().getDeviceBleAdress());
    }

    public void bindUi(){
        txtLoading = findViewById(R.id.txtLoading);
        linearLoading = findViewById(R.id.linearLoading);
        txtTitle = findViewById(R.id.txtTitle);
        pagerExercice = findViewById(R.id.pagerExercice);
        txtFrequencyNormal = findViewById(R.id.txtFrequencyNormal);
        txtFrequencyRelax = findViewById(R.id.txtFrequencyRelax);
        txtPotencyRelax = findViewById(R.id.txtPotencyRelax);
        txtPulse = findViewById(R.id.txtPulse);
        txtEstimTime = findViewById(R.id.txtEstimTime);
        txtPauseTime = findViewById(R.id.txtPauseTime);
        txtRampUp = findViewById(R.id.txtRampUp);
        txtRampDown = findViewById(R.id.txtRampDwon);
        txtBatery = findViewById(R.id.txtBatery);
        txtFirmware = findViewById(R.id.txtFirmware);
        linearLoadingMaxforce = findViewById(R.id.linearLoadingMaxforce);
    }

    public void loadPagerExercice(){
        txtTitle.setText(trainingGlobal.getTitle());

        pagerExercice.setOffscreenPageLimit(1);
        exerciceMultiplePagerAdapter = new ExerciseMultiplePagerAdapter(getSupportFragmentManager());
        exerciceMultiplePagerAdapter.addFrag(fragmentNormal);
        exerciceMultiplePagerAdapter.addFrag(fragmentGlobal);
        pagerExercice.setAdapter(exerciceMultiplePagerAdapter);
    }

    public void loadToolbar(){
        Toolbar toolbar = findViewById(R.id.action_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(ExerciseMultipleActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(ExerciseMultipleActivity.this));
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                showEndDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        showEndDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopConfigTimer();
    }

    public void loadParticipants(){
        trainings = new ArrayList<>();
        CorporalZone cz;
        for (int i=0; i<participants.size(); i++){
            //Load corporal zones
            cz = participants.get(i).getCorporalZone(trainingGlobal.getIdTraining());
            if (cz == null){
                cz = new CorporalZone(trainingGlobal.getIdTraining(),0,0,0,0,0,0,0,0,0,0,
                        trainingGlobal.getChronaxy(Vest.CHANNEL_ABS),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_BIC),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_DOR),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_LUM),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_SHO),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_LEG),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_GLU),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_PEC),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_AUX),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_AUX_2),
                        0);
            } else if (cz.getCronLeg() == 0){
                cz = new CorporalZone(trainingGlobal.getIdTraining(),cz.getAbs(),cz.getBic(),cz.getDor(),cz.getLum(),cz.getSho(),
                        cz.getLeg(),cz.getGlu(),cz.getPec(),cz.getAux(),cz.getAux2(),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_ABS),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_BIC),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_DOR),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_LUM),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_SHO),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_LEG),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_GLU),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_PEC),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_AUX),
                        trainingGlobal.getChronaxy(Vest.CHANNEL_AUX_2),
                        0);
            }

            //Load
            Training trainingCopy  = new Training();
            trainingCopy = Training.copyTraining(trainingGlobal);
            trainingCopy.setLevelLeg(cz.getLeg());trainingCopy.setCronLeg(cz.getCronLeg());
            trainingCopy.setLevelGlu(cz.getGlu());trainingCopy.setCronGlu(cz.getCronGlu());
            trainingCopy.setLevelLum(cz.getLum());trainingCopy.setCronLum(cz.getCronLum());
            trainingCopy.setLevelDor(cz.getDor());trainingCopy.setCronDor(cz.getCronDor());
            trainingCopy.setLevelSho(cz.getSho());trainingCopy.setCronSho(cz.getCronSho());
            trainingCopy.setLevelAbs(cz.getAbs());trainingCopy.setCronAbs(cz.getCronAbs());
            trainingCopy.setLevelPec(cz.getPec());trainingCopy.setCronPec(cz.getCronPec());
            trainingCopy.setLevelBic(cz.getBic());trainingCopy.setCronBic(cz.getCronBic());
            trainingCopy.setLevelAux(cz.getAux());trainingCopy.setCronAux(cz.getCronAux());
            trainingCopy.setLevelAux2(cz.getAux2());trainingCopy.setCronAux2(cz.getCronAux2());
            trainingCopy.setUser(participants.get(i));
            trainingCopy.setLevel(0, 0);
            trainingCopy.setLastPotency(cz.getPotency());

            //Load default potency
            ArrayList<Device> devices = pref.getAllDevices();
            for (Device device: devices){
                if (device.getUserAsigned() != null && participants.get(i).getId().equals(device.getUserAsigned().getId())){
                    trainingCopy.setDevice(device);

                    switch (device.getDeviceType()){
                        case Device.DEVICE_WIFI:
                            trainingCopy.setUid(device.getUidString());
                            break;
                        case Device.DEVICE_BLE:
                            trainingCopy.setUid(device.getDeviceBleAdress());
                            break;
                    }

                    trainingCopy.setLevel(0, 0);
                    setPotencyLevelInitial(trainingCopy, trainingCopy.getUid());
                    break;
                }
            }
            if (trainingCopy.getDevice().getDeviceBle() != null)
                haveMaxForce = true;

            trainings.add(trainingCopy);
        }
    }

    public void setListeners(){
        pagerExercice.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        fragmentNormal.updatePotencyPagerUi();
                        fragmentNormal.updateButtonsUi();
                        break;
                    case 1:
                        fragmentGlobal.updatePotencyUi();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void scrollPager(){
        pagerExercice.setCurrentItem(1);
    }

    public void stopAll(){

        switch (connectionType){
            case Device.DEVICE_WIFI:
                for (Vest vest: data.getVests()){
                    try{
                        server.sendMessage(vest.getIP(),vest.UID,
                                new byte[] { (byte) 0x01},//type 1 byte
                                new byte[] { (byte) 0x01},// command 1 byte
                                new byte[] { (byte) 0x00},//lenght 1 byte
                                new byte[] { });//data n bytes(lenght)
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Device.DEVICE_BLE:
                for (Training training: trainings){
                    EmsApp.getInstance().getmBluetoothLeService().sendStopProgram(training.getDevice().getDeviceBleAdress());
                }
                break;
        }

        stopConfigTimer();
        stopCastService();

        for (User user: participants)
            user.setSelected(false);

        disconnectAll();

        Intent data = new Intent();
        data.putExtra(TrainingsActivity.RESULT_PARTICIPANTS, participants);
        setResult(RESULT_OK, data);
        finish();
    }

    public void showRampDialog(Training training){
        RampDialog rampDialog = RampDialog.newInstance(this, training.getRampUp(),training.getRampDown());
        rampDialog.show(getSupportFragmentManager(), RampDialog.class.getSimpleName());
    }

    public void showFrequencyDialog(Training training,  boolean fromGlobal){
        FrequencyDialog frequencyDialog = FrequencyDialog.newInstance(this, training.getFrequency(), training, fromGlobal);
        frequencyDialog.show(getSupportFragmentManager(), FrequencyDialog.class.getSimpleName());
    }

    public void showRelaxDialog(Training training){
        RelaxDialog relaxDialog = RelaxDialog.newInstance(this, training.getFrequencyRelax(),
                training.getPulseRelax(), training.getPotencyRelax());
        relaxDialog.show(getSupportFragmentManager(), FrequencyDialog.class.getSimpleName());
    }

    public void showImpulseTimeDialog(Training training){
        if (isStartGlobal()){
            showInfoGlobal();
        } else {
            ImpulseTimeDialog impulseTimeDialog = ImpulseTimeDialog.newInstance(this, training.getEstimulationTime(),training.getPauseTime());
            impulseTimeDialog.show(getSupportFragmentManager(), ImpulseTimeDialog.class.getSimpleName());
        }
    }

    public void showTimeDialog(Training training, int from){
        if (isStartGlobal() && from == TimeDialog.FROM_INDIVIDUAL){
            showInfoGlobal();
        } else {
            TimeDialog timeDialog = TimeDialog.newInstance(this, training.getTime(), from);
            timeDialog.show(getSupportFragmentManager(), TimeDialog.class.getSimpleName());
        }
    }

    public void showPotencyDialog(int potency, int from){
        PotencyDialog potencyDialog = PotencyDialog.newInstance(this, potency, from);
        potencyDialog.show(getSupportFragmentManager(), PotencyDialog.class.getSimpleName());
    }

    public void showStandardDialog(Training training){
        StandardDialog standardDialog = StandardDialog.newInstance(this, training);
        standardDialog.show(getSupportFragmentManager(), StandardDialog.class.getSimpleName());
    }

    public void showEndDialog(){
        EndExerciseDialog endExerciseDialog = EndExerciseDialog.newInstance(this);
        endExerciseDialog.show(getSupportFragmentManager(), StandardDialog.class.getSimpleName());
    }

    public void showInfoGlobal(){
        GlobalInformationDialog globalInformationDialog = GlobalInformationDialog.newInstance();
        globalInformationDialog.show(getSupportFragmentManager(), StandardDialog.class.getSimpleName());
    }

    @Override
    public void onConfirmationEnd(boolean end) {
        if (end){
            stopAll();
        }
    }

    @Override
    public void onConfirmationStandard(int action) {
        switch (action){
            case StandardDialog.ACTION_LOAD:
                sendStandardUser(fragmentNormal.getVestIndex(), fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
                break;
            case StandardDialog.ACTION_SAVE:

                Training training = fragmentNormal.getTraining();
                user.getStandard().setLeg(training.getLevelLeg());
                user.getStandard().setGlu(training.getLevelGlu());
                user.getStandard().setLum(training.getLevelLum());
                user.getStandard().setDor(training.getLevelDor());
                user.getStandard().setSho(training.getLevelSho());
                user.getStandard().setAbs(training.getLevelAbs());
                user.getStandard().setPec(training.getLevelPec());
                user.getStandard().setBic(training.getLevelBic());
                user.getStandard().setCronLeg(training.getCronaxyLeg());
                user.getStandard().setCronGlu(training.getCronaxyGlu());
                user.getStandard().setCronLum(training.getCronaxyLum());
                user.getStandard().setCronDor(training.getCronaxyDor());
                user.getStandard().setCronSho(training.getCronaxySho());
                user.getStandard().setCronAbs(training.getCronaxyAbs());
                user.getStandard().setCronPec(training.getCronaxyPec());
                user.getStandard().setCronBic(training.getCronaxyBic());
                pref.saveUser(user);

                if (!EmsApp.isModeLocal())
                    EmsApi.getInstance().saveStandardValues(training);
                else {
                    user.getStandard().setUserid(user.getId());
                    AppDatabase.updateUser(ExerciseMultipleActivity.this, user);
                    Toast.makeText(ExerciseMultipleActivity.this, getString(R.string.exercise_standard_updated), Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onConfirmationRamp(boolean sendRamp, int rampUpValue, int rampDownValue) {
        if (sendRamp){
            fragmentNormal.getTraining().setRampUp(rampUpValue);
            fragmentNormal.getTraining().setRampDown(rampDownValue);
            updateMainValues(fragmentNormal.getVestIndex(), fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
            updateValuesDetails(fragmentNormal.getTraining());
        }
    }

    @Override
    public void onConfirmationRelax(boolean sendFrequency, int freqRelax, int pulseRelax, int potencyRelax) {
        if (sendFrequency){
            fragmentNormal.getTraining().setFrequencyRelax(freqRelax);
            fragmentNormal.getTraining().setPulseRelax(pulseRelax);
            fragmentNormal.getTraining().setPotencyRelax(potencyRelax);
            updateMainValues(fragmentNormal.getVestIndex(), fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
            setPotencyLevelInitial(fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
            updateValuesDetails(fragmentNormal.getTraining());
        }
    }

    @Override
    public void onConfirmationImpulseTime(boolean sendImpulseTime, int impulseEstimValue, int impulsePauseValue) {
        if (sendImpulseTime){
            fragmentNormal.getTraining().setEstimulationTime(impulseEstimValue);
            fragmentNormal.getTraining().setPauseTime(impulsePauseValue);
            updateMainValues(fragmentNormal.getVestIndex(), fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
            updateValuesDetails(fragmentNormal.getTraining());
        }
    }

    @Override
    public void onConfirmationTime(boolean sendTime, int time, int from) {
        if (sendTime){
            switch (from){
                case TimeDialog.FROM_GLOBAL:
                    trainingGlobal.setTime(time);
                    fragmentGlobal.stopAllUsers();
                    break;
                case TimeDialog.FROM_INDIVIDUAL:
                    fragmentNormal.getTraining().setTime(time);
                    fragmentNormal.callOnclickButtonStopChangeTime();
                    break;
            }
        }
    }

    @Override
    public void onConfirmationPotency(boolean send, int potency, int from) {
        if (send){
            switch (from){
                case TimeDialog.FROM_GLOBAL:
                    //TODO: algo?
                    break;
                case TimeDialog.FROM_INDIVIDUAL:
                    fragmentNormal.updatePotencyValue(potency);
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetTimeEvent(ProgressExerciceEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        timeGlobalRemaining = event.getTime();

        if (event.getAdress() != null){
            for (Training training: trainings){
                if (event.getAdress().equals(training.getUid())){
                    fragmentNormal.updateStatus(event.getTime(), event.isEstimulating(), event.getEstimulationMs(), getVestIndexUser(training));

                    //TODO: revisar cycle en pantalla global
                    if (vestIndexGlobal == event.getVestIndex())
                        fragmentGlobal.updateStatus(event.getTime(), event.isEstimulating(), event.getEstimulationMs());

                    break;
                }
            }
        } else {
            fragmentNormal.updateStatus(event.getTime(), event.isEstimulating(), event.getEstimulationMs(), event.getVestIndex());
            if (vestIndexGlobal == event.getVestIndex())
                fragmentGlobal.updateStatus(event.getTime(), event.isEstimulating(), event.getEstimulationMs());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetBleStatusEvent(BleStatusEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        fragmentNormal.updateBleStatus(event.isBleAvailable(), event.getAdress());
        fragmentGlobal.updateBleStatus(event.isBleAvailable(), event.getAdress());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetPotencyEvent(PotencyEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        String deviceID = event.getUid();
        for (Training training: trainings){
            if (deviceID.equals(training.getUid())){
                fragmentNormal.updatePotencyClickDevice(training, event.getPotency());
                fragmentGlobal.updatePotencyClickDevice(training, event.getPotency());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetStopEvent(StopEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        String deviceID = event.getUid();
        for (Training training: trainings){
            if (deviceID.equals(training.getUid())){
                if (isStartNormal()) {
                    training.setTrainingStatus(Training.TRAINING_STATUS_PAUSE);
                    training.setLevel(0, 0);
                    setPotencyLevel(getVestIndexUser(training), training, training.getUid());
                    fragmentNormal.updatePauseUi(0);
                    fragmentNormal.updateEstimulationUi(100);
                    fragmentNormal.updateButtonsUi();
                } else {
                    setPotencyLevel(getVestIndexUser(training), training, training.getUid());
                    fragmentGlobal.updateStopDevice(training);
                    fragmentGlobal.updateControlsUi();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeviceStatusEvent(DeviceStatusEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().removeStickyEvent(event);

        if (event.getUid().equals(trainings.get(positionSelected).getUid())){
            txtBatery.setText(getString(R.string.status_batery)+event.getBatery()+"%");
            txtFirmware.setText(getString(R.string.status_firmware)+event.getFirmwareVersion());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetStartEvent(StartEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        String deviceID = event.getUid();
        for (Training training: trainings){
            if (deviceID.equals(training.getUid())){
                if (!isStartGlobal()){
                    startConfigTimer();
                    switch (training.getTrainingStatus()) {
                        case Training.TRAINING_STATUS_PAUSE:

                            switch (connectionType){
                                case Device.DEVICE_WIFI:
                                    restartProgram(getVestIndexUser(training), training.getUid(), training);
                                    sendChannelsLevel(getVestIndexUser(training),training, training.getUid());
                                    setPotencyLevel(getVestIndexUser(training),training, training.getUid());
                                    break;
                                case Device.DEVICE_BLE:
                                    EmsApp.getInstance().getmBluetoothLeService().sendRestartAndChannelsProgram(training.getUid(), training);
                                    break;
                            }
                            break;
                        case Training.TRAINING_STATUS_STOP:
                            sendProgram(getVestIndexUser(training),training, training.getUid());
                            break;
                    }
                    training.setTrainingStatus(Training.TRAINING_STATUS_PLAY);
                    fragmentNormal.updateButtonsUi();
                } else {
                    fragmentGlobal.updateRestartDevice(training, ExerciseMultipleActivity.this);
                }
            }
        }
    }

    public ArrayList<Training> getTrainings() {
        return trainings;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public Training getTrainingGlobal() {
        return trainingGlobal;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      DEVICE COMUNICATION
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Send Program
    public void sendProgram(int vestIndex, Training training, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                //Program -> Main -> Levels -> Chronaxy
                server.send(ServerEmsCommands.getProgramCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getMainCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getLevelsCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getChronaxyCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendProgramAndStart(adress, training);
                break;
        }
    }

    public void sendProgramChange(int vestIndex, Training training, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                //Program -> Main -> Levels -> Chronaxy
                server.send(ServerEmsCommands.getProgramCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getMainCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getLevelsCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getChronaxyCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendProgram(adress, training);
                break;
        }
    }

    //Send Start
    public void sendStartProgram(int vestIndex, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getStartCommand(data, vestIndex) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendStartProgram(adress);
                break;
        }
    }

    //Send restart
    public void restartProgram(int vestIndex, String adress, Training training){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getRestartCommand(data, vestIndex) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendRestartProgram(adress, training);
                break;
        }
    }

    //Send standart selected
    public void sendStandardSelected(int vestIndex, Training training, String adress){
        try{
            switch (connectionType){
                case Device.DEVICE_WIFI:
                    server.send(ServerEmsCommands.getMainCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                    server.send(ServerEmsCommands.getLevelsCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                    break;
                case Device.DEVICE_BLE:
                    EmsApp.getInstance().getmBluetoothLeService().sendStandardSelected(adress, training);
                    break;
            }
            fragmentNormal.loadLevelsValue();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Send standart user
    public void sendStandardUser(int vestIndex, Training training, String adress){
        Training trainingUpdated = setStandarValuesToUser(training);
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getMainCommand(data, vestIndex, trainingUpdated) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getLevelsCommand(data, vestIndex, trainingUpdated) ,data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getChronaxyCommand(data, vestIndex, trainingUpdated) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendStandardUser(adress, trainingUpdated);
                break;
        }

        fragmentNormal.loadLevelsValue();
    }

    //Send chronaxy
    public void sendChronaxyLevels(int vestIndex, Training training, String adress) {
        data = EmsApp.getData();

        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getChronaxyCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendChronaxyProgram(adress, training);
                break;
        }
    }

    //Update Main
    public void updateMainValues(int vestIndex, Training training, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getStageCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendStageProgram(adress, training);
                break;
        }
    }

    //Send levels
    public void  sendChannelsLevel(int vestIndex, Training training, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getLevelsCommand(data, vestIndex, training) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendLevelsProgram(adress, training);
                break;
        }
    }

    //Send Stop
    public void sendStopButton(int vestIndex, String adress){
        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getStopCommand(data, vestIndex) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendStopProgram(adress);
                break;
        }
    }

    //Send Potenci initial
    public void setPotencyLevelInitial(Training training, String adress) {
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updatePotency(trainings.get(0).getLevel(0), data.getVest(0).getUidString());

        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getMainCommand(data, getVestIndexUser(training), training) , data.getVest(getVestIndexUser(training)).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendMainProgram(adress, training);
                break;
        }
    }

    //Send Potency level
    public void setPotencyLevel(int vestIndex, Training training, String adress) {
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updatePotency(training.getLevel(0), data.getVest(vestIndex).getUidString());

        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getMainCommand(data, vestIndex, training) , data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendMainProgram(adress, training);
                break;
        }
    }

    //Send Potency level
    public void setPotencyLevelAndStop(int vestIndex, Training training, String adress) {
        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
        if (exerciseMaxForcePresentationService != null)
            exerciseMaxForcePresentationService.updatePotency(training.getLevel(0), data.getVest(vestIndex).getUidString());

        switch (connectionType){
            case Device.DEVICE_WIFI:
                server.send(ServerEmsCommands.getMainCommand(data, vestIndex, training) , data.getVest(vestIndex).getIP());
                server.send(ServerEmsCommands.getStopCommand(data, vestIndex) ,data.getVest(vestIndex).getIP());
                break;
            case Device.DEVICE_BLE:
                EmsApp.getInstance().getmBluetoothLeService().sendMainAndStopProgram(adress, training);
                break;
        }
    }

    private void bindServer() {
        data = EmsApp.getData();
        server = EmsApp.getServer();

        myServerFilter = new IntentFilter();
        myServerFilter.addAction(UDPServer.MESSAGE_RECEIVED);
        registerReceiver(myServerReciver, myServerFilter);
    }

    private BroadcastReceiver myServerReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String myParam = intent.getExtras().getString("MESSAGE_STRING");
            if (myParam != null) {
                ExerciseMultipleActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ExerciseMultipleActivity.this, myParam, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    };

    public void sendResultsTraining()  {
        ArrayList<Device> devices = pref.getAllDevices();
        ArrayList<UserMachine> userMachine = new ArrayList<>();

        for (User user: participants){
            for (Device device: devices){
                if (device.getUserAsigned() != null && user.getId().equals(device.getUserAsigned().getId())){
                    try {
                        switch (connectionType){
                            case Device.DEVICE_WIFI:
                                userMachine.add(new UserMachine(new String(device.getUIDNewByte(), "UTF-8").trim(),user.getId()));
                                break;
                            case Device.DEVICE_BLE:
                                userMachine.add(new UserMachine(device.getDeviceBleAdress(),user.getId()));
                                break;
                        }
                    } catch (UnsupportedEncodingException e) {
                        linearLoading.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(c.getTime());

        Gson gson = new Gson();
        String jsonUserMachine = gson.toJson(userMachine);

        HashMap<String, Object> params = new HashMap<>();
        try {
            params.put(WSConfig.PARAM_USER_MACHINE, new JSONArray(jsonUserMachine));
            params.put(WSConfig.PARAM_DATE, date);
            params.put(WSConfig.PARAM_DURATION, ""+trainingGlobal.getTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!EmsApp.isModeLocal()) {
            linearLoading.setVisibility(View.VISIBLE);
            txtLoading.setText(getString(R.string.loading_send_results));
            EmsApi.getInstance().doSendResults(new JSONObject(params));
        } else {
            SessionResults sessionResults = new SessionResults(UUID.randomUUID().toString() ,user.getId(), jsonUserMachine, date, ""+trainingGlobal.getTime());
            AppDatabase.addSessionResult(ExerciseMultipleActivity.this, sessionResults);
        }
    }

    @Subscribe
    public void onSendResults(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        if (event.getRequestType() == EmsApi.SAVE_STANDARD_VALUES){
            Toast.makeText(ExerciseMultipleActivity.this, getString(R.string.exercise_standard_updated), Toast.LENGTH_SHORT).show();
        }
    }

    public void startConfigTimer(){
        countDownTimer = new CountDownTimer(36000*1000, 30000) {
            public void onTick(long millisUntilFinished) {
                sendConfigUser();
            }

            public void onFinish() {
                if (countDownTimer != null)
                    countDownTimer.cancel();
            }
        }.start();
    }

    public void stopConfigTimer(){
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    //Save levels config
    public void sendConfigUser(){
        for (int i=0; i<trainings.size(); i++){
            if (trainings.get(i).isChangeLevel()){
                trainings.get(i).setChangeLevel(false);

                saveUserValuesParams(trainings.get(i), participants.get(i), trainings.get(i).getLevel(0));
            }
        }
    }

    /**
     * Chromecast
     */

    @Override
    public void onStart() {
        super.onStart();
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    @Override
    public void onStop() {
        super.onStop();
        mMediaRouter.removeCallback(mMediaRouterCallback);
    }

    public void setMediaRoute() {
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(CastMediaControlIntent.categoryForCast(getString(R.string.app_id_cast)))
                .build();
        mMediaRouter = MediaRouter.getInstance(this);

        mediaRouteButton = findViewById(R.id.media_route_button);
        mediaRouteButton.setRouteSelector(mMediaRouteSelector);
    }

    private void startCastService() {
        Intent intent = new Intent(ExerciseMultipleActivity.this, ExerciseMultipleActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(ExerciseMultipleActivity.this, 0, intent, 0);

        CastRemoteDisplayLocalService.NotificationSettings settings = new CastRemoteDisplayLocalService
                .NotificationSettings.Builder()
                .setNotificationPendingIntent(notificationPendingIntent)
                .build();

        CastRemoteDisplayLocalService.startService(ExerciseMultipleActivity.this, ExerciseMaxForcePresentationService.class, getString(R.string.app_id_cast), castDevice, settings,
                new CastRemoteDisplayLocalService.Callbacks() {
                    @Override
                    public void onServiceCreated(CastRemoteDisplayLocalService service) {
                        Log.d("TAG", "onServiceCreated");
                    }

                    @Override
                    public void onRemoteDisplaySessionStarted(CastRemoteDisplayLocalService service) {
                        Log.d("TAG", "onServiceStarted");
                        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
                        if (exerciseMaxForcePresentationService != null){
                            exerciseMaxForcePresentationService.saveDefaultValues(trainings, fragmentNormal.getTraining().getTime(), ExerciseMultipleActivity.this, trainingGlobal.getTitle());
                        }

                    }

                    @Override
                    public void onRemoteDisplaySessionError(Status errorReason) {
                        Log.d("TAG", "onServiceError: " + errorReason.getStatusCode());
                        initError();
                        castDevice = null;
                    }

                    @Override
                    public void onRemoteDisplaySessionEnded(CastRemoteDisplayLocalService castRemoteDisplayLocalService) {

                    }
                });
    }

    public void stopCastService(){
        CastRemoteDisplayLocalService.stopService();
    }

    private void initError() {
        Toast toast = Toast.makeText(
                getApplicationContext(),"Error starting the remote display" , Toast.LENGTH_SHORT);
        mMediaRouter.selectRoute(mMediaRouter.getDefaultRoute());
        toast.show();
    }

    public int getVestIndexUser(Training training){
        if (connectionType == Device.DEVICE_WIFI) {
            if (data == null)
                data = EmsApp.getData();

            if (data.getVests().isEmpty()){
                ArrayList<Device> devices = pref.getAllDevices();
                for (Device device: devices){
                    data.addVest(device.getName(),device.getUid(),device.getIp(),device.isNewDevice());
                    EmsApp.setData(data);
//                server.uids.add(device.getUid());
//                server.ips.add(device.getIp());

//                EmsApp.setServer(server);
                }
            }

            for (int i=0; i<data.getVestsCount(); i++){
                if (data.getVest(i).getname().equals(training.getDevice().getName())){
                    return i;
                }
            }
        }

        return 0;
    }

    public void goPageIndividual(int positionSelected){
        for (User user: participants){
            user.setSelected(false);
        }

        participants.get(positionSelected).setSelected(true);
        fragmentNormal.updateVestIndex(positionSelected);
        pagerExercice.setCurrentItem(0);
    }

    public boolean isStartGlobal() {
        return startGlobal;
    }

    public void setStartGlobal(boolean startGlobal) {
        this.startGlobal = startGlobal;
    }

    public boolean isStartNormal() {
        return startNormal;
    }

    public boolean isStart(){
        if (startGlobal || startNormal)
            return true;

        return false;
    }

    public void setStartNormal(boolean startNormal) {
        this.startNormal = startNormal;
    }

    public int getTimeGlobalRemaining() {
        return timeGlobalRemaining;
    }

    public static int getVestIndexGlobal() {
        return vestIndexGlobal;
    }

    public void setVestIndexGlobal(Training training) {
        vestIndexGlobal = getVestIndexUser(training);
    }

    public void updateValuesDetails(Training training){
        txtFrequencyNormal.setText(getString(R.string.btn_frequency)+": "+training.getFrequency());
        txtFrequencyRelax.setText(getString(R.string.dialog_freq_relax_info)+": "+training.getFrequencyRelax());
        txtPotencyRelax.setText(getString(R.string.dialog_freq_power_info)+": "+training.getPotencyRelax());
        txtPulse.setText(getString(R.string.dialog_freq_pulse)+": "+training.getPulseRelax());
        txtEstimTime.setText(getString(R.string.estim_time)+": "+((double)training.getEstimulationTime()/1000));
        txtPauseTime.setText(getString(R.string.pause_time)+": "+((double)training.getPauseTime()/1000));
        txtRampUp.setText(getString(R.string.dialog_ramp_up)+": "+((double)training.getRampUp()/1000));
        txtRampDown.setText(getString(R.string.dialog_ramp_down)+": "+((double)training.getRampDown()/1000));
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }

    //----------------------------------------------------------------------------------------------
    //-----------------------------------------BLUETOOTH--------------------------------------------
    //----------------------------------------------------------------------------------------------
    public void connectDevice(int gatPosition){
        switch (gatPosition){
            case 0:
                if (mGattManager == null) mGattManager = new GattManager(ExerciseMultipleActivity.this);
                mGattManager.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManager);
                break;
            case 1:
                if (mGattManagerSecond == null) mGattManagerSecond = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerSecond.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerSecond);
                break;
            case 2:
                if (mGattManagerThird == null) mGattManagerThird = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerThird.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerThird);
                break;
            case 3:
                if (mGattManagerFourth == null) mGattManagerFourth = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerFourth.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerFourth);
                break;
            case 4:
                if (mGattManagerFive == null) mGattManagerFive = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerFive.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerFive);
                break;
            case 5:
                if (mGattManagerSix == null) mGattManagerSix = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerSix.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerSix);
                break;
            case 6:
                if (mGattManagerSeven == null) mGattManagerSeven = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerSeven.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerSeven);
                break;
            case 7:
                if (mGattManagerEigth == null) mGattManagerEigth = new GattManager(ExerciseMultipleActivity.this);
                mGattManagerEigth.queue(new GattSetNotificationOperation(trainings.get(gatPosition).getDevice().getDeviceBle(), GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
                trainings.get(gatPosition).getDevice().setDeviceBleConnected(true);
                addGattListener(mGattManagerEigth);
                break;
        }
    }

    public void createConnections(){
        if (!maxForceIndividual){
            if (haveMaxForce && !maxForceConected){
                linearLoadingMaxforce.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (Training training: trainings){
                            if (training.getDevice().getDeviceBle() != null && !training.getDevice().isDeviceBleConnected()){
                                connectDevice(trainings.indexOf(training));
                            }
                        }
                    }
                }, 1000);

                maxForceConected = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentGlobal.doPlay();
                        linearLoadingMaxforce.setVisibility(View.GONE);
                    }
                }, 4000);
            } else {
                fragmentGlobal.doPlay();
                linearLoadingMaxforce.setVisibility(View.GONE);
            }
        } else {
            maxForceIndividual = false;
        }
    }

    public void createConnectionIndividual(){
        BluetoothDevice device = trainings.get(positionSelected).getDevice().getDeviceBle();
        if (device != null && !trainings.get(positionSelected).getDevice().isDeviceBleConnected()){
            if (mGattManager == null)
                mGattManager = new GattManager(ExerciseMultipleActivity.this);

            maxForceIndividual = true;
            trainings.get(positionSelected).getDevice().setDeviceBleConnected(true);
            mGattManager.queue(new GattSetNotificationOperation(device, GattManager.FORCE_SERVICE_UUID, GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, GattManager.CLIENT_UUID));
            addGattListener();
        }
    }

    public void addGattListener(GattManager gatt) {
        gatt.addCharacteristicChangeListener(GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, new CharacteristicChangeListener() {
            @Override
            public void onCharacteristicChanged(String deviceAddress, BluetoothGattCharacteristic characteristic) {
                final byte[] data = characteristic.getValue();
                if (data != null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data) {
                        stringBuilder.append(String.format("%02X ", byteChar));
                    }

                    String dataHEX = stringBuilder.toString().replaceAll("\\s+", "");
                    int force = (Integer.valueOf(dataHEX.substring(2, 4) + dataHEX.substring(0, 2), 16).shortValue());

                    fragmentGlobal.updateForceMaxForce(deviceAddress, Math.abs(force));

                    if (trainings.get(positionSelected).getDevice().getDeviceBle() != null && trainings.get(positionSelected).getDevice().getDeviceBle().getAddress().equals(deviceAddress))
                        fragmentNormal.updateForceMaxForce(Math.abs(force));

                    ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
                    if (exerciseMaxForcePresentationService != null) {
                        exerciseMaxForcePresentationService.updateMaxForce(Math.abs(force), deviceAddress);
                    }
                }
            }
        });
    }

    public void addGattListener(){
        if (mGattManager != null){
            mGattManager.addCharacteristicChangeListener(GattManager.FORCE_MEASURE_CHARACTERISTIC_UUID, new CharacteristicChangeListener() {
                @Override
                public void onCharacteristicChanged(String deviceAddress, BluetoothGattCharacteristic characteristic) {
                    final byte[] data = characteristic.getValue();
                    if (data != null && data.length > 0) {
                        final StringBuilder stringBuilder = new StringBuilder(data.length);
                        for(byte byteChar : data){
                            stringBuilder.append(String.format("%02X ", byteChar));
                        }

                        String dataHEX = stringBuilder.toString().replaceAll("\\s+","");
                        int force = (Integer.valueOf(dataHEX.substring(2,4)+dataHEX.substring(0,2), 16).shortValue());

                        fragmentGlobal.updateForceMaxForce(deviceAddress, Math.abs(force));

                        if (trainings.get(positionSelected).getDevice().getDeviceBle() != null && trainings.get(positionSelected).getDevice().getDeviceBle().getAddress().equals(deviceAddress))
                            fragmentNormal.updateForceMaxForce(Math.abs(force));

                        ExerciseMaxForcePresentationService exerciseMaxForcePresentationService = (ExerciseMaxForcePresentationService) CastRemoteDisplayLocalService.getInstance();
                        if (exerciseMaxForcePresentationService != null){
                            exerciseMaxForcePresentationService.updateMaxForce(Math.abs(force), deviceAddress);
                        }
                    }
                }
            });
        }
    }

    public void setMaxForceIndividual(boolean maxForceIndividual) {
        this.maxForceIndividual = maxForceIndividual;
    }

    public void disconnectAll(){
        if (mGattManager != null)
            mGattManager.disconnectAll();

        if (mGattManagerSecond != null)
            mGattManagerSecond.disconnectAll();

        if (mGattManagerThird != null)
            mGattManagerThird.disconnectAll();

        if (mGattManagerFourth != null)
            mGattManagerFourth.disconnectAll();

        if (mGattManagerFive != null)
            mGattManagerFive.disconnectAll();

        if (mGattManagerSix != null)
            mGattManagerSix.disconnectAll();

        if (mGattManagerSeven != null)
            mGattManagerSeven.disconnectAll();

        if (mGattManagerEigth != null)
            mGattManagerEigth.disconnectAll();
    }

    public void globalSectionPermission(){
        if (!user.getUserPmissions().isSectionGlobal())
            pagerExercice.setPagingEnabled(false);
    }

    public Training setStandarValuesToUser(Training training){
        training.setLevel(Vest.CHANNEL_LEG, user.getStandard().getLeg());
        training.setLevel(Vest.CHANNEL_GLU, user.getStandard().getGlu());
        training.setLevel(Vest.CHANNEL_LUM, user.getStandard().getLum());
        training.setLevel(Vest.CHANNEL_DOR, user.getStandard().getDor());
        training.setLevel(Vest.CHANNEL_SHO, user.getStandard().getSho());
        training.setLevel(Vest.CHANNEL_ABS, user.getStandard().getAbs());
        training.setLevel(Vest.CHANNEL_PEC, user.getStandard().getPec());
        training.setLevel(Vest.CHANNEL_BIC, user.getStandard().getBic());

        if (!training.getIdTraining().equals("id_relax")){
            training.setChronaxy(Vest.CHANNEL_LEG, user.getStandard().getCronLeg());
            training.setChronaxy(Vest.CHANNEL_GLU, user.getStandard().getCronGlu());
            training.setChronaxy(Vest.CHANNEL_LUM, user.getStandard().getCronLum());
            training.setChronaxy(Vest.CHANNEL_DOR, user.getStandard().getCronDor());
            training.setChronaxy(Vest.CHANNEL_SHO, user.getStandard().getCronSho());
            training.setChronaxy(Vest.CHANNEL_ABS, user.getStandard().getCronAbs());
            training.setChronaxy(Vest.CHANNEL_PEC, user.getStandard().getCronPec());
            training.setChronaxy(Vest.CHANNEL_BIC, user.getStandard().getCronBic());
        }

        return training;
    }

    public void saveUserValuesParams(Training training, User userSave, int potency ) {
        ArrayList<CorporalZone> corporalZones = new ArrayList<>();
        CorporalZone cz = null;

        if (!EmsApp.isModeLocal()) {
            corporalZones.addAll(userSave.getCorporalZones());
            cz = userSave.getCorporalZone(training.getIdTraining());
        } else {
            for (User profile: user.getProfiles()){
                if (profile.getId().equals(userSave.getId())) {
                    corporalZones.addAll(profile.getCorporalZones());
                    cz = profile.getCorporalZone(training.getIdTraining());
                    break;
                }
            }
        }

        if (cz == null){
            cz = new CorporalZone(training.getIdTraining(), training.getLevelAbs(),training.getLevelBic(), training.getLevelDor(),
                    training.getLevelLum(), training.getLevelSho(), training.getLevelLeg(), training.getLevelGlu(), training.getLevelPec(),
                    training.getLevelAux(), training.getLevelAux2(),training.getCronaxyAbs(), training.getCronaxyBic(), training.getCronaxyDor(),
                    training.getCronaxyLum(), training.getCronaxySho(), training.getCronaxyLeg(), training.getCronaxyGlu(), training.getCronaxyPec(),
                    training.getCronaxyAux(), training.getCronaxyAux2(),potency);
        } else {
            corporalZones.remove(cz);
            cz.setId(training.getIdTraining());cz.setLeg(training.getLevelLeg());cz.setGlu(training.getLevelGlu());cz.setLum(training.getLevelLum());
            cz.setDor(training.getLevelDor());cz.setSho(training.getLevelSho());cz.setAbs(training.getLevelAbs());cz.setPec(training.getLevelPec());
            cz.setBic(training.getLevelBic());cz.setAux(training.getLevelAux());cz.setAux2(training.getLevelAux2());cz.setCronAbs(training.getCronaxyAbs());
            cz.setCronBic(training.getCronaxyBic());cz.setCronDor(training.getCronaxyDor());cz.setCronLum(training.getCronaxyLum());cz.setCronSho(training.getCronaxySho());
            cz.setCronLeg(training.getCronaxyLeg());cz.setCronGlu(training.getCronaxyGlu());cz.setCronPec(training.getCronaxyPec());cz.setCronAux(training.getCronaxyAux());
            cz.setCronAux2(training.getCronaxyAux2());

            if (potency > 0)
                cz.setPotency(potency);
        }
        corporalZones.add(cz);

        if (!EmsApp.isModeLocal()) {
            ArrayList<JSONObject> allResults = new ArrayList<>();
            for (CorporalZone corporalZone: corporalZones){
                HashMap<String, String> params = new HashMap<>();
                params.put(WSConfig.PARAM_ID, corporalZone.getId());
                params.put(WSConfig.PARAM_LEG, ""+corporalZone.getLeg());
                params.put(WSConfig.PARAM_GLU, ""+corporalZone.getGlu());
                params.put(WSConfig.PARAM_LUM, ""+corporalZone.getLum());
                params.put(WSConfig.PARAM_DOR, ""+corporalZone.getDor());
                params.put(WSConfig.PARAM_SHO, ""+corporalZone.getSho());
                params.put(WSConfig.PARAM_ABS, ""+corporalZone.getAbs());
                params.put(WSConfig.PARAM_PEC, ""+corporalZone.getPec());
                params.put(WSConfig.PARAM_BIC, ""+corporalZone.getBic());
                params.put(WSConfig.PARAM_AUX, ""+corporalZone.getAux());
                params.put(WSConfig.PARAM_AUX2, ""+corporalZone.getAux2());
                params.put(WSConfig.PARAM_CRON_LEG, ""+corporalZone.getCronLeg());
                params.put(WSConfig.PARAM_CRON_GLU, ""+corporalZone.getCronGlu());
                params.put(WSConfig.PARAM_CRON_LUM, ""+corporalZone.getCronLum());
                params.put(WSConfig.PARAM_CRON_DOR, ""+corporalZone.getCronDor());
                params.put(WSConfig.PARAM_CRON_SHO, ""+corporalZone.getCronSho());
                params.put(WSConfig.PARAM_CRON_ABS, ""+corporalZone.getCronAbs());
                params.put(WSConfig.PARAM_CRON_PEC, ""+corporalZone.getCronPec());
                params.put(WSConfig.PARAM_CRON_BIC, ""+corporalZone.getCronBic());
                params.put(WSConfig.PARAM_CRON_AUX, ""+corporalZone.getCronAux());
                params.put(WSConfig.PARAM_CRON_AUX2, ""+corporalZone.getCronAux2());
                params.put(WSConfig.PARAM_POTENCY, ""+corporalZone.getPotency());
                allResults.add(new JSONObject(params));
            }

            HashMap<String, Object> params = new HashMap<>();
            params.put(WSConfig.PARAM_DATA, new JSONArray(allResults));
            params.put(WSConfig.PARAM_LOG, new JSONArray(new ArrayList<>()));

            EmsApi.getInstance().doSendUserConfig(params, userSave);
        } else {
            for (User profile: user.getProfiles()){
                if (profile.getId().equals(userSave.getId())) {

                    //Update user profile
                    profile.getCorporalZones().clear();
                    profile.setCorporalZone(corporalZones);

                    //Update participant
                    for (User participant: participants){
                        if (participant.getId().equals(profile.getId())) {
                            participant.getCorporalZones().clear();
                            participant.setCorporalZone(corporalZones);
                        }
                    }

                    //Update database
                    Log.v("Save", "Save corporal zones");
                    AppDatabase.updateUser(ExerciseMultipleActivity.this, user);
                    break;
                }
            }
            pref.saveUser(user);
        }
    }

    @Override
    public void onConfirmationFrequencyProgram(boolean sendFrequency, int frequency, int action, Training trainingLoad, Training trainingOld, boolean fromGlobal) {
        switch (action){
            case FrequencyDialog.ACTION_LOAD:
                //GLOBAL
                if (fromGlobal){
                    updateGlobalTraining(trainingLoad, trainingGlobal);

                    for (Training training: trainings){
                        updateTrainingUser(trainingLoad, trainings.indexOf(training));
                        sendProgramChange(trainings.indexOf(training), trainingLoad, training.getDevice().getDeviceBleAdress());
                    }
                    fragmentNormal.updateTimeText(trainingGlobal.getTime());
                    fragmentGlobal.doStopChangeProgram();
                    //NORMAL
                } else {
                    int indexUser = getVestIndexUser(trainingOld);
                    updateTrainingUser(trainingLoad, indexUser);

                    fragmentNormal.doStopChangeProgram();
                    sendProgramChange(indexUser, trainingLoad, trainingOld.getDevice().getDeviceBleAdress());
                }
                break;
            case FrequencyDialog.ACTION_FREQUENCY:
                if (sendFrequency){
                    fragmentNormal.getTraining().setFrequency(frequency);
                    updateMainValues(fragmentNormal.getVestIndex(), fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
                    setPotencyLevelInitial(fragmentNormal.getTraining(), fragmentNormal.getTraining().getUid());
                    updateValuesDetails(fragmentNormal.getTraining());
                }
                break;
        }
    }

    public void updateTrainingUser(Training trainingLoad, int position){
        trainings.get(position).setIdTraining(trainingLoad.getIdTraining());
        trainings.get(position).setTrainingType(trainingLoad.getTrainingType());
        trainings.get(position).setImage(trainingLoad.getImage());
        trainings.get(position).setTitle(trainingLoad.getTitle());
        trainings.get(position).setChronaxy(Vest.CHANNEL_LEG,trainingLoad.getChronaxy(Vest.CHANNEL_LEG));
        trainings.get(position).setChronaxy(Vest.CHANNEL_GLU,trainingLoad.getChronaxy(Vest.CHANNEL_GLU));
        trainings.get(position).setChronaxy(Vest.CHANNEL_LUM,trainingLoad.getChronaxy(Vest.CHANNEL_LUM));
        trainings.get(position).setChronaxy(Vest.CHANNEL_SHO,trainingLoad.getChronaxy(Vest.CHANNEL_SHO));
        trainings.get(position).setChronaxy(Vest.CHANNEL_BIC,trainingLoad.getChronaxy(Vest.CHANNEL_BIC));
        trainings.get(position).setChronaxy(Vest.CHANNEL_DOR,trainingLoad.getChronaxy(Vest.CHANNEL_DOR));
        trainings.get(position).setChronaxy(Vest.CHANNEL_ABS,trainingLoad.getChronaxy(Vest.CHANNEL_ABS));
        trainings.get(position).setChronaxy(Vest.CHANNEL_PEC,trainingLoad.getChronaxy(Vest.CHANNEL_PEC));
        trainings.get(position).setChronaxy(Vest.CHANNEL_AUX,trainingLoad.getChronaxy(Vest.CHANNEL_AUX));
        trainings.get(position).setChronaxy(Vest.CHANNEL_AUX_2,trainingLoad.getChronaxy(Vest.CHANNEL_AUX_2));
        trainings.get(position).setFrequency(trainingLoad.getFrequency());
        trainings.get(position).setFrequencyRelax(trainingLoad.getFrequencyRelax());
        trainings.get(position).setPulseRelax(trainingLoad.getPulseRelax());
        trainings.get(position).setEstimulationTime(trainingLoad.getEstimulationTime());
        trainings.get(position).setPauseTime(trainingLoad.getPauseTime());
        trainings.get(position).setRampUp(trainingLoad.getRampUp());
        trainings.get(position).setRampDown(trainingLoad.getRampDown());
        trainings.get(position).setTime(trainingLoad.getTime());
        trainings.get(position).setChronaxyDefault(trainingLoad.getChronaxyDefault());
        trainings.get(position).setPotencyRelax(trainingLoad.getPotencyRelax());
        updateValuesDetails(trainings.get(position));
    }

    public void updateGlobalTraining(Training trainingLoad, Training trainingGlobal){
        trainingGlobal.setIdTraining(trainingLoad.getIdTraining());
        trainingGlobal.setTrainingType(trainingLoad.getTrainingType());
        trainingGlobal.setImage(trainingLoad.getImage());
        trainingGlobal.setTitle(trainingLoad.getTitle());
        trainingGlobal.setChronaxy(Vest.CHANNEL_LEG,trainingLoad.getChronaxy(Vest.CHANNEL_LEG));
        trainingGlobal.setChronaxy(Vest.CHANNEL_GLU,trainingLoad.getChronaxy(Vest.CHANNEL_GLU));
        trainingGlobal.setChronaxy(Vest.CHANNEL_LUM,trainingLoad.getChronaxy(Vest.CHANNEL_LUM));
        trainingGlobal.setChronaxy(Vest.CHANNEL_SHO,trainingLoad.getChronaxy(Vest.CHANNEL_SHO));
        trainingGlobal.setChronaxy(Vest.CHANNEL_BIC,trainingLoad.getChronaxy(Vest.CHANNEL_BIC));
        trainingGlobal.setChronaxy(Vest.CHANNEL_DOR,trainingLoad.getChronaxy(Vest.CHANNEL_DOR));
        trainingGlobal.setChronaxy(Vest.CHANNEL_ABS,trainingLoad.getChronaxy(Vest.CHANNEL_ABS));
        trainingGlobal.setChronaxy(Vest.CHANNEL_PEC,trainingLoad.getChronaxy(Vest.CHANNEL_PEC));
        trainingGlobal.setChronaxy(Vest.CHANNEL_AUX,trainingLoad.getChronaxy(Vest.CHANNEL_AUX));
        trainingGlobal.setChronaxy(Vest.CHANNEL_AUX_2,trainingLoad.getChronaxy(Vest.CHANNEL_AUX_2));
        trainingGlobal.setFrequency(trainingLoad.getFrequency());
        trainingGlobal.setFrequencyRelax(trainingLoad.getFrequencyRelax());
        trainingGlobal.setPulseRelax(trainingLoad.getPulseRelax());
        trainingGlobal.setEstimulationTime(trainingLoad.getEstimulationTime());
        trainingGlobal.setPauseTime(trainingLoad.getPauseTime());
        trainingGlobal.setRampUp(trainingLoad.getRampUp());
        trainingGlobal.setRampDown(trainingLoad.getRampDown());
        trainingGlobal.setTime(trainingLoad.getTime());
        trainingGlobal.setChronaxyDefault(trainingLoad.getChronaxyDefault());
        trainingGlobal.setPotencyRelax(trainingLoad.getPotencyRelax());
        updateValuesDetails(trainingGlobal);
    }
}
