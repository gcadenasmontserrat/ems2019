package com.myofx.emsapp.ui.local;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.SyncAction;
import com.myofx.emsapp.api.events.SyncCorporalZones;
import com.myofx.emsapp.api.events.SyncStandardValues;
import com.myofx.emsapp.api.events.SyncUserEvent;
import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.api.events.UserLocalEvent;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.database.AppDatabase;
import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CorporalZone;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.SessionResults;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.ui.dialogs.DeviceNameDialog;
import com.myofx.emsapp.ui.dialogs.PasswordDialog;
import com.myofx.emsapp.ui.local.adapters.LocalActionsAdapter;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class LocalFragment extends Fragment implements PasswordDialog.OnConfirmationNameListener {

    //UI
    private View rootView;
    private RelativeLayout relativeBackground;
    private LinearLayout linearHeaders;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private Button btnSync;
    private TextView txtInformation;
    private RecyclerView listActions;
    private TextView txtName;
    private ImageView imgUser;

    //VARIABLES
    private PreferencesManager pref;
    private LocalActionsAdapter localActionsAdapter;
    private ArrayList<LocalAction> localActions = new ArrayList<>();
    private ArrayList<LocalAction> localUserActions = new ArrayList<>();
    private int syncActionPosition = 0;
    private int syncUserActionPosition = 0;
    private String userTemporalId;
    private List<Booking> bookings = new ArrayList<>();
    private List<Training> trainings = new ArrayList<>();
    private SessionResults sessionResults;
    private User userWork;
    private int syncCorporalZonesNumber = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_local, container, false);
        pref = PreferencesManager.getInstance(getContext());

        bindUi();
        loadTheme();
        setListeners();
        loadData();

        return rootView;
    }

    public void bindUi(){
        listActions = rootView.findViewById(R.id.listActions);
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        linearHeaders = rootView.findViewById(R.id.linearHeaders);
        btnSync = rootView.findViewById(R.id.btnSync);
        txtInformation = rootView.findViewById(R.id.txtInformation);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        txtLoading = rootView.findViewById(R.id.txtLoading);
        txtName = rootView.findViewById(R.id.txtName);
        imgUser = rootView.findViewById(R.id.imgUser);

        listActions.setLayoutManager(new LinearLayoutManager(getActivity()));
        listActions.setNestedScrollingEnabled(false);
    }

    public void setListeners(){
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!EmsApi.isThereInternetConnection())
                    Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
                else
                    showPasswordDialog();
            }
        });
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        linearHeaders.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(getActivity()));
    }

    public void loadData(){
        //Objects to work
        bookings.addAll(AppDatabase.getBookingsUser(getContext(), pref.getUser().getId()));
        trainings.addAll(AppDatabase.getTrainings(getContext(), pref.getUser().getCenter()));
        sessionResults = AppDatabase.getSessionsResults(getContext(), pref.getUser().getId());
        userWork = AppDatabase.getUserById(getContext(), pref.getUser().getId());

        txtName.setText(userWork.getName());
        ImageUtils.loadImageFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+userWork.getImage());
        localUserActions.addAll(AppDatabase.getLocalUserActions(getContext(),userWork.getId()));
        localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getId()));
        localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getCenter()));

        localActionsAdapter = new LocalActionsAdapter(localActions, getActivity());
        listActions.setAdapter(localActionsAdapter);
    }

    //Sync users
    private void doNextUserSyncAction(){
        if (syncUserActionPosition < localUserActions.size() && localUserActions.size() !=0){
            syncUser(localActions.get(syncUserActionPosition));
        } else {
            localActions = new ArrayList<>();
            localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getId()));
            localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getCenter()));

            localActionsAdapter = new LocalActionsAdapter(localActions, getActivity());
            listActions.setAdapter(localActionsAdapter);

            doNextSyncAction();
        }
    }

    //Sync other actions
    private void doNextSyncAction(){
        if (syncActionPosition < localActions.size()){
            switch (localActions.get(syncActionPosition).getType()){
                case LocalAction.ACTION_TYPE_ADD_USER:
                    syncActionPosition = syncActionPosition + 1;
                    doNextSyncAction();
                    break;
                case LocalAction.ACTION_TYPE_ADD_TRAINING:
                    syncTraining(localActions.get(syncActionPosition));
                    break;
                case LocalAction.ACTION_TYPE_BOOKING:
                    syncBooking(localActions.get(syncActionPosition));
                    break;
            }
        } else {
            Log.v("Sync Type","Type: STANDARD");
            EmsApi.getInstance().syncStandardValues(userWork);
        }
    }

    /**
     * Sync user. After sync update relations with new ID.
     * @param action
     */
    private void syncUser(LocalAction action){
        User userSync = null;
        User user = AppDatabase.getUserById(getContext(), action.getOwnerId());
        for (User profile: user.getProfiles()){
            if (profile.getId().equals(action.getDataId())) {
                userSync = profile;
                break;
            }
        }

        if (userSync != null){
            userTemporalId = userSync.getId();
            EmsApi.getInstance().syncUser(userSync);
        } else {
            syncUserActionPosition = syncUserActionPosition + 1;
            doNextUserSyncAction();
        }
    }

    /**
     * Sync booking
     * @param action
     */
    private void syncBooking(LocalAction action){
        Booking bookingSync = null;

        for (Booking booking: bookings){
            if (booking.getId().equals(action.getDataId())) {
                bookingSync = booking;
                break;
            }
        }

        if (bookingSync != null){
            EmsApi.getInstance().syncBooking(bookingSync);
        } else {
            syncActionPosition = syncActionPosition + 1;
            doNextSyncAction();
        }
    }

    /**
     * Sync training
     * @param action
     */
    private void syncTraining(LocalAction action){
        Training trainingSync = null;
        for (Training training: trainings){
            if (training.getIdTraining().equals(action.getDataId())) {
                trainingSync = training;
                break;
            }
        }

        if (trainingSync != null){
            EmsApi.getInstance().syncTraining(trainingSync);
        } else {
            syncActionPosition = syncActionPosition + 1;
            doNextSyncAction();
        }
    }

    private void syncCorporalZone(){
        Log.v("CORPORAL TOTAL",""+userWork.getProfiles().size());
        Log.v("CORPORAL POSITION",""+syncCorporalZonesNumber);

        if (syncCorporalZonesNumber < userWork.getProfiles().size()) {
            ArrayList<JSONObject> allResults = new ArrayList<>();

            if (userWork.getProfiles().get(syncCorporalZonesNumber).getCorporalZones() != null) {
                for (CorporalZone corporalZone: userWork.getProfiles().get(syncCorporalZonesNumber).getCorporalZones()){
                    HashMap<String, String> params = new HashMap<>();
                    params.put(WSConfig.PARAM_ID, corporalZone.getId());
                    params.put(WSConfig.PARAM_LEG, ""+corporalZone.getLeg());
                    params.put(WSConfig.PARAM_GLU, ""+corporalZone.getGlu());
                    params.put(WSConfig.PARAM_LUM, ""+corporalZone.getLum());
                    params.put(WSConfig.PARAM_DOR, ""+corporalZone.getDor());
                    params.put(WSConfig.PARAM_SHO, ""+corporalZone.getSho());
                    params.put(WSConfig.PARAM_ABS, ""+corporalZone.getAbs());
                    params.put(WSConfig.PARAM_PEC, ""+corporalZone.getPec());
                    params.put(WSConfig.PARAM_BIC, ""+corporalZone.getBic());
                    params.put(WSConfig.PARAM_AUX, ""+corporalZone.getAux());
                    params.put(WSConfig.PARAM_AUX2, ""+corporalZone.getAux2());
                    params.put(WSConfig.PARAM_CRON_LEG, ""+corporalZone.getCronLeg());
                    params.put(WSConfig.PARAM_CRON_GLU, ""+corporalZone.getCronGlu());
                    params.put(WSConfig.PARAM_CRON_LUM, ""+corporalZone.getCronLum());
                    params.put(WSConfig.PARAM_CRON_DOR, ""+corporalZone.getCronDor());
                    params.put(WSConfig.PARAM_CRON_SHO, ""+corporalZone.getCronSho());
                    params.put(WSConfig.PARAM_CRON_ABS, ""+corporalZone.getCronAbs());
                    params.put(WSConfig.PARAM_CRON_PEC, ""+corporalZone.getCronPec());
                    params.put(WSConfig.PARAM_CRON_BIC, ""+corporalZone.getCronBic());
                    params.put(WSConfig.PARAM_CRON_AUX, ""+corporalZone.getCronAux());
                    params.put(WSConfig.PARAM_CRON_AUX2, ""+corporalZone.getCronAux2());
                    params.put(WSConfig.PARAM_POTENCY, ""+corporalZone.getPotency());
                    allResults.add(new JSONObject(params));
                }
            }

            HashMap<String, Object> params = new HashMap<>();
            params.put(WSConfig.PARAM_DATA, new JSONArray(allResults));
            params.put(WSConfig.PARAM_LOG, new JSONArray(new ArrayList<>()));
            EmsApi.getInstance().syncUserConfig(params, userWork.getProfiles().get(syncCorporalZonesNumber));
        } else {
            EmsApi.getInstance().updateLocalUserTask();
//            syncTrainingSession();
        }
    }

    private void syncTrainingSession(){
        Log.v("Sync Type","Type: SESSION");

        if (sessionResults != null) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(c.getTime());

            HashMap<String, Object> params = new HashMap<>();
            try {
                params.put(WSConfig.PARAM_USER_MACHINE, new JSONArray(sessionResults.getUsersMachines()));
                params.put(WSConfig.PARAM_DATE, sessionResults.getDate());
                params.put(WSConfig.PARAM_DURATION, ""+sessionResults.getDuration());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            EmsApi.getInstance().syncResults(new JSONObject(params));
        } else
            EmsApi.getInstance().updateLocalUserTask();
    }

    @Subscribe
    public void onSyncUserAction(SyncUserEvent syncUserEvent) {
        EventBus.getDefault().removeStickyEvent(syncUserEvent);

        //Update bookings with new created user ID.
        if (syncUserEvent.getUserSync() != null) {
            for (Booking booking: bookings){
                if (booking.getUserId() != null && booking.getUserId().equals(userTemporalId)){
                    booking.setUser_id(syncUserEvent.getUserSync().getId());
                }
            }
        }

        //Update CorporalZones with new created user ID
        if (syncUserEvent.getUserSync() != null) {
            for (User profile: userWork.getProfiles()){
                if (profile.getId().equals(userTemporalId)){
                    profile.setId(syncUserEvent.getUserSync().getId());
                }
            }
        }
        AppDatabase.updateUser(getContext(), userWork);

        syncUserActionPosition = syncUserActionPosition + 1;
        doNextUserSyncAction();
    }

    @Subscribe
    public void onSyncAction(SyncAction syncActionEvent) {
        syncActionPosition = syncActionPosition + 1;

        if (syncActionPosition < localActions.size())
            EventBus.getDefault().removeStickyEvent(syncActionEvent);

        doNextSyncAction();
    }

    @Subscribe
    public void onSyncStandard(SyncStandardValues syncStandardValues) {
        EventBus.getDefault().removeStickyEvent(syncStandardValues);

        syncCorporalZone();
    }

    @Subscribe
    public void onSynCorporalZones(SyncCorporalZones syncCorporalZones) {
        EventBus.getDefault().removeStickyEvent(syncCorporalZones);

        syncCorporalZonesNumber = syncCorporalZonesNumber +1;
        syncCorporalZone();
    }

    @Subscribe
    public void onSynSessionResults(SessionResults sessionResults) {
        EventBus.getDefault().removeStickyEvent(sessionResults);

        EmsApi.getInstance().updateLocalUserTask();
    }

    @Subscribe
    public void onLoginFailed(DefaultEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(event.getError().getMessageError()))
            Toast.makeText(getActivity(), event.getError().getMessageError(), Toast.LENGTH_SHORT).show();
        else  if (!TextUtils.isEmpty(event.getError().getErrorDescription()))
            Toast.makeText(getActivity(), event.getError().getErrorDescription(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        Toast.makeText(getActivity(), event.getMsgError(), Toast.LENGTH_SHORT).show();
        linearLoading.setVisibility(View.GONE);
    }

    private void showPasswordDialog() {
        PasswordDialog passwordDialog = PasswordDialog.newInstance(this);
        passwordDialog.setCancelable(true);
        passwordDialog.show(getFragmentManager(), DeviceNameDialog.class.getSimpleName());
    }

    @Override
    public void onPutPassword(String password) {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        EmsApi.getInstance().doLoginTask(pref.getUser().getEmail(), password);
        txtLoading.setText(getString(R.string.loading_sync));
        linearLoading.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onGetMainEvent(UserEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        pref.saveUser(event.getUser());
        pref.saveProfile(event.getUser().getId());
        pref.saveAccessToken(event.getUser().getAccesToken());
        pref.saveRefreshToken(event.getUser().getRefreshToken());

        doNextUserSyncAction();
    }

    @Subscribe
    public void onUpdateUserLocalEvent(UserLocalEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        linearLoading.setVisibility(View.GONE);

        AppDatabase.clearUserBookings(getContext(), userWork.getId());
        AppDatabase.clearSessionsResults(getContext(), userWork.getId());
        AppDatabase.clearTrainings(getContext(), userWork.getCenter());
        AppDatabase.clearLocalUserActions(getContext(), userWork.getId());
        AppDatabase.clearLocalUserActions(getContext(), userWork.getCenter());
        AppDatabase.clearUsers(getContext(), userTemporalId);
        AppDatabase.updateUser(getContext(), event.getUser());

        pref.saveUser(event.getUser());
        pref.saveProfile(event.getUser().getId());
        pref.saveAccessToken(event.getUser().getAccesToken());
        pref.saveRefreshToken(event.getUser().getRefreshToken());

        //Update List Actions
        localActions = new ArrayList<>();
        localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getId()));
        localActions.addAll(AppDatabase.getLocalActions(getContext(),userWork.getCenter()));

        localActionsAdapter = new LocalActionsAdapter(localActions, getActivity());
        listActions.setAdapter(localActionsAdapter);

        Toast.makeText(getContext(), getString(R.string.la_sync_eneded), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
