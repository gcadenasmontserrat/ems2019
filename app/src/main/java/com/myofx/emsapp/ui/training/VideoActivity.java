package com.myofx.emsapp.ui.training;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.YoutubePlayerFragment;
import com.myofx.emsapp.utils.ColorTheme;

/**
 * Created by Gerard on 30/11/2016.
 */

public class VideoActivity extends BaseActivity {

    public static final String EXTRA_ID_VIDEO = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_ID_VIDEO";

    //Ui
    private RelativeLayout relativeBackground;

    //Variable
    private String idVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (getIntent().hasExtra(EXTRA_ID_VIDEO)) {
            idVideo = getIntent().getStringExtra(EXTRA_ID_VIDEO);
        }

        bindUi();
        loadTheme();
        loadToolbar();
        loadVideo();
    }

    public void loadToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(VideoActivity.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(VideoActivity.this));
        }
    }

    public void bindUi(){
        relativeBackground = (RelativeLayout) findViewById(R.id.relativeBackground);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(VideoActivity.this));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadVideo(){
        YoutubePlayerFragment player = YoutubePlayerFragment.newInstance(idVideo);
        getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, player).commit();
    }
}
