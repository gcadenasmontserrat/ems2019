package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.MainActivity;

public class ConfirmationUpdateDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnAccept;

    //Variable
    private OnConfirmationUpdateListener mCallback;
    private MainActivity mainActivity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        mCallback = mainActivity;

        alertdialog = builder.create();
        return alertdialog;
    }

    public static ConfirmationUpdateDialog newInstance(MainActivity mainActivity) {
        ConfirmationUpdateDialog confirmationUpdateDialog = new ConfirmationUpdateDialog();
        confirmationUpdateDialog.mainActivity = mainActivity;
        return confirmationUpdateDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_confirmation, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnAccept = view.findViewById(R.id.btnAccept);

        setListeners();

        return view;
    }

    public void setListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false);
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(true);
            }
        });
    }

    public interface OnConfirmationUpdateListener {
        void onConfirmationUpdate (boolean delete);
    }

    private void sendResult(boolean delete) {
        if(mCallback != null) {
            mCallback.onConfirmationUpdate(delete);
            alertdialog.dismiss();
        }
    }
}
