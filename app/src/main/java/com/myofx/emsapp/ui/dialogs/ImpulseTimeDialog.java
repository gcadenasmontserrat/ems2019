package com.myofx.emsapp.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleActivity;

/**
 * Created by Gerard on 29/3/2017.
 */

public class ImpulseTimeDialog extends DialogFragment {

    public static final String EXTRA_IMPULSE_ESTIM = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_IMPULSE_ESTIM";
    public static final String EXTRA_IMPULSE_PAUSE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_IMPULSE_PAUSE";

    //Ui
    private AlertDialog alertdialog;
    private Button btnCancel;
    private Button btnSend;
    private ImageView imgEstimPlus;
    private ImageView imgEstimMinus;
    private ImageView imgPausePlus;
    private ImageView imgPauseMinus;
    private EditText editEstim;
    private EditText editPause;


    //Variable
    private ImpulseTimeDialog.OnConfirmationImpulseTimeListener mCallback;
    private Activity exerciseActivity;
    private int impulseEstimValue = 0;
    private int impulsePauseValue = 0;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        impulseEstimValue = (getArguments().getInt(EXTRA_IMPULSE_ESTIM));
        impulsePauseValue = (getArguments().getInt(EXTRA_IMPULSE_PAUSE));

        mCallback = (ExerciseMultipleActivity) exerciseActivity;

        loadImpulseTimeValues();

        alertdialog = builder.create();
        return alertdialog;
    }

    public static ImpulseTimeDialog newInstance(Activity exerciseActivity, int impulseEstim, int impulsePause) {
        ImpulseTimeDialog impulseTimeDialog = new ImpulseTimeDialog();
        Bundle args = new Bundle();
        args.putInt(EXTRA_IMPULSE_ESTIM, impulseEstim);
        args.putInt(EXTRA_IMPULSE_PAUSE, impulsePause);
        impulseTimeDialog.exerciseActivity = exerciseActivity;
        impulseTimeDialog.setArguments(args);
        return impulseTimeDialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_impulse_time, null);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        imgEstimPlus = (ImageView) view.findViewById(R.id.imgEstimPlus);
        imgEstimMinus = (ImageView) view.findViewById(R.id.imgEstimMinus);
        imgPausePlus = (ImageView) view.findViewById(R.id.imgPausePlus);
        imgPauseMinus = (ImageView) view.findViewById(R.id.imgPauseMinus);
        editEstim = (EditText) view.findViewById(R.id.editEstim);
        editPause = (EditText) view.findViewById(R.id.editPause);

        setListeners();

        return view;
    }

    public void loadImpulseTimeValues(){
        editEstim.setText(""+((double)impulseEstimValue/1000));
        editPause.setText(""+((double)impulsePauseValue/1000));
    }

    public void setListeners() {
        imgEstimPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impulseEstimValue = (int)(Double.parseDouble(editEstim.getText().toString())*1000);
                impulseEstimValue = impulseEstimValue +100;

                if (impulseEstimValue > 60000)
                    impulseEstimValue = 60000;

                editEstim.setText(""+((double)impulseEstimValue/1000));
            }
        });

        imgEstimMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impulseEstimValue = (int)(Double.parseDouble(editEstim.getText().toString())*1000);
                impulseEstimValue = impulseEstimValue -100;

                if (impulseEstimValue < 0)
                    impulseEstimValue = 0;

                editEstim.setText(""+((double)impulseEstimValue/1000));
            }
        });

        imgPausePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impulsePauseValue = (int)(Double.parseDouble(editPause.getText().toString())*1000);
                impulsePauseValue = impulsePauseValue +100;

                if (impulsePauseValue > 90000)
                    impulsePauseValue = 90000;

                editPause.setText(""+((double)impulsePauseValue/1000));
            }
        });

        imgPauseMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impulsePauseValue = (int)(Double.parseDouble(editPause.getText().toString())*1000);
                impulsePauseValue = impulsePauseValue -100;

                if (impulsePauseValue < 0)
                    impulsePauseValue = 0;

                editPause.setText(""+((double)impulsePauseValue/1000));
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(false, (int)(Double.parseDouble(editEstim.getText().toString())*1000), (int)(Double.parseDouble(editPause.getText().toString())*1000));
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editEstim.getText().toString()))
                    editEstim.setText(""+0);

                if (TextUtils.isEmpty(editPause.getText().toString()))
                    editPause.setText(""+0);

                sendResult(true, (int)(Double.parseDouble(editEstim.getText().toString())*1000), (int)(Double.parseDouble(editPause.getText().toString())*1000));
            }
        });
    }

    public interface OnConfirmationImpulseTimeListener {
        void onConfirmationImpulseTime(boolean send, int impulseEstimValue, int impulsePauseValue);
    }

    private void sendResult(boolean send, int impulseEstimValue, int impulsePauseValue) {
        if(mCallback != null) {
            alertdialog.dismiss();

            if (impulseEstimValue > 60000)
                impulseEstimValue = 60000;
            else if (impulseEstimValue < 0)
                impulseEstimValue = 0;

            if (impulsePauseValue > 90000)
                impulsePauseValue = 90000;
            else if (impulsePauseValue < 0)
                impulsePauseValue = 0;


            mCallback.onConfirmationImpulseTime(send, impulseEstimValue, impulsePauseValue);
        }
    }
}