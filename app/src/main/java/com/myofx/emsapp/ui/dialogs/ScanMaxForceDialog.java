package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.myofx.emsapp.R;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.ui.adapters.DeviceMaxForceAdapter;

import java.util.ArrayList;

/**
 * Created by Gerard on 2/10/2017.
 */

public class ScanMaxForceDialog extends DialogFragment {

    //Ui
    private AlertDialog alertdialog;
    private RecyclerView listDevices;
    private Button btnClose;
    private ProgressBar pbScan;
    private TextView txtDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        alertdialog = builder.create();
        return alertdialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_maxforce_devices, null);
        listDevices = (RecyclerView) view.findViewById(R.id.listDevices);
        txtDialog = (TextView) view.findViewById(R.id.txtDialog);
        pbScan = (ProgressBar) view.findViewById(R.id.pbScan);
        btnClose = (Button) view.findViewById(R.id.btnClose);

        setListeners();

        return view;
    }

    public void setListeners(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() instanceof MainActivity)
                    ((MainActivity)getActivity()).assignDevice(null);

                alertdialog.dismiss();
            }
        });
    }

    public static ScanMaxForceDialog newInstance() {
        ScanMaxForceDialog scanMaxForceDialog = new ScanMaxForceDialog();
        return scanMaxForceDialog;
    }

    public void loadDevices(ArrayList<BluetoothDevice> devices){
        pbScan.setVisibility(View.GONE);

        if (devices.size() <= 0) {
            txtDialog.setText(getString(R.string.empty_devices_maxforce));
        } else {
            txtDialog.setText(getString(R.string.dialog_devices));
            listDevices.setVisibility(View.VISIBLE);
            listDevices.setLayoutManager(new LinearLayoutManager(getActivity()));
            listDevices.setNestedScrollingEnabled(false);
            listDevices.setAdapter(new DeviceMaxForceAdapter(devices, getActivity()));
        }
    }
}
