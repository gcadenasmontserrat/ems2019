package com.myofx.emsapp.ui.notices;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.myofx.emsapp.BaseActivity;
import com.myofx.emsapp.R;
import com.myofx.emsapp.utils.ColorTheme;

public class ActivityNotices extends BaseActivity {

    //Ui
    private RelativeLayout relativeBackground;

    //Variable
    private String idVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notices);


        bindUi();
        loadTheme();
        loadToolbar();
        loadNotices();
    }

    public void loadToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ColorTheme.getPrimaryColor(ActivityNotices.this)));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ColorTheme.getPrimaryDarkColor(ActivityNotices.this));
        }
    }

    public void bindUi(){
        relativeBackground = (RelativeLayout) findViewById(R.id.relativeBackground);
    }

    public void loadTheme(){
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(ActivityNotices.this));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadNotices(){

    }
}