package com.myofx.emsapp.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.adapters.DevicesAdapter;

import java.util.ArrayList;

/**
 * Created by Gerard on 14/3/2017.
 */

public class ScanDevicesDialog extends DialogFragment {

    public static final String EXTRA_LIST_DEVICES = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_LIST_DEVICES";
    public static final String EXTRA_SERVER_TYPE = EmsApp.APP_PACKAGE + ".intent.extra.EXTRA_SERVER_TYPE";

    //Ui
    private AlertDialog alertdialog;
    private RecyclerView listDevices;
    private Button btnClose;

    //Variables
    private ArrayList<String> devices;
    private int serverType = UDPServer.SERVER_NORMAL;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(bindUi()).setCancelable(true);

        devices = (ArrayList<String>) getArguments().getSerializable(EXTRA_LIST_DEVICES);
        serverType = (Integer) getArguments().getSerializable(EXTRA_SERVER_TYPE);

        loadDevices();

        alertdialog = builder.create();
        return alertdialog;
    }

    public View bindUi(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_devices, null);
        listDevices = (RecyclerView) view.findViewById(R.id.listDevices);
        btnClose = (Button) view.findViewById(R.id.btnClose);

        setListeners();

        return view;
    }

    public void setListeners(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertdialog.dismiss();
            }
        });
    }

    public static ScanDevicesDialog newInstance(ArrayList<String> devices, int serverType) {
        ScanDevicesDialog scanDevicesDialog = new ScanDevicesDialog();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_LIST_DEVICES, devices);
        args.putSerializable(EXTRA_SERVER_TYPE, serverType);
        scanDevicesDialog.setArguments(args);
        return scanDevicesDialog;
    }

    public void loadDevices(){
        listDevices.setVisibility(View.VISIBLE);
        listDevices.setLayoutManager(new LinearLayoutManager(getActivity()));
        listDevices.setNestedScrollingEnabled(false);
        listDevices.setAdapter(new DevicesAdapter(devices, getActivity()));
    }
}

