package com.myofx.emsapp.ui.training;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.EmsApi;
import com.myofx.emsapp.api.events.SearchEvent;
import com.myofx.emsapp.api.events.StudentsEvent;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.ui.dialogs.AvailableDevicesDialog;
import com.myofx.emsapp.ui.dialogs.InfoUserDialog;
import com.myofx.emsapp.ui.training.adapters.ParticipantsListAdapter;
import com.myofx.emsapp.ui.training.adapters.SelectedListAdapter;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.ImageUtils;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Gerard on 28/12/2016.
 */

public class TrainingParticipantsFragment extends Fragment implements InfoUserDialog.OnConfirmationUserListener {

    //UI
    private View rootView;
    private RelativeLayout relativeBackground;
    private Button btnStart;
    private EditText editSearch;
    private RelativeLayout relativeEdit;
    private TextView txtEmpty;
    private LinearLayout linearLoading;
    private TextView txtLoading;
    private TextView txtName;
    private ImageView imgUser;
    private TextView txtTitle;
    private RelativeLayout relativeHeaders;
    private LinearLayout linearHeadersSelected;
    private TextView txtEmptySelected;
    private RecyclerView listParticipants;
    private RecyclerView listSelected;
    private RecyclerView listSearch;

    //VARIABLE
    private PreferencesManager pref;
    private User user;
    private ArrayList<Device> devices;
    private ArrayList<User> students;
    private ArrayList<User> studentsSelected;
    private ArrayList<User> studentsSearch;
    private ArrayList<User> participantsTrained;
    private ParticipantsListAdapter trainingParticipantsAdapter;
    private ParticipantsListAdapter searchParticipantsAdapter;
    private SelectedListAdapter selectedListAdapter;
    private ImageView imgSearch;
    private boolean isSearch = false;

    //LoadMore
    private NestedScrollView scroll;
    private boolean isloading = false;
    private int page = 1;
    private boolean loadMore = false;
    private boolean allItemsLoaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_training, container, false);
        pref = PreferencesManager.getInstance(getContext());
        devices = pref.getAllDevices();
        user = pref.getUser();

        bindUi();
        setListeners();
        loadTheme();
        loadData();
        loadStudents();

        return rootView;
    }

    public void bindUi(){
        relativeBackground = rootView.findViewById(R.id.relativeBackground);
        btnStart = rootView.findViewById(R.id.btnStart);
        imgSearch = rootView.findViewById(R.id.imgSearch);
        editSearch = rootView.findViewById(R.id.editSearch);
        relativeEdit = rootView.findViewById(R.id.relativeEdit);
        txtEmpty = rootView.findViewById(R.id.txtEmpty);
        linearLoading = rootView.findViewById(R.id.linearLoading);
        txtLoading = rootView.findViewById(R.id.txtLoading);
        txtName = rootView.findViewById(R.id.txtName);
        imgUser = rootView.findViewById(R.id.imgUser);
        scroll = rootView.findViewById(R.id.scroll);
        txtTitle = rootView.findViewById(R.id.txtTitle);
        relativeHeaders = rootView.findViewById(R.id.relativeHeaders);
        linearHeadersSelected = rootView.findViewById(R.id.linearHeadersSelected);
        txtEmptySelected = rootView.findViewById(R.id.txtEmptySelected);
        listSelected = rootView.findViewById(R.id.listSelected);
        listSearch = rootView.findViewById(R.id.listSearch);
        listParticipants = rootView.findViewById(R.id.listParticipants);

        students = new ArrayList<>();
        listParticipants.setLayoutManager(new LinearLayoutManager(getActivity()));
        listParticipants.setNestedScrollingEnabled(false);
        setNestedScrollEndless();
        trainingParticipantsAdapter = new ParticipantsListAdapter(students, getActivity(), this);
        listParticipants.setAdapter(trainingParticipantsAdapter);

        studentsSelected = new ArrayList<>();
        listSelected.setLayoutManager(new LinearLayoutManager(getActivity()));
        listSelected.setNestedScrollingEnabled(false);
        selectedListAdapter = new SelectedListAdapter(studentsSelected, getActivity(), this);
        listSelected.setAdapter(selectedListAdapter);

        studentsSearch = new ArrayList<>();
        listSearch.setLayoutManager(new LinearLayoutManager(getActivity()));
        listSearch.setNestedScrollingEnabled(false);
        setNestedScrollEndless();
        searchParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
        listSearch.setAdapter(searchParticipantsAdapter);
    }

    public void setListeners(){
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (studentsSelected.size() > 0) {
                    Intent intent = new Intent(getActivity(), TrainingsActivity.class);
                    intent.putExtra(TrainingsActivity.EXTRA_STUDENTS, studentsSelected);
                    startActivityForResult(intent,TrainingsActivity.RESULT_NORMAL);
                } else {
                    Toast.makeText(getActivity(), R.string.empty_selected_students, Toast.LENGTH_SHORT).show();
                }
            }
        });

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateListSearch(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){

                    if (!TextUtils.isEmpty(editSearch.getText().toString()))
                        searchStudents(editSearch.getText().toString());

                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearch) {
                    isSearch = false;
                    editSearch.setText("");
                    imgSearch.setImageResource(R.drawable.ic_search);
                    listSearch.setVisibility(View.GONE);
                    listParticipants.setVisibility(View.VISIBLE);
                    studentsSearch.clear();
                }
            }
        });
    }

    public void loadTheme(){
        relativeEdit.setBackgroundColor(ColorTheme.getPrimaryColor(getActivity()));
        relativeBackground.setBackgroundColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        relativeHeaders.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        imgSearch.setColorFilter(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setHintTextColor(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setTextColor(ColorTheme.getPrimaryColor(getActivity()));
        editSearch.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtLoading.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        txtEmpty.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        txtTitle.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));
        linearHeadersSelected.setBackgroundColor(ColorTheme.getLightBackgroundColor(getActivity()));
        txtEmptySelected.setTextColor(ColorTheme.getPrimaryDarkColor(getActivity()));

        Drawable background = linearLoading.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ColorTheme.getLightBackgroundColor(getActivity()));
    }

    public void loadData(){
        User user = pref.getUser();
        txtName.setText(user.getName());

        ImageUtils.loadImageProfileFromUrl(imgUser, BuildConfig.IMAGE_PATH_URL+user.getImage());

        //Clean assignments
        devices = pref.getAllDevices();
        for (Device device: devices)
            device.setUserAsigned(null);

        pref.saveDevices(devices);
    }

    public void loadStudents(){

        if (EmsApp.isModeLocal()){
            if (user.getProfiles() != null && user.getProfiles().size() <= 0)
                txtEmpty.setVisibility(View.VISIBLE);
            else {
                txtEmpty.setVisibility(View.GONE);
                students.addAll(user.getProfiles());
                trainingParticipantsAdapter.notifyDataSetChanged();
                allItemsLoaded = true;
            }
        } else {
            txtLoading.setText(getString(R.string.loading_get_students));
            linearLoading.setVisibility(View.VISIBLE);

            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            EmsApi.getInstance().getStudentsTask(page);
        }
    }

    public void searchStudents(String search){
        txtLoading.setText(getString(R.string.loading_get_students));
        linearLoading.setVisibility(View.VISIBLE);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        EmsApi.getInstance().searchStudentsTask(search);
    }

    @Subscribe
    public void onGetSearchEvent(SearchEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);
        listParticipants.setVisibility(View.GONE);
        listSearch.setVisibility(View.VISIBLE);
        imgSearch.setImageResource(R.drawable.ic_close);
        isSearch = true;

        studentsSearch = new ArrayList<>();
        studentsSearch.addAll(event.getUsers());

        searchParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
        listSearch.setAdapter(searchParticipantsAdapter);
    }

    @Subscribe
    public void onGetStudentsEvent(StudentsEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().unregister(this);

        linearLoading.setVisibility(View.GONE);

        page = page +1;
        if (loadMore){
            loadMore = false;
        } else {
            students.clear();
        }

        ArrayList<User> usersAdd = event.getUsers();
        for (User user: usersAdd)
            students.add(user);

        trainingParticipantsAdapter.notifyDataSetChanged();

        if (students.size() >= event.getTotalItems()) {
            allItemsLoaded = true;
        }
        isloading = false;

        if (students.size() <= 0)
            txtEmpty.setVisibility(View.VISIBLE);
        else
            txtEmpty.setVisibility(View.GONE);
    }

    public void showDevices(User user){
        if (user.isBlock() || user.isNotify()){
            InfoUserDialog infoUserDialog = InfoUserDialog.newInstance(user, TrainingParticipantsFragment.this, UDPServer.SERVER_NORMAL);
            infoUserDialog.setCancelable(false);
            infoUserDialog.show(getFragmentManager(), InfoUserDialog.class.getSimpleName());
        } else {
            AvailableDevicesDialog availableDevicesDialog = AvailableDevicesDialog.newInstance(user, UDPServer.SERVER_NORMAL);
            availableDevicesDialog.setCancelable(false);
            availableDevicesDialog.show(getFragmentManager(), AvailableDevicesDialog.class.getSimpleName());
        }
    }

    public void updateList(){
        if (isSearch){
            trainingParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
            listSearch.setAdapter(trainingParticipantsAdapter);
        } else {
            trainingParticipantsAdapter = new ParticipantsListAdapter(students, getActivity(), this);
            listParticipants.setAdapter(trainingParticipantsAdapter);
        }

        devices = pref.getAllDevices();
        for (Device device: devices){
            if (device.getUserAsigned() != null){
                for (User student: studentsSearch){
                    if (student.getId().equals(device.getUserAsigned().getId()) && !studentsSelected.contains(student)){
                        studentsSelected.add(student);
                    }
                }
            }
        }

        for (Device device: devices){
            if (device.getUserAsigned() != null){
                for (User student: students){
                    if (student.getId().equals(device.getUserAsigned().getId()) && !studentsSelected.contains(student)){
                        studentsSelected.add(student);
                    }
                }
            }
        }

        if (studentsSelected.size()>0)
            txtEmptySelected.setVisibility(View.GONE);
        else
            txtEmptySelected.setVisibility(View.VISIBLE);

        selectedListAdapter = new SelectedListAdapter(studentsSelected, getActivity(), this);
        listSelected.setAdapter(selectedListAdapter);
    }

    public void updateListRemove(ArrayList<User> selected){
        selectedListAdapter = new SelectedListAdapter(selected, getActivity(), this);
        listSelected.setAdapter(selectedListAdapter);

        if (isSearch){
            trainingParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
            listSearch.setAdapter(trainingParticipantsAdapter);
        } else {
            trainingParticipantsAdapter = new ParticipantsListAdapter(students, getActivity(), this);
            listParticipants.setAdapter(trainingParticipantsAdapter);
        }

        if (studentsSelected.size()>0)
            txtEmptySelected.setVisibility(View.GONE);
        else
            txtEmptySelected.setVisibility(View.VISIBLE);
    }

    public void removeSelected(User user){
        if (studentsSelected != null) {
            int index = students.indexOf(user);
            studentsSelected.remove(user);
            selectedListAdapter.notifyItemRemoved(index);

            updateListRemove(studentsSelected);
        }
    }

    public void updateListSearch(String search){
        if (students != null && students.size() > 0){
            if (!search.equals("")){
                studentsSearch = new ArrayList<>();
                for (User student: students){
                    if ( student.getName()!=null && student.getName().toLowerCase().contains(search.toLowerCase()))
                        studentsSearch.add(student);
                }

                trainingParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
                listParticipants.setAdapter(trainingParticipantsAdapter);
            } else {
                trainingParticipantsAdapter = new ParticipantsListAdapter(students, getActivity(), this);
                listParticipants.setAdapter(trainingParticipantsAdapter);
            }
        }
    }

    @Override
    public void onConfirmationUser(User user) {
        AvailableDevicesDialog availableDevicesDialog = AvailableDevicesDialog.newInstance(user, UDPServer.SERVER_NORMAL);
        availableDevicesDialog.setCancelable(false);
        availableDevicesDialog.show(getFragmentManager(), AvailableDevicesDialog.class.getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TrainingsActivity.RESULT_NORMAL) {
            participantsTrained = new ArrayList<>();
            participantsTrained = (ArrayList<User>) data.getSerializableExtra(TrainingsActivity.RESULT_PARTICIPANTS);
            updateEndTraining();
        }
    }

    public void setNestedScrollEndless(){
        scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                if (!allItemsLoaded) {
                    if (!isloading && scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) && !isSearch) {
                        loadMore = true;
                        isloading = true;
                        loadStudents();
                    }
                }
            }
        });
    }

    public void updateEndTraining(){
        for (User student: participantsTrained){
            if(studentsSelected.contains(student)){
                studentsSelected.get(studentsSelected.indexOf(student)).setCorporalZone(student.getCorporalZones());
            }

            if(students.contains(student)){
                students.get(students.indexOf(student)).setCorporalZone(student.getCorporalZones());
            }

            if(studentsSearch.contains(student)){
                studentsSearch.get(studentsSearch.indexOf(student)).setCorporalZone(student.getCorporalZones());
            }
        }
    }

    public void scanDevice(Device device){
        ((MainActivity)getContext()).scanMaxForceDevice(device);
    }

    public void updateMaxForceAssigment(Device deviceSelected, BluetoothDevice deviceBleSelected){
        if (deviceBleSelected != null){
            devices = pref.getAllDevices();
            for (Device device: devices){

                switch (pref.getUser().getConfiguration().getConnectionType()){
                    case Device.DEVICE_WIFI:
                        if (device.getUidString().equals(deviceSelected.getUidString()))
                            device.setDeviceBle(deviceBleSelected);
                        break;
                    case Device.DEVICE_BLE:
                        if (device.getDeviceBleAdress().equals(deviceSelected.getDeviceBleAdress()))
                            device.setDeviceBle(deviceBleSelected);
                        break;
                }
            }
            pref.saveDevices(devices);
        }

        if (isSearch){
            trainingParticipantsAdapter = new ParticipantsListAdapter(studentsSearch, getActivity(), this);
            listSearch.setAdapter(trainingParticipantsAdapter);
        } else {
            trainingParticipantsAdapter = new ParticipantsListAdapter(students, getActivity(), this);
            listParticipants.setAdapter(trainingParticipantsAdapter);
        }

        if (studentsSelected.size()>0)
            txtEmptySelected.setVisibility(View.GONE);
        else
            txtEmptySelected.setVisibility(View.VISIBLE);

        selectedListAdapter = new SelectedListAdapter(studentsSelected, getActivity(), this);
        listSelected.setAdapter(selectedListAdapter);

    }
}