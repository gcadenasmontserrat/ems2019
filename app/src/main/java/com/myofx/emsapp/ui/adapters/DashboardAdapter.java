package com.myofx.emsapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Section;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ColorTheme;
import com.myofx.emsapp.utils.PreferencesManager;

import java.util.ArrayList;

/**
 * Created by Gerard on 29/11/2016.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    public static final int SECTION_STUDENTS = 0;
    public static final int SECTION_TRAINING = 1;
    public static final int SECTION_NOTICES_LOCAL = 2;
    public static final int SECTION_DIARY = 3;
    public static final int SECTION_CONFIGURATION = 4;
    public static final int SECTION_DEVICES = 5;

    //VARIABLE
    private PreferencesManager pref;
    private ArrayList<Section> sections;
    private Context context;

    public DashboardAdapter(ArrayList<Section> sections, Context context) {
        pref = PreferencesManager.getInstance(context);
        this.sections = sections;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.ViewHolder holder, final int position) {

        switch (position){
            case (SECTION_STUDENTS):
                holder.txtSection.setText(context.getString(R.string.menu_section_students));
                holder.imgSection.setImageResource(R.drawable.ic_db_profiles);
                holder.imgNews.setImageResource(R.drawable.ic_profiles);

                if (!EmsApp.isModeLocal())
                    holder.txtNews.setText(""+pref.getUser().getStudentsNumber());
                else if (EmsApp.isModeLocal() && pref.getUser().getProfiles() != null)
                    holder.txtNews.setText(""+pref.getUser().getProfiles().size());
                break;
            case (SECTION_TRAINING):
                holder.txtSection.setText(context.getString(R.string.menu_section_training));
                holder.imgSection.setImageResource(R.drawable.ic_db_training);
                holder.txtNews.setVisibility(View.GONE);
                holder.imgNews.setVisibility(View.GONE);
                break;
            case (SECTION_NOTICES_LOCAL):
                if (!EmsApp.isModeLocal()){
                    holder.txtSection.setText(context.getString(R.string.menu_section_notices));
                    holder.imgSection.setImageResource(R.drawable.ic_db_tutorial);
                    holder.txtNews.setVisibility(View.GONE);
                    holder.imgNews.setVisibility(View.GONE);
                } else {
                    holder.txtSection.setText(context.getString(R.string.menu_section_local));
                    holder.imgSection.setImageResource(R.drawable.ic_db_tutorial);
                    holder.txtNews.setVisibility(View.GONE);
                    holder.imgNews.setVisibility(View.GONE);
                }
                break;
            case (SECTION_DIARY):
                holder.txtSection.setText(context.getString(R.string.menu_section_diary));
                holder.imgSection.setImageResource(R.drawable.ic_db_diary);
                holder.imgNews.setImageResource(R.drawable.ic_new);
                holder.txtNews.setText(""+pref.getUser().getReservationsNumber());
                break;
            case (SECTION_CONFIGURATION):
                holder.txtSection.setText(context.getString(R.string.menu_section_configuration));
                holder.imgSection.setImageResource(R.drawable.ic_db_settings);
                holder.txtNews.setVisibility(View.GONE);
                holder.imgNews.setVisibility(View.GONE);
                break;
            case (SECTION_DEVICES):
                holder.txtSection.setText(context.getString(R.string.menu_section_devices));
                holder.imgSection.setImageResource(R.drawable.ic_db_devices);
                holder.txtNews.setVisibility(View.GONE);
                holder.imgNews.setVisibility(View.GONE);
                break;
        }

        DashboardAdapter.ViewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position){
                    case (SECTION_STUDENTS):
                        ((MainActivity)context).loadSectionFromDashboard(R.id.itemStudents);
                        break;
                    case (SECTION_TRAINING):
                        ((MainActivity)context).loadSectionFromDashboard(R.id.itemTraining);
                        break;
                    case (SECTION_NOTICES_LOCAL):
                        if (!EmsApp.isModeLocal())
                            ((MainActivity)context).loadNotices();
                        else
                            ((MainActivity)context).loadSectionFromDashboard(R.id.itemLocal);
                        break;
                    case (SECTION_DIARY):
                        ((MainActivity)context).loadSectionFromDashboard(R.id.itemDiary);
                        break;
                    case (SECTION_CONFIGURATION):
                        ((MainActivity)context).loadSectionFromDashboard(R.id.itemConfiguration);
                        break;
                    case (SECTION_DEVICES):
                        ((MainActivity)context).loadSectionFromDashboard(R.id.itemDevices);
                        break;
                }

            }
        });

        //Load colorTheme
        holder.txtSection.setTextColor(ColorTheme.getPrimaryColor(context));
        holder.txtNews.setTextColor(ColorTheme.getPrimaryColor(context));
        holder.linearItem.setBackgroundColor(ColorTheme.getLightBackgroundColor(context));
        holder.imgSection.setColorFilter(ColorTheme.getIconsColor(context));
        holder.imgNews.setColorFilter(ColorTheme.getIconsColor(context));
    }

    @Override
    public DashboardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_dashboard, viewGroup, false);
        return new DashboardAdapter.ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearItem;
        private ImageView imgSection;
        private ImageView imgNews;
        private TextView txtSection;
        private TextView txtNews;
        private static View mView;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;

            imgSection = (ImageView) mView.findViewById(R.id.imgSection);
            imgNews = (ImageView) mView.findViewById(R.id.imgNews);
            txtSection = (TextView) mView.findViewById(R.id.txtSection);
            txtNews = (TextView) mView.findViewById(R.id.txtNews);
            linearItem = (LinearLayout) mView.findViewById(R.id.linearItem);
        }
    }

}