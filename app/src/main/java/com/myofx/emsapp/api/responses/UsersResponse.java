package com.myofx.emsapp.api.responses;

import com.google.gson.annotations.SerializedName;
import com.myofx.emsapp.models.User;

import java.util.ArrayList;

/**
 * Created by Gerard on 3/7/2017.
 */

public class UsersResponse {

    @SerializedName("data")
    private ArrayList<User> users;

    @SerializedName("page")
    private int page;

    @SerializedName("items")
    private int items;

    @SerializedName("totalItems")
    private int totalItems;

    public int getPage() {
        return page;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public ArrayList<User> getUsers() {
        return users;
    }
}
