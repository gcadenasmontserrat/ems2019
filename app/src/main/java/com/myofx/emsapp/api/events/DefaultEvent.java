package com.myofx.emsapp.api.events;


import com.myofx.emsapp.models.ErrorWs;

/**
 * Created by Gerard on 20/2/2017.
 */

public class DefaultEvent {

    private int requestType;
    private ErrorWs error;
    private boolean succes;

    public DefaultEvent(int requestType, ErrorWs error) {
        this.requestType = requestType;
        this.error = error;
    }

    public DefaultEvent(boolean succes) {
        this.succes = succes;
    }

    public int getRequestType() {
        return requestType;
    }

    public ErrorWs getError() {
        return error;
    }
}
