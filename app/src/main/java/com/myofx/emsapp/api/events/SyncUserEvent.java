package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.User;

public class SyncUserEvent {

    private int syncType;
    private User userSync;

    public SyncUserEvent(int syncType, User userSync) {
        this.syncType = syncType;
        this.userSync = userSync;
    }

    public int getSyncType() {
        return syncType;
    }

    public User getUserSync() {
        return userSync;
    }
}
