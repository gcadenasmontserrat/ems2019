package com.myofx.emsapp.api.events;

public class BleStatusEvent {

    private boolean bleAvailable;
    private String adress;

    public BleStatusEvent(boolean bleAvailable, String adress) {
        this.bleAvailable = bleAvailable;
        this.adress = adress;
    }

    public boolean isBleAvailable() {
        return bleAvailable;
    }

    public String getAdress() {
        return adress;
    }
}
