package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.AppInfo;

/**
 * Created by Gerard on 7/3/2017.
 */

public class AppInfoEvent {

    private AppInfo appInfo;

    public AppInfoEvent(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

}
