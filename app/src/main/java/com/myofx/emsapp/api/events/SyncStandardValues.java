package com.myofx.emsapp.api.events;

public class SyncStandardValues {

    private boolean success;

    public SyncStandardValues(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

}
