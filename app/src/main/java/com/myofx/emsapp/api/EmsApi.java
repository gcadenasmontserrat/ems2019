package com.myofx.emsapp.api;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.myofx.emsapp.BuildConfig;
import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.myofx.emsapp.api.events.AppInfoEvent;
import com.myofx.emsapp.api.events.ConnectionErrorEvent;
import com.myofx.emsapp.api.events.DefaultEvent;
import com.myofx.emsapp.api.events.DeviceInfoEvent;
import com.myofx.emsapp.api.events.PushTokenEvent;
import com.myofx.emsapp.api.events.SearchEvent;
import com.myofx.emsapp.api.events.SyncAction;
import com.myofx.emsapp.api.events.SyncCorporalZones;
import com.myofx.emsapp.api.events.SyncSessionResults;
import com.myofx.emsapp.api.events.SyncStandardValues;
import com.myofx.emsapp.api.events.SyncUserEvent;
import com.myofx.emsapp.api.events.TrainingsEvent;
import com.myofx.emsapp.api.events.StudentsEvent;
import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.api.events.UserLocalEvent;
import com.myofx.emsapp.api.responses.UsersResponse;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.models.AppInfo;
import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.DeviceInfo;
import com.myofx.emsapp.models.ErrorWs;
import com.myofx.emsapp.models.LogRegister;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.utils.LogApp;
import com.myofx.emsapp.utils.PreferencesManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myofx.emsapp.utils.PreferencesManagerLocal;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Gerard on 7/12/2016.
 */

public class EmsApi {

    private static int LOGIN_TASK = 1;
    private static int USER_TASK = 2;
    private static int LOGOUT_TASK = 3;
    private static int SAVE_CONF_TASK = 4;
    private static int STUDENT_TASK = 5;
    public static int GET_TRAININGS_TASK = 6;
    public static int GET_DEVICE_INFO = 6;
    public static int SEND_RESULTS_TASK = 7;
    public static int SAVE_STANDARD_VALUES = 8;
    public static int SEARCH_STUDENT_TASK = 9;

    //Variable
    public static EmsApi instance;
    public static boolean firstTime = true;

    public static EmsApi getInstance() {
        if (instance == null)
            instance = new EmsApi();

        return instance;
    }

    private static Context getContext() {
        return EmsApp.getInstance();
    }

    public static boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());
        return isConnected;
    }

    //Login user
    public void doLoginTask(String username, String password) {
        HashMap<String, String> params = generateParamsRequest();
        params.put(WSConfig.PARAM_USERNAME, "staff:"+username);
        params.put(WSConfig.PARAM_PASSWORD, password);
        params.put(WSConfig.PARAM_GRANT_TYPE, "password");

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_LOGIN, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("login", response.toString());

                    User user = new Gson().fromJson(response.toString(), User.class);
                    EventBus.getDefault().postSticky(new UserEvent(user, UserEvent.REQUEST_TYPE_LOGIN));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse != null){
                            VolleyError errorMessage = new VolleyError(new String(error.networkResponse.data));
                            ErrorWs errorWs = new Gson().fromJson(errorMessage.getMessage(), ErrorWs.class);
                            EventBus.getDefault().postSticky(new DefaultEvent(LOGIN_TASK, errorWs));
                        } else {
                            ErrorWs errorWs = new ErrorWs("");
                            EventBus.getDefault().postSticky(new DefaultEvent(LOGIN_TASK, errorWs));
                        }
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateBaseHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    //Get trainings
    public void doGetTrainingsTask() {
        if (isThereInternetConnection()) {
            JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, WSConfig.WS_TRAININGS, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    LogApp.v("trainings", response.toString());

                    ArrayList<Training> trainings = new Gson().fromJson(response.toString(), new TypeToken<ArrayList<Training>>(){}.getType());
                    EventBus.getDefault().postSticky(new TrainingsEvent(trainings));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            checkCallRefreshToken(error, GET_TRAININGS_TASK, null, null, 0, null, null, false, null);

                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void getAppInfo() {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, WSConfig.WS_INFO, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("App info", response.toString());

                    AppInfo appInfo = new Gson().fromJson(response.toString(), AppInfo.class);
                    EventBus.getDefault().postSticky(new AppInfoEvent(appInfo));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            EventBus.getDefault().postSticky(new DefaultEvent(false));
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateBaseHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    //Register Device
    public void doRegisterDevice(String deviceToken) {
        HashMap<String, String> params = generateParamsRequest();
        params.put(WSConfig.PARAM_DEVICE_TOKEN, deviceToken);
        params.put(WSConfig.PARAM_PLATFORM, "android");

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_REGISTER_DEVICE, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new PushTokenEvent());
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            EventBus.getDefault().postSticky(new DefaultEvent(false));
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateBaseHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    //Get User information
    public void doUserTask() {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, WSConfig.WS_USER, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("me", response.toString());

                    User user = new Gson().fromJson(response.toString(), User.class);
                    EventBus.getDefault().postSticky(new UserEvent(user, UserEvent.REQUEST_TYPE_BASE));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        checkCallRefreshToken(error, USER_TASK, null, null, 0, null, null, false, null);
                    }
                }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void saveConfiguration(final String units, final int connectionType, final int color) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(WSConfig.PARAM_CONFIG_UNITS, units);
        params.put(WSConfig.PARAM_CONFIG_COLOR, color);
        params.put(WSConfig.PARAM_CONFIG_CONNECTION_TYPE, connectionType);

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, WSConfig.WS_CONFIG, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("queryTest", response.toString());

                    User user = new Gson().fromJson(response.toString(), User.class);
                    EventBus.getDefault().postSticky(new UserEvent(user, UserEvent.REQUEST_TYPE_BASE));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!checkCallRefreshToken(error, SAVE_CONF_TASK, units, null, color, null, null, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    //Get User information
    public void getStudentsTask(final int page) {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, WSConfig.WS_STUDENTS+"?p="+page+"&l=20", null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("queryTest", response.toString());

                    UsersResponse usersResponse = new Gson().fromJson(response.toString(), UsersResponse.class);
                    EventBus.getDefault().postSticky(new StudentsEvent(usersResponse.getUsers(), usersResponse.getPage(), usersResponse.getTotalItems()));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!checkCallRefreshToken(error, STUDENT_TASK, null, null, page, null, null, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void searchStudentsTask(final String search) {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, WSConfig.WS_STUDENTS+"?s="+search, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    UsersResponse usersResponse = new Gson().fromJson(response.toString(), UsersResponse.class);
                    EventBus.getDefault().postSticky(new SearchEvent(usersResponse.getUsers(), usersResponse.getPage(), usersResponse.getTotalItems()));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!checkCallRefreshToken(error, SEARCH_STUDENT_TASK, search, null, 0, null, null, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void getDevicesInformation(final ArrayList<String> devicesUID) {
        if (isThereInternetConnection()) {
            JsonArrayRequest req = new JsonArrayRequest(Request.Method.POST, WSConfig.WS_DEVICES, new JSONArray(devicesUID), new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    ArrayList<DeviceInfo> devicesInfo = new Gson().fromJson(response.toString(), new TypeToken<ArrayList<DeviceInfo>>(){}.getType());
                    EventBus.getDefault().postSticky(new DeviceInfoEvent(devicesInfo));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse == null){
                                restartApp();
                            } else if(!checkCallRefreshToken(error, GET_DEVICE_INFO, null, null, 0, null, devicesUID, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            } else
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public static void recoveryPasswordTask(String email) {
        HashMap<String, String> params = new HashMap<>();
        params.put(WSConfig.PARAM_PROFILE_EMAIL, email);

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_RECOVERY_PASS, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("queryTest", response.toString());
                    EventBus.getDefault().postSticky(new DefaultEvent(true));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            EventBus.getDefault().postSticky(new DefaultEvent(true));
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateBaseHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void doSendResults(final JSONObject params) {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_SEND_RESULTS, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new DefaultEvent(true));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse == null){
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            } else if (!checkCallRefreshToken(error, SEND_RESULTS_TASK, null, null, 0, params, null, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void doSendUserConfig(HashMap<String, Object> params, User user) {
        Log.v("AAA",  new JSONObject(params).toString());
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, WSConfig.WS_SAVE_USER_CONFIG+"/"+user.getId()+"/user-conf", new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new DefaultEvent(true));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void saveStandardValues(Training training) {
        if (isThereInternetConnection()) {
            HashMap<String, Integer> params = new HashMap<>();
            params.put(WSConfig.PARAM_LEG, training.getLevelLeg());
            params.put(WSConfig.PARAM_GLU, training.getLevelGlu());
            params.put(WSConfig.PARAM_LUM, training.getLevelLum());
            params.put(WSConfig.PARAM_DOR, training.getLevelDor());
            params.put(WSConfig.PARAM_SHO, training.getLevelSho());
            params.put(WSConfig.PARAM_ABS, training.getLevelAbs());
            params.put(WSConfig.PARAM_PEC, training.getLevelPec());
            params.put(WSConfig.PARAM_BIC, training.getLevelBic());
            params.put(WSConfig.PARAM_CRON_LEG, training.getCronaxyLeg());
            params.put(WSConfig.PARAM_CRON_GLU, training.getCronaxyGlu());
            params.put(WSConfig.PARAM_CRON_LUM, training.getCronaxyLum());
            params.put(WSConfig.PARAM_CRON_DOR, training.getCronaxyDor());
            params.put(WSConfig.PARAM_CRON_SHO, training.getCronaxySho());
            params.put(WSConfig.PARAM_CRON_ABS, training.getCronaxyAbs());
            params.put(WSConfig.PARAM_CRON_PEC, training.getCronaxyPec());
            params.put(WSConfig.PARAM_CRON_BIC, training.getCronaxyBic());

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, WSConfig.WS_SAVE_STANDARD_VALUES, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new DefaultEvent(SAVE_STANDARD_VALUES, null));
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EventBus.getDefault().postSticky(new DefaultEvent(SAVE_STANDARD_VALUES, null));
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    //Refres acces token
    public boolean checkCallRefreshToken(VolleyError error, int task, String param1, ArrayList<Integer> param2, int param3,
                                         JSONObject param4, ArrayList<String> param5, boolean param6, String param7){
        if (error != null && error.networkResponse.statusCode == 401) {
            if (firstTime) {
                firstTime = false;
                LogApp.v("queryTest", "Error 401");
                PreferencesManager pref = PreferencesManager.getInstance(getContext());
                doRefreshTokenTask(pref.getRefreshToken(), task, param1, param2, param3, param4, param5, param6, param7);
                return true;
            } else {
                LogApp.v("queryTest", "Error2 401");
                restartApp();

                return true;
            }
        } else {
            VolleyError errorMessage = new VolleyError(new String(error.networkResponse.data));
            ErrorWs errorWs = new Gson().fromJson(errorMessage.getMessage(), ErrorWs.class);
            EventBus.getDefault().postSticky(new DefaultEvent(task, errorWs));
            return false;
        }
    }

    public void doRefreshTokenTask(String refreshToken, final int task, final String param1, final ArrayList<Integer> param2, final int param3,
                                   final JSONObject param4, final ArrayList<String> param5, final boolean param6, final String param7) {
        HashMap<String, String> params = generateParamsRequest();
        params.put(WSConfig.PARAM_REFRESH_TOKEN, refreshToken);
        params.put(WSConfig.PARAM_GRANT_TYPE, "refresh_token");

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_LOGIN, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("queryTest", response.toString());

                    User refreshResponse = new Gson().fromJson(response.toString(), User.class);
                    PreferencesManager pref = PreferencesManager.getInstance(getContext());
                    pref.saveAccessToken(refreshResponse.getAccesToken());
                    pref.saveRefreshToken(refreshResponse.getRefreshToken());
                    recallTasks(task, param1, param2, param3, param4, param5, param6, param7);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            restartApp();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateBaseHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void doLogout() {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_LOGOUT, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("Logout", response.toString());
                    EventBus.getDefault().postSticky(new DefaultEvent(true));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse == null){
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            } else if (!checkCallRefreshToken(error, LOGOUT_TASK, null, null, 0, null, null, false, null)) {
                                EventBus.getDefault().postSticky(new DefaultEvent(true));
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void syncUser(User user) {
        HashMap<String, String> params = new HashMap<>();
        params.put(WSConfig.PARAM_PROFILE_EMAIL, user.getEmail());
        params.put(WSConfig.PARAM_NAME, user.getName());
        params.put(WSConfig.PARAM_LAST_AME, user.getLastName());
        params.put(WSConfig.PARAM_CENTER_ID, user.getCenterId());

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_SYNC_USER, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("Sync User", response.toString());

                    User user = new Gson().fromJson(response.toString(), User.class);
                    EventBus.getDefault().postSticky(new SyncUserEvent(0, user));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void syncBooking(Booking booking) {
        HashMap<String, String> params = new HashMap<>();
        params.put(WSConfig.PARAM_DATE, booking.getDate());
        params.put(WSConfig.PARAM_STAFF_ID, booking.getStaffId());
        params.put(WSConfig.PARAM_USER_ID, booking.getUserId());
        params.put(WSConfig.PARAM_SERVICE_ID, booking.getServiceId());
        Log.v("AAA",  new JSONObject(params).toString());

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_SYNC_BOOKING, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("Sync Booking", response.toString());
                    EventBus.getDefault().postSticky(new SyncAction(true));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EventBus.getDefault().postSticky(new SyncAction(false));
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().postSticky(new SyncAction(false));
        }
    }

    public void syncTraining(Training training) {
        HashMap<String, String> params = new HashMap<>();
        params.put(WSConfig.PARAM_TITLE, training.getTitle());
        params.put(WSConfig.PARAM_TRAINING_VALUES, Arrays.toString(training.getMain()));
        params.put(WSConfig.PARAM_CENTER_ID, training.getCenter());
        Log.v("AAA",  new JSONObject(params).toString());

        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_SYNC_TRAINING, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("Sync Training", response.toString());
                    EventBus.getDefault().postSticky(new SyncAction(true));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EventBus.getDefault().postSticky(new SyncAction(false));
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().postSticky(new SyncAction(false));
        }
    }

    public void syncStandardValues(User user) {
        if (isThereInternetConnection()) {
            HashMap<String, Integer> params = new HashMap<>();
            params.put(WSConfig.PARAM_LEG, user.getStandard().getLeg());
            params.put(WSConfig.PARAM_GLU, user.getStandard().getGlu());
            params.put(WSConfig.PARAM_LUM, user.getStandard().getLum());
            params.put(WSConfig.PARAM_DOR, user.getStandard().getDor());
            params.put(WSConfig.PARAM_SHO, user.getStandard().getSho());
            params.put(WSConfig.PARAM_ABS, user.getStandard().getAbs());
            params.put(WSConfig.PARAM_PEC, user.getStandard().getPec());
            params.put(WSConfig.PARAM_BIC, user.getStandard().getBic());
            params.put(WSConfig.PARAM_CRON_LEG, user.getStandard().getCronLeg());
            params.put(WSConfig.PARAM_CRON_GLU, user.getStandard().getCronGlu());
            params.put(WSConfig.PARAM_CRON_LUM, user.getStandard().getCronLum());
            params.put(WSConfig.PARAM_CRON_DOR, user.getStandard().getCronDor());
            params.put(WSConfig.PARAM_CRON_SHO, user.getStandard().getCronSho());
            params.put(WSConfig.PARAM_CRON_ABS, user.getStandard().getCronAbs());
            params.put(WSConfig.PARAM_CRON_PEC, user.getStandard().getCronPec());
            params.put(WSConfig.PARAM_CRON_BIC, user.getStandard().getCronBic());
            Log.v("std", new JSONObject(params).toString());

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, WSConfig.WS_SAVE_STANDARD_VALUES, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new SyncStandardValues(true));
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EventBus.getDefault().postSticky(new SyncStandardValues(false));
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else
            EventBus.getDefault().postSticky(new SyncStandardValues(false));
    }

    public void syncUserConfig(HashMap<String, Object> params, User user) {
        Log.v("AAA",  new JSONObject(params).toString());
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, WSConfig.WS_SAVE_USER_CONFIG+"/"+user.getId()+"/user-conf", new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new SyncCorporalZones(true));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EventBus.getDefault().postSticky(new SyncCorporalZones(true));
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else
            EventBus.getDefault().postSticky(new SyncCorporalZones(true));
    }

    public void syncResults(final JSONObject params) {
        Log.v("AAA",  params.toString());
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, WSConfig.WS_SEND_RESULTS, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    EventBus.getDefault().postSticky(new SyncSessionResults(true));
                }
            },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EventBus.getDefault().postSticky(new SyncSessionResults(true));
                    }
                })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else
            EventBus.getDefault().postSticky(new SyncSessionResults(true));
    }

    //Get User information
    public void updateLocalUserTask() {
        if (isThereInternetConnection()) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, WSConfig.WS_USER, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    LogApp.v("me", response.toString());

                    User user = new Gson().fromJson(response.toString(), User.class);
                    EventBus.getDefault().postSticky(new UserLocalEvent(user));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            checkCallRefreshToken(error, USER_TASK, null, null, 0, null, null, false, null);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return generateLogedHeaders();
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyManager.getInstance(getContext()).addToRequestQueue(req);
        } else {
            EventBus.getDefault().post(new ConnectionErrorEvent(getContext()));
        }
    }

    public void recallTasks(int task, String param1, ArrayList<Integer> param2, int param3,
                            JSONObject param4, ArrayList<String> param5, boolean param6, String param7){
        if (USER_TASK == task) {
            doUserTask();
        } else if (SAVE_CONF_TASK == task) {
//            saveConfiguration(param1,param7, param3);
        } else if (LOGOUT_TASK == task) {
            doLogout();
        } else if (STUDENT_TASK == task) {
            getStudentsTask(param3);
        } else if (GET_TRAININGS_TASK == task) {
            doGetTrainingsTask();
        } else if (GET_DEVICE_INFO == task){
            getDevicesInformation(param5);
        } else if (SEND_RESULTS_TASK == task){
            doSendResults(param4);
        } else if (SEARCH_STUDENT_TASK == task){
            searchStudentsTask(param1);
        }
    }

    /**
     * Generate default request headers
     * @return
     */
    public static Map<String, String> generateBaseHeaders(){
        Map<String, String>  params = new HashMap<>();
        params.put(WSConfig.HEADER_VERSION_ID, BuildConfig.APPLICATION_ID);
        params.put(WSConfig.HEADER_LANG, Locale.getDefault().getLanguage());
        params.put(WSConfig.HEADER_VERSION_ID, BuildConfig.VERSION_NAME);
        params.put(WSConfig.HEADER_DEVICE_ID, "a48e-9874d");
        params.put(WSConfig.HEADER_CONTENT_TYPE, "application/json");
        params.put(WSConfig.HEADER_LANGUAGE, Locale.getDefault().getLanguage());
        return params;
    }

    public static Map<String, String> generateLogedHeaders(){
        PreferencesManager pref = PreferencesManager.getInstance(getContext());

        Map<String, String>  params = new HashMap<>();
        params.put(WSConfig.HEADER_VERSION_ID, BuildConfig.APPLICATION_ID);
        params.put(WSConfig.HEADER_LANG, Locale.getDefault().getLanguage());
        params.put(WSConfig.HEADER_VERSION_ID, BuildConfig.VERSION_NAME);
        params.put(WSConfig.HEADER_CONTENT_TYPE, "application/json");
        params.put(WSConfig.HEADER_LANGUAGE, Locale.getDefault().getLanguage());

        if (pref.getAccessToken() != null){
            params.put(WSConfig.HEADER_AUTHORIZATION, "Bearer "+pref.getAccessToken());
        }

        if (pref.getProfile() != null){
            params.put(WSConfig.HEADER_PROFILE, pref.getProfile());
        }

        return params;
    }

    /**
     * Generate default request params
     * @return
     */
    public  HashMap<String, String> generateParamsRequest(){
        HashMap<String, String>  params = new HashMap<>();
        params.put(WSConfig.PARAM_CLIENT_ID, BuildConfig.API_CLIENT_ID);
        params.put(WSConfig.PARAM_CLIENT_SECRET, BuildConfig.API_CLIENT_SECRET);
        return params;
    }

    public void restartApp() {
        PreferencesManager pref = PreferencesManager.getInstance(getContext());
        pref.saveColorTheme(R.style.AppThemeBlue);
        pref.clear();

        Intent i = getContext().getPackageManager().getLaunchIntentForPackage( getContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getContext().startActivity(i);
    }
}
