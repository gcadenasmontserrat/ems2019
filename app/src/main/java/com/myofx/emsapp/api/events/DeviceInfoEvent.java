package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.DeviceInfo;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/4/2017.
 */

public class DeviceInfoEvent {

    private ArrayList<DeviceInfo> devicesInfo;

    public DeviceInfoEvent(ArrayList<DeviceInfo> devicesInfo) {
        this.devicesInfo = devicesInfo;
    }

    public ArrayList<DeviceInfo> getDevicesInfo() {
        return devicesInfo;
    }

}

