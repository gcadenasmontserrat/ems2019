package com.myofx.emsapp.api.events;

/**
 * Created by Gerard on 11/10/2017.
 */

public class DeviceStatusEvent {

    private String uid;
    private String batery;
    private String firmwareVersion;

    public DeviceStatusEvent(String uid, String batery, String firmwareVersion) {
        this.uid = uid;
        this.batery = batery;
        this.firmwareVersion = firmwareVersion;
    }

    public String getBatery() {
        return batery;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getUid() {
        return uid;
    }
}
