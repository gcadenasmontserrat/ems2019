package com.myofx.emsapp.api.events;


import com.myofx.emsapp.models.User;

/**
 * Created by Gerard on 6/2/2017.
 */

public class UserEvent {

    public static final int REQUEST_TYPE_BASE = 0;
    public static final int REQUEST_TYPE_LOGIN = 1;
    public static final int REQUEST_TYPE_REGISTER = 2;

    private User user;
    private int requestType;

    public UserEvent(User user, int requestType) {
        this.user = user;
        this.requestType = requestType;
    }

    public User getUser() {
        return user;
    }

    public int getRequestType() {
        return requestType;
    }
}
