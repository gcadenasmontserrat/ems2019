package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.Training;

import java.util.ArrayList;

/**
 * Created by Gerard on 20/4/2017.
 */

public class TrainingsEvent {

    private ArrayList<Training> trainings;

    public TrainingsEvent(ArrayList<Training> trainings) {
        this.trainings = trainings;
    }

    public ArrayList<Training> getTrainings() {
        return trainings;
    }

}
