package com.myofx.emsapp.api.events;

public class SyncAction {

    private boolean success;

    public SyncAction(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
