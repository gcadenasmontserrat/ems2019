package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.User;

public class UserLocalEvent {

    private User user;

    public UserLocalEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
