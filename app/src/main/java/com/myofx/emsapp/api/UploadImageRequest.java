package com.myofx.emsapp.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.myofx.emsapp.api.events.UserEvent;
import com.myofx.emsapp.config.WSConfig;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.utils.PreferencesManager;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerard on 1/3/2017.
 */

public class UploadImageRequest extends AsyncTask<Void, Void, String> {

    private String imagePath;
    private Context context;
    private Exception exception;
    private String userProfile;

    public UploadImageRequest(Context context, String imagePath, String userProfile) {
        this.context = context;
        this.imagePath = imagePath;
        this.userProfile = userProfile;
    }

    protected String doInBackground(Void... path) {

        try {
            HttpClient httpClient = new DefaultHttpClient();
            httpClient.getParams().setParameter("http.useragent", System.getProperty("http.agent"));

            HttpPost request = new HttpPost(WSConfig.WS_UPLOAD_IMAGE);

            try {

                //Add headers
                Map<String, String> headers = generateLogedHeaders(context, userProfile);
                for(Map.Entry<String, String> entry : headers.entrySet()) {
                    request.addHeader(entry.getKey(), entry.getValue());
                }

                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

                //Add file if exist
                if (imagePath!=null && !imagePath.equals("")) {
                    File file = new File(imagePath);
                    FileBody fb = new FileBody(file);
                    entityBuilder.addPart(WSConfig.PARAM_PROFILE_FILE, fb);
                }

                HttpEntity entity = entityBuilder.build();
                request.setEntity(entity);

                HttpResponse result = httpClient.execute(request);
                final int statusCode = result.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    Log.v("Wsresponse", "Error response " + statusCode + " for URL " + WSConfig.WS_UPLOAD_IMAGE);
                }

                HttpEntity getResponseEntity = result.getEntity();
                return convertStreamToString(getResponseEntity.getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            this.exception = e;
            return null;
        }

        return null;
    }

    protected void onPostExecute(String result) {
        User user = new Gson().fromJson(result.toString(), User.class);
        EventBus.getDefault().postSticky(new UserEvent(user, UserEvent.REQUEST_TYPE_BASE));
    }

    public static Map<String, String> generateLogedHeaders(Context context, String userProfile){
        PreferencesManager pref = PreferencesManager.getInstance(context);

        Map<String, String>  params = new HashMap<>();
        params.put(WSConfig.HEADER_APP_ID, "8");
        params.put(WSConfig.HEADER_LANG, "ES");
        params.put(WSConfig.HEADER_VERSION_ID, "1.0.0");
        params.put(WSConfig.HEADER_DEVICE_ID, "a48e-9874d");
        params.put(WSConfig.HEADER_AUTHORIZATION, "Bearer "+pref.getAccessToken());

        if (pref.getProfile() != null){
            params.put(WSConfig.HEADER_PROFILE, userProfile);
        }

        return params;
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
