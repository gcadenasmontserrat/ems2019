package com.myofx.emsapp.api.events;

/**
 * Created by Gerard on 20/9/2017.
 */

public class StartEvent {

    private String uid;

    public StartEvent(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }
}
