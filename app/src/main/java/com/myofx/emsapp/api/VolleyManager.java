package com.myofx.emsapp.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.myofx.emsapp.BuildConfig;

/**
 * Created by Gerard on 7/12/2016.
 */

public class VolleyManager {

    private static VolleyManager mInstance;
    private RequestQueue mRequestQueue;
    private static Context mContext;

    private VolleyManager(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyManager(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        if (BuildConfig.DEBUG)
            getRequestQueue().getCache().remove(req.getUrl());

        getRequestQueue().add(req);
    }

}
