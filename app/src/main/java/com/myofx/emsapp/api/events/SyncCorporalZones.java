package com.myofx.emsapp.api.events;

public class SyncCorporalZones {

    private boolean success;

    public SyncCorporalZones(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

}
