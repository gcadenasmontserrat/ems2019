package com.myofx.emsapp.api.events;

public class SyncSessionResults {

    private boolean success;

    public SyncSessionResults(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

}

