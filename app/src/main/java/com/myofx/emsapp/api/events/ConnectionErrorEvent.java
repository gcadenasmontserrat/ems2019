package com.myofx.emsapp.api.events;

import android.content.Context;

import com.myofx.emsapp.R;

/**
 * Created by Gerard on 7/12/2016.
 */

public final class ConnectionErrorEvent {

    private Context mContext;

    public ConnectionErrorEvent(Context mContext) {
        this.mContext = mContext;
    }

    public String getMsgError() {
        return mContext.getString(R.string.error_connection);
    }

}
