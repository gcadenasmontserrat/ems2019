package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.User;

import java.util.ArrayList;

/**
 * Created by Gerard on 4/7/2017.
 */

public class SearchEvent {

    private ArrayList<User> users;
    private int page;
    private int totalItems;


    public SearchEvent(ArrayList<User> users, int page, int totalItems) {
        this.users = users;
        this.page = page;
        this.totalItems = totalItems;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public int getPage() {
        return page;
    }

    public int getTotalItems() {
        return totalItems;
    }


}
