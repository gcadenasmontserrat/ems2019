package com.myofx.emsapp.api.events;

/**
 * Created by Gerard on 29/3/2017.
 */

public class ProgressExerciceEvent {

    private boolean isEstimulating = true;
    private int estimulationMs;
    private int time;
    private int vestIndex;
    private String adress;

    public ProgressExerciceEvent(boolean isEstimulating, int estimulationMs, int time, int vestIndex) {
        this.isEstimulating = isEstimulating;
        this.estimulationMs = estimulationMs;
        this.time = time;
        this.vestIndex = vestIndex;
    }

    public ProgressExerciceEvent(boolean isEstimulating, int estimulationMs, int time, String adress) {
        this.isEstimulating = isEstimulating;
        this.estimulationMs = estimulationMs;
        this.time = time;
        this.adress = adress;
    }

    public int getTime() {
        return time;
    }

    public boolean isEstimulating() {
        return isEstimulating;
    }

    public int getEstimulationMs() {
        return estimulationMs;
    }

    public int getVestIndex() {
        return vestIndex;
    }

    public String getAdress() {
        return adress;
    }
}
