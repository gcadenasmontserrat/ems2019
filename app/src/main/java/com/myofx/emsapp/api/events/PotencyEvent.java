package com.myofx.emsapp.api.events;

/**
 * Created by Gerard on 29/3/2017.
 */

public class PotencyEvent {

    private int potency;
    private String uid;

    public PotencyEvent(int potency, String uid) {
        this.potency = potency;
        this.uid = uid;
    }

    public int getPotency() {
        return potency;
    }

    public String getUid() {
        return uid;
    }
}
