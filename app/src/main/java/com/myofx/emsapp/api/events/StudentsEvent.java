package com.myofx.emsapp.api.events;

import com.myofx.emsapp.models.User;

import java.util.ArrayList;

/**
 * Created by Gerard on 18/4/2017.
 */

public class StudentsEvent {

    private ArrayList<User> users;
    private int page;
    private int totalItems;


    public StudentsEvent(ArrayList<User> users, int page, int totalItems) {
        this.users = users;
        this.page = page;
        this.totalItems = totalItems;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public int getPage() {
        return page;
    }

    public int getTotalItems() {
        return totalItems;
    }


}
