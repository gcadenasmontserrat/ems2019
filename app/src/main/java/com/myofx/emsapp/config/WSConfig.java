package com.myofx.emsapp.config;

import com.myofx.emsapp.BuildConfig;

/**
 * Created by Gerard on 7/12/2016.
 */

public class WSConfig {

    //VERSION
    private static final String WS_VERSION = "/1";
    
    //HOTSPOT
    public static final String HOTSPOT_SSID = "myoFX-EMS";
    public static final String HOTSPOT_PASS = "My0FX3m0t10n!!!";

    //WS
    public static final String WS_LOGIN = BuildConfig.API_URL+WS_VERSION +"/login";
    public static final String WS_INFO =  BuildConfig.API_URL+WS_VERSION +"/info";
    public static final String WS_USER =  BuildConfig.API_URL+WS_VERSION +"/me";
    public static final String WS_STUDENTS = BuildConfig.API_URL +"/2/user";
    public static final String WS_RECOVERY_PASS =  BuildConfig.API_URL+WS_VERSION +"/send-reset-password";
    public static final String WS_UPLOAD_IMAGE =  BuildConfig.API_URL+WS_VERSION +"/user/image";
    public static final String WS_LOGOUT =  BuildConfig.API_URL+WS_VERSION +"/logout";
    public static final String WS_CONFIG =  BuildConfig.API_URL+WS_VERSION +"/config";
    public static final String WS_TRAININGS = BuildConfig.API_URL +"/2/training";
    public static final String WS_DEVICES =  BuildConfig.API_URL+WS_VERSION +"/machines";
    public static final String WS_SEND_RESULTS =  BuildConfig.API_URL+WS_VERSION +"/session/new";
    public static final String WS_SAVE_USER_CONFIG =  BuildConfig.API_URL+"/2/user";
    public static final String WS_SEND_LOG =  BuildConfig.API_URL+WS_VERSION +"/log";
    public static final String WS_SAVE_STANDARD_VALUES =  BuildConfig.API_URL+WS_VERSION +"/standard-values";
    public static final String WS_REGISTER_DEVICE =  BuildConfig.API_URL+WS_VERSION +"/register-device";

    public static final String WS_SYNC_USER =  "http://api.myofx.eu/sync_user";
    public static final String WS_SYNC_BOOKING =  "http://api.myofx.eu/sync_booking";
    public static final String WS_SYNC_TRAINING =  "http://api.myofx.eu/sync_training";

    //HEADERS
    public static final String HEADER_APP_ID = "X-APP-ID";
    public static final String HEADER_LANG = "X-LANG";
    public static final String HEADER_VERSION_ID = "X-VERSION-ID";
    public static final String HEADER_DEVICE_ID = "X-DEVICE-ID";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_PROFILE = "X-PROFILE";
    public static final String HEADER_LANGUAGE = "accept-language";

    //PARAMS
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_REFRESH_TOKEN = "refresh_token";
    public static final String PARAM_GRANT_TYPE = "grant_type";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_PROFILE_EMAIL = "email";
    public static final String PARAM_PROFILE_FILE = "file";
    public static final String PARAM_CONFIG_COLOR = "color";
    public static final String PARAM_CONFIG_UNITS = "unit_of_length";
    public static final String PARAM_CONFIG_CONNECTION_TYPE = "connection_type";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_LAST_AME = "last_name";
    public static final String PARAM_CENTER_ID = "center_id";

    public static final String PARAM_DEVICE_TOKEN = "deviceToken";
    public static final String PARAM_PLATFORM = "platform";

    public static final String PARAM_USERS = "users";
    public static final String PARAM_UIDS = "uids";
    public static final String PARAM_DATE = "date";
    public static final String PARAM_DURATION = "duration";
    public static final String PARAM_USER_MACHINE = "users-machines";
    public static final String PARAM_TITLE = "title";

    public static final String PARAM_STAFF_ID = "staff_id";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_SERVICE_ID = "center_service_id";
    public static final String PARAM_TRAINING_VALUES = "values";

    public static final String PARAM_LEG = "LEG";
    public static final String PARAM_GLU = "GLU";
    public static final String PARAM_LUM = "LUM";
    public static final String PARAM_DOR = "DOR";
    public static final String PARAM_SHO = "SHO";
    public static final String PARAM_ABS = "ABS";
    public static final String PARAM_PEC = "PEC";
    public static final String PARAM_BIC = "BIC";
    public static final String PARAM_AUX = "AUX";
    public static final String PARAM_AUX2 = "AUX2";
    public static final String PARAM_CRON_LEG = "CRON_LEG";
    public static final String PARAM_CRON_GLU = "CRON_GLU";
    public static final String PARAM_CRON_LUM = "CRON_LUM";
    public static final String PARAM_CRON_DOR = "CRON_DOR";
    public static final String PARAM_CRON_SHO = "CRON_SHO";
    public static final String PARAM_CRON_ABS = "CRON_ABS";
    public static final String PARAM_CRON_PEC = "CRON_PEC";
    public static final String PARAM_CRON_BIC = "CRON_BIC";
    public static final String PARAM_CRON_AUX = "CRON_AUX";
    public static final String PARAM_CRON_AUX2 = "CRON_AUX2";
    public static final String PARAM_ID = "id";
    public static final String PARAM_POTENCY = "POTENCY";
    public static final String PARAM_LOG = "log";
    public static final String PARAM_DATA = "data";
    public static final String PARAM_LOG_DATE = "date";
    public static final String PARAM_LOG_UUID = "uuid";
    public static final String PARAM_LOG_TYPE = "type";
    public static final String PARAM_LOG_COMMAND = "command";
    public static final String PARAM_LOG_DATA = "data";

}
