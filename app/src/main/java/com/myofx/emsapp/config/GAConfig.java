package com.myofx.emsapp.config;

/**
 * Created by Gerard on 27/4/2017.
 */

public class GAConfig {

    //SCEENS
    public static final String SCREEN_LOGIN = "Login";
    public static final String SCREEN_PORTADA = "Portada";
    public static final String SCREEN_ALUMNOS = "Alumnos";
    public static final String SCREEN_AGENDA = "Agenda";
    public static final String SCREEN_ENTRENAMIENTO = "Entrenamiento";
    public static final String SCREEN_DISPOSITIVOS = "Dispositivos";
    public static final String SCREEN_CONFIGURACION = "Configuración";

    //LOGIN
    public static final String SCREEN_LOGIN_SPLASH = "/Splash";
    public static final String SCREEN_LOGIN_LOGIN = "/Login";

    //PORTADA
    public static final String SCREEN_PORTADA_INICIO = "/Inicio";

    //ALUMNOS
    public static final String SCREEN_ALUMNOS_ALUMNOS = "/Alumnos";
    public static final String SCREEN_ALUMNOS_VER = "/Verperfil";

    //AGENDA
    public static final String SCREEN_AGENDA_AGENDA = "/Agenda";

    //ENTRENAMIENTO
    public static final String SCREEN_ENTRENAMIENTO_EJERCICIOS = "/Ejercicios";
    public static final String SCREEN_ENTRENAMIENTO_PARTICIPANTES = "/Participantes";
    public static final String SCREEN_ENTRENAMIENTO_EJERCICIO = "/Ejercicio";

    //DISPOSITIUS
    public static final String SCREEN_DISPOSITIVOS_DISPOSITIVOS = "/Dispositivos";

    //CONFIGURACION
    public static final String SCREEN_CONFIGURACION_CONFIGURACION = "/Configuracion";
}
