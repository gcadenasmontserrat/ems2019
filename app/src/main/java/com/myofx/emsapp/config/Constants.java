package com.myofx.emsapp.config;

/**
 * Created by Gerard on 29/11/2016.
 */

public class Constants {

    //Youtube
    public static final String YOUTUBE_API_KEY = "AIzaSyBlHUWCb34_oVCFbsciPAImKAfZGb-twLw";

    //Flurry
    public static final String FLURRY_API_KEY = "GC67YS5JBWHD7ZFFVK56";

    //Gender
    public static final String GENDER_MAN = "H";

    //Fonts
    public static final String FONT = "fonts/";
    public static final String FONT_MS_100 = FONT+"MuseoSans-100.ttf";
    public static final String FONT_MS_300 = FONT+"MuseoSans-300.ttf";
    public static final String FONT_MS_500 = FONT+"MuseoSans-500.ttf";
    public static final String FONT_MS_700 = FONT+"MuseoSans-700.ttf";
    public static final String FONT_MS_700italic = FONT+"MuseoSans-700italic.ttf";
    public static final String FONT_MS_900 = FONT+"MuseoSans-900.ttf";
    public static final String FONT_MP_BOLD = FONT+"MetronicPro-Bold.otf";
    public static final String FONT_MP_BLACK = FONT+"MetronicPro-Black.otf";
    public static final String FONT_LATO_MEDIUM = FONT+"Lato-Medium.ttf";
    public static final String FONT_MP_BOLD_ITALIC = FONT+"MetronicPro-BoldItalic.otf";

    //Local
    public static final String LOCAL_USER_1 = "local";
    public static final String LOCAL_PASS_1 = "local";
    public static final String LOCAL_USER_2 = "dynamix";
    public static final String LOCAL_PASS_2 = "local";
    public static final String LOCAL_USER_3 = "emsltd";
    public static final String LOCAL_PASS_3 = "local";
    public static final String LOCAL_USER_4 = "kz";
    public static final String LOCAL_PASS_4 = "kz";

}
