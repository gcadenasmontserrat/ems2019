package com.myofx.emsapp.bluetooth;

/**
 * Created by Gerard on 19/5/2017.
 */

public interface GattCharacteristicReadCallback {
    void call(byte[] characteristic);
}
