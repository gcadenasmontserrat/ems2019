package com.myofx.emsapp.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

/**
 * Created by Gerard on 20/1/2017.
 */

public class BluetoothMaxForceLeService extends Service {

    private final static String TAG = BluetoothMaxForceLeService.class.getSimpleName();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    //ACTIONS
    public final static String ACTION_GATT_CONNECTED = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_CHAR_AVAILABLE = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_CHAR_AVAILABLE";
    public final static String ACTION_DATA_ACCEL_AVAILABLE = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_DATA_ACCEL_AVAILABLE";
    public final static String ACTION_DATA_BATTERY_AVAILABLE = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_DATA_BATTERY_AVAILABLE";
    public final static String ACTION_DATA_INFORMATION_AVAILABLE = "com.myofx.emsapp.bluetooth.BluetoothMaxForceLeService.ACTION_DATA_INFORMATION_AVAILABLE";
    public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";

    //UUID
    public static UUID FORCE_SERVICE_UUID = UUID.fromString("E0DE1300-C860-4813-A6C5-A9EB4EF6257E");
    public static UUID FORCE_MEASURE_CHARACTERISTIC_UUID = UUID.fromString("E0DE1401-C860-4813-A6C5-A9EB4EF6257E");
    public static UUID IMU_SERVICE_UUID = UUID.fromString("E0DE1301-C860-4813-A6C5-A9EB4EF6257E");
    public static UUID ACCELEROMETER_MEASURE_CHARACTERISTIC_UUIDD = UUID.fromString("E0DE1402-C860-4813-A6C5-A9EB4EF6257E");
    private static final UUID BATTERY_SERVICE_UUID = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    private static final UUID BATTERY_LEVEL_CHARACTERISTIC_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    public static UUID CLIENT_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID INFORMATION_SERVICE_UUID = UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb");
    public static UUID INFORMATION_FIRMWARE_REVISION_CHARACTERISTIC_UUID = UUID.fromString("00002A26-0000-1000-8000-00805f9b34fb");

    //DFU
    public static final UUID MAXFORCE_LEGACY_DFU_SERVICE_UUID  = UUID.fromString("00001530-1212-EFDE-1523-785FEABCD123");
    public static String MAXFORCE_DFUTARG_NAME  = "DfuTarg";

    //Characteristics
    private BluetoothGattCharacteristic force;
    private BluetoothGattCharacteristic accelerometer;
    private BluetoothGattCharacteristic battery;

    // Queues for characteristic read (synchronous)
    private Queue<BluetoothGattCharacteristic> readQueue;
    private Queue<BluetoothGattDescriptor> descriptorWriteQueue= new LinkedList<>();
    private Queue<BluetoothGattCharacteristic> characteristicReadQueue = new LinkedList<>();

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i("STATE", "CONNECTED: OK");
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i("STATE", "STATE CONNECTED BROADCAST SENT - DISCOVERING SERVICES: OK");
                mBluetoothGatt.discoverServices();
                Log.i("STATE", "AFTER DISCOVER SERVICES: OK");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i("STATE", "STATE DISCONNECTED: OK");
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                broadcastUpdate(intentAction);
                Log.i("STATE", "DISCONNECTED BROADCAST SENT: OK");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                Log.i("GATT SUCCESS", "OK");
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }

            force = gatt.getService(FORCE_SERVICE_UUID).getCharacteristic(FORCE_MEASURE_CHARACTERISTIC_UUID);
            accelerometer = gatt.getService(IMU_SERVICE_UUID).getCharacteristic(ACCELEROMETER_MEASURE_CHARACTERISTIC_UUIDD);
            battery = gatt.getService(BATTERY_SERVICE_UUID).getCharacteristic(BATTERY_LEVEL_CHARACTERISTIC_UUID);

            setNotifications(gatt,force);
            setNotifications(gatt,accelerometer);
            setNotifications(gatt,battery);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "Callback: Wrote GATT Descriptor successfully.");
            } else{
                Log.d(TAG, "Callback: Error writing GATT Descriptor: "+ status);
            }
            descriptorWriteQueue.remove();  //pop the item that we just finishing writing
            //if there is more to write, do it!
            if(descriptorWriteQueue.size() > 0)
                gatt.writeDescriptor(descriptorWriteQueue.element());
            else if(characteristicReadQueue.size() > 0)
                gatt.readCharacteristic(readQueue.element());
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        private void setNotifications(BluetoothGatt gatt,BluetoothGattCharacteristic bleChar){
            if (!gatt.setCharacteristicNotification(bleChar, true)) {
                Log.e("connectfailed", "T1 " );
                return;
            }
            BluetoothGattDescriptor desc = bleChar.getDescriptor(CLIENT_UUID);
            if (desc == null) {
                Log.e("connectfailed", "T2 " );
                return;
            }
            desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            writeGattDescriptor(desc);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(characteristic);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
        Log.i("INTENT SENT", "OK");
    }

    private void broadcastUpdate(final BluetoothGattCharacteristic characteristic) {
        String action = null;
        if (characteristic.getUuid().toString().equals(FORCE_MEASURE_CHARACTERISTIC_UUID.toString())){
            action = ACTION_CHAR_AVAILABLE;
        } else if (characteristic.getUuid().toString().equals(ACCELEROMETER_MEASURE_CHARACTERISTIC_UUIDD.toString())){
            action = ACTION_DATA_ACCEL_AVAILABLE;
        } else if (characteristic.getUuid().toString().equals(BATTERY_LEVEL_CHARACTERISTIC_UUID.toString())){
            action = ACTION_DATA_BATTERY_AVAILABLE;
        } else if (characteristic.getUuid().toString().equals(INFORMATION_FIRMWARE_REVISION_CHARACTERISTIC_UUID.toString())){
            action = ACTION_DATA_INFORMATION_AVAILABLE;
        }

        //Send battery
        if (action.equals(ACTION_DATA_BATTERY_AVAILABLE)) {
            final Intent intent = new Intent(action);
            intent.putExtra(EXTRA_DATA, Integer.toString(characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)));
            sendBroadcast(intent);
        } else if (action.equals(ACTION_DATA_INFORMATION_AVAILABLE)) {
            final Intent intent = new Intent(action);
            intent.putExtra(EXTRA_DATA, new String(characteristic.getValue()));
            sendBroadcast(intent);
        } else if (action != null){
            final Intent intent = new Intent(action);
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for(byte byteChar : data){
                    stringBuilder.append(String.format("%02X ", byteChar));
                }

                intent.putExtra(EXTRA_DATA,stringBuilder.toString());
            }
            sendBroadcast(intent);
        }
    }

    public class LocalBinder extends Binder {
        public BluetoothMaxForceLeService getService() {
            return BluetoothMaxForceLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

    }

    public void writeGattDescriptor(BluetoothGattDescriptor d){
        //put the descriptor into the write queue
        descriptorWriteQueue.add(d);
        //if there is only 1 item in the queue, then write it.  If more than 1, we handle asynchronously in the callback above
        if(descriptorWriteQueue.size() == 1){
            if(!mBluetoothGatt.writeDescriptor(d))
                Log.e("write desc failed", "T3 " );
        }
        Log.e("descwrite ", "+1 ");
    }



    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public int getmConnectionState() {
        return mConnectionState;
    }

    public void getFirmwareVersion(){
        BluetoothGattService deviceService = mBluetoothGatt.getService(INFORMATION_SERVICE_UUID);
        if (deviceService == null)
            return;

        BluetoothGattCharacteristic firmwareRevision = deviceService.getCharacteristic(INFORMATION_FIRMWARE_REVISION_CHARACTERISTIC_UUID);
        if(firmwareRevision != null) {
            mBluetoothGatt.readCharacteristic(firmwareRevision);
        }


    }
}
