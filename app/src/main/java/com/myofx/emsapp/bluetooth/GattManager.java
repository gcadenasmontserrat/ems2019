package com.myofx.emsapp.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.AsyncTask;

import com.myofx.emsapp.bluetooth.operations.GattCharacteristicReadOperation;
import com.myofx.emsapp.bluetooth.operations.GattDescriptorReadOperation;
import com.myofx.emsapp.bluetooth.operations.GattOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Gerard on 19/5/2017.
 */

public class GattManager {

    public static UUID FORCE_SERVICE_UUID = UUID.fromString("E0DE1300-C860-4813-A6C5-A9EB4EF6257E");
    public static UUID FORCE_MEASURE_CHARACTERISTIC_UUID = UUID.fromString("E0DE1401-C860-4813-A6C5-A9EB4EF6257E");
    public static UUID CLIENT_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private ConcurrentLinkedQueue<GattOperation> mQueue;
    private ConcurrentHashMap<String, BluetoothGatt> mGatts;
    private GattOperation mCurrentOperation;
    private HashMap<UUID, ArrayList<CharacteristicChangeListener>> mCharacteristicChangeListeners;
    private AsyncTask<Void, Void, Void> mCurrentOperationTimeout;
    private Context context;

    public GattManager(Context context) {
        this.context = context;
        mQueue = new ConcurrentLinkedQueue<>();
        mGatts = new ConcurrentHashMap<>();
        mCurrentOperation = null;
        mCharacteristicChangeListeners = new HashMap<>();
    }

    public synchronized void cancelCurrentOperationBundle() {
        if(mCurrentOperation != null && mCurrentOperation.getBundle() != null) {
            for(GattOperation op : mCurrentOperation.getBundle().getOperations()) {
                mQueue.remove(op);
            }
        }
        mCurrentOperation = null;
        connect();
    }

    public synchronized void queue(GattOperation gattOperation) {
        mQueue.add(gattOperation);
        connect();
    }

    private synchronized void connect() {

        final GattOperation operation = mQueue.poll();
        setCurrentOperation(operation);

        final BluetoothDevice device = operation.getDevice();
        if(mGatts.containsKey(device.getAddress())) {
            execute(mGatts.get(device.getAddress()), operation);
        } else {
            device.connectGatt(context, true, new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    super.onConnectionStateChange(gatt, status, newState);

                    if (status == 133) {
                        mGatts.remove(device.getAddress());
                        setCurrentOperation(null);
                        gatt.close();

                        return;
                    }

                    if (status == 8) {
                        mGatts.remove(device.getAddress());
                        setCurrentOperation(null);
                        gatt.close();
                    }

                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        mGatts.put(device.getAddress(), gatt);
                        gatt.discoverServices();

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        mGatts.remove(device.getAddress());
                        setCurrentOperation(null);
                        gatt.close();
                        connect();
                    }
                }

                @Override
                public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                    super.onDescriptorRead(gatt, descriptor, status);
                    ((GattDescriptorReadOperation) mCurrentOperation).onRead(descriptor);
                    setCurrentOperation(null);
                    connect();
                }

                @Override
                public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                    super.onDescriptorWrite(gatt, descriptor, status);
                    setCurrentOperation(null);
                    connect();
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    super.onCharacteristicRead(gatt, characteristic, status);
                    ((GattCharacteristicReadOperation) mCurrentOperation).onRead(characteristic);
                    setCurrentOperation(null);
                    connect();
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    super.onServicesDiscovered(gatt, status);
//                    Log.v("services discovered, status: " + status,"");
                    execute(gatt, operation);
                }


                @Override
                public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    super.onCharacteristicWrite(gatt, characteristic, status);
                    setCurrentOperation(null);
                    connect();
                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                    super.onCharacteristicChanged(gatt, characteristic);
                    if (mCharacteristicChangeListeners.containsKey(characteristic.getUuid())) {
                        for (CharacteristicChangeListener listener : mCharacteristicChangeListeners.get(characteristic.getUuid())) {
                            listener.onCharacteristicChanged(device.getAddress(), characteristic);
                        }
                    }
                }
            });
            waitIdle();
        }
    }

    private void execute(BluetoothGatt gatt, GattOperation operation) {
        if(operation != mCurrentOperation) {
            return;
        }
        operation.execute(gatt);
        if(!operation.hasAvailableCompletionCallback()) {
            setCurrentOperation(null);
            connect();
        }
    }

    public synchronized void setCurrentOperation(GattOperation currentOperation) {
        mCurrentOperation = currentOperation;
    }

    public BluetoothGatt getGatt(BluetoothDevice device) {
        return mGatts.get(device);
    }

    public void addCharacteristicChangeListener(UUID characteristicUuid, CharacteristicChangeListener characteristicChangeListener) {
        if(!mCharacteristicChangeListeners.containsKey(characteristicUuid)) {
            mCharacteristicChangeListeners.put(characteristicUuid, new ArrayList<CharacteristicChangeListener>());
        }
        mCharacteristicChangeListeners.get(characteristicUuid).add(characteristicChangeListener);
    }

    public static boolean waitIdle() {
        int i = 300;
        i /= 10;
        while (--i > 0) {
            if (true)
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
        return i > 0;
    }

    public void disconnectAll() {
        for (String key : mGatts.keySet())
            mGatts.get(key).disconnect();
    }

    public void disconnectDevice(String adress) {
        for (String key : mGatts.keySet())
        if (mGatts.get(key).getDevice().getAddress().equals(adress)){
            mGatts.get(key).disconnect();
            break;
        }
    }

}
