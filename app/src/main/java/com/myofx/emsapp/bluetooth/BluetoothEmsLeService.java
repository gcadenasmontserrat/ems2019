package com.myofx.emsapp.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.myofx.emsapp.api.events.BleStatusEvent;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.utils.PreferencesManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

public class BluetoothEmsLeService extends Service {

    private final static String TAG = BluetoothEmsLeService.class.getSimpleName();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private HashMap<String,BluetoothGatt> mBluetoothGatts;
    private int mConnectionState = STATE_DISCONNECTED;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    //ACTIONS GATT
    public final static String ACTION_GATT_CONNECTED = "com.myofx.emsapp.bluetooth.BluetoothEmsLeService.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "com.myofx.emsapp.bluetooth.BluetoothEmsLeService.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.myofx.emsapp.bluetooth.BluetoothEmsLeService.ACTION_GATT_SERVICES_DISCOVERED";

    //UUID
    public static UUID CLIENT_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static UUID COMUNICATION_SERVICE_UUID = UUID.fromString("06611500-865D-42F4-80DA-0071647920FE");
    public static UUID COMUNICATION_CHARACTERISTIC_RECIVE_UUID = UUID.fromString("06611601-865D-42F4-80DA-0071647920FE");
    public static UUID COMUNICATION_CHARACTERISTIC_SEND_UUID = UUID.fromString("06611602-865D-42F4-80DA-0071647920FE");

    private Queue<BluetoothGattCharacteristic> readQueue;
    private Queue<BluetoothGattDescriptor> descriptorWriteQueue = new LinkedList<>();
    private Queue<BluetoothGattCharacteristic> characteristicReadQueue = new LinkedList<>();

    //Write Queue

    private HashMap<String,BluetoothGattCharacteristic> valuesReceived;
    private HashMap<String,BluetoothGattCharacteristic> valuesSend;
    private HashMap<String,Queue<byte[]>> writeQueues;

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i("STATE", "CONNECTED: OK");
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i("STATE", "STATE CONNECTED BROADCAST SENT - DISCOVERING SERVICES: OK");
                mBluetoothGatts.get(gatt.getDevice().getAddress()).discoverServices();
                Log.i("STATE", "AFTER DISCOVER SERVICES: OK");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i("STATE", "STATE DISCONNECTED: OK");
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                broadcastUpdate(intentAction);
                Log.i("STATE", "DISCONNECTED BROADCAST SENT: OK");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                Log.i("GATT SUCCESS", "OK");
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }

            //Check if have Hash of queues
            if (valuesSend == null)
                valuesSend = new HashMap<>();

            if (valuesReceived == null)
                valuesReceived = new HashMap<>();

            valuesReceived.put(gatt.getDevice().getAddress(), gatt.getService(COMUNICATION_SERVICE_UUID).getCharacteristic(COMUNICATION_CHARACTERISTIC_RECIVE_UUID));
            valuesSend.put(gatt.getDevice().getAddress(), gatt.getService(COMUNICATION_SERVICE_UUID).getCharacteristic(COMUNICATION_CHARACTERISTIC_SEND_UUID));

            setNotifications(gatt,valuesReceived.get(gatt.getDevice().getAddress()));
            setNotifications(gatt,valuesSend.get(gatt.getDevice().getAddress()));
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "Callback: Wrote GATT Descriptor successfully.");
            } else{
                Log.d(TAG, "Callback: Error writing GATT Descriptor: "+ status);
            }

            descriptorWriteQueue.remove();
            if(descriptorWriteQueue.size() > 0)
                gatt.writeDescriptor(descriptorWriteQueue.element());
            else if(characteristicReadQueue.size() > 0)
                gatt.readCharacteristic(readQueue.element());
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void setNotifications(BluetoothGatt gatt,BluetoothGattCharacteristic bleChar){
            if (!gatt.setCharacteristicNotification(bleChar, true)) {
                Log.e(TAG, "T1 " );
                return;
            }

            BluetoothGattDescriptor desc = bleChar.getDescriptor(CLIENT_UUID);
            if (desc == null) {
                Log.e(TAG, "T2 " );
                return;
            }
            desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            writeGattDescriptor(desc, gatt.getDevice().getAddress());
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(characteristic, gatt.getDevice().getAddress());
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(characteristic, gatt.getDevice().getAddress());
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            writeQueues.get(gatt.getDevice().getAddress()).remove();

            //Notify device ble status
            EventBus.getDefault().postSticky(new BleStatusEvent(writeQueues.get(gatt.getDevice().getAddress()).isEmpty(), gatt.getDevice().getAddress()));
            nextAction(gatt.getDevice().getAddress());
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final BluetoothGattCharacteristic characteristic, String address) {
        if (characteristic.getUuid().toString().equals(COMUNICATION_CHARACTERISTIC_RECIVE_UUID.toString())) {
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0)
                BluetoothEmsCommands.doActionCommandValue(data, address);
        }
    }

    public class LocalBinder extends Binder {
        public BluetoothEmsLeService getService() {
            return BluetoothEmsLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new BluetoothEmsLeService.LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatts != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatts for connection.");
            if (mBluetoothGatts.get(address).connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        if (mBluetoothGatts == null)
            mBluetoothGatts = new HashMap<>();

        mBluetoothGatts.put(address, device.connectGatt(this, false, mGattCallback));
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect(String deviceaddress) {
        if (mBluetoothGatts != null){
            for (String key : mBluetoothGatts.keySet()) {
                if (mBluetoothGatts.get(key).getDevice() != null){
                    if (mBluetoothGatts.get(key).getDevice().getAddress().equals(deviceaddress)){
                        mBluetoothGatts.get(key).disconnect();
                        mBluetoothGatts.get(key).close();
                        mBluetoothGatts.remove(key);

                        if (mBluetoothGatts.size() == 0)
                            mBluetoothGatts = null;
                        break;
                    }
                }
            }
        }
    }

    public void disconnectAll() {
        if (mBluetoothGatts == null)
            return;

        for (String key : mBluetoothGatts.keySet()){
            mBluetoothGatts.get(key).disconnect();
            mBluetoothGatts.get(key).close();
        }

        mBluetoothGatts = null;
    }

    public void close() {
        if (mBluetoothGatts == null)
            return;

        for (String key : mBluetoothGatts.keySet())
            mBluetoothGatts.get(key).close();

        mBluetoothGatts = null;
    }

    public void writeGattDescriptor(BluetoothGattDescriptor d, String adress){
        descriptorWriteQueue.add(d);
        if(descriptorWriteQueue.size() == 1){
            if(!mBluetoothGatts.get(adress).writeDescriptor(d))
                Log.e("write desc failed", "T3 " );
        }
        Log.e("descwrite ", "+1 ");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      SEND COMMANDS                                         //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void startKeepAlive(Context context){
        new Thread(new KeepAliveBleThread(context)).start();
    }

    class KeepAliveBleThread implements Runnable {

        private ArrayList<Device> bleDevices = new ArrayList<>();
        private Context context;

        public KeepAliveBleThread(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            while(true){
                try {
                    Thread.sleep(4000);
                    bleDevices = PreferencesManager.getInstance(context).getAllDevices();
                    for (Device device: bleDevices){
                        if (valuesSend.get(device.getDeviceBleAdress()) != null) {
                            addAction(BluetoothEmsCommands.getKeepAliveCommand() ,device.getDeviceBleAdress());
                        }
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //SEND PROGRAM + START
    public void sendProgramAndStart(String address, Training training){
        addAction(BluetoothEmsCommands.getProgramCommand(training) ,address);
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
        addAction(BluetoothEmsCommands.getChronaxyCommand(training) ,address);
        addAction(BluetoothEmsCommands.getStartCommand() ,address);
    }

    //SEND PROGRAM + START
    public void sendProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getProgramCommand(training) ,address);
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
        addAction(BluetoothEmsCommands.getChronaxyCommand(training) ,address);
    }

    //SEND STANDARD
    public void sendStandardSelected(String address, Training training){
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
    }

    //SEND STANDARD USER
    public void sendStandardUser(String address, Training training){
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
        addAction(BluetoothEmsCommands.getChronaxyCommand(training) ,address);
    }

    //SEND START PROGRAM
    public void sendStartProgram(String address){
        addAction(BluetoothEmsCommands.getStartCommand() ,address);
    }

    //SEND RESTART
    public void sendRestartProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getRestartCommand() ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
    }

    //SEND RESTART + CHANNELS
    public void sendRestartAndChannelsProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getRestartCommand() ,address);
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
        addAction(BluetoothEmsCommands.getProgramCommand(training) ,address);
    }

    //CHRONAXY
    public void sendChronaxyProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getChronaxyCommand(training) ,address);
    }

    //STAGE
    public void sendStageProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getProgramCommand(training) ,address);
    }

    //MAIN
    public void sendMainProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
    }

    //MAIN + STOP
    public void sendMainAndStopProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getMainCommand(training) ,address);
        addAction(BluetoothEmsCommands.getStopCommand() ,address);
    }

    //STOP
    public void sendStopProgram(String address){
        addAction(BluetoothEmsCommands.getStopCommand() ,address);
    }

    //LEVELS
    public void sendLevelsProgram(String address, Training training){
        addAction(BluetoothEmsCommands.getLevelsCommand(training) ,address);
    }

    //WRITE CHARACTERISIC QUEUE
    public void addAction(byte[] valueWrite, String address){
        //Check if have Hash of queues
        if (writeQueues == null)
            writeQueues = new HashMap<>();

        //Check if exist queue for device
        Queue<byte[]> writeQueueDevice = writeQueues.get(address);
        if (writeQueueDevice == null)
            writeQueueDevice = new LinkedList<>();

        //Add write action
        writeQueueDevice.add(valueWrite);
        writeQueues.put(address, writeQueueDevice);

        //If have only one action -> write.
        if (writeQueues.get(address).size() == 1)
            nextAction(address);
    }

    public void nextAction(String address){

        if (writeQueues.get(address).isEmpty())
            return;

        valuesSend.get(address).setValue(writeQueues.get(address).element());
        mBluetoothGatts.get(address).writeCharacteristic(valuesSend.get(address));
    }
}