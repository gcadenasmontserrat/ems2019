package com.myofx.emsapp.bluetooth;

/**
 * Created by Gerard on 19/5/2017.
 */

public interface GattDescriptorReadCallback {
    void call(byte[] value);
}
