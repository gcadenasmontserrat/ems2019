package com.myofx.emsapp.bluetooth;

import android.util.Log;

import com.myofx.emsapp.api.events.PotencyEvent;
import com.myofx.emsapp.api.events.ProgressExerciceEvent;
import com.myofx.emsapp.api.events.StartEvent;
import com.myofx.emsapp.api.events.StopEvent;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.server.DataParser;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class BluetoothEmsCommands {

    //Variables
    private static ArrayList<String> checkDevicesId = new ArrayList<>();
    private static boolean startCheckDevice = false;

    //KEEP ALIVE
    public static byte[] getKeepAliveCommand(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x00});
            outputStream.write(new byte[] {(byte) 0x00});
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputStream.toByteArray();
    }

    //START
    public static byte[] getStartCommand(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x03});
            outputStream.write(new byte[] {(byte) 0x00});
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getStartCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //RESTART
    public static byte[] getRestartCommand(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x02});
            outputStream.write(new byte[] {(byte) 0x00});
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getRestartCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //STOP
    public static byte[] getStopCommand(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x01});
            outputStream.write(new byte[] {(byte) 0x00});
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getStopCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //PROGRAM
    public static byte[] getProgramCommand(Training training){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x05});
            outputStream.write(new byte[] {(byte) 13});
            outputStream.write(DataParser.sendStage(new int[]{
                    training.getMain()[0],
                    training.getMain()[1],
                    training.getMain()[4],
                    training.getMain()[7],
                    training.getMain()[5],
                    training.getMain()[2],
                    training.getMain()[6],
                    training.getMain()[3],
                    training.getMain()[9],
                    training.getMain()[10]}));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getProgramCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //MAIN
    public static byte[] getMainCommand(Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x04});
            outputStream.write(new byte[] {(byte) 02});
            outputStream.write(DataParser.sendMains(new int[]{(int) Math.ceil(training.getLevel(0)), (int) Math.ceil((float)(training.getPotencyRelax()))}));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getMainCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //LEVELS
    public static byte[] getLevelsCommand(Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x06});
            outputStream.write(new byte[] {(byte) 10});
            outputStream.write(DataParser.sendLevels(training.getLevels()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getLevelsCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //CHRONAXY
    public static byte[] getChronaxyCommand(Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(new byte[] {(byte) 0x07});
            outputStream.write(new byte[] {(byte) 10});
            outputStream.write(DataParser.sendChronaxy(training.getChronaxys()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("BluetoothEmsCommands", "getChronaxyCommand -> " +Arrays.toString(outputStream.toByteArray()));
        return outputStream.toByteArray();
    }

    //RECIVE COMMANDS
    public static void doActionCommandValue(byte[] data, String adress) {

        final int COMMAND_STOP = 1;
        final int COMMAND_START = 2;
        final int COMMAND_MAIN = 4;
        final int COMMAND_STATUS = 11;
        final int COMMAND_CYCLE = 12;

        Byte b1 = new Byte(data[0]);

        switch (b1.intValue()){
            case COMMAND_STOP:
                EventBus.getDefault().postSticky(new StopEvent(adress));
                break;
            case COMMAND_START:
                EventBus.getDefault().postSticky(new StartEvent(adress));
                break;
            case COMMAND_MAIN:
                EventBus.getDefault().postSticky(new PotencyEvent(new Byte(data[2]).intValue(), adress));
                break;
            case COMMAND_STATUS:
                if (startCheckDevice)
                    addDeviceToCheck(adress);

                break;
            case COMMAND_CYCLE:
                EventBus.getDefault().postSticky(new ProgressExerciceEvent(getCycle(data)[1] == 1, getCycle(data)[3], getCycle(data)[0], adress));
                break;
        }
    }

    public static void addDeviceToCheck(String adress){
        if (!checkDevicesId.contains(adress)) {
            checkDevicesId.add(adress);
        }
    }

    public static void setStartCheckDevice(boolean startCheckDevice) {
        if (startCheckDevice)
            checkDevicesId.clear();

        BluetoothEmsCommands.startCheckDevice = startCheckDevice;
    }

    public static ArrayList<String> getCheckDevicesId() {
        return checkDevicesId;
    }

    public static int[] getCycle(byte[] cycle){
        int[] aux = new int[4];
        aux[0] = ((cycle[3] & 0xff) << 8) | (cycle[2] & 0xff);
        aux[1] = cycle[4];
        aux[2] = cycle[5];
        aux[3] = ((cycle[7] & 0xff) << 8) | (cycle[6] & 0xff);
        return aux;
    }
}
