package com.myofx.emsapp.bluetooth;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by Gerard on 19/5/2017.
 */

public interface CharacteristicChangeListener {
    public void onCharacteristicChanged(String deviceAddress, BluetoothGattCharacteristic characteristic);
}
