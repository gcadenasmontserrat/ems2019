package com.myofx.emsapp.bluetooth.operations;

import java.util.ArrayList;

/**
 * Created by Gerard on 19/5/2017.
 */

public class GattOperationBundle {

    final ArrayList<GattOperation> operations;

    public GattOperationBundle() {
        operations = new ArrayList<>();
    }

    public void addOperation(GattOperation operation) {
        operations.add(operation);
        operation.setBundle(this);
    }

    public ArrayList<GattOperation> getOperations() {
        return operations;
    }

}
