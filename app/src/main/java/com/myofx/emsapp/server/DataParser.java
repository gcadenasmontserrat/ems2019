package com.myofx.emsapp.server;


public class DataParser {

	public static boolean checkNewDevice(byte[]  message){
		if (message[14] == 2 || message[14] == 3 || message[15] == 20 || message[16] == 0){
			return false;
		} else if (message[22] == 2 || message[22] == 3 || message[23] == 20 || message[24] == 0){
			return true;
		}

		return false;
	}

	//GET UUID
	public static byte[] getUID(byte[]  status, boolean newDevices){
		if (!newDevices)
			return getUIDOld(status);
		else
			return getUIDNew(status);
	}

	//GET COMMAND TYPE
	public static int getCommandType(byte[] type, boolean newDevices){
		if (!newDevices)
			return getCommandTypeOld(type);
		else
			return getCommandTypeNew(type);
	}


	//GET COMMAND
	public static int getCommand(byte[] command, boolean newDevices){
		if (!newDevices)
			return getCommandOld(command);
		else
			return getCommandOldNew(command);
	}

	//GET CYCLE
	public static int[] getCycle(byte[] cycle, boolean newDevices){
		if (!newDevices)
			return getCycleOld(cycle);
		else
			return getCycleNew(cycle);
	}

	//GET STATUS
	public static int[] getStatus(byte[] status, boolean newDevices){
		if (!newDevices)
			return getStatusOld(status);
		else
			return getStatusNew(status);
	}

	//GET MAINS
	public static int[] getMains(byte[] mains, boolean newDevices){
		if (!newDevices)
			return getMainsOld(mains);
		else
			return getMainsNew(mains);
	}

	public static byte[] getUIDOld(byte[]  status){
		byte[] aux = new byte [] {status[8],status[9],status[10],status[11],status[12],status[13]};
		return aux;
	}

	public static byte[] getUIDNew(byte[]  status){
		byte[] aux = new byte [] {status[8],status[9],status[10],status[11],status[12],status[13],status[14],status[15]
				,status[16],status[17],status[18],status[19],status[20],status[21]};
		return aux;
	}

	public static int getCommandTypeOld(byte[] type){
		int aux;
		aux = type[14];
		return aux;
	}

	public static int getCommandTypeNew(byte[] type){
		int aux;
		aux = type[22];
		return aux;
	}

	public static int getCommandOld(byte[] command){
		int aux;
		aux = command[15];		
		return aux;
	}

	public static int getCommandOldNew(byte[] command){
		int aux;
		aux = command[23];
		return aux;
	}

	public static int[] getCycleOld(byte[] cycle){
		int[] aux = new int[4];
		aux[0] = ((cycle[18] & 0xff) << 8) | (cycle[17] & 0xff);
		aux[1] = cycle[19];
		aux[2] = cycle[20];
		aux[3] = ((cycle[22] & 0xff) << 8) | (cycle[21] & 0xff);
		return aux;
	}

	public static int[] getCycleNew(byte[] cycle){
		int[] aux = new int[4];
		aux[0] = ((cycle[26] & 0xff) << 8) | (cycle[25] & 0xff);
		aux[1] = cycle[27];
		aux[2] = cycle[28];
		aux[3] = ((cycle[30] & 0xff) << 8) | (cycle[29] & 0xff);
		return aux;
	}

	public static int[] getStatusOld(byte[] status){
		int[] aux = new int[5];
		aux[0] = status[16];
		aux[1] = status[17];
		aux[2] = status[18];
		aux[3] = status[19];
		aux[4] = status[20];
		return aux;
	}

	public static int[] getStatusNew(byte[] status){
		int[] aux = new int[5];
		aux[0] = status[25];
		aux[1] = status[26];
		aux[2] = status[27];
		aux[3] = status[28];
		aux[4] = status[29];
		return aux;
	}

	public static int[] getMainsOld(byte[] mains){
		int[] aux = new int[2];
		aux[0] = mains[17];
		aux[1] = mains[18];
		return aux;
	}

	public static int[] getMainsNew(byte[] mains){
		int[] aux = new int[2];
		aux[0] = mains[25];
		aux[1] = mains[26];
		return aux;
	}

	//SEND POTENCY MAX
	public static byte[] sendPotencyMax(int[] potencyMax){
		byte[] aux = new byte[1];
		aux[0] = (byte) potencyMax[0];

		return aux;
	}

	//SEND MAINS
	public static byte[] sendMains(int[] mains){
		byte[] aux = new byte[2];
		aux[0] = (byte) mains[0];
		aux[1] = (byte) mains[1];

		return aux;
	}

	//SEND STAGE
	public static byte[] sendStage(int[] stage){
		//stage = stage,freq,pulse,duration,rampup,stim,rdown,pause,freqrel,tpulserel;
		byte[] aux = new byte[13];

		aux[0] = (byte) stage[0];//
		aux[1] = (byte) stage[1];//
		aux[4] = (byte) (stage[2]/5);//
		aux[2] = (byte) (stage[3] & 0xFF);
		aux[3] = (byte) ((stage[3] >> 8) & 0xFF);
		aux[5] = (byte) (stage[4]/100);//
		aux[6] = (byte) ((stage[5]/100) & 0xFF);
		aux[7] = (byte) (((stage[5]/100) >> 8) & 0xFF);
		aux[10] = (byte) (stage[6]/100);//
		aux[8] = (byte) ((stage[7]/100) & 0xFF);
		aux[9] = (byte) (((stage[7]/100) >> 8) & 0xFF);
		aux[11] = (byte) stage[8];//
		aux[12] = (byte) (stage[9]/5);//

		return aux;
	}

	//SEND CHRONAXY
	public static byte[] sendChronaxy(int[] Chronaxy){
		byte[] aux = new byte[10];
		
		 aux[0] = (byte) (Chronaxy[0]/5);
		 aux[1] = (byte) (Chronaxy[1]/5);
		 aux[2] = (byte) (Chronaxy[2]/5);
		 aux[3] = (byte) (Chronaxy[3]/5);
		 aux[4] = (byte) (Chronaxy[4]/5);
		 aux[5] = (byte) (Chronaxy[5]/5);
		 aux[6] = (byte) (Chronaxy[6]/5);
		 aux[7] = (byte) (Chronaxy[7]/5);
		 aux[8] = (byte) (Chronaxy[8]/5);
		 aux[9] = (byte) (Chronaxy[9]/5);

		return aux;
	}

	//SEND IMPULSE TYPE
	public static byte[] sendImpulseType(int impulseType){
		byte[] aux = new byte[10];
		aux[0] = (byte) impulseType;
		aux[1] = (byte) impulseType;
		aux[2] = (byte) impulseType;
		aux[3] = (byte) impulseType;
		aux[4] = (byte) impulseType;
		aux[5] = (byte) impulseType;
		aux[6] = (byte) impulseType;
		aux[7] = (byte) impulseType;
		aux[8] = (byte) impulseType;
		aux[9] = (byte) impulseType;

		return aux;
	}
	
	public static byte[] sendLevels(int[] levels){
		byte[] aux = new byte[10];
		 aux[0] = (byte) levels[0];
		 aux[1] = (byte) levels[1];
		 aux[2] = (byte) levels[2];
		 aux[3] = (byte) levels[3];
		 aux[4] = (byte) levels[4];
		 aux[5] = (byte) levels[5];
		 aux[6] = (byte) levels[6];
		 aux[7] = (byte) levels[7];
		 aux[8] = (byte) levels[8];
		 aux[9] = (byte) levels[9];
		
		return aux;
	}
}
