package com.myofx.emsapp.server;

import com.myofx.emsapp.models.Training;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ServerEmsCommands {

    //KEEP ALIVE
    public static byte[] getKeepAliveCommand(byte[] uid){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(uid);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x00});
            outputStream.write(new byte[] { (byte) 0x00});
            outputStream.write(new byte[] { });
        } catch (IOException e) {
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //START
    public static byte[] getStartCommand(ServerAppModel data, int vestIndex){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x03});
            outputStream.write(new byte[] { (byte) 0x00});
            outputStream.write(new byte[] { });
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //RESTART
    public static byte[] getRestartCommand(ServerAppModel data, int vestIndex){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x02});
            outputStream.write(new byte[] { (byte) 0x00});
            outputStream.write(new byte[] { });
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //STOP
    public static byte[] getStopCommand(ServerAppModel data, int vestIndex){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x00});
            outputStream.write(new byte[] { });
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //PROGRAM
    public static byte[] getProgramCommand(ServerAppModel data, int vestIndex, Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x05});
            outputStream.write(new byte[] { (byte) 13});
            outputStream.write(DataParser.sendStage(new int[]{
                    training.getMain()[0],
                    training.getMain()[1],
                    training.getMain()[4],
                    training.getMain()[7],
                    training.getMain()[5],
                    training.getMain()[2],
                    training.getMain()[6],
                    training.getMain()[3],
                    training.getMain()[9],
                    training.getMain()[10]}));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //MAIN
    public static byte[] getMainCommand(ServerAppModel data, int vestIndex, Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x04});
            outputStream.write(new byte[] { (byte) 0x02});
            outputStream.write(DataParser.sendMains(new int[]{(int) Math.ceil(training.getLevel(0)), (int) Math.ceil((float)training.getPotencyRelax())}));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //STAGE
    public static byte[] getStageCommand(ServerAppModel data, int vestIndex, Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x05});
            outputStream.write(new byte[] { (byte) 0x13});
            outputStream.write(DataParser.sendStage(new int[]{
                    training.getMain()[0],
                    training.getMain()[1],
                    training.getMain()[4],
                    training.getMain()[7],
                    training.getMain()[5],
                    training.getMain()[2],
                    training.getMain()[6],
                    training.getMain()[3],
                    training.getMain()[9],
                    training.getMain()[10]}));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //LEVELS
    public static byte[] getLevelsCommand(ServerAppModel data, int vestIndex, Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x06});
            outputStream.write(new byte[] { (byte) 0x10});
            outputStream.write(DataParser.sendLevels(training.getLevels()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //Chronaxy
    public static byte[] getChronaxyCommand(ServerAppModel data, int vestIndex, Training training){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write(getBuffer());
            outputStream.write(data.getVest(vestIndex).UID);
            outputStream.write(new byte[] { (byte) 0x01});
            outputStream.write(new byte[] { (byte) 0x07});
            outputStream.write(new byte[] { (byte) 0x10});
            outputStream.write(DataParser.sendChronaxy(training.getChronaxys()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte c[] = outputStream.toByteArray( );
        outputStream.write( (byte) (UDPServer.crc16(c) & 0xFF));
        outputStream.write( (byte) ((UDPServer.crc16(c) >> 8) & 0xFF));

        return outputStream.toByteArray();
    }

    //BUFFER
    public static byte[] getBuffer(){
        byte[] buffer = new byte[8];
        buffer[0] = 'm';
        buffer[1] = 'y';
        buffer[2] = 'o';
        buffer[3] = 'F';
        buffer[4] = 'X';
        buffer[5] = '\0';
        buffer[6] = '\0';
        buffer[7] = 00;

        return buffer;
    }
}
