package com.myofx.emsapp.server;

import java.net.InetAddress;
import java.util.ArrayList;

/**
 * Created by Gerard on 14/3/2017.
 */

public class ServerAppModel {

    //Sessions
    public ArrayList<Vest> vests = new ArrayList<>();

    public void addVest(String name, byte[] UID,InetAddress ip, boolean newDevice) {
        vests.add(new Vest(name, UID, ip, newDevice));
    }

    public int getVestsCount() {
        return vests.size();
    }

    public Vest getVest(int i) {
        return vests.get(i);
    }

    public ArrayList<Vest> getVests() {
        return vests;
    }

}
