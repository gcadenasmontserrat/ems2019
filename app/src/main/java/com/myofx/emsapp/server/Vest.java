package com.myofx.emsapp.server;

import java.net.InetAddress;

public class Vest {

	public static final int CHANNEL_LEG = 1;
	public static final int CHANNEL_GLU = 2;
	public static final int CHANNEL_LUM = 3;
	public static final int CHANNEL_SHO = 4;
	public static final int CHANNEL_BIC = 8;
	public static final int CHANNEL_DOR = 5;
	public static final int CHANNEL_ABS = 6;
	public static final int CHANNEL_PEC = 7;
	public static final int CHANNEL_AUX = 9;
	public static final int CHANNEL_AUX_2 = 10;

	int remainingTime=0;
	private boolean newDevice;
	
	public Vest(String name, byte[] UIDaddr, InetAddress ip, boolean newDevice) {
		UID = UIDaddr;
		Name = name;
		IP = ip;
		this.newDevice = newDevice;
	}

	public InetAddress IP;
	public byte[] UID;
	public String Name;
	
	public String getname(){
		return Name;
	}
	public InetAddress getIP(){
		return IP;
	}

	public String getUidString() {
		if (!newDevice)
			return getUidStringOld();
		else
			return getUIDStringNew();
	}

	public String getUidStringOld() {
		return (new String(UID[0]+"" +UID[1]+"" +UID[2]+"" +UID[3]+"" +UID[4]+"" +UID[5]));
	}

	public String getUIDStringNew() {
		return (new String(UID[0]+"" +UID[1]+"" +UID[2]+"" +UID[3]+"" +UID[4]+"" +UID[5]+"" +UID[6]+"" +UID[7]+"" +UID[8]
				+"" +UID[9]+"" +UID[10]+"" +UID[11]+"" +UID[12]+"" +UID[13]));
	}

	public String getTime(){
		String aux1="";
		String aux2="";
		if((remainingTime/60)<10)
			aux1="0";
		if((remainingTime%60)<10)
			aux2="0";
		return (aux1+remainingTime/60+" : "+aux2+remainingTime%60);
	}

	public void setIP(InetAddress IP) {
		this.IP = IP;
	}
}
