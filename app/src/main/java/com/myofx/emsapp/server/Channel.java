package com.myofx.emsapp.server;

import java.io.ByteArrayOutputStream;

public class Channel {

	public enum PulseType {
		POSITIVE(0), NEGATIVE(1), BIPOLAR(2);

		private int _value;

		PulseType(int Value) {
			this._value = Value;
		}

		public int getValue() {
			return _value;
		}
	}

	public int channel;
	public int phase;
	public PulseType type;
	public int F, A, T;
	public int E, S, M, B, P;

	public byte[] getBytes() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		outputStream.write(phase);
		outputStream.write(channel);
		outputStream.write(type.getValue());
		outputStream.write(F);
		outputStream.write(A);
		outputStream.write(T);
		outputStream.write(E);
		outputStream.write(S);
		outputStream.write(M);
		outputStream.write(B);
		outputStream.write(P);
		return outputStream.toByteArray();
	}
}
