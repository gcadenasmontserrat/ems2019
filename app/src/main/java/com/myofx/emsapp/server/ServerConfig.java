package com.myofx.emsapp.server;

/**
 * Created by Gerard on 14/3/2017.
 */

public class ServerConfig {
    public static final int CHANNELS = 14;
    public static final int UDP_PORT_RECEIVE = 42422;
    public static final int UDP_PORT_SEND = 42421;
}
