package com.myofx.emsapp.server;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.myofx.emsapp.models.LogRegister;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.PreferencesManager;
import com.myofx.emsapp.utils.PreferencesManagerLocal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

/**
 * Created by Gerard on 14/3/2017.
 */

public class UDPServer {

    //Server type
    public static final int SERVER_NORMAL = 0;

    public static final String MESSAGE_RECEIVED = "NewMessage";

    //Variables
    private Context context;
    private AsyncTask<Void, Void, Void> async;
    private static InetAddress addr = null;
    private final int PortSend = ServerConfig.UDP_PORT_SEND ;
    private final int PortReceive = ServerConfig.UDP_PORT_RECEIVE;
    private int serverType;


    public ArrayList<byte[]> uids  = new ArrayList<>();
    public ArrayList<InetAddress> ips  = new ArrayList<>();
    private DatagramSocket SendSocket;

    @SuppressLint("NewApi")
    public void runUdpServer(final Activity context) {
        this.context = context;

        new Thread(new KeepAliveThread()).start();
        addr = getDeviceIP();

        async = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                final byte[] lMsg = new byte[64];
                try (DatagramSocket ReceiveSocket = new DatagramSocket(PortReceive)){

                    while (true) {
                        DatagramPacket msgPacket = new DatagramPacket(lMsg, lMsg.length);
                        ReceiveSocket.receive(msgPacket);
                        String response = new String(lMsg, 0, lMsg.length);

                        MainActivity.ip = msgPacket.getAddress();
                        MainActivity.message = lMsg;
                        MainActivity.checkBroadcastList();
                        MainActivity.setBrodcastValue();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        };

        if (Build.VERSION.SDK_INT >= 11)
            async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            async.execute();
    }

    class KeepAliveThread implements Runnable {
        @Override
        public void run() {
            while(true){
                try {
                    Thread.sleep(4000);
                    for (int i = 0; i < uids.size(); i++)
                        send(ServerEmsCommands.getKeepAliveCommand(uids.get(i)),ips.get(i));
                } catch (Exception e) {
                }
            }
        }
    }

    //uid 6 byte, type 1 byte, command 1 byte, lenght 1 byte, data n bytes(lenght)
    public void sendMessage(InetAddress clientIp,byte[] uid, byte[] type, byte[] command, byte[] lenght, byte[] data){

        byte[] buffer = new byte[8];
        buffer[0] = 'm';
        buffer[1] = 'y';
        buffer[2] = 'o';
        buffer[3] = 'F';
        buffer[4] = 'X';
        buffer[5] = '\0';
        buffer[6] = '\0';
        buffer[7] = 00;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try {
            outputStream.write( buffer );
            outputStream.write( uid );
            outputStream.write( type );
            outputStream.write( command );
            outputStream.write( lenght );
            outputStream.write( data );
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte c[] = outputStream.toByteArray( );

        outputStream.write( (byte) (crc16(c) & 0xFF));
        outputStream.write( (byte) ((crc16(c) >> 8) & 0xFF));

        send(outputStream.toByteArray(),clientIp);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public boolean send(byte[] message, InetAddress clientIp) throws IllegalArgumentException {
        if(message == null )
            throw new IllegalArgumentException();

        // Create the send socket
        if(SendSocket == null) {
            try {
                SendSocket = new DatagramSocket();
            } catch (SocketException e) {
                Log.d("UDPSEND", "There was a problem creating the sending socket. Aborting.");
                e.printStackTrace();
                return false;
            }
        }

        // Build the packet
        DatagramPacket packet;
        byte data[] = message;
        packet = new DatagramPacket(data, data.length, clientIp, PortSend);

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            SendSocket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    static int crc16(final byte[] buffer) {
        int crc = 0xFFFF;

        for (int j = 0; j < buffer.length ; j++) {
            crc = ((crc  >>> 8) | (crc  << 8) )& 0xffff;
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;
    }

    public static InetAddress getDeviceIP() {
        if (addr != null) {

        } else {
            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                        InetAddress intAdress = enumIpAddr.nextElement();
                        if (!intAdress.isLoopbackAddress()) {
                            if (intAdress instanceof Inet4Address) {
                                addr = intAdress;
                                Log.d("ServerIpHelper", "Client Ip Address: " + addr.getHostAddress().toString());
                                return addr;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return addr;
    }

    public UDPServer(int serverType) {
        this.serverType = serverType;
    }

    public void generateLog(byte[] UID, byte[] COMMAND, byte[] DATA){
        String data = "";
        for (int i=0; i<DATA.length; i++){
            if (i==0)
                data = ""+DATA[0];
            else
                data = data+","+DATA[i];
        }

        String currentTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());

        String uidFormat = null;
        try {
            uidFormat = (new String(new byte [] {UID[0],UID[1],UID[2],UID[3],UID[4],UID[5],UID[6],UID[7],UID[8],UID[9],UID[10],UID[11],UID[12],UID[13]}, "UTF-8").trim());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        LogRegister logRegister = new LogRegister(currentTime, uidFormat, new String(""+COMMAND[0]),"["+data+"]");

        PreferencesManager pref = PreferencesManager.getInstance(context);
        pref.saveLog(logRegister);
        PreferencesManagerLocal prefLocal = PreferencesManagerLocal.getInstance(context);
        prefLocal.saveLog(logRegister);
    }
}