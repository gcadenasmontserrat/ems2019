package com.myofx.emsapp.server;

public class Program {

	public String image;
	public String title;
	int[] programLevels = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	public Program(String title, int[]levels, String image) {
		this.title = title;
		this.programLevels= levels;
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
