package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Booking implements Serializable {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "user_id")
    private String user_id;

    @ColumnInfo(name = "center_service_id")
    private String service_id;

    @ColumnInfo(name = "staff_id")
    private String staff_id;

    public Booking(String id, String date, String service_id, String staff_id, String user_id) {
        this.id = id;
        this.date = date;
        this.service_id = service_id;
        this.staff_id = staff_id;
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getUserId() {
        return user_id;
    }

    public String getServiceId() {
        return service_id;
    }

    public String getStaffId() {
        return staff_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getService_id() {
        return service_id;
    }

    public String getStaff_id() {
        return staff_id;
    }
}
