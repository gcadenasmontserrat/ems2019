package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 11/1/2018.
 */


@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class UserPermissions implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @SerializedName("chronaxy") @ColumnInfo(name = "chronaxy")
    private boolean chronaxy = false;

    @SerializedName("freq") @ColumnInfo(name = "freq")
    private boolean freq = false;

    @SerializedName("ramp") @ColumnInfo(name = "ramp")
    private boolean ramp = false;

    @SerializedName("impulse") @ColumnInfo(name = "impulse")
    private boolean impulse = false;

    @SerializedName("relax") @ColumnInfo(name = "relax")
    private boolean relax = false;

    @SerializedName("time") @ColumnInfo(name = "time")
    private boolean time = false;

    @SerializedName("global_time") @ColumnInfo(name = "global_time")
    private boolean timeGlobal = false;

    @SerializedName("global_section") @ColumnInfo(name = "global_section")
    private boolean sectionGlobal = false;

    public UserPermissions() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public boolean isChronaxy() {
        return chronaxy;
    }

    public void setChronaxy(boolean chronaxy) {
        this.chronaxy = chronaxy;
    }

    public boolean isFreq() {
        return freq;
    }

    public void setFreq(boolean freq) {
        this.freq = freq;
    }

    public boolean isRamp() {
        return ramp;
    }

    public void setRamp(boolean ramp) {
        this.ramp = ramp;
    }

    public boolean isImpulse() {
        return impulse;
    }

    public void setImpulse(boolean impulse) {
        this.impulse = impulse;
    }

    public boolean isRelax() {
        return relax;
    }

    public void setRelax(boolean relax) {
        this.relax = relax;
    }

    public boolean isTime() {
        return time;
    }

    public void setTime(boolean time) {
        this.time = time;
    }

    public boolean isTimeGlobal() {
        return timeGlobal;
    }

    public void setTimeGlobal(boolean timeGlobal) {
        this.timeGlobal = timeGlobal;
    }

    public boolean isSectionGlobal() {
        return sectionGlobal;
    }

    public void setSectionGlobal(boolean sectionGlobal) {
        this.sectionGlobal = sectionGlobal;
    }
}
