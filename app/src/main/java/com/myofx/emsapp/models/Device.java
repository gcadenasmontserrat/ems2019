package com.myofx.emsapp.models;

import android.bluetooth.BluetoothDevice;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by Gerard on 12/1/2017.
 */

public class Device implements Serializable {

    public static final int DEVICE_WIFI = 0;
    public static final int DEVICE_BLE = 1;

    @SerializedName("name")
    private String name;

    @SerializedName("uid")
    private byte[] uid;

    @SerializedName("ip")
    private InetAddress ip;

    @SerializedName("selected")
    private boolean selected = false;

    @SerializedName("potency")
    private int potency = 0;

    @SerializedName("user")
    private User userAsigned;

    @SerializedName("deviceBleAdress")
    String deviceBleAdress;

    @SerializedName("deviceBle")
    private BluetoothDevice deviceBle;

    private boolean available = false;
    private boolean deviceBleConnected = false;
    private String nameUser;
    private boolean isNewDevice = false;
    private int deviceType;

    public Device(String name, byte[] uid, InetAddress ip) {
        this.deviceType = DEVICE_WIFI;
        this.name = name;
        this.uid = uid;
        this.ip = ip;

        if (uid.length == 14)
            isNewDevice = true;
        else if (uid.length == 6)
            isNewDevice = false;
    }

    public Device(String deviceBleName, String deviceBleAdress) {
        this.deviceType = DEVICE_BLE;
        this.name = deviceBleName;
        this.deviceBleAdress = deviceBleAdress;
    }

    public String getName() {
        return name;
    }

    public byte[] getUid() {
        return uid;
    }

    public String getUidString() {
        if (!isNewDevice)
            return getUidStringOld();
        else
            return getUidStringNew();
    }

    public String getUidStringOld() {
        return (new String(uid[0]+"" +uid[1]+"" +uid[2]+"" +uid[3]+"" +uid[4]+"" +uid[5]));
    }

    public String getUidStringNew() {
        return (new String(uid[0]+"" +uid[1]+"" +uid[2]+"" +uid[3]+"" +uid[4]+"" +uid[5]+"" +uid[6]+"" +uid[7]+"" +uid[8]
                +"" +uid[9]+"" +uid[10]+"" +uid[11]+"" +uid[12]+"" +uid[13]));
    }

    public byte[] getUIDOldByte(){
        byte[] aux = new byte [] {uid[0],uid[1],uid[2],uid[3],uid[4],uid[5]};
        return aux;
    }

    public byte[] getUIDNewByte(){
        byte[] aux = new byte [] {uid[0],uid[1],uid[2],uid[3],uid[4],uid[5],uid[6],uid[7]
                ,uid[8],uid[9],uid[10],uid[11],uid[12],uid[13]};


        return aux;
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPotency() {
        return potency;
    }

    public User getUserAsigned() {
        return userAsigned;
    }

    public void setUserAsigned(User userAsigned) {
        this.userAsigned = userAsigned;
    }

    public void setPotency(int potency) {
        this.potency = potency;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public boolean isNewDevice() {
        return isNewDevice;
    }

    public BluetoothDevice getDeviceBle() {
        return deviceBle;
    }

    public void setDeviceBle(BluetoothDevice deviceBle) {
        this.deviceBle = deviceBle;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public boolean isDeviceBleConnected() {
        return deviceBleConnected;
    }

    public void setDeviceBleConnected(boolean deviceBleConnected) {
        this.deviceBleConnected = deviceBleConnected;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public String getDeviceBleAdress() {
        return deviceBleAdress;
    }
}

