package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 19/4/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class CorporalZone implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @SerializedName("ABS") @ColumnInfo(name = "ABS")
    private int abs = 0;

    @SerializedName("BIC") @ColumnInfo(name = "BIC")
    private int bic = 0;

    @SerializedName("DOR") @ColumnInfo(name = "DOR")
    private int dor = 0;

    @SerializedName("LUM") @ColumnInfo(name = "LUM")
    private int lum = 0;

    @SerializedName("SHO") @ColumnInfo(name = "SHO")
    private int sho = 0;

    @SerializedName("LEG") @ColumnInfo(name = "LEG")
    private int leg = 0;

    @SerializedName("GLU") @ColumnInfo(name = "GLU")
    private int glu = 0;

    @SerializedName("PEC") @ColumnInfo(name = "PEC")
    private int pec = 0;

    @SerializedName("AUX") @ColumnInfo(name = "AUX")
    private int aux = 0;

    @SerializedName("AUX2") @ColumnInfo(name = "AUX2")
    private int aux2 = 0;

    @SerializedName("CRON_ABS") @ColumnInfo(name = "CRON_ABS")
    private int cronAbs = 0;

    @SerializedName("CRON_BIC") @ColumnInfo(name = "CRON_BIC")
    private int cronBic = 0;

    @SerializedName("CRON_DOR") @ColumnInfo(name = "CRON_DOR")
    private int cronDor = 0;

    @SerializedName("CRON_LUM") @ColumnInfo(name = "CRON_LUM")
    private int cronLum = 0;

    @SerializedName("CRON_SHO") @ColumnInfo(name = "CRON_SHO")
    private int cronSho = 0;

    @SerializedName("CRON_LEG") @ColumnInfo(name = "CRON_LEG")
    private int cronLeg = 0;

    @SerializedName("CRON_GLU") @ColumnInfo(name = "CRON_GLU")
    private int cronGlu = 0;

    @SerializedName("CRON_PEC") @ColumnInfo(name = "CRON_PEC")
    private int cronPec = 0;

    @SerializedName("CRON_AUX") @ColumnInfo(name = "CRON_AUX")
    private int cronAux = 0;

    @SerializedName("CRON_AUX2") @ColumnInfo(name = "CRON_AUX2")
    private int cronAux2 = 0;

    @SerializedName("POTENCY") @ColumnInfo(name = "POTENCY")
    private int potency = 0;

    public String getUserid() {
        return userid;
    }

    public int getAbs() {
        return abs;
    }

    public int getBic() {
        return bic;
    }

    public int getDor() {
        return dor;
    }

    public int getLum() {
        return lum;
    }

    public int getSho() {
        return sho;
    }

    public int getLeg() {
        return leg;
    }

    public int getGlu() {
        return glu;
    }

    public int getPec() {
        return pec;
    }

    public int getAux() {
        return aux;
    }

    public int getAux2() {
        return aux2;
    }

    public void setAbs(int abs) {
        this.abs = abs;
    }

    public void setBic(int bic) {
        this.bic = bic;
    }

    public void setDor(int dor) {
        this.dor = dor;
    }

    public void setLum(int lum) {
        this.lum = lum;
    }

    public void setSho(int sho) {
        this.sho = sho;
    }

    public void setLeg(int leg) {
        this.leg = leg;
    }

    public void setGlu(int glu) {
        this.glu = glu;
    }

    public void setPec(int pec) {
        this.pec = pec;
    }

    public void setAux(int aux) {
        this.aux = aux;
    }

    public void setAux2(int aux2) {
        this.aux2 = aux2;
    }

    public String getId() {
        return id;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPotency() {
        return potency;
    }

    public void setPotency(int potency) {
        this.potency = potency;
    }

    public int getCronAbs() {
        return cronAbs;
    }

    public void setCronAbs(int cronAbs) {
        this.cronAbs = cronAbs;
    }

    public int getCronBic() {
        return cronBic;
    }

    public void setCronBic(int cronBic) {
        this.cronBic = cronBic;
    }

    public int getCronDor() {
        return cronDor;
    }

    public void setCronDor(int cronDor) {
        this.cronDor = cronDor;
    }

    public int getCronLum() {
        return cronLum;
    }

    public void setCronLum(int cronLum) {
        this.cronLum = cronLum;
    }

    public int getCronSho() {
        return cronSho;
    }

    public void setCronSho(int cronSho) {
        this.cronSho = cronSho;
    }

    public int getCronLeg() {
        return cronLeg;
    }

    public void setCronLeg(int cronLeg) {
        this.cronLeg = cronLeg;
    }

    public int getCronGlu() {
        return cronGlu;
    }

    public void setCronGlu(int cronGlu) {
        this.cronGlu = cronGlu;
    }

    public int getCronPec() {
        return cronPec;
    }

    public void setCronPec(int cronPec) {
        this.cronPec = cronPec;
    }

    public int getCronAux() {
        return cronAux;
    }

    public void setCronAux(int cronAux) {
        this.cronAux = cronAux;
    }

    public int getCronAux2() {
        return cronAux2;
    }

    public void setCronAux2(int cronAux2) {
        this.cronAux2 = cronAux2;
    }

    public CorporalZone(String id, int abs, int bic, int dor, int lum, int sho, int leg, int glu, int pec,
                        int aux, int aux2, int cronAbs, int cronBic, int cronDor, int cronLum, int cronSho,
                        int cronLeg, int cronGlu, int cronPec, int cronAux, int cronAux2, int potency) {
        this.id = id;
        this.abs = abs;
        this.bic = bic;
        this.dor = dor;
        this.lum = lum;
        this.sho = sho;
        this.leg = leg;
        this.glu = glu;
        this.pec = pec;
        this.aux = aux;
        this.aux2 = aux2;
        this.cronAbs = cronAbs;
        this.cronBic = cronBic;
        this.cronDor = cronDor;
        this.cronLum = cronLum;
        this.cronSho = cronSho;
        this.cronLeg = cronLeg;
        this.cronGlu = cronGlu;
        this.cronPec = cronPec;
        this.cronAux = cronAux;
        this.cronAux2 = cronAux2;
        this.potency = potency;
    }
}

