package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gerard on 6/2/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class User implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @SerializedName("email") @ColumnInfo(name = "email")
    private String email;

    @SerializedName("center") @ColumnInfo(name = "center")
    private String center;

    @SerializedName("center_id") @ColumnInfo(name = "center_id")
    private String centerId;

    @SerializedName("dni") @ColumnInfo(name = "dni")
    private String dni;

    @SerializedName("name") @ColumnInfo(name = "name")
    private String name;

    @SerializedName("last_name") @ColumnInfo(name = "last_name")
    private String lastName;

    @SerializedName("profiles") @ColumnInfo(name = "profiles")
    private ArrayList<User> profiles;

    @SerializedName("phone") @ColumnInfo(name = "phone")
    private String phone;

    @SerializedName("gender") @ColumnInfo(name = "gender")
    private String gender;

    @SerializedName("birthdate") @ColumnInfo(name = "birthdate")
    private String birthdate;

    @SerializedName("address") @ColumnInfo(name = "address")
    private String address;

    @SerializedName("city") @ColumnInfo(name = "city")
    private String city;

    @SerializedName("state") @ColumnInfo(name = "state")
    private String state;

    @SerializedName("postal_code") @ColumnInfo(name = "postal_code")
    private String postalCode;

    @SerializedName("country") @ColumnInfo(name = "country")
    private String country;

    @SerializedName("active") @ColumnInfo(name = "active")
    private boolean active;

    @SerializedName("type") @ColumnInfo(name = "type")
    private String type;

    @SerializedName("web") @ColumnInfo(name = "web")
    private String web;

    @SerializedName("image") @ColumnInfo(name = "image")
    private String image;

    @SerializedName("nickname") @ColumnInfo(name = "nickname")
    private String nickname;

    @SerializedName("config") @ColumnInfo(name = "config")
    private Configuration configuration;

    @SerializedName("measures") @ColumnInfo(name = "measures")
    private Measures measures;

    @SerializedName("access_token") @ColumnInfo(name = "access_token")
    private String accesToken;

    @SerializedName("refresh_token") @ColumnInfo(name = "refresh_token")
    private String refreshToken;

    @SerializedName("expires_in") @ColumnInfo(name = "expires_in")
    private int expiresIn;

    @SerializedName("results") @ColumnInfo(name = "results")
    private int results;

    @SerializedName("total_kilos") @ColumnInfo(name = "total_kilos")
    private int totalKg;

    @SerializedName("total_minutes") @ColumnInfo(name = "total_minutes")
    private int totalMins;

    @SerializedName("level") @ColumnInfo(name = "level")
    private int level;

    @SerializedName("admin") @ColumnInfo(name = "admin")
    private boolean admin;

    @SerializedName("friendship_code") @ColumnInfo(name = "friendship_code")
    private String friendshipCode;

    @SerializedName("ems_conf") @ColumnInfo(name = "ems_conf")
    private ArrayList<CorporalZone> corporalZone;

    @SerializedName("standard") @ColumnInfo(name = "standard")
    private CorporalZone standard;

    @SerializedName("reservations_number") @ColumnInfo(name = "reservations_number")
    private int reservationsNumber;

    @SerializedName("students_number") @ColumnInfo(name = "students_number")
    private int studentsNumber;

    @SerializedName("block") @ColumnInfo(name = "block")
    private boolean block;

    @SerializedName("notify") @ColumnInfo(name = "notify")
    private boolean notify;

    @SerializedName("user_permissions") @ColumnInfo(name = "user_permissions")
    private UserPermissions userPmissions;

    @SerializedName("bookings") @ColumnInfo(name = "bookings")
    private ArrayList<Booking> bookings;

    @SerializedName("center_services") @ColumnInfo(name = "center_services")
    private ArrayList<CenterService> centerServices;

    @SerializedName("trainings") @ColumnInfo(name = "trainings")
    private ArrayList<Training> trainings;

    @SerializedName("bleAvailable")
    private boolean bleAvailable = true;

    @Ignore
    public User(String name, String lastName, String id, ArrayList<CorporalZone> corporalZone) {
        this.name = name;
        this.lastName = lastName;
        this.id = id;
        this.corporalZone = corporalZone;
    }

    @Ignore
    public User() {
    }

    public User(String name, String id) {
        this.name = name;
        this.id = id;
    }

    private boolean isSelected = false;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<User> getProfiles() {
        return profiles;
    }

    public void setProfiles(ArrayList<User> profiles) {
        this.profiles = profiles;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Measures getMeasures() {
        return measures;
    }

    public void setMeasures(Measures measures) {
        this.measures = measures;
    }

    public String getAccesToken() {
        return accesToken;
    }

    public void setAccesToken(String accesToken) {
        this.accesToken = accesToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

    public int getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(int totalKg) {
        this.totalKg = totalKg;
    }

    public int getTotalMins() {
        return totalMins;
    }

    public void setTotalMins(int totalMins) {
        this.totalMins = totalMins;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getFriendshipCode() {
        return friendshipCode;
    }

    public void setFriendshipCode(String friendshipCode) {
        this.friendshipCode = friendshipCode;
    }

    public ArrayList<CorporalZone> getCorporalZone() {
        return corporalZone;
    }

    public void setStandard(CorporalZone standard) {
        this.standard = standard;
    }

    public void setReservationsNumber(int reservationsNumber) {
        this.reservationsNumber = reservationsNumber;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public void setStudentsNumber(int studentsNumber) {
        this.studentsNumber = studentsNumber;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isBleAvailable() {
        return bleAvailable;
    }

    public void setBleAvailable(boolean bleAvailable) {
        this.bleAvailable = bleAvailable;
    }

    public void setUserPmissions(UserPermissions userPmissions) {
        this.userPmissions = userPmissions;
    }

    public String getCenterId() {
        return centerId;
    }

    public ArrayList<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(ArrayList<Booking> bookings) {
        this.bookings = bookings;
    }

    public ArrayList<CenterService> getCenterServices() {
        return centerServices;
    }

    public void setCenterServices(ArrayList<CenterService> centerServices) {
        this.centerServices = centerServices;
    }

    public ArrayList<Training> getTrainings() {
        return trainings;
    }

    public void setTrainings(ArrayList<Training> trainings) {
        this.trainings = trainings;
    }

    public CorporalZone getCorporalZone(String idTraining) {
        if (corporalZone != null){
            for (CorporalZone trainingZone: corporalZone)
                if (trainingZone.getId().equals(idTraining))
                    return trainingZone;
        }
        return null;
    }

    public ArrayList<CorporalZone> getCorporalZones() {
        if (corporalZone != null)
            return corporalZone;
        else
            return new ArrayList<>();
    }


    public CorporalZone getStandard() {
        return standard;
    }

    public int getReservationsNumber() {
        return reservationsNumber;
    }

    public int getStudentsNumber() {
        return studentsNumber;
    }

    public boolean isBlock() {
        return block;
    }

    public boolean isNotify() {
        return notify;
    }

    public void setCorporalZone(ArrayList<CorporalZone> corporalZone) {
        this.corporalZone = corporalZone;
    }

    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && object instanceof User) {
            sameSame = this.id.equals(((User) object).id);
        }

        return sameSame;
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(this.id);
    }

    public static boolean containsId(ArrayList<User> list, String id) {
        for (User object : list) {
            if (object.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public UserPermissions getUserPmissions() {
        if (userPmissions == null)
            return new UserPermissions();
        else
            return userPmissions;
    }
}
