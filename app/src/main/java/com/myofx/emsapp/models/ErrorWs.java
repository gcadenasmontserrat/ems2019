package com.myofx.emsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 3/3/2017.
 */

public class ErrorWs implements Serializable {

    @SerializedName("code")
    private int codeError;

    @SerializedName("message")
    private String messageError;

    @SerializedName("error_description")
    private String errorDescription;

    public String getMessageError() {
        return messageError;
    }

    public ErrorWs(String messageError) {
        this.messageError = messageError;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
