package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 7/2/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class Configuration implements Serializable {

    public static final String DISTANCE_METERS = "metros";
    public static final String DISTANCE_MILLES = "millas";
    public static final String UNITS_KG = "kilos";

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @SerializedName("unit_of_length") @ColumnInfo(name = "unit_of_length")
    private String units;

    @SerializedName("unit_of_weight") @ColumnInfo(name = "unit_of_weight")
    private String unitsStrength;

    @SerializedName("notifications") @ColumnInfo(name = "notifications")
    private boolean notifications;

    @SerializedName("connection_type") @ColumnInfo(name = "connection_type")
    private int connectionType;

    @SerializedName("color") @ColumnInfo(name = "color")
    private int color;

    public Configuration() {
        this.units = DISTANCE_METERS;
        this.unitsStrength = UNITS_KG;
        this.notifications = false;
        this.color = 1;
        this.connectionType = Device.DEVICE_WIFI;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getUnitsStrength() {
        return unitsStrength;
    }

    public void setUnitsStrength(String unitsStrength) {
        this.unitsStrength = unitsStrength;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(int connectionType) {
        this.connectionType = connectionType;
    }
}
