package com.myofx.emsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 2/10/2017.
 */

public class DeviceBle implements Serializable {

    @SerializedName("deviceName")
    String deviceName;

    @SerializedName("deviceAdress")
    String deviceAdress;

    int forceCalibrate;

    public DeviceBle(String deviceName, String devcieAdress) {
        this.deviceName = deviceName;
        this.deviceAdress = devcieAdress;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceAdress() {
        return deviceAdress;
    }

    public int getForceCalibrate() {
        return forceCalibrate;
    }

    public void setForceCalibrate(int forceCalibrate) {
        this.forceCalibrate = forceCalibrate;
    }
}