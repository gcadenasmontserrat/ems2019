package com.myofx.emsapp.models;

/**
 * Created by Gerard on 21/12/2016.
 */

public class Color {

    private int colorId;
    private int colorTheme;
    private int colorPrimary;
    private boolean selected = false;

    public Color(int colorId, int colorTheme, int colorPrimary) {
        this.colorId = colorId;
        this.colorTheme = colorTheme;
        this.colorPrimary = colorPrimary;
    }

    public int getColorId() {
        return colorId;
    }

    public int getColorTheme() {
        return colorTheme;
    }

    public int getColorPrimary() {
        return colorPrimary;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
