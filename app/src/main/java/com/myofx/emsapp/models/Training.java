package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.myofx.emsapp.server.Vest;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 29/11/2016.
 */

@Entity
public class Training implements Serializable{

    //Training type
    public static int TRAINING_TYPE_NORMAL = 0;

    //Training Stauts
    public static final int TRAINING_STATUS_STOP = 0;
    public static final int TRAINING_STATUS_PLAY = 1;
    public static final int TRAINING_STATUS_PAUSE = 2;
    public static final int TRAINING_STATUS_RESTART = 3;
    public static final int TRAINING_STATUS_CHANGE_PROGRAM = 4;

    @Ignore
    public int trainingType = TRAINING_TYPE_NORMAL;

    @ColumnInfo(name = "chronaxy")
    private int[] chronaxy = new int[]{0,0,0,0,0,0,0,0,0,0,0};

    @ColumnInfo(name = "levels")
    private int[] levels = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    @ColumnInfo(name = "values")
    @SerializedName("values")
    private int[] main;

    @Ignore
    @SerializedName("image")
    public String image;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    public String title;

    @Ignore
    @SerializedName("UID")
    public String uid;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public String idTraining;

    @ColumnInfo(name = "center")
    private String center = "";

    @Ignore
    public User user;

    @Ignore
    public Device device;

    @Ignore
    public int potencyRelax = 0;

    @Ignore
    private int loadPotency = 0;

    @Ignore
    public int timeChromecast;

    @Ignore
    private boolean changeLevel = false;

    @Ignore
    private int trainingStatus = TRAINING_STATUS_STOP;

    @Ignore
    private int lastPotency = 0;

    @Ignore
    private int forceMaxForce = 0;

    @Ignore
    private int forceTotal = 0;

    @Ignore
    private int forceSecond = 0;

    @Ignore
    private int forceMax = 0;

    public Training(String idTraining, String title, int[] main, String image) {
        this.idTraining = idTraining;
        this.title = title;
        this.main = main;
        this.image = image;
        this.chronaxy = new int[]{getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),
                getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault(),getChronaxyDefault()};
    }

    public Training(int trainingType, int[] main, String image, String title, String uid, User user, Device device, int timeChromecast, boolean changeLevel) {
        this.trainingType = trainingType;
        this.main = main;
        this.image = image;
        this.title = title;
        this.uid = uid;
        this.user = user;
        this.device = device;
        this.timeChromecast = timeChromecast;
        this.changeLevel = changeLevel;
    }

    public Training() {
    }

    public Training(int trainingType, int timeChromecast) {
        this.trainingType = trainingType;
        this.timeChromecast = timeChromecast;
    }

    public int[] getMain() {
        return main;
    }

    public int getFrequency(){
        return main[1];
    }

    public int getFrequencyRelax(){
        return main[9];
    }

    public int getPotencyRelax(){
        return main[8];
    }

    public int getPulseRelax(){
        return main[10];
    }

    public int getEstimulationTime(){
        return main[2];
    }

    public int getTime(){
        return main[7];
    }

    public int getPauseTime(){
        return main[3];
    }

    public int getRampUp(){
        return main[5];
    }

    public int getRampDown(){
        return main[6];
    }

    public int[] getChronaxys() {
        return new int[]{chronaxy[1],chronaxy[2],chronaxy[3],chronaxy[4],chronaxy[5],chronaxy[6],chronaxy[7],chronaxy[8],chronaxy[9],chronaxy[10]};
    }

    public int getChronaxy(int chronaxyPosition) {
        return round(chronaxy[chronaxyPosition],5);
    }

    public int getChronaxyDefault() {
        return main[4];
    }

    public void setChronaxyDefault(int crhonaxyDefault) {
        main[4] = crhonaxyDefault;
    }

    public int[] getLevels() {
        return new int[]{levels[1],levels[2],levels[3],levels[4],levels[5],levels[6],levels[7],levels[8],levels[9],levels[10]};
    }

    public int getLevel(int levelPosition){
        return levels[levelPosition];
    }

    public void setFrequency(int freq){
        main[1] = freq;
    }

    public void setFrequencyRelax(int freq){
        main[9] = freq;
    }

    public void setPulseRelax(int pulse){
        main[10] = pulse;
    }

    public void setEstimulationTime(int estimTime){
        main[2] = estimTime;
    }

    public void setPauseTime(int pauseTime){
        main[3] = pauseTime;
    }

    public void setRampUp(int rampUp){
        main[5] = rampUp;
    }

    public void setRampDown(int rampDown){
        main[6] = rampDown;
    }

    public void setChronaxy(int chronaxyPosition, int chronaxyValue){
        if (this.chronaxy == null)
            this.chronaxy = new int[]{0,0,0,0,0,0,0,0,0,0,0};

        this.chronaxy[chronaxyPosition] = chronaxyValue;
    }

    public void setLevel(int levelPosition, int levelValue){
        if (this.levels == null)
            this.levels = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        this.levels[levelPosition] = levelValue;
    }

    public void setTime(int time){
        main[7] = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setLevelLeg(int level){
        levels[1] = level;
    }

    public void setLevelGlu(int level){
        levels[2] = level;
    }

    public void setLevelLum(int level){
        levels[3] = level;
    }

    public void setLevelSho(int level){
        levels[4] = level;
    }

    public void setLevelBic(int level){
        levels[8] = level;
    }

    public void setLevelDor(int level){
        levels[5] = level;
    }

    public void setLevelAbs(int level){
        levels[6] = level;
    }

    public void setLevelPec(int level){
        levels[7] = level;
    }

    public void setLevelAux(int level){
        levels[9] = level;
    }

    public void setLevelAux2(int level){
        levels[10] = level;
    }

    public void setCronLeg(int level){
        chronaxy[1] = level;
    }

    public void setCronGlu(int level){
        chronaxy[2] = level;
    }

    public void setCronLum(int level){
        chronaxy[3] = level;
    }

    public void setCronSho(int level){
        chronaxy[4] = level;
    }

    public void setCronBic(int level){
        chronaxy[8] = level;
    }

    public void setCronDor(int level){
        chronaxy[5] = level;
    }

    public void setCronAbs(int level){
        chronaxy[6] = level;
    }

    public void setCronPec(int level){
        chronaxy[7] = level;
    }

    public void setCronAux(int level){
        chronaxy[9] = level;
    }

    public void setCronAux2(int level){
        chronaxy[10] = level;
    }

    public int getLevelLeg(){
        return levels[1];
    }

    public int getLevelGlu(){
        return levels[2];
    }

    public int getLevelLum(){
        return levels[3];
    }

    public int getLevelSho(){
        return levels[4];
    }

    public int getLevelBic(){
        return levels[8];
    }

    public int getLevelDor(){
        return levels[5];
    }

    public int getLevelAbs(){
        return levels[6];
    }

    public int getLevelPec(){
        return levels[7];
    }

    public int getLevelAux(){
        return levels[9];
    }

    public int getLevelAux2(){
        return levels[10];
    }

    public int getCronaxyLeg(){
        return chronaxy[1];
    }

    public int getCronaxyGlu(){
        return chronaxy[2];
    }

    public int getCronaxyLum(){
        return chronaxy[3];
    }

    public int getCronaxySho(){
        return chronaxy[4];
    }

    public int getCronaxyBic(){
        return chronaxy[8];
    }

    public int getCronaxyDor(){
        return chronaxy[5];
    }

    public int getCronaxyAbs(){
        return chronaxy[6];
    }

    public int getCronaxyPec(){
        return chronaxy[7];
    }

    public int getCronaxyAux(){
        return chronaxy[9];
    }

    public int getCronaxyAux2(){
        return chronaxy[10];
    }

    public boolean isChangeLevel() {
        return changeLevel;
    }

    public void setChangeLevel(boolean changeLevel) {
        this.changeLevel = changeLevel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTimeChromecast() {
        return timeChromecast;
    }

    public void setTimeChromecast(int timeChromecast) {
        this.timeChromecast = timeChromecast;
    }

    public int getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(int trainingType) {
        this.trainingType = trainingType;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public void setMain(int[] main) {
        this.main = main;
    }

    public String getIdTraining() {
        return idTraining;
    }

    public void setIdTraining(String idTraining) {
        this.idTraining = idTraining;
    }

    public void setPotencyRelax(int potencyRelax) {
        main[8] = potencyRelax;
    }

    public static Training copyTraining(Training training){
        Training trainingCopy = new Training();
        trainingCopy.setIdTraining(training.getIdTraining());
        trainingCopy.setTrainingType(training.getTrainingType());
        trainingCopy.setImage(training.getImage());
        trainingCopy.setTitle(training.getTitle());
        trainingCopy.setUid(training.getUid());
        trainingCopy.setUser(training.getUser());
        trainingCopy.setDevice(training.getDevice());
        trainingCopy.setTimeChromecast(training.getTimeChromecast());
        trainingCopy.setChangeLevel(training.isChangeLevel());
        trainingCopy.setChronaxy(Vest.CHANNEL_LEG,training.getChronaxy(Vest.CHANNEL_LEG));
        trainingCopy.setChronaxy(Vest.CHANNEL_GLU,training.getChronaxy(Vest.CHANNEL_GLU));
        trainingCopy.setChronaxy(Vest.CHANNEL_LUM,training.getChronaxy(Vest.CHANNEL_LUM));
        trainingCopy.setChronaxy(Vest.CHANNEL_SHO,training.getChronaxy(Vest.CHANNEL_SHO));
        trainingCopy.setChronaxy(Vest.CHANNEL_BIC,training.getChronaxy(Vest.CHANNEL_BIC));
        trainingCopy.setChronaxy(Vest.CHANNEL_DOR,training.getChronaxy(Vest.CHANNEL_DOR));
        trainingCopy.setChronaxy(Vest.CHANNEL_ABS,training.getChronaxy(Vest.CHANNEL_ABS));
        trainingCopy.setChronaxy(Vest.CHANNEL_PEC,training.getChronaxy(Vest.CHANNEL_PEC));
        trainingCopy.setChronaxy(Vest.CHANNEL_AUX,training.getChronaxy(Vest.CHANNEL_AUX));
        trainingCopy.setChronaxy(Vest.CHANNEL_AUX_2,training.getChronaxy(Vest.CHANNEL_AUX_2));
        trainingCopy.setMain(new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        trainingCopy.setFrequency(training.getFrequency());
        trainingCopy.setFrequencyRelax(training.getFrequencyRelax());
        trainingCopy.setPulseRelax(training.getPulseRelax());
        trainingCopy.setEstimulationTime(training.getEstimulationTime());
        trainingCopy.setPauseTime(training.getPauseTime());
        trainingCopy.setRampUp(training.getRampUp());
        trainingCopy.setRampDown(training.getRampDown());
        trainingCopy.setTime(training.getTime());
        trainingCopy.setChronaxyDefault(training.getChronaxyDefault());
        trainingCopy.setPotencyRelax(training.getPotencyRelax());
        trainingCopy.setLastPotency(training.getLastPotency());
        trainingCopy.setCenter(training.getCenter());
        return trainingCopy;
    }

    public int getTrainingStatus() {
        return trainingStatus;
    }

    public void setTrainingStatus(int trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    public int getLastPotency() {
        return lastPotency;
    }

    public void setLastPotency(int lastPotency) {
        this.lastPotency = lastPotency;
    }

    public int getForceMaxForce() {
        return forceMaxForce;
    }

    public void setForceMaxForce(int forceMaxForce) {
        this.forceMaxForce = forceMaxForce;
    }

    public int getForceTotal() {
        return forceTotal;
    }

    public void setForceTotal(int forceTotal) {
        this.forceTotal = forceTotal;
    }

    public int getForceMax() {
        return forceMax;
    }

    public void setForceMax(int forceMax) {
        this.forceMax = forceMax;
    }

    public int round(double i, int v){
        return (int) Math.round(i/v) * v;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public int[] getChronaxy() {
        return chronaxy;
    }

    public void setLevels(int[] levels) {
        this.levels = levels;
    }

    public void setChronaxy(int[] chronaxy) {
        this.chronaxy = chronaxy;
    }

    public int getLoadPotency() {
        return loadPotency;
    }

    public void setLoadPotency(int loadPotency) {
        this.loadPotency = loadPotency;
    }
}