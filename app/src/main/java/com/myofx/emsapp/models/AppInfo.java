package com.myofx.emsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 13/3/2017.
 */

public class AppInfo {

    @SerializedName("links")
    private Links links;

    public Links getLinks() {
        return links;
    }

    public class Links  implements Serializable{
        @SerializedName("product_info")
        private String productInfo;

        @SerializedName("technical_support")
        private String technicalSupport;

        @SerializedName("video")
        private String video;

        @SerializedName("terms_conditions")
        private String termsConditions;

        @SerializedName("medical_recommendations")
        private String medicalRecommendations;

        @SerializedName("contraindications")
        private String contraindications;

        public String getProductInfo() {
            return productInfo;
        }

        public String getTechnicalSupport() {
            return technicalSupport;
        }

        public String getVideo() {
            return video;
        }

        public String getTermsConditions() {
            return termsConditions;
        }

        public String getMedicalRecommendations() {
            return medicalRecommendations;
        }

        public String getContraindications() {
            return contraindications;
        }
    }
}
