package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Base64;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity
public class Credentials {

    @PrimaryKey @NonNull
    @ColumnInfo(name = "userId")
    private String userId;

    @ColumnInfo(name = "password")
    private String password;

    @ColumnInfo(name = "email")
    private String email;

    public static final String saltKey = "dfg254bFG3kjSnljnxDRfTg";

    public Credentials(String password, String email, String userId) {
        this.password = password;
        this.email = email;
        this.userId = userId;
    }

    public static byte[] getHashedPasswordInByteArray(byte[] byteSalt, String userPassword) {
        byte[] pass = userPassword.getBytes();

        //Concat salt + password
        byte[] passToHash = new byte[byteSalt.length + pass.length];
        System.arraycopy(byteSalt, 0, passToHash, 0, byteSalt.length);
        System.arraycopy(pass, 0, passToHash, byteSalt.length, pass.length);

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return digest.digest(passToHash);
    }

    public static boolean isCorrectPass(String password, Credentials credential) {
        byte[] byteSalt = saltKey.getBytes(Charset.forName("UTF-8"));
        byte[] hash = getHashedPasswordInByteArray(byteSalt, password);

        if (credential.getPassword().equals(Base64.encodeToString(hash, Base64.DEFAULT)))
            return true;
        else
            return false;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}




