package com.myofx.emsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 4/10/2017.
 */

public class UserMachine implements Serializable {

    @SerializedName("user")
    private String user;

    @SerializedName("machine")
    private String machine;

    public String getUser() {
        return user;
    }

    public String getMachine() {
        return machine;
    }

    public UserMachine(String machine, String user) {
        this.machine = machine;
        this.user = user;
    }
}
