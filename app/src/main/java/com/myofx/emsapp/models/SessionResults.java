package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class SessionResults {

    @PrimaryKey @NonNull
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @ColumnInfo(name = "users-machines")
    private String usersMachines;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "duration")
    private String duration;

    public SessionResults(String id, String userid, String usersMachines, String date, String duration) {
        this.id = id;
        this.userid = userid;
        this.usersMachines = usersMachines;
        this.date = date;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsersMachines() {
        return usersMachines;
    }

    public void setUsersMachines(String usersMachines) {
        this.usersMachines = usersMachines;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
