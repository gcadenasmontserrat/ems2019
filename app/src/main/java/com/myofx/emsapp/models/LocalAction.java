package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class LocalAction implements Serializable {

    public static final int ACTION_TYPE_ADD_USER = 0;
    public static final int ACTION_TYPE_ADD_TRAINING = 1;
    public static final int ACTION_TYPE_BOOKING = 2;

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private int id;

    @ColumnInfo(name = "owner_id")
    private String ownerId;

    @ColumnInfo(name = "data_id")
    private String dataId;

    @ColumnInfo(name = "data")
    private String data;

    @ColumnInfo(name = "sincronized")
    private boolean sincronized;

    @ColumnInfo(name = "type")
    private int type;

    public LocalAction(String ownerId, String dataId, String data, boolean sincronized, int type) {
        this.ownerId = ownerId;
        this.dataId = dataId;
        this.data = data;
        this.sincronized = sincronized;
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public boolean isSincronized() {
        return sincronized;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getDataId() {
        return dataId;
    }

    public int getType() {
        return type;
    }
}
