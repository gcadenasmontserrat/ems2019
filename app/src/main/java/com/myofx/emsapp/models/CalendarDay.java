package com.myofx.emsapp.models;

public class CalendarDay {

    private String date;
    private boolean typeDay = false;
    private Booking booking;

    public CalendarDay(String date, boolean typeDay) {
        this.date = date;
        this.typeDay = typeDay;
    }

    public CalendarDay(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public boolean isTypeDay() {
        return typeDay;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}
