package com.myofx.emsapp.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 8/2/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))
public class Measures implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @ColumnInfo(name = "user_id")
    private String userid;

    @SerializedName("height") @ColumnInfo(name = "height")
    private double height;

    @SerializedName("weight") @ColumnInfo(name = "weight")
    private double weight;

    @SerializedName("body_fat") @ColumnInfo(name = "body_fat")
    private double bodyFat;

    @SerializedName("waist_measurement") @ColumnInfo(name = "waist_measurement")
    private double waistMeasurement;

    @SerializedName("right_arm_measurement") @ColumnInfo(name = "right_arm_measurement")
    private double rightArmMeasurement;

    @SerializedName("right_leg_measurement") @ColumnInfo(name = "right_leg_measurement")
    private double rightLegMeasurement;

    @SerializedName("left_arm_measurement") @ColumnInfo(name = "left_arm_measurement")
    private double leftArmMeasurement;

    @SerializedName("left_leg_measurement") @ColumnInfo(name = "left_leg_measurement")
    private double leftLegMeasurement;

    @SerializedName("non_fat_mass") @ColumnInfo(name = "non_fat_mass")
    private double nonFatMass;

    @SerializedName("strength") @ColumnInfo(name = "strength")
    private double strength;

    @SerializedName("diastolic_blood_pressure") @ColumnInfo(name = "diastolic_blood_pressure")
    private double diastolicBloodPressure;

    @SerializedName("systolic_blood_pressure") @ColumnInfo(name = "systolic_blood_pressure")
    private double systolicBloodPressure;

    @SerializedName("flexibility") @ColumnInfo(name = "flexibility")
    private double flexibility;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getBodyFat() {
        return bodyFat;
    }

    public void setBodyFat(double bodyFat) {
        this.bodyFat = bodyFat;
    }

    public double getWaistMeasurement() {
        return waistMeasurement;
    }

    public void setWaistMeasurement(double waistMeasurement) {
        this.waistMeasurement = waistMeasurement;
    }

    public double getRightArmMeasurement() {
        return rightArmMeasurement;
    }

    public void setRightArmMeasurement(double rightArmMeasurement) {
        this.rightArmMeasurement = rightArmMeasurement;
    }

    public double getRightLegMeasurement() {
        return rightLegMeasurement;
    }

    public void setRightLegMeasurement(double rightLegMeasurement) {
        this.rightLegMeasurement = rightLegMeasurement;
    }

    public double getLeftArmMeasurement() {
        return leftArmMeasurement;
    }

    public void setLeftArmMeasurement(double leftArmMeasurement) {
        this.leftArmMeasurement = leftArmMeasurement;
    }

    public double getLeftLegMeasurement() {
        return leftLegMeasurement;
    }

    public void setLeftLegMeasurement(double leftLegMeasurement) {
        this.leftLegMeasurement = leftLegMeasurement;
    }

    public double getNonFatMass() {
        return nonFatMass;
    }

    public void setNonFatMass(double nonFatMass) {
        this.nonFatMass = nonFatMass;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public double getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public void setDiastolicBloodPressure(double diastolicBloodPressure) {
        this.diastolicBloodPressure = diastolicBloodPressure;
    }

    public double getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(double systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public double getFlexibility() {
        return flexibility;
    }

    public void setFlexibility(double flexibility) {
        this.flexibility = flexibility;
    }
}
