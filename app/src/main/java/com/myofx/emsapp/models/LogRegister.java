package com.myofx.emsapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gerard on 18/10/2017.
 */

public class LogRegister implements Serializable {

    @SerializedName("date")
    private String date;

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("command")
    private String command;

    @SerializedName("data")
    private String data;

    public LogRegister(String date, String uuid, String command, String data) {
        this.date = date;
        this.uuid = uuid;
        this.command = command;
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public String getUuid() {
        return uuid;
    }

    public String getCommand() {
        return command;
    }

    public String getData() {
        return data;
    }
}
