package com.myofx.emsapp;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.myofx.emsapp.api.VolleyManager;
import com.myofx.emsapp.bluetooth.BluetoothEmsLeService;
import com.myofx.emsapp.config.Constants;
import com.myofx.emsapp.server.ServerAppModel;
import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.utils.ImageUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Gerard on 13/12/2016.
 */

public class EmsApp extends Application {

    public static final String APP_PACKAGE = BuildConfig.APPLICATION_ID;
    private static EmsApp instance;

    //Server connection
    public static ServerAppModel data;
    public static UDPServer server;

    //Imageloader
    private ImageLoader mImageLoader;
    private DisplayImageOptions mImageLoaderOptions;
    private DisplayImageOptions mImageLoaderProfileOptions;

    //App mode local
    private static boolean  modeLocal = false;

    //Google Analytics
    private Tracker mTracker;

    //Ble
    private BluetoothEmsLeService mBluetoothEmsLeService;
    private Activity activity;
    private int receiverSection;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;

        //Init VolleyManager
        VolleyManager.getInstance(this);

        //Init imageloader
        initImageLoader();

        //Flurry
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, Constants.FLURRY_API_KEY);
    }

    public static boolean isTablet(Context context){
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    public static EmsApp getInstance() {
        return instance;
    }

    public static ServerAppModel getData() {
        return data;
    }

    public static void setData(ServerAppModel data) {
        EmsApp.data = data;
    }

    public static UDPServer getServer() {
        return server;
    }

    public static void setServer(UDPServer server) {
        EmsApp.server = server;
    }

    public void initImageLoader(){
        mImageLoader = ImageUtils.initImageloader(this);
        mImageLoaderOptions = ImageUtils.optionsFadeImageLoader();
        mImageLoaderProfileOptions = ImageUtils.optionsProfileImageLoader();
    }

    public ImageLoader getmImageLoader() {
        return mImageLoader;
    }

    public DisplayImageOptions getmImageLoaderOptions() {
        return mImageLoaderOptions;
    }

    public DisplayImageOptions getmImageLoaderProfileOptions() {
        return mImageLoaderProfileOptions;
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public static boolean isModeLocal() {
        return modeLocal;
    }

    public static void setModeLocal(boolean modeLocal) {
        EmsApp.modeLocal = modeLocal;
    }

    /*
     * Manage EMS BLE connection.
     */
    public void initBleEms(Activity activity){
        Intent gattServiceIntent = new Intent(this, BluetoothEmsLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        this.activity = activity;
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothEmsLeService = ((BluetoothEmsLeService.LocalBinder) service).getService();
            mBluetoothEmsLeService.initialize();

            if (activity instanceof MainActivity)
                ((MainActivity)activity).connectPreviousEmsDevices();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothEmsLeService = null;
        }
    };

    public BluetoothEmsLeService getmBluetoothLeService() {
        return mBluetoothEmsLeService;
    }
}
