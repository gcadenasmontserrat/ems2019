package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;

import java.util.List;

@Dao
public interface TrainingsDao {

    @Query("SELECT * FROM Training WHERE center LIKE :centerId")
    List<Training> getTrainings(String centerId);

    @Query("DELETE FROM Training WHERE center LIKE :centerId")
    void deleteTrainings(String centerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Training... training);
}
