package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.myofx.emsapp.models.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM User WHERE id LIKE :userId")
    User findById(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User... user);

    @Update
    void updateUser(User user);

    @Delete
    void delete(User user);

    @Query("DELETE FROM User WHERE user_id LIKE :ownerId")
    void deleteAll(String ownerId);
}
