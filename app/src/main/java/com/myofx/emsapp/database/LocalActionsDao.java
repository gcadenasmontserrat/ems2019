package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.myofx.emsapp.models.LocalAction;

import java.util.List;

@Dao
public interface LocalActionsDao {

    @Query("SELECT * FROM LocalAction WHERE data_id LIKE :parentId")
    LocalAction findById(String parentId);

    @Query("SELECT * FROM LocalAction WHERE owner_id LIKE :parentId")
    List<LocalAction> getLocalActions(String parentId);

    @Query("SELECT * FROM LocalAction WHERE owner_id LIKE :parentId AND type LIKE 0")
    List<LocalAction> getLocalUserActions(String parentId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(LocalAction... localAction);

    @Query("DELETE FROM LocalAction WHERE owner_id LIKE :parentId")
    void deleteAll(String parentId);
}
