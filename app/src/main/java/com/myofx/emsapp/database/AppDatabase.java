package com.myofx.emsapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.util.Base64;

import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CorporalZone;
import com.myofx.emsapp.models.Credentials;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.SessionResults;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;

import java.nio.charset.Charset;
import java.util.List;

@Database(entities = {Credentials.class, User.class, LocalAction.class, Training.class, SessionResults.class, Booking.class} , version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract CredentialsDao credentialsDao();
    public abstract UserDao userDao();
    public abstract LocalActionsDao localActionsDao();
    public abstract TrainingsDao trainingsDao();
    public abstract SessionsDao sessionsDao();
    public abstract BookingsDao bookingsDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "emsdatabase")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    /* ------------ USER ------------ */
    public static void addLoginUser(Context context, String password, User user) {
        byte[] byteSalt = Credentials.saltKey.getBytes(Charset.forName("UTF-8"));
        byte[] hash = Credentials.getHashedPasswordInByteArray(byteSalt, password);

        getAppDatabase(context).credentialsDao().insertAll(new Credentials(Base64.encodeToString(hash, Base64.DEFAULT), user.getEmail(), user.getId()));
        getAppDatabase(context).userDao().insertAll(user);
    }

    public static void updateUser(Context context, User user) {
        getAppDatabase(context).userDao().updateUser(user);
    }

    public static User checkUserCredential(Context context, String password, String email) {
        Credentials credential = getAppDatabase(context).credentialsDao().findByEmail(email);

        if (credential != null && Credentials.isCorrectPass(password, credential))
            return getAppDatabase(context).userDao().findById(credential.getUserId());

        return null;
    }

    public static User getUserById(Context context, String userId) {
        Credentials credential = getAppDatabase(context).credentialsDao().findById(userId);

        if (credential != null)
            return getAppDatabase(context).userDao().findById(credential.getUserId());

        return null;
    }

    public static void clearUsers(Context context, String ownerId) {
        getAppDatabase(context).userDao().deleteAll(ownerId);
    }

    /* ------------ ACTIONS ------------ */
    public static void addLocalAction(Context context, LocalAction localAction) {
        getAppDatabase(context).localActionsDao().insertAll(localAction);
    }

    public static List<LocalAction> getLocalActions(Context context, String ownerId) {
        return getAppDatabase(context).localActionsDao().getLocalActions(ownerId);
    }

    public static List<LocalAction> getLocalUserActions(Context context, String ownerId) {
        return getAppDatabase(context).localActionsDao().getLocalUserActions(ownerId);
    }

    public static void clearLocalUserActions(Context context, String ownerId) {
        getAppDatabase(context).localActionsDao().deleteAll(ownerId);
    }

    /* ------------ TRAININGS ------------ */
    public static void addTraining(Context context, Training training) {
        getAppDatabase(context).trainingsDao().insertAll(training);
    }

    public static List<Training> getTrainings(Context context, String centerId) {
        return getAppDatabase(context).trainingsDao().getTrainings(centerId);
    }

    public static void clearTrainings(Context context, String centerId) {
        getAppDatabase(context).trainingsDao().deleteTrainings(centerId);
    }

    /* ------------ BOOKINGS ------------ */
    public static void addBooking(Context context, Booking booking) {
        getAppDatabase(context).bookingsDao().insertAll(booking);
    }

    public static List<Booking> getBookingsUser(Context context, String ownerId) {
        return getAppDatabase(context).bookingsDao().getBookingsUser(ownerId);
    }

    public static void clearUserBookings(Context context, String ownerId) {
        getAppDatabase(context).bookingsDao().deleteBookingsUser(ownerId);
    }

    /* ------------ SESSION ------------ */
    public static void addSessionResult(Context context, SessionResults sessionResults) {
        getAppDatabase(context).sessionsDao().insertAll(sessionResults);
    }

    public static SessionResults getSessionsResults(Context context, String ownerId) {
        return getAppDatabase(context).sessionsDao().getSessionResults(ownerId);
    }

    public static void clearSessionsResults(Context context, String ownerId) {
        getAppDatabase(context).sessionsDao().deleteSessionResults(ownerId);
    }
}



