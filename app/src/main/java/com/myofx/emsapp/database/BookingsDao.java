package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.LocalAction;

import java.util.List;

@Dao
public interface BookingsDao {

    @Query("SELECT * FROM Booking WHERE staff_id LIKE :ownerId")
    List<Booking> getBookingsUser(String ownerId);

    @Query("DELETE FROM Booking WHERE staff_id LIKE :ownerId")
    void deleteBookingsUser(String ownerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Booking... booking);
}
