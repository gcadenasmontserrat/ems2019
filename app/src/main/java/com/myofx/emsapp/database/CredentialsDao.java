package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.myofx.emsapp.models.Credentials;

@Dao
public interface CredentialsDao {

    @Query("SELECT * FROM credentials WHERE email LIKE :userEmail LIMIT 1")
    Credentials findByEmail(String userEmail);

    @Query("SELECT * FROM credentials WHERE userId LIKE :userId LIMIT 1")
    Credentials findById(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Credentials... credentials);

    @Delete
    void delete(Credentials credentials);
}
