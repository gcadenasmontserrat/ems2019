package com.myofx.emsapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.SessionResults;

import java.util.List;

@Dao
public interface SessionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(SessionResults... sessionResults);

    @Query("SELECT * FROM SessionResults WHERE user_id LIKE :ownerId LIMIT 1")
    SessionResults getSessionResults(String ownerId);

    @Query("DELETE FROM SessionResults WHERE user_id LIKE :ownerId")
    void deleteSessionResults(String ownerId);

}
