package com.myofx.emsapp.database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myofx.emsapp.models.Booking;
import com.myofx.emsapp.models.CenterService;
import com.myofx.emsapp.models.Configuration;
import com.myofx.emsapp.models.CorporalZone;
import com.myofx.emsapp.models.LocalAction;
import com.myofx.emsapp.models.Measures;
import com.myofx.emsapp.models.Training;
import com.myofx.emsapp.models.User;
import com.myofx.emsapp.models.UserPermissions;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Converters {

    @TypeConverter
    public static ArrayList<User> getArrayUsers(String value) {
        Type listType = new TypeToken<ArrayList<User>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putArrayUsers(ArrayList<User> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static Configuration getConfiguration(String value) {
        Type type = new TypeToken<Configuration>() {}.getType();
        return new Gson().fromJson(value, type);
    }

    @TypeConverter
    public static String putConfiguration(Configuration configuration) {
        Gson gson = new Gson();
        String json = gson.toJson(configuration);
        return json;
    }

    @TypeConverter
    public static Measures getMeasures(String value) {
        Type type = new TypeToken<Measures>() {}.getType();
        return new Gson().fromJson(value, type);
    }

    @TypeConverter
    public static String putMeasures(Measures measures) {
        Gson gson = new Gson();
        String json = gson.toJson(measures);
        return json;
    }

    @TypeConverter
    public static CorporalZone getCorporalZone(String value) {
        Type type = new TypeToken<CorporalZone>() {}.getType();
        return new Gson().fromJson(value, type);
    }

    @TypeConverter
    public static String putCorporalZone(CorporalZone corporalZone) {
        Gson gson = new Gson();
        String json = gson.toJson(corporalZone);
        return json;
    }

    @TypeConverter
    public static ArrayList<CorporalZone> getCorporalZones(String value) {
        Type listType = new TypeToken<ArrayList<CorporalZone>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putCorporalZones(ArrayList<CorporalZone> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static ArrayList<Booking> getBookings(String value) {
        Type listType = new TypeToken<ArrayList<Booking>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putBookings(ArrayList<Booking> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static UserPermissions getUserPermissions(String value) {
        Type type = new TypeToken<UserPermissions>() {}.getType();
        return new Gson().fromJson(value, type);
    }

    @TypeConverter
    public static String putUserPermissions(UserPermissions userPermissions) {
        Gson gson = new Gson();
        String json = gson.toJson(userPermissions);
        return json;
    }

    @TypeConverter
    public static ArrayList<LocalAction> getLocalActions(String value) {
        Type listType = new TypeToken<ArrayList<LocalAction>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putMains(int[] main) {
        Gson gson = new Gson();
        String json = gson.toJson(main);
        return json;
    }

    @TypeConverter
    public static int[] getTrainingMains(String value) {
        Type listType = new TypeToken<int[]>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static ArrayList<CenterService> getCenterServices(String value) {
        Type listType = new TypeToken<ArrayList<CenterService>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putCenterServices(ArrayList<CenterService> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static ArrayList<Training> getTrainings(String value) {
        Type listType = new TypeToken<ArrayList<Training>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String putTrainings(ArrayList<Training> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}
