package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Gerard on 1/12/2016.
 */

public class MS500Button extends Button {

    private Context context;

    public MS500Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public MS500Button(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public MS500Button(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(SingletonFonts.getInstance(context).getMS500());
        }
    }
}