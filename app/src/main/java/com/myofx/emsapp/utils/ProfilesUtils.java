package com.myofx.emsapp.utils;

import android.content.Context;

import com.myofx.emsapp.models.User;

import java.util.ArrayList;

/**
 * Created by Gerard on 2/3/2017.
 */

public class ProfilesUtils {

    /**
     * Check if profile active is type Admin
     * @param context
     * @return
     */
    public static boolean isProfileAdmin(Context context) {

        PreferencesManager pref = PreferencesManager.getInstance(context);
        ArrayList<User> profiles = pref.getAllUsers();

        for (User user: profiles){
            if (user.getId().equals(pref.getProfile()) && user.isAdmin())
                return true;
        }
        return false;
    }

    /**
     * Check if some profile is active profile
     * @param context
     * @param profile
     * @return
     */
    public static boolean isMyProfile(Context context, User profile){
        PreferencesManager pref = PreferencesManager.getInstance(context);
        if (pref.getProfile().equals(profile.getId()))
            return true;
        else
            return false;
    }
}
