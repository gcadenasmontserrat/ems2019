package com.myofx.emsapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.myofx.emsapp.EmsApp;
import com.myofx.emsapp.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

/**
 * Created by Gerard on 16/1/2017.
 */

public class ImageUtils {

    public static ImageLoader initImageloader(Context ctx){
        ImageLoader mImageLoader;

        mImageLoader = ImageLoader.getInstance();
        if (!mImageLoader.isInited()){
            ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(ctx)
                    .build();

            ImageLoader.getInstance().init(configuration);
        }
        return mImageLoader;
    }

    public static DisplayImageOptions optionsFadeImageLoader(){
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(false)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();

        return defaultOptions;
    }

    public static DisplayImageOptions optionsProfileImageLoader(){
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(false)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.img_user_default)
                .build();

        return defaultOptions;
    }

    /**
     * Get id of image by name
     * @param name
     * @param ctx
     * @return
     */
    public static int getImageIdByName(String name, Context ctx){
        int drawableResourceId = ctx.getResources().getIdentifier(name, "drawable", ctx.getPackageName());
        return drawableResourceId;
    }

    /**
     * Load image from drawable efficiently
     * @param image
     * @param idImage
     */
    public static void loadImageFromDrawable(ImageView image, int idImage){
        EmsApp.getInstance().getmImageLoader().displayImage("drawable://"+Integer.toString(idImage), image ,
                EmsApp.getInstance().getmImageLoaderOptions() , null, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });
    }

    /**
     * Load image from url efficiently
     * @param image
     * @param url
     */
    public static void loadImageFromUrl(ImageView image, String url){
        EmsApp.getInstance().getmImageLoader().displayImage(url, image, EmsApp.getInstance().getmImageLoaderOptions() , null, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });
    }

    /**
     * Load image from url efficiently
     * @param image
     * @param url
     */
    public static void loadImageProfileFromUrl(ImageView image, String url){
        EmsApp.getInstance().getmImageLoader().displayImage(url, image, EmsApp.getInstance().getmImageLoaderProfileOptions() , null, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });
    }
}
