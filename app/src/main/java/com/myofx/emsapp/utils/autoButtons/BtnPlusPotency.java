package com.myofx.emsapp.utils.autoButtons;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.training.adapters.GlobalGridAdapter;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleNormalFragment;

/**
 * Created by Gerard on 7/8/2017.
 */

public class BtnPlusPotency implements Runnable {

    private static final int REP_DELAY = 150;
    public static final int FROM_MULTIPLE_NORMAL = 0;
    public static final int FROM_MULTIPLE_GLOBAL = 1;

    private Fragment fragment;
    private Context context;
    private Handler repeatUpHandler;
    private int from;
    private boolean autoIncrement = false;
    private GlobalGridAdapter adapter;
    private GlobalGridAdapter.ViewHolder holder;
    private int position;
    private int serverType;

    public void run() {
        if (autoIncrement){
            switch (from){
                case FROM_MULTIPLE_NORMAL:
                    ((ExerciseMultipleNormalFragment)fragment).incrementPotency();
                    break;
                case FROM_MULTIPLE_GLOBAL:
                    adapter.incrementPotency(holder, position);
                    break;
            }
            repeatUpHandler.postDelayed(this, REP_DELAY);
        }
    }

    public BtnPlusPotency(Fragment fragment, int from, Handler repeatUpHandler, Context context, GlobalGridAdapter adapter,
                          GlobalGridAdapter.ViewHolder holder, int position, int serverType) {
        this.fragment = fragment;
        this.from = from;
        this.repeatUpHandler = repeatUpHandler;
        this.context = context;
        this.adapter = adapter;
        this.holder = holder;
        this.position = position;
        this.serverType = serverType;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }
}