package com.myofx.emsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myofx.emsapp.R;
import com.myofx.emsapp.models.CorporalZone;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.LogRegister;
import com.myofx.emsapp.models.User;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Gerard on 9/8/2017.
 */

public class PreferencesManagerLocal {

    //APP CONFIG
    public static final String MAXFORCE_PREFERENCES_LOCAL = "MAXFORCE_PREFERENCES_LOCAL";
    public static final String PREF_THEME_COLOR = "THEME_COLOR";

    //LOCAL
    public static final String PREF_USER = "PREF_USER";

    //OTHER
    public static final String PREF_USERS = "PREF_USERS";
    public static final String PREF_DEVICES = "PREF_DEVICES";
    public static final String PREF_STANDARD_VALUES = "PREF_STANDARD_VALUES";
    public static final String PREF_HOTSPOT_STATUS = "PREF_HOTSPOT_STATUS";
    public static final String PREF_UDP_LOG = "PREF_UDP_LOG";

    //Variables
    private static PreferencesManagerLocal preferencesManager = null;
    private static SharedPreferences sharedPreferences;

    private PreferencesManagerLocal(Context context) {
        sharedPreferences = context.getSharedPreferences(MAXFORCE_PREFERENCES_LOCAL, Context.MODE_PRIVATE);
    }

    public static PreferencesManagerLocal getInstance(Context context) {
        if (preferencesManager == null) {
            preferencesManager = new PreferencesManagerLocal(context);
        }
        return preferencesManager;
    }

    //Clear SharedPreferences
    public static void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    //Save color theme
    public void saveColorTheme(int colorTheme) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PREF_THEME_COLOR, colorTheme);
        editor.commit();
    }

    //Get color theme
    public int getColorTheme() {
        return sharedPreferences.getInt(PREF_THEME_COLOR, R.style.AppThemeBlue);
    }

    public void saveUser(User user) {
        ArrayList<User> allUsers = getAllUsers();
        allUsers.add(0,user);

        Gson gson = new Gson();
        String json = gson.toJson(allUsers);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USERS, json);
        editor.commit();
    }

    public void deleteUser(User user) {
        ArrayList<User> allUsers = getAllUsers();
        allUsers.remove(user);

        Gson gson = new Gson();
        String json = gson.toJson(allUsers);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USERS, json);
        editor.commit();
    }

    public ArrayList<User> getAllUsers() {
        String usersJson = sharedPreferences.getString(PREF_USERS, null);
        if (usersJson != null){
            Type listType = new TypeToken<ArrayList<User>>(){}.getType();
            ArrayList<User> users = new Gson().fromJson(usersJson, listType);
            return users;
        } else {
            return new ArrayList<>();
        }
    }

    public ArrayList<Device> getAllDevices() {
        String usersJson = sharedPreferences.getString(PREF_DEVICES, null);
        if (usersJson != null){
            Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
            ArrayList<Device> devices = new Gson().fromJson(usersJson, listType);
            return devices;
        } else {
            return new ArrayList<>();
        }
    }

    public void saveDevices(ArrayList<Device> devices) {
        ArrayList<Device> allDevices = new ArrayList<>();
        allDevices.addAll(devices);

        Gson gson = new Gson();
        String json = gson.toJson(allDevices);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_DEVICES, json);
        editor.commit();
    }

    public void saveDevice(Device device) {
        ArrayList<Device> allDevices = getAllDevices();
        allDevices.add(device);

        Gson gson = new Gson();
        String json = gson.toJson(allDevices);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_DEVICES, json);
        editor.commit();
    }

    public void saveStandardValues(CorporalZone standard) {
        Gson gson = new Gson();
        String json = gson.toJson(standard);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_STANDARD_VALUES, json);
        editor.commit();
    }

    public CorporalZone getStandardValues() {
        String standardJson = sharedPreferences.getString(PREF_STANDARD_VALUES, null);
        CorporalZone standard = new Gson().fromJson(standardJson, CorporalZone.class);
        return standard;
    }

    public void saveCorporalZonesTraining(User userSave, ArrayList<CorporalZone> corporalZones) {
        ArrayList<User> allUsers = getAllUsers();

        for (User user: allUsers){
            if (user.getId().equals(userSave.getId()))
                user.setCorporalZone(corporalZones);
        }

        Gson gson = new Gson();
        String json = gson.toJson(allUsers);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USERS, json);
        editor.commit();
    }

    public void saveHotSpotStatus(boolean activated) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_HOTSPOT_STATUS, activated);
        editor.commit();
    }

    public boolean getHotSpotStatus() {
        return sharedPreferences.getBoolean(PREF_HOTSPOT_STATUS, true);
    }

    public void saveLog(LogRegister logRegister) {
        ArrayList<LogRegister> allLog = getAllLog();
        allLog.add(logRegister);

        Gson gson = new Gson();
        String json = gson.toJson(allLog);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_UDP_LOG, json);
        editor.commit();
    }

    public ArrayList<LogRegister> getAllLog() {
        String logJson = sharedPreferences.getString(PREF_UDP_LOG, null);
        if (logJson != null){
            Type listType = new TypeToken<ArrayList<LogRegister>>(){}.getType();
            ArrayList<LogRegister> log = new Gson().fromJson(logJson, listType);
            return log;
        } else {
            return new ArrayList<>();
        }
    }

    public ArrayList<LogRegister> getAllLogAndClear() {
        String logJson = sharedPreferences.getString(PREF_UDP_LOG, null);
        if (logJson != null){
            Type listType = new TypeToken<ArrayList<LogRegister>>(){}.getType();
            ArrayList<LogRegister> log = new Gson().fromJson(logJson, listType);

            //Save empty list
            clearLog();
            return log;
        } else {
            //Save empty list
            clearLog();
            return new ArrayList<>();
        }
    }

    public void clearLog(){
        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_UDP_LOG, gson.toJson(new ArrayList<>()));
        editor.commit();
    }
}
