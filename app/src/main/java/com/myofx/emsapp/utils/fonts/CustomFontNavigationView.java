package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.myofx.emsapp.utils.CustomTypefaceSpan;


/**
 * Created by peremestresdomenech on 1/12/16.
 */

public class CustomFontNavigationView extends NavigationView {

    private Context context;

    public CustomFontNavigationView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomFontNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public CustomFontNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public void init() {
        if (!isInEditMode()) {
            Typeface font = SingletonFonts.getInstance(context).getMS300();
            Menu m = getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);

                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);
                        applyFontToMenuItem(subMenuItem, font);
                    }
                }
                applyFontToMenuItem(mi, font);
            }
        }
    }

    private void applyFontToMenuItem(MenuItem mi, Typeface font) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
