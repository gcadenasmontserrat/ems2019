package com.myofx.emsapp.utils.autoButtons;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleNormalFragment;

/**
 * Created by Gerard on 7/8/2017.
 */

public class BtnCorporalPlus implements Runnable {

    private static final int MODE_LEVEL = 0;
    private static final int MODE_CRONAXIA = 1;

    private static final int REP_DELAY = 150;
    public static final int FROM_MULTIPLE_NORMAL = 0;

    private Fragment fragment;
    private Context context;
    private Handler repeatUpHandler;
    private int from;
    private boolean autoIncrement = false;
    private int channel;
    private int plusValueChannel;
    private int modeActive;
    private int serverType;

    public void run() {
        if (autoIncrement){
            switch (modeActive){
                case MODE_LEVEL:
                    plusValueChannel = plusValueChannel +1;
                    break;
                case MODE_CRONAXIA:
                    plusValueChannel = plusValueChannel +5;
                    break;
            }

            switch (from){
                case FROM_MULTIPLE_NORMAL:
                    ((ExerciseMultipleNormalFragment)fragment).doPlusValue(plusValueChannel, channel);
                    break;
            }
            repeatUpHandler.postDelayed(this, REP_DELAY);
        }
    }

    public BtnCorporalPlus(Fragment fragment, int from, Handler repeatUpHandler, Context context, int channel, int plusValueChannel,
                           int modeActive, int serverType) {
        this.fragment = fragment;
        this.from = from;
        this.repeatUpHandler = repeatUpHandler;
        this.context = context;
        this.channel = channel;
        this.plusValueChannel = plusValueChannel;
        this.modeActive = modeActive;
        this.serverType = serverType;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }
}