package com.myofx.emsapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Gerard on 23/2/2017.
 */

public class DateUtils {

    public static String changeDateFormat(String oldDate){
        String newDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(oldDate);
            newDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static boolean isSunday (String day) {
        try {
            Date date = new SimpleDateFormat("EEEE,dd MMM").parse(day);
            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.setTime(date);

            if (calendar.get(Calendar.DAY_OF_WEEK) == 4)
                return true;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }


}
