package com.myofx.emsapp.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.Color;

import java.util.ArrayList;

/**
 * Created by Gerard on 10/8/2017.
 */

public class ColorThemeLocal {

    public static final int GREEN = 0;
    public static final int ORANGE = 1;
    public static final int PURPLE = 2;
    public static final int BLUE = 3;
    public static final int GREY = 4;

    //Variables
    private static PreferencesManagerLocal pref;

    /**
     * Return primaryColor
     * @param context
     * @return
     */
    public static int getPrimaryColor(Context context){
        pref = PreferencesManagerLocal.getInstance(context);

        int color = pref.getColorTheme();
        switch (color){
            case R.style.AppThemeGreen:
                return ContextCompat.getColor(context, R.color.greenPrimary);
            case R.style.AppThemeOrange:
                return ContextCompat.getColor(context, R.color.orangePrimary);
            case R.style.AppThemePurple:
                return ContextCompat.getColor(context, R.color.purplePrimary);
            case R.style.AppThemeBlue:
                return ContextCompat.getColor(context, R.color.bluePrimary);
            case R.style.AppThemeGrey:
                return ContextCompat.getColor(context, R.color.greyPrimary);
        }
        return ContextCompat.getColor(context, R.color.orangePrimary);
    }

    /**
     * Return primaryDarkColor
     * @param context
     * @return
     */
    public static int getPrimaryDarkColor(Context context){
        pref = PreferencesManagerLocal.getInstance(context);

        int color = pref.getColorTheme();
        switch (color){
            case R.style.AppThemeGreen:
                return ContextCompat.getColor(context, R.color.greenPrimaryDark);
            case R.style.AppThemeOrange:
                return ContextCompat.getColor(context, R.color.orangePrimaryDark);
            case R.style.AppThemePurple:
                return ContextCompat.getColor(context, R.color.purplePrimaryDark);
            case R.style.AppThemeBlue:
                return ContextCompat.getColor(context, R.color.bluePrimaryDark);
            case R.style.AppThemeGrey:
                return ContextCompat.getColor(context, R.color.greyPrimaryDark);
        }
        return ContextCompat.getColor(context, R.color.orangePrimaryDark);
    }

    /**
     * Return accentColor
     * @param context
     * @return
     */
    public static int getAccentColor(Context context){
        pref = PreferencesManagerLocal.getInstance(context);

        int color = pref.getColorTheme();
        switch (color){
            case R.style.AppThemeGreen:
                return ContextCompat.getColor(context, R.color.greenAccent);
            case R.style.AppThemeOrange:
                return ContextCompat.getColor(context, R.color.orangeAccent);
            case R.style.AppThemePurple:
                return ContextCompat.getColor(context, R.color.purpleAccent);
            case R.style.AppThemeBlue:
                return ContextCompat.getColor(context, R.color.blueAccent);
            case R.style.AppThemeGrey:
                return ContextCompat.getColor(context, R.color.greyAccent);
        }
        return ContextCompat.getColor(context, R.color.orangeAccent);
    }

    /**
     * Return lightBackgroundColor
     * @param context
     * @return
     */
    public static int getLightBackgroundColor(Context context){
        pref = PreferencesManagerLocal.getInstance(context);

        int color = pref.getColorTheme();
        switch (color){
            case R.style.AppThemeGreen:
                return ContextCompat.getColor(context, R.color.greenLightBackground);
            case R.style.AppThemeOrange:
                return ContextCompat.getColor(context, R.color.orangeLightBackground);
            case R.style.AppThemePurple:
                return ContextCompat.getColor(context, R.color.purpleLightBackground);
            case R.style.AppThemeBlue:
                return ContextCompat.getColor(context, R.color.blueLightBackground);
            case R.style.AppThemeGrey:
                return ContextCompat.getColor(context, R.color.greyLightBackground);
        }
        return ContextCompat.getColor(context, R.color.orangeLightBackground);
    }

    /**
     * Return iconsColor
     * @param context
     * @return
     */
    public static int getIconsColor(Context context){
        pref = PreferencesManagerLocal.getInstance(context);

        int color = pref.getColorTheme();
        switch (color){
            case R.style.AppThemeGreen:
                return ContextCompat.getColor(context, R.color.greenIcons);
            case R.style.AppThemeOrange:
                return ContextCompat.getColor(context, R.color.orangeIcons);
            case R.style.AppThemePurple:
                return ContextCompat.getColor(context, R.color.purpleIcons);
            case R.style.AppThemeBlue:
                return ContextCompat.getColor(context, R.color.blueIcons);
            case R.style.AppThemeGrey:
                return ContextCompat.getColor(context, R.color.greyIcons);
        }
        return ContextCompat.getColor(context, R.color.orangeIcons);
    }

    public static ArrayList<Color> getColors(){
        ArrayList<Color> colors = new ArrayList<>();
        colors.add(new Color(ColorTheme.GREEN, R.style.AppThemeGreen, R.color.greenPrimary));
        colors.add(new Color(ColorTheme.ORANGE, R.style.AppThemeOrange, R.color.orangePrimary));
        colors.add(new Color(ColorTheme.PURPLE, R.style.AppThemePurple, R.color.purplePrimary));
        colors.add(new Color(ColorTheme.BLUE, R.style.AppThemeBlue, R.color.bluePrimary));
        colors.add(new Color(ColorTheme.GREY, R.style.AppThemeGrey, R.color.greyPrimary));
        return colors;
    }

    public static int getThemeFromColor(int color){
        switch (color){
            case GREEN:
                return R.style.AppThemeGreen;
            case ORANGE:
                return R.style.AppThemeOrange;
            case PURPLE:
                return R.style.AppThemePurple;
            case BLUE:
                return R.style.AppThemeBlue;
            case GREY:
                return R.style.AppThemeGrey;
            default:
                return R.style.AppThemeGreen;
        }
    }

    public static int getColorFromTheme(int theme){
        switch (theme){
            case R.style.AppThemeGreen:
                return GREEN;
            case R.style.AppThemeOrange:
                return ORANGE;
            case R.style.AppThemePurple:
                return PURPLE;
            case R.style.AppThemeBlue:
                return BLUE;
            case R.style.AppThemeGrey:
                return GREY;
            default:
                return GREEN;
        }
    }

    /**
     * Converts the given hex-color-string to rgb.
     *
     * @param hex
     * @return
     */
    public static int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return android.graphics.Color.rgb(r, g, b);
    }
}
