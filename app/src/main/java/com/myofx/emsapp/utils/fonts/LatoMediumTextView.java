package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Gerard on 30/11/2017.
 */

public class LatoMediumTextView extends android.support.v7.widget.AppCompatTextView {

    private Context context;

    public LatoMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public LatoMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public LatoMediumTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(SingletonFonts.getInstance(context).getLatoMedium());
        }
    }
}