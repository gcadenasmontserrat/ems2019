package com.myofx.emsapp.utils.autoButtons;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.myofx.emsapp.server.UDPServer;
import com.myofx.emsapp.ui.training.multiple.ExerciseMultipleNormalFragment;

/**
 * Created by Gerard on 7/8/2017.
 */

public class BtnCorporalMinus implements Runnable {

    private static final int MODE_LEVEL = 0;
    private static final int MODE_CRONAXIA = 1;

    private static final int REP_DELAY = 150;
    public static final int FROM_MULTIPLE_NORMAL = 0;

    private Fragment fragment;
    private Context context;
    private Handler repeatUpHandler;
    private int from;
    private boolean autoDecrement = false;
    private int channel;
    private int minusValueChannel;
    private int modeActive;
    private int serverType;

    public void run() {
        if (autoDecrement){

            switch (modeActive){
                case MODE_LEVEL:
                    minusValueChannel = minusValueChannel -1;
                    break;
                case MODE_CRONAXIA:
                    minusValueChannel = minusValueChannel -5;
                    break;
            }

            if(minusValueChannel < 0)
                minusValueChannel = 0;

            ((ExerciseMultipleNormalFragment)fragment).doMinusValue(minusValueChannel, channel);
            repeatUpHandler.postDelayed(this, REP_DELAY);
        }
    }

    public BtnCorporalMinus(Fragment fragment, int from, Handler repeatUpHandler, Context context, int channel, int minusValueChannel,
                            int modeActive, int serverType) {
        this.fragment = fragment;
        this.from = from;
        this.repeatUpHandler = repeatUpHandler;
        this.context = context;
        this.channel = channel;
        this.minusValueChannel = minusValueChannel;
        this.modeActive = modeActive;
        this.serverType = serverType;
    }

    public void setAutoDecrement(boolean autoDecrement) {
        this.autoDecrement = autoDecrement;
    }
}