package com.myofx.emsapp.utils.autoButtons;

/**
 * Created by Gerard on 7/8/2017.
 */

public class AutoButtonsUtils {

    public static boolean checkPlusPotencyValue(int potency){
        if (potency == 29 || potency == 39 || potency == 49 || potency == 59 || potency == 69
                || potency == 79 || potency == 89){
            return true;
        }
        return false;
    }

    public static boolean checkMinusPotencyValue(int potency){
        if (potency == 31 || potency == 41 || potency == 51 || potency == 61 || potency == 71
                || potency == 81 || potency == 91){
            return true;
        }
        return false;
    }
}
