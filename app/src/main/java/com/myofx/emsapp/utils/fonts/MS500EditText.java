package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Gerard on 1/12/2016.
 */

public class MS500EditText extends EditText {

    private Context context;

    public MS500EditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public MS500EditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public MS500EditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(SingletonFonts.getInstance(context).getMS500());
        }
    }
}