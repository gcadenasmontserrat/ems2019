package com.myofx.emsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.myofx.emsapp.R;
import com.myofx.emsapp.models.AppInfo;
import com.myofx.emsapp.models.Device;
import com.myofx.emsapp.models.LogRegister;
import com.myofx.emsapp.models.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Gerard on 20/12/2016.
 */

public class PreferencesManager {

    //APP CONFIG
    public static final String MAXFORCE_PREFERENCES = "MAXFORCE_PREFERENCES";
    public static final String PREF_THEME_COLOR = "THEME_COLOR";

    //OTHER
    public static final String PREF_ACCES_TOKEN = "PREF_ACCES_TOKEN";
    public static final String PREF_REFRESH_TOKEN = "PREF_REFRESH_TOKEN";
    public static final String PREF_USERS = "PREF_USERS";
    public static final String PREF_DEVICES = "PREF_DEVICES";
    public static final String PREF_DEVICES_BLE = "PREF_DEVICES_BLE";
    public static final String PREF_PROFILE = "PREF_PROFILE";
    public static final String PREF_APP_INFO = "PREF_APP_INFO";
    public static final String PREF_HOTSPOT_STATUS = "PREF_HOTSPOT_STATUS";
    public static final String PREF_UDP_LOG = "PREF_UDP_LOG";

    //Variables
    private static PreferencesManager preferencesManager = null;
    private static SharedPreferences sharedPreferences;

    private PreferencesManager(Context context) {
        sharedPreferences = context.getSharedPreferences(MAXFORCE_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static PreferencesManager getInstance(Context context) {
        if (preferencesManager == null) {
            preferencesManager = new PreferencesManager(context);
        }
        return preferencesManager;
    }

    //Clear SharedPreferences
    public static void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    //Save color theme
    public void saveColorTheme(int colorTheme) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PREF_THEME_COLOR, colorTheme);
        editor.commit();
    }

    //Get color theme
    public int getColorTheme() {
        return sharedPreferences.getInt(PREF_THEME_COLOR, R.style.AppThemeBlue);
    }

    public void saveAccessToken(String accessToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_ACCES_TOKEN, accessToken);
        editor.commit();
    }

    public String getAccessToken() {
        return sharedPreferences.getString(PREF_ACCES_TOKEN, null);
    }

    public void saveRefreshToken(String refreshToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_REFRESH_TOKEN, refreshToken);
        editor.commit();
    }

    public String getRefreshToken() {
        return sharedPreferences.getString(PREF_REFRESH_TOKEN, null);
    }

    public void saveProfile(String profile) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_PROFILE, profile);
        editor.commit();
    }

    public String getProfile() {
        String profile = sharedPreferences.getString(PREF_PROFILE, null);
        return profile;
    }

    public void saveUsers(ArrayList<User> users) {
        ArrayList<User> allUsers = new ArrayList<>();
        allUsers.addAll(users);

        Gson gson = new Gson();
        String json = gson.toJson(allUsers);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USERS, json);
        editor.commit();
    }

    public void saveUser(User user) {
        ArrayList<User> allUsers = getAllUsers();
        allUsers.add(0,user);

        Gson gson = new Gson();
        String json = gson.toJson(allUsers);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USERS, json);
        editor.commit();
    }

    public ArrayList<User> getAllUsers() {
        String usersJson = sharedPreferences.getString(PREF_USERS, null);
        if (usersJson != null){
            Type listType = new TypeToken<ArrayList<User>>(){}.getType();
            ArrayList<User> users = new Gson().fromJson(usersJson, listType);
            return users;
        } else {
            return new ArrayList<>();
        }
    }

    public ArrayList<Device> getAllDevices() {
        String usersJson = "";
        switch (getUser().getConfiguration().getConnectionType()){
            case Device.DEVICE_WIFI:
                usersJson = sharedPreferences.getString(PREF_DEVICES, null);
                break;
            case Device.DEVICE_BLE:
                usersJson = sharedPreferences.getString(PREF_DEVICES_BLE, null);
                break;
        }

        if (usersJson != null){
            Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
            ArrayList<Device> devices = new Gson().fromJson(usersJson, listType);
            return devices;
        } else {
            return new ArrayList<>();
        }
    }

    public void saveDevices(ArrayList<Device> devices) {
        ArrayList<Device> allDevices = new ArrayList<>();
        allDevices.addAll(devices);

        Gson gson = new Gson();
        String json = gson.toJson(allDevices);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        switch (getUser().getConfiguration().getConnectionType()){
            case Device.DEVICE_WIFI:
                editor.putString(PREF_DEVICES, json);
                break;
            case Device.DEVICE_BLE:
                editor.putString(PREF_DEVICES_BLE, json);
                break;
        }

        editor.commit();
    }

    public void saveDevice(Device device) {
        ArrayList<Device> allDevices = getAllDevices();
        allDevices.add(device);

        Gson gson = new Gson();
        String json = gson.toJson(allDevices);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        switch (getUser().getConfiguration().getConnectionType()){
            case Device.DEVICE_WIFI:
                editor.putString(PREF_DEVICES, json);
                break;
            case Device.DEVICE_BLE:
                editor.putString(PREF_DEVICES_BLE, json);
                break;
        }

        editor.commit();
    }

    public User getUser() {
        String userId = this.getProfile();

        String usersJson = sharedPreferences.getString(PREF_USERS, null);
        if (usersJson != null){
            Type listType = new TypeToken<ArrayList<User>>(){}.getType();
            ArrayList<User> users = new Gson().fromJson(usersJson, listType);

            for (User user: users){
                if (user.getId().equals(userId))
                    return user;
            }
            return null;
        } else {
            return null;
        }
    }

    public void saveAppInfo(AppInfo appInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(appInfo);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_APP_INFO, json);
        editor.commit();
    }

    public AppInfo getAppInfo() {
        String infoJson = sharedPreferences.getString(PREF_APP_INFO, null);
        AppInfo appInfo = new Gson().fromJson(infoJson, AppInfo.class);
        return appInfo;
    }

    public void saveHotSpotStatus(boolean activated) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_HOTSPOT_STATUS, activated);
        editor.commit();
    }

    public boolean getHotSpotStatus() {
        return sharedPreferences.getBoolean(PREF_HOTSPOT_STATUS, true);
    }

    public void saveLog(LogRegister logRegister) {
        ArrayList<LogRegister> allLog = getAllLog();
        allLog.add(logRegister);

        Gson gson = new Gson();
        String json = gson.toJson(allLog);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_UDP_LOG, json);
        editor.commit();
    }

    public ArrayList<LogRegister> getAllLog() {
        String logJson = sharedPreferences.getString(PREF_UDP_LOG, null);
        if (logJson != null){
            Type listType = new TypeToken<ArrayList<LogRegister>>(){}.getType();
            ArrayList<LogRegister> log = new Gson().fromJson(logJson, listType);
            return log;
        } else {
            return new ArrayList<>();
        }
    }

    public ArrayList<LogRegister> getAllLogAndClear() {
        String logJson = sharedPreferences.getString(PREF_UDP_LOG, null);
        if (logJson != null){
            Type listType = new TypeToken<ArrayList<LogRegister>>(){}.getType();
            ArrayList<LogRegister> log = new Gson().fromJson(logJson, listType);

            //Save empty list
            clearLog();
            return log;
        } else {
            //Save empty list
            clearLog();
            return new ArrayList<>();
        }
    }

    public void clearLog(){
        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_UDP_LOG, gson.toJson(new ArrayList<>()));
        editor.commit();
    }
}
