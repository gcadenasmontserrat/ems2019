package com.myofx.emsapp.utils;

import com.myofx.emsapp.models.User;

/**
 * Created by Gerard on 24/2/2017.
 */

public class VelocimeterUtils {

    //Meta
    public static final float MAX_ROTATION = 242;

    public static int getForcePercentDefault(User user, int forceValue) {
        int percent;
        float rotation=0;
        float forceUnits = (float) (forceValue);
        if (forceUnits >= 60)
            percent = 100;
        else{
            rotation = ((forceUnits*MAX_ROTATION)/60);
            percent= (int) (rotation/MAX_ROTATION*100);
        }
        return percent;
    }
}
