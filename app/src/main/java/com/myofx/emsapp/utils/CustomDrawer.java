package com.myofx.emsapp.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;

public class CustomDrawer extends android.support.v4.widget.DrawerLayout {



    public CustomDrawer(Context context) {
        super(context);

    }

    public CustomDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if(isDrawerOpen(Gravity.START)){
            if(event.getX() > getChildAt(1).getWidth()){
                return false;
            }
        }
        return super.onInterceptTouchEvent(event);
    }



}