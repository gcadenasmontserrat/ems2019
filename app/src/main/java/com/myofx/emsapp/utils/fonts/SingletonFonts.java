package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.graphics.Typeface;

import com.myofx.emsapp.config.Constants;

/**
 * Created by Gerard on 1/12/2016.
 */
public class SingletonFonts {

    private Typeface MS100;
    private Typeface MS300;
    private Typeface MS500;
    private Typeface MS700;
    private Typeface MS900;
    private Typeface MS700Italic;
    private Typeface MPBold;
    private Typeface MPBlack;
    private Typeface LatoMedium;
    private Typeface MPBoldItalic;

    private static SingletonFonts ourInstance = null;

    public static SingletonFonts getInstance(Context context){
        if(ourInstance == null){
            ourInstance = new SingletonFonts(context);
        }
        return ourInstance;
    }

    private SingletonFonts(Context context){
        MS100 = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_100);
        MS300 = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_300);
        MS500 = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_500);
        MS700 = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_700);
        MS900 = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_900);
        MS700Italic = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MS_700italic);
        MPBold = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MP_BOLD);
        MPBlack = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MP_BLACK);
        LatoMedium = Typeface.createFromAsset(context.getAssets(), Constants.FONT_LATO_MEDIUM);
        MPBoldItalic = Typeface.createFromAsset(context.getAssets(), Constants.FONT_MP_BOLD_ITALIC);

    }

    public Typeface getMS100() {
        return MS100;
    }

    public Typeface getMS300() {
        return MS300;
    }

    public Typeface getMS500() {
        return MS500;
    }

    public Typeface getMS700() {
        return MS700;
    }

    public Typeface getMS900() {
        return MS900;
    }

    public Typeface getMS700Italic() {
        return MS700Italic;
    }

    public Typeface getMPBold() {
        return MPBold;
    }

    public Typeface getMPBlack() {
        return MPBlack;
    }

    public Typeface getLatoMedium() {
        return LatoMedium;
    }

    public Typeface getMPBoldItalic() {
        return MPBoldItalic;
    }

}
