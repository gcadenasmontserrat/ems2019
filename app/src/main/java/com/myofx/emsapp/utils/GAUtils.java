package com.myofx.emsapp.utils;

import android.app.Activity;

import com.myofx.emsapp.EmsApp;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Gerard on 27/4/2017.
 */

public class GAUtils {

    public static void trackScreen (Activity activity, String screenName, String contentGroup){
        if (activity != null) {

            // Get tracker.
            Tracker t = ((EmsApp) activity.getApplication()).getTracker();
            t.set(contentGroup, contentGroup);
            t.setScreenName(screenName);

            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }
}
