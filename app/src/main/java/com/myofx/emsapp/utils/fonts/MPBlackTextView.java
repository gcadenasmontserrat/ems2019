package com.myofx.emsapp.utils.fonts;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Gerard on 27/11/2017.
 */

public class MPBlackTextView extends android.support.v7.widget.AppCompatTextView {

    private Context context;

    public MPBlackTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public MPBlackTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public MPBlackTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(SingletonFonts.getInstance(context).getMPBlack());
        }
    }
}
