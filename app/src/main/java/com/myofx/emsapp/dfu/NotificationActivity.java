package com.myofx.emsapp.dfu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.myofx.emsapp.ui.MainActivity;
import com.myofx.emsapp.ui.SplashActivity;

public class NotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isTaskRoot()) {
            // Start the app before finishing
            final Intent parentIntent = new Intent(this, SplashActivity.class);
            parentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            final Intent startAppIntent = new Intent(this, MainActivity.class);
            startAppIntent.putExtras(getIntent().getExtras());
            startActivities(new Intent[] { parentIntent, startAppIntent });
        }

        finish();
    }
}